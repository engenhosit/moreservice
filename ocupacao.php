<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
	<div id="tela" class="tela">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Ocupação <small>Cadastro de Ocupação</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Cadastro
                            </li>
                        </ol>

					<div class="form-group">
						<form method="POST" action="" id="FormOcupacao">
							<label>Ocupação *</label>
							<input type="text" id="descricao" name="ocupacao" class="form-control" placeholder="Digite a descrição da ocupação aqui..." required />
							<br/>
							<label>Número do cbo *</label>
							<input type="text" id="cbo" name="cbo" class="form-control" placeholder="Digite o número do CBO aqui..." required />
							<br/>
<?php
//SALVAR
$FormOcupacao = @$_POST["FormOcupacaoSalvar"];
if(!empty($FormOcupacao)){
		require_once("controller/controllerOcupacao.php");
		$class = new controllerOcupacao;
		$class->Salvar();
}
?>
							<input type="submit" value="Salvar" name="FormOcupacaoSalvar" class="btn btn-default btn-lg">
							<a href="lista-ocupacao.php"  class="btn btn-default btn-lg">Listar</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>