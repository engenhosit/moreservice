<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div id="tela" class="tela">
	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
				Especialidade <small>Cadastro de Especialidade</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-users"></i> Cadastro
					</li>
				</ol>

<?php
require_once("controller/controllerEspecialidade.php");
//SALVAR
$FormTipoConta = @$_POST["FormEspecialidadeSalvar"];
if(!empty($FormTipoConta)){
		$class = new controllerEspecialidade;
		$class->Salvar();
}
?>
				<form role="form" action="" method="POST">
					<div class="form-group">
						<label>Especialidade *</label>
						<br>
						<input class="form-control" placeholder=" Digite a descrição aqui" name="descricao" required />
						<br>

						<button type="submit" class="btn btn-default btn-lg" value="salvar" name="FormEspecialidadeSalvar">Salvar</button>
						<a href="lista-especialidade.php"  class="btn btn-default btn-lg">Listar</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>