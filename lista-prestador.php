<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<!-- Load Image -->
<script type="text/javascript">
	var loadFile = function(event) {
		var output = document.getElementById('visualizar');
		output.src = URL.createObjectURL(event.target.files[0]);
	}
</script>
<!-- CSS para esconder -->
<style type="text/css">
	.carregando{
		display:none;
	}
	.docimg{
		width:560px;
		height:300px;
	}
	.color-link{
		color:#333;
	}
</style>

<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
				Prestador <small>Listagem de prestador</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-edit"></i> Lista
					</li>
				</ol>
				<a href="prestador.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
				<?php
				//EDITAR
				$FormPrestador = @$_POST["FormPrestadorEditar"];
				if(!empty($FormPrestador)){
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					echo"<br/><br/>";
					$class->Editar();
				}
				//EDITAR Disponibilidade
				$FormDisponibilidadeEditar = @$_POST["FormDisponibilidadeEditar"];
				if(!empty($FormDisponibilidadeEditar)){
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					echo"<br/><br/>";
					$class->editarDisponibilidade();
				}
				//Modificar Status
				$FormPrestador = @$_POST["FormPrestadorExcluir"];
				if(!empty($FormPrestador)){
					$cpf = @$_POST["cpfexcluir"];
					$status = @$_POST["status"];
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					echo"<br/><br/>";
					$class->Excluir($cpf,$status);
				}
				?>
				<div id="lista">
					<?php
					//LISTAR
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->Listar();
					?>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- ModalUpdate -->
<div class='modal fade' id='contact' tabindex='-1'>
	<div class='modal-dialog'>
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action="" id="FormPrestador" enctype="multipart/form-data">
			<div class='modal-body' style='padding: 5px;'>
					<div style="width:35%;margin-left:30%">
					<img src="../img/perfil/default.png"  class="img-rounded img-responsive" id="visualizar"/>
					<br/>
					<label>Imagem *</label>
					<input type="file" id="fileimage" name="imagem" onchange="loadFile(event)"/>
					</div>
					<br/>
					<label>CPF *</label>
					<div class="form-inline">
					<input class="form-control"  name="cpf" id="cpf" maxlength="14" style="width:50%;" readonly required="true" />
					<span id="validcpf"></span>
					</div>
					<br/>
					<label>Nome *</label>
					<input class="form-control" id="nome" placeholder=" Digite o nome aqui..."  name="nome" required />
					<br/>
					<label>Data de Nascimento *</label>
					<input type="date" class="form-control" id="dtnasc" name="dtnasc" required>
					<br/>
					<label>Email *</label>
					<input type="email" class="form-control" id="email" placeholder=" Digite o email aqui..." name="email" required />
					<br/>
					<label>Celular *</label>
					<div class="form-inline">
					<input class="form-control" placeholder=" Digite o celular aqui..." id="cell" name="cell" style="width:50%"
					onkeyup="lengthCell();" value="<?php echo @$_POST["cell"]; ?>" required />
					<span id="confirmCell"></span>
					</div>
					<br/>
					<label>Telefone</label>
					<input class="form-control" id="fone" placeholder=" Digite o telefone aqui..." id="fone" name="fone" type="text" />
					<br/>
					<label class="control-label">Sexo *</label>
					<select name="sexo" id="sexo" class="form-control" style="width:50%;">
					</select>
					<br/>
					<label>Cep *</label>
					<div class="form-inline">
					<input class="form-control cep" placeholder=" Digite o CEP aqui..."  name="cep" id="cep" maxlength="9" style="width:50%;" />
					<span id="validcep"></span>
					</div>
					<span id="confirmCep" class="confirmCep" style="display: none; color:#E62117;">  Cep é obrigatório</span>
					<br/>
					<label>Rua *</label>
					<input class="form-control" id="rua" name="rua" placeholder=" Digite a rua aqui..." type="text"  />
					<span id="confirmRua" class="confirmRua" style="display: none; color:#E62117;">  Rua é obrigatório</span>
					<br/>
					<label>Numero *</label>
					<input class="form-control" id="numero" name="numero" placeholder=" Digite o numero aqui..." type="text"  />
					<span id="confirmNumero" class="confirmNumero" style="display: none; color:#E62117;">  Numero é obrigatório</span>
					<br/>
					<label>Bairro *</label>
					<input class="form-control" placeholder=" Digite o bairro aqui..." id="bairro" name="bairro" type="text" />
					<span id="confirmBairro" class="confirmBairro" style="display: none; color:#E62117;">  Bairro é obrigatório</span>
					<br/>
					<label>Complemento</label>
					<textarea class="form-control" id="complemento"
					placeholder=" Digite o complento do endereço aqui..."  name="complemento" type="text"></textarea>
					<br/>
					<div class="form-inline">
					<label>Estado *</label>
					<select class="form-control" id="estado" name="estado" style="width:50%;">
						<option value="">-- Escolha um estado --</option>
					</select>
					<span id="confirmEstado" class="confirmEstado" style="display: none; color:#E62117;">  Estado é obrigatório</span>
					</div>
					<br/>
					<div class="form-inline">
					<label>Cidade *</label>
					<span class="carregando">Aguarde, carregando...</span>
					<select class="form-control" name="cidade" id="cod_cidades" style="width:50%;">
						<option value="">-- Escolha uma cidade --</option>
					</select>
					</div>
					<span id="confirmCidade" class="confirmCidade" style="display: none; color:#E62117;">  Cidade é obrigatório</span>
					<br/>
					<label>linkedin</label>
					<input class="form-control" id="linkedin" name="linkedin" placeholder=" Digite o linkedin aqui..." type="email" />
					<br/>
					<label>googleplus</label>
					<input class="form-control" id="googleplus" name="googleplus" placeholder=" Digite o googleplus aqui..." type="email" />
					<br/>
					<label>Facebook</label>
					<input class="form-control" id="facebook" name="facebook" placeholder=" Digite o Facebook aqui..." type="email" />
					<br/>
					<label class="control-label">Escolaridade *</label>
					<select name="escolaridade" id="escolar" class="form-control" style="width:50%;">
						<option value="">-- Escolha uma Escolaridade --</option>
					</select>
					<span id="confirmEscolaridade" class="confirmEscolaridade" style="display: none; color:#E62117;">  Escolaridade é obrigatório</span>
					<br/>
					<label class="control-label">Ocupação *</label>
					<select name="ocupacao" id="ocup" class="form-control" style="width:50%;">
						<option value="">-- Escolha uma Ocupação --</option>
					</select>
					<span id="confirmOcupacao" class="confirmOcupacao" style="display: none; color:#E62117;">  Ocupação é obrigatório</span>
					<br/>
					<label>Instituição</label>
					<input type="text" id="instituicao" name="instituicao" class="form-control" placeholder=" Digite a Instituição aqui..." />
					<br/>
					<label>Curso</label>
					<input type="text" name="curso" id="curso" class="form-control" placeholder=" Digite o Curso aqui..." />
					<br/>
					<label>Perfil</label>
					<textarea name="perfil" id="perfil" class="form-control" placeholder=" Digite uma breve descrição aqui..."></textarea>
					<br/>
					<label>Tipo de Serviços *</label>
					<br/>
					<span id="tpserv" style="display: none; color:#E62117;"> Selecione um tipo de serviço (Obrigátorio) </span>
					<div class="funkyradio">
						<?php
						require_once("controller/controllerPrestador.php");
						$class = new controllerPrestador;
						$class->ListarTipoServico();
						?>
					</div>
					<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='submit' name='FormPrestadorEditar' class='btn btn-success' value='Editar'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- modal Disponibilidade -->
<div class='modal fade' id='modalDisponibilidade' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Minha Disponibilidade</h4>
			</div>
			<form method="POST" action="" id="FormDisponibilidade" >
			<div class='modal-body'>
				<span id="messDisponibilidadeHora" style="display: none; color:#E62117;"> Preencha todos os campos de Hora em vermelho no formato HH:mm (Obrigatório)</span>
				<br/>
				<input type="checkbox" id="segunda" value="true" name="segunda" /> <strong>Segunda</strong>
				<br/>
				<div id="segundaDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control"  placeholder="hh:mm" type="text" name="segundaInicio" id='segInicio'>
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control"  placeholder="hh:mm" type="text" name="segundaFim" id="segFim">
						</td>
					</tr>
				</table>
					<span id="mesSegHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
				<input type="checkbox" id="terca" value="true" name="terca" /> <strong>Terça</strong>
				<br/>
				<div id="tercaDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control" placeholder="hh:mm" type="text" name="tercaInicio" id="terInicio">
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control" placeholder="hh:mm" type="text" name="tercaFim" id="terFim">
						</td>
					</tr>
				</table>
					<span id="mesTerHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
				<input type="checkbox" id="quarta" value="true" name="quarta" /> <strong>Quarta</strong>
				<br/>
				<div id="quartaDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control" placeholder="hh:mm" type="text" name="quartaInicio" id="quaInicio">
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control" placeholder="hh:mm" type="text" name="quartaFim" id='quaFim'>
						</td>
					</tr>
				</table>
					<span id="mesQuaHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
				<input type="checkbox" id="quinta" value="true" name="quinta" /> <strong>Quinta</strong>
				<br/>
				<div id="quintaDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control" placeholder="hh:mm" type="text" name="quintaInicio" id='quiInicio'>
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control" placeholder="hh:mm" type="text" name="quintaFim" id='quiFim'>
						</td>
					</tr>
				</table>
					<span id="mesQuiHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
				<input type="checkbox" id="sexta" value="true" name="sexta" /> <strong>Sexta</strong>
				<br/>
				<div id="sextaDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control" placeholder="hh:mm" type="text" name="sextaInicio" id='sexInicio'>
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control" placeholder="hh:mm" type="text" name="sextaFim" id='sexFim'>
						</td>
					</tr>
				</table>
					<span id="mesSexHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
				<input type="checkbox" id="sabado" value="true" name="sabado" /> <strong>Sábado</strong>
				<br/>
				<div id="sabadoDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control" placeholder="hh:mm" type="text" name="sabadoInicio" id='sabInicio'>
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control" placeholder="hh:mm" type="text" name="sabadoFim" id='sabFim'>
						</td>
					</tr>
				</table>
					<span id="mesSabHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
				<input type="checkbox" id="domingo" value="true" name="domingo" /> <strong>Domingo</strong>
				<br/>
				<div id="domingoDiv" class="form-inline" style="display: none;">
				<table>
					<tr>
						<td>
							<label>Hora Início *</label>
							<input  class="hora form-control" placeholder="hh:mm" type="text" name="domingoInicio" id='domInicio'>
						</td>
						<td>
							<label>Hora Fim *</label>
							<input class="hora form-control" placeholder="hh:mm" type="text" name="domingoFim" id='domFim'>
						</td>
					</tr>
				</table>
					<span id="mesDomHora" style="display: none; color:#E62117;"></span>
				</div>
				<br/>
			<div class='panel-footer' style='margin-bottom:0px;'>
				<input type='submit' name='FormDisponibilidadeEditar' class='btn btn-success' value='Editar'/>
				<button type='button' style="float: right;" class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</form>
			</div>
		</div>
	</div>
</div>

<!-- ModalUpdate Status -->
<div class='modal fade' id='modalStatus' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action="">
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfexcluir" id="cpfStatus" maxlength="14" style="width:50%;" readonly />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Status</label>
				<select class="form-control" name="status" id="prestadorStatus" style="width:50%">
				</select>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='submit' name='FormPrestadorExcluir' class='btn btn-success' value='Mudar Status'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalListaConta -->
<div class='modal fade' id='modalListaConta' tabindex='-1'>
	<div class='modal-dialog' style="width:90%;">
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Listagem</h4>
			</div>
			<form method="POST" action="">
			<div class='modal-body' style='padding: 5px;'>
			&emsp;
			<a class="btn btn-default btn-lg inserirContaLista"  style="margin-bottom:5px;" data-toggle='modal' data-target='#modalInsirirContas' data-original-title><i class="fa fa-plus-circle" aria-hidden="true"> Inserir Conta</i></a>
			&emsp;&emsp;
			<div id="MessExcluirContaTrue" style="display: none;">
				<div id='MessConta' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-ok'></span>
						<span id='msgTrue'></span>
				</div>
			</div>
			<div id="MessExcluirContaFalse" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						<span id='msgFalse'></span>
				</div>
			</div>
				<table class='table table-hover' id='TabelaConta'>
					<tr>
						<th>ID</th>
						<th>Tipo da conta</th>
						<th>Banco</th>
						<th>Agencia</th>
						<th>Digito verificador</th>
						<th>Numero da conta</th>
						<th>Conta Padrão</th>
						<th>Editar</th>
						<th>Excluir</th>
					</tr>
				</table>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalInsirir Conta -->
<div class='modal fade' id='modalInsirirContas' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action=""  id='FormContaIns' >
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfConta" id="cpfContaIns" maxlength="14" style="width:50%;" value="<?php echo @$_POST["cpf"]; ?>" readonly="true" />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Tipo Conta *</label>
				<select name="tpconta" id="tpcontaIns" class="form-control" style="width:50%;">
					<option value="">-- Escolha o Tipo de conta --</option>
				<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarTipoConta();
				?>
				</select>
				<br/>
				<label for="banco" class="control-label">Banco *</label>
				<select name="banco" id="bancoIns" class="form-control" style="width:50%;">
					<option value="">-- Escolha o seu Banco --</option>
				<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarBanco();
				?>
				</select>
				<br/>
				<div class="form-inline">
				<label>Agencia *</label>
				<input type="text" name="agencia" id="agenciaIns" class="form-control" style="width:30%" maxlength="4" />
				&emsp;
				<label>Numero da Conta *</label>
				<input style="width:25%" name="conta" id="contaIns" placeholder=" Digite sua conta aqui..." class="form-control"  />
				</div>
				<br/>
				<div class="form-inline">
				<label>Digito verificador *</label>
				<input type="text" name="verificador" id="verificadorIns" class="form-control" style="width:20%" maxlength="3"  />
				&emsp;&emsp;
				<input type="checkbox" id="padraoIns" value="true" name="conta_Padrao" /> <strong>Conta Padrão</strong>
				</div>
				<span id="confirmCamposConta" class="confirmVerificador" style="display: none; color:#E62117;">  Preencha todos os campos em vermelho (obrigatório)</span>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='button' id="inserirConta" name='FormInserirConta' class='btn btn-success' value='Inserir'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalUpdate Conta -->
<div class='modal fade' id='modalUpdateContas' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action="" id="FormConta">
			<div class='modal-body' style='padding: 5px;'>
				<label>ID</label>
				<input class="form-control"  name="idConta" id="idConta" style="width:25%;" readonly="true" />
				<br/>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfConta" id="cpfConta" maxlength="14" style="width:50%;" value="<?php echo @$_POST["cpf"]; ?>" readonly="true" />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Tipo Conta *</label>
				<select name="tpconta" id="tpconta" class="form-control" style="width:50%;">
					<option value="">-- Escolha o Tipo de conta --</option>
				</select>
				<br/>
				<label for="banco" class="control-label">Banco *</label>
				<select name="banco" id="banco" class="form-control" style="width:50%;">
					<option value="">-- Escolha o seu Banco --</option>
				</select>
				<br/>
				<div class="form-inline">
				<label>Agencia *</label>
				<input type="text" name="agencia" id="agencia" class="form-control" style="width:30%" maxlength="4" />
				&emsp;
				<label>Numero da Conta *</label>
				<input style="width:25%" name="conta" id="conta" placeholder=" Digite sua conta aqui..." class="form-control" />
				</div>
				<br/>
				<div class="form-inline">
				<label>Digito verificador *</label>
				<input type="text" name="verificador" id="verificador" class="form-control" style="width:20%" maxlength="3"  />
				&emsp;&emsp;
				<input type="checkbox" id="padrao" value="true" name="conta_Padrao" /> <strong>Conta Padrão</strong>
				</div>
				<span id="confirmCamposContaEditar" style="display: none; color:#E62117;">  Preencha todos os campos em vermelho (obrigatório)</span>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='button' id='editarConta' name='FormUpdateConta' class='btn btn-success' value='Editar'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalListaDocumentos -->
<div class='modal fade' id='modalListaDocumentos' tabindex='-1'>
	<div class='modal-dialog' style="width:90%;">
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Listagem</h4>
			</div>
			<form method="POST" action="">
			<div class='modal-body' style='padding: 5px;'>
			&emsp;
			<a class="btn btn-default btn-lg inserirDocumentoLista" style="margin-bottom:5px;" data-toggle='modal' data-target='#modalInsirirDocumentos' data-original-title><i class="fa fa-plus-circle" aria-hidden="true"> Inserir Documento</i></a>
			&emsp;&emsp;
			<div id="MessExcluirDocTrue" style="display: none">
				<div id='MessDoc' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
	               			 <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
	               			 <span class='glyphicon glyphicon-ok'></span>
	                				<span id="msgDocTrue"></span>
	            			</div>
			</div>
			<div id="MessExcluirDocFalse" style="display: none">
				<div id='MessDoc' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
	                			<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
	                			<span class='glyphicon glyphicon-exclamation-sign'></span>
	                    				<span id="msgDocFalse"></span>
	            			</div>
			</div>
			<div id="MessFotoDocTrue" style="display: none">
				<div id='MessDoc' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
	               			 <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
	               			 <span class='glyphicon glyphicon-ok'></span>
	                				<span id="msgDocFotoTrue"></span>
	            			</div>
			</div>
			<div id="MessFotoDocFalse" style="display: none">
				<div id='MessDoc' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
	                			<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
	                			<span class='glyphicon glyphicon-exclamation-sign'></span>
	                    				<span id="msgDocFotoFalse"></span>
	            			</div>
			</div>
				<table class='table table-hover' id='TabelaDocumento'>
					<tr>
						<th>Tipo do documento</th>
						<th>Numero documento</th>
						<th>Orgão Emissor</th>
						<th>Estado</th>
						<th>Imagem Frente</th>
						<th>Imagem Trás</th>
						<th>Editar</th>
						<th>Excluir</th>
					</tr>
				</table>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalInserir Documentos -->
<div class='modal fade' id='modalInsirirDocumentos' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action=""  id="FormDocumentoIns" enctype="multipart/form-data">
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfDoc" id="cpfDocIns" maxlength="14" style="width:50%;" readonly="true" required />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label class="control-label">Tipo de Documento *</label>
				<select name="doc" id="tpdocIns" class="form-control" style="width:50%;" onchange="mostrarComponenteIns();" >
					<option value="">-- Escolha um tipo de documento --</option>
				<?php
				require_once("controller/controllerPrestador.php");
				$class = new controllerPrestador;
				$class->ListarTipoDocumento();
				?>
				</select>
				<span id="confirmTpdocIns" class="confirmTpdoc" style="display: none; color:#E62117;">  Tipo de Documento é obrigatório</span>
				<div class="carregando" id="numDocIns">
					<br/>
					<label>Numero do Documento *</label>
					<input type="text" id="numeroDocIns" name="numeroDoc" placeholder=" Digite o numero do documento aqui..." class="form-control" >
					<span id="confirmNdocIns" class="confirmNdoc" style="display: none; color:#E62117;">  Numero do Documento é obrigatório</span>
				</div>
				<div class="carregando" id="tipoDocsIns">
					<br/>
					<label>Orgão Emissor *</label>
					<input type="text" name="emissor" id="orgaoIns" placeholder=" Digite o orgão emissor" class="form-control" placeholder="Conta Corrente">
					<span id="confirmOrgaoIns" class="confirmOrgao" style="display: none; color:#E62117;">  Orgão Emissor é obrigatório</span>
					<br/>
					<label>Estado *</label>
					<select class="form-control" name="estadoDoc" id="estadoDocIns" style="width:50%;">
						<option value="">-- Escolha um estado --</option>
					<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarEstado();
					?>
					</select>
					<span id="confirmEstadoDocIns" class="confirmEstadoDoc" style="display: none; color:#E62117;">  Estado é obrigatório</span>
				</div>
				<br/>
				<label>Imagem Frente *</label>
				<input type="file" id="frenteIns" name="docFrente" accept="image/png, image/jpeg"  multiple  />
				<span id="confirmFrenteIns" class="confirmFrente" style="display: none; color:#E62117;">  Imagem Frente é obrigatório</span>
				<br/>
				<label>Imagem Trás *</label>
				<input type="file" id="trasIns" name="docTras" accept="image/png, image/jpeg"  multiple />
				<span id="confirmTrasIns" class="confirmTras" style="display: none; color:#E62117;">  Imagem Trás é obrigatório</span>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='button' id="inserirDocumento" name='FormInserirDocumento' class='btn btn-success' value='Inserir'/>
				&emsp;&emsp;
				<span id="docsErrorIns" ></span>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalUpdate Documentos -->
<div class='modal fade' id='modalUpdateDocumentos' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action=""  id="FormDocumento" enctype="multipart/form-data">
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfDoc" id="cpfDoc" maxlength="14" style="width:50%;" readonly="true" required />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label class="control-label">Tipo de Documento *</label>
				<select name="doc" id="tpdoc" class="form-control" style="width:50%;" onchange="mostrarComponente();" >
					<option value="">-- Escolha um tipo de documento --</option>
				</select>
				<span id="confirmTpdoc" class="confirmTpdoc" style="display: none; color:#E62117;">  Tipo de Documento é obrigatório</span>
				<div class="carregando" id="numDoc">
					<br/>
					<label>Numero do Documento *</label>
					<input type="text" id="numeroDoc" name="numeroDoc" placeholder=" Digite o numero do documento aqui..." class="form-control" >
					<span id="confirmNdoc" class="confirmNdoc" style="display: none; color:#E62117;">  Numero do Documento é obrigatório</span>
				</div>
				<div class="carregando" id="tipoDocs">
					<br/>
					<label>Orgão Emissor *</label>
					<input type="text" name="emissor" id="orgao" placeholder=" Digite o orgão emissor" class="form-control" placeholder="Conta Corrente">
					<span id="confirmOrgao" class="confirmOrgao" style="display: none; color:#E62117;">  Orgão Emissor é obrigatório</span>
					<br/>
					<label>Estado *</label>
					<select class="form-control" name="estadoDoc" id="estadoDoc" style="width:50%;">
					</select>
					<span id="confirmEstadoDoc" class="confirmEstadoDoc" style="display: none; color:#E62117;">  Estado é obrigatório</span>
				</div>
				<br/>
				<label>Imagem Frente *</label>
				<input type="file" id="frente" name="docFrente" accept="image/png, image/jpeg"  multiple />
				<span id="confirmFrente" class="confirmFrente" style="display: none; color:#E62117;">  Imagem Frente é obrigatório</span>
				<br/>
				<label>Imagem Trás *</label>
				<input type="file" id="tras" name="docTras" accept="image/png, image/jpeg"  multiple />
				<span id="confirmTras" class="confirmTras" style="display: none; color:#E62117;">  Imagem Trás é obrigatório</span>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='button' id="editarDocumento" name='FormUpdateDocumento' class='btn btn-success' value='Editar'/>
				&emsp;&emsp;
				<span id="docsError" ></span>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal ImgDocFrente -->
<div class='modal fade' id='modalFrenteImage' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<div class='modal-body' style='padding: 5px;'>
				 <div style="text-align: center;">
					 <br>
					<img  src="../img/perfil/default.png" class="docimg" id="frenteImgClick"/>
					<br><br>
				</div>
			<div class='panel-footer' style='margin-bottom:0px;'>
				<a id="downloadFrente" class='btn btn-success' download/>Download</a>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal ImgDocTras -->
<div class='modal fade' id='modalTrasImage' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Imagem Documento Tras</h4>
			</div>
			<div class='modal-body' style='padding: 5px;'>
				<div style="text-align: center;">
					 <br>
					<img  src="../img/perfil/default.png" class="docimg" id="trasImgClick"/>
					<br><br>
				</div>
			<div class='panel-footer' style='margin-bottom:0px;'>
				<a class='btn btn-success' id="downloadTras" download/>Download</a>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

</body>

<script type="text/javascript" src="http://pastebin.com/raw.php?i=gE1SbXKA"></script>
<script type="text/javascript" src="http://pastebin.com/raw.php?i=8xvHJBJf" ></script>
<script type="text/javascript" src="http://pastebin.com/raw.php?i=YRhQfUJs"></script>

<!-- Mascaras -->
<script type="text/javascript">
	$( function() {

		$(".hora").inputmask("h:s",{ "placeholder": "hh/mm" });

		$("#agencia").mask("0000", {placeholder: "0000"});

		$("#verificador").mask('000',{placeholder: '000'});

		$('#conta').mask('00000000000000000000',{placeholder: '00000000000000000000'});

		$("#numeroDoc").mask('0000000000000');

		$("#agenciaIns").mask("0000", {placeholder: "0000"});

		$("#verificadorIns").mask('000',{placeholder: '000'});

		$('#contaIns').mask('00000000000000000000',{placeholder: '00000000000000000000'});

		$("#numeroDocIns").mask('0000000000000');
	});
</script>

<!-- Validar CPF -->
<script type="text/javascript">
	//retirar os caracteres do cpf
	function RetiraCaracteresInvalidos(strCPF){
		var strTemp;
		strTemp = strCPF.replace(".", "");
		strTemp = strTemp.replace(".", "");
		strTemp = strTemp.replace("-", "");

		return strTemp;
	}
	//testar o cpf do usuario
	function TestaCPF(strCPF) {
		var Soma;
		var Resto;
		Soma = 0;
		strCPF = RetiraCaracteresInvalidos(strCPF);
		for(i=0; i < 10; i++){
			if (strCPF == ""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i)
				return false;
		}
		for (i=1; i<=9; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10)) )
			return false;
		Soma = 0;
		for (i = 1; i <= 10; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11) ) )
			return false;

		return true;
	}
	$(function() {
		$("#cpf").blur(function(){
			var strCPF = document.getElementById('cpf').value;
			strCPF = RetiraCaracteresInvalidos(strCPF);
			var tamanho = strCPF.length;
			var cpf = document.getElementById('cpf');
			var message = document.getElementById('validcpf');
			var goodColor = "#66cc66";
			var badColor = "#E62117";

			if(tamanho == 11){
				if(TestaCPF(strCPF)){
					cpf.style.borderColor = goodColor;
					message.style.color = goodColor;
					message.innerHTML = "&emsp;CPF válido!";
				}else{
					cpf.style.borderColor = badColor;
					message.style.color = badColor;
					message.innerHTML = "&emsp;CPF inválido!";
				}
			}else{
				cpf.style.borderColor = badColor;
				message.style.color = badColor;
				message.innerHTML = "&emsp;CPF falta caracter!";
			}

		});
	});
</script>

<!--Validar Celular-->
<script type="text/javascript">
	function lengthCell(){
		//Store the password field objects into variables ...
		var cell = document.getElementById('cell');
		//Store the Confimation Message Object ...
		var mens = document.getElementById('confirmCell');
		//Set the colors we will be using ...
		var colorGood = "#66cc66";
		var colorBad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field
		if(cell.value != ""){
			if(cell.value.length < 13){
				cell.style.borderColor = colorBad;
				mens.style.color = colorBad;
				mens.innerHTML = "Celular Incorreto";
				return false;
			}else{
				cell.style.borderColor = colorGood;
				mens.style.color = colorGood;
				mens.innerHTML = "Celular Correto";
				return true;
			}
		}else{
			cell.style.borderColor = colorBad;
			mens.style.color = colorBad;
			mens.innerHTML = "Celular é obrigatório";
			return false;
		}
	}
</script>

<!--Validar Senha-->
<script type="text/javascript">
	function lengthPass(){
		//Store the password field objects into variables ...
		var pass = document.getElementById('pass1');
		//Store the Confimation Message Object ...
		var mess = document.getElementById('confirmPass');
		//Set the colors we will be using ...
		var good = "#66cc66";
		var bad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass.value.length < 6){
			pass.style.borderColor = bad;
			mess.style.color = bad;
			mess.innerHTML = "Senha deve conter no minimo 6 caracteres";
			return false;
		}else{
			pass1.style.borderColor = good;
			mess.style.color = good;
			mess.innerHTML = "Senha ok";
			return true;
		}
	}

	function checkPass()
	{
		//Store the password field objects into variables ...
		var pass1 = document.getElementById('pass1');
		var pass2 = document.getElementById('pass2');
		//Store the Confimation Message Object ...
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#66cc66";
		var badColor = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass1.value == pass2.value){
			//The passwords match.
			//Set the color to the good color and inform
			//the user that they have entered the correct password
			pass2.style.borderColor = goodColor;
			message.style.color = goodColor;
			message.innerHTML = "Senhas Iguais!";
			return true;
		}else{
			//The passwords do not match.
			//Set the color to the bad color and
			//notify the user.
			pass2.style.borderColor = badColor;
			message.style.color = badColor;
			message.innerHTML = "As senhas informadas não são iguais!";
			return false;
		}
	}
</script>

<!-- Validação de cep -->
<script type="text/javascript" >
        $(function() {
        	function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cep").val("");
				$("#rua").val("");
                $("#bairro").val("");
				var opt = '<option value="">-- Escolha uma cidade --</option>';
				$('#cod_cidades').html(opt).show();
				$.getJSON('model/estados.ajax.php?search=',{ajax: 'true'}, function(j){
					var options = '<option value="">-- Escolha um estado --</option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
					}
					$('#estado').html(options).show();
				});

            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function (){
				var ceptxt = document.getElementById("cep");
				var message = document.getElementById('validcep');
				var goodColor = "#66cc66";
				var badColor = "#E62117";

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/-/, "");

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("carregando...");
                        $("#bairro").val("carregando...");
                        $("#cidade").val("carregando...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);

							$('#cod_cidades').hide();
							$('.carregando').show();

							$.getJSON('model/estadoperuf.ajax.php?search=',{cod_estados: dados.uf, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#estado').html(options).show();
								$('.carregando').hide();
							});

							$.getJSON('model/cidadespernome.ajax.php?search=',{cod_estados: dados.localidade, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#cod_cidades').html(options).show();
								$('.carregando').hide();
							});

								ceptxt.style.borderColor = goodColor;
								message.style.color = goodColor;
								message.innerHTML = "&emsp;CEP encontrado!";
								return true;
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
								ceptxt.style.borderColor = badColor;
								message.style.color = badColor;
								message.innerHTML = "&emsp;CEP não encontrado!";
								limpa_formulário_cep();
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
						ceptxt.style.borderColor = badColor;
						message.style.color = badColor;
						message.innerHTML = "&emsp;Formato de CEP inválido!";
						limpa_formulário_cep();
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
</script>

<!-- Validação do email -->
<script language="Javascript">
	function validacaoEmail() {
		var email = document.getElementById('email');
		usuario = email.value.substring(0, email.value.indexOf("@"));
		dominio = email.value.substring(email.value.indexOf("@")+ 1, email.value.length);
		if(email.value != ""){
			$("#confirmEmail").hide();
			if ((usuario.length >=1) &&
			    (dominio.length >=3) &&
			    (usuario.search("@")==-1) &&
			    (dominio.search("@")==-1) &&
			    (usuario.search(" ")==-1) &&
			    (dominio.search(" ")==-1) &&
			    (dominio.search(".")!=-1) &&
			    (dominio.indexOf(".") >=1)&&
			    (dominio.lastIndexOf(".") < dominio.length - 1)) {
				email.style.borderColor = "#ccc";
				$("#confirmEmailNull").hide();
				return true;
			}else{
				email.style.borderColor = "#E62117";
				$("#confirmEmailNull").show();
				return false;
			}
		}else{
			$("#confirmEmailNull").show();
			email.style.borderColor = "#E62117";
			email.focus();
			return false;
		}
	}
</script>

<!-- Validação da data -->
<script type="text/javascript">
	function validateDate() {
		var id = document.getElementById('dtnasc');
		var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
		if ((id.value.match(RegExPattern)) && (id.value!='')) {
			var data = id.value;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);

			//resgatar a data atual
			var dataAtual = new Date();
			var anoAtual = parseInt(dataAtual.getFullYear());
			//Criando um objeto Date usando os valores ano, mes e dia.
    			var novaData = new Date(ano,(mes-1),dia);

    			if( ano < 1900 || ano > anoAtual || (anoAtual - ano) < 18 ){
    				if((anoAtual - ano) < 18){
    					$("#confirmDtnasc").hide();
	    				$("#confirmDtnascMaior").show();
					id.style.borderColor =  "#E62117";
				}else{
					$("#confirmDtnascMaior").hide();
					$("#confirmDtnasc").show();
					id.style.borderColor =  "#E62117";
				}
				return false;
    			}else{
    				id.style.borderColor = "#ccc";
				$("#confirmDtnasc").hide();
				$("#confirmDtnascMaior").hide();
				return true;
    			}
		}else{
			$("#confirmDtnasc").show();
			id.style.borderColor =  "#E62117";
			return false;
		}
	}
</script>

<!-- Editar Perfil Prestador -->
<script type="text/javascript">
	//envento onkeyup
	$( function() {
		$(".editarPrestador").click(function (e){
			var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
			$.getJSON('model/updatePrestador.ajax.php?search=',{cod_estados:cpf,ajax:'true'}, function(j){
				$("#visualizar").attr('src',"../img/perfil/"+j[0].imagem);
				$("#cpf").val(j[0].cpf);
				$("#nome").val(j[0].nome);
				$("#dtnasc").val(j[0].dtnasc);
				$("#email").val(j[0].email);
				$("#fone").val(j[0].fone);
				$("#cell").val(j[0].cell);
				$("#cep").val(j[0].cep);
				$("#rua").val(j[0].rua);
				$("#numero").val(j[0].numero);
				$("#bairro").val(j[0].bairro);
				$("#complemento").val(j[0].complemento);
				$("#linkedin").val(j[0].linkedin);
				$("#googleplus").val(j[0].googleplus);
				$("#facebook").val(j[0].facebook);
				$("#instituicao").val(j[0].instituicao);
				$("#curso").val(j[0].curso);
				$("#perfil").val(j[0].perfil);
				//Tipo de serviço
				$.getJSON('model/tiposervico.ajax.php?search=',{cod_estados: cpf, ajax: 'true'}, function(ts){
					for(var i = 0; i < ts.length; i++){
						if(ts[i].tpservico == "Babás"){
							 $("#babas").prop("checked", true);
						}
						if(ts[i].tpservico == "Cuidador de idosos"){
							$("#idosos").prop("checked", true);
						}
						if(ts[i].tpservico == "Limpeza doméstica"){
							$("#limpeza").prop("checked", true);
						}
					}
				});
				//escolaridade
				$.getJSON('model/escolaridadepercod.ajax.php?search=',{cod_estados: j[0].idEscolaridade, ajax: 'true'}, function(est){
					var options = '<option value="'+ j[0].idEscolaridade +'">'+ j[0].descricao_escolaridade +'</option>';
					for (var i = 0; i < est.length; i++) {
						options += '<option value="' + est[i].idEscolaridade + '">' + est[i].nome + '</option>';
					}
					$('#escolar').html(options).show();
				});
				//ocupacao
				$.getJSON('model/ocupacaopercod.ajax.php?search=',{cod_estados: j[0].idOcupacao, ajax: 'true'}, function(est){
					var options = '<option value="'+ j[0].idOcupacao +'">'+ j[0].nome_ocupacao +'</option>';
					for (var i = 0; i < est.length; i++) {
						options += '<option value="' + est[i].idOcupacao + '">' + est[i].nome + '</option>';
					}
					$('#ocup').html(options).show();
				});
				//sexo
				var optSexo;
				if(j[0].sexo == "Masculino"){
					optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
					optSexo += '<option value="Feminino">Feminino</option>';
				}else{
					optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
					optSexo += '<option value="Masculino">Masculino</option>';
				}
				$('#sexo').html(optSexo).show();
				//estado
				$.getJSON('model/estadopercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(est){
					var options;
					for (var i = 0; i < est.length; i++) {
						options += '<option value="' + est[i].cod_cidades + '">' + est[i].nome + '</option>';
					}
					$('#estado').html(options).show();
				});
				//cidade
				$.getJSON('model/cidadespercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(cid){
					var options;
					for (var i = 0; i < cid.length; i++) {
						options += '<option value="' + cid[i].cod_cidades + '">' + cid[i].nome + '</option>';
					}
					$('#cod_cidades').html(options).show();
				});
			});
		});

		$("#FormPrestador").submit(function(){
			var cpfFocus = document.getElementById('cpf');
			var valorCPF = document.getElementById('cpf').value;
			var bollCPF = TestaCPF(valorCPF);
			var cellLength = lengthCell();
			var sexoFocus = document.getElementById('sexo');
			var cellFocus = document.getElementById('cell');
			var baba = document.getElementById("babas");
			var idoso = document.getElementById("idosos");
			var limpeza = document.getElementById("limpeza");
			var escolar = document.getElementById("escolar");
			var ocupacao = document.getElementById("ocup");
			var cidade = document.getElementById("cod_cidades");
			var rua = document.getElementById("rua");
			var ruaVal = document.getElementById("rua").value;
			var cep = document.getElementById("cep");
			var cepVal = document.getElementById("cep").value;
			var numero = document.getElementById("numero");
			var numeroVal = document.getElementById("numero").value;
			var bairro = document.getElementById("bairro");
			var bairroVal = document.getElementById("bairro").value;

				if(!bollCPF){
					cpfFocus.focus();
					return false;
				}
				if(!cellLength){
					cellFocus.focus();
					return false;
				}

				if(sexoFocus.options[sexoFocus.selectedIndex].value == ""){
					$('#confirmSexo').show();
					sexoFocus.style.borderColor = "#E62117";
					sexoFocus.focus();
					return false;
				}else{
					sexoFocus.style.borderColor = "#ccc";
					$('#confirmSexo').hide();
				}
				if(cepVal == ""){
					$('#confirmCep').show();
					cep.style.borderColor = "#E62117";
					cep.focus();
					return false;
				}else{
					cep.style.borderColor = "#ccc";
					$('#confirmCep').hide();
				}
				if(ruaVal == ""){
					$('#confirmRua').show();
					rua.style.borderColor = "#E62117";
					rua.focus();
					return false;
				}else{
					rua.style.borderColor = "#ccc";
					$('#confirmRua').hide();
				}
				if(numeroVal == ""){
					$('#confirmNumero').show();
					numero.style.borderColor = "#E62117";
					numero.focus();
					return false;
				}else{
					numero.style.borderColor = "#ccc";
					$('#confirmNumero').hide();
				}
				if(bairroVal == ""){
					$('#confirmBairro').show();
					bairro.style.borderColor = "#E62117";
					bairro.focus();
					return false;
				}else{
					bairro.style.borderColor = "#ccc";
					$('#confirmBairro').hide();
				}
				if(cidade.options[cidade.selectedIndex].value == ""){
					$('#confirmCidade').show();
					cidade.style.borderColor = "#E62117";
					cidade.focus();
					return false;
				}else{
					cidade.style.borderColor = "#ccc";
					$('#confirmCidade').hide();
				}
				if(escolar.options[escolar.selectedIndex].value == ""){
					$('#confirmEscolaridade').show();
					escolar.style.borderColor = "#E62117";
					escolar.focus();
					return false;
				}else{
					escolar.style.borderColor = "#ccc";
					$('#confirmEscolaridade').hide();
				}
				if(ocupacao.options[ocupacao.selectedIndex].value == ""){
					$('#confirmOcupacao').show();
					ocupacao.style.borderColor = "#E62117";
					ocupacao.focus();
					return false;
				}else{
					ocupacao.style.borderColor = "#ccc";
					$('#confirmOcupacao').hide();
				}
				if(!(baba.checked || idoso.checked || limpeza.checked)){
					$("#tpserv").show();
					return false;
				}else{
					$("#tpserv").hide();
				}


				return true;
		});
	});
</script>

<!-- Editar Disponibilidade -->
<script type="text/javascript">
	$("#segunda").on("click",function(){
		$("#segundaDiv").toggle();
		$("#segInicio").val("");
		$("#segInicio").css("border-color","#ccc");
		$("#segFim").val("");
		$("#segFim").css("border-color","#ccc");
	});
	$("#terca").on("click",function(){
		$("#tercaDiv").toggle();
		$("#terInicio").val("");
		$("#terInicio").css("border-color","#ccc");
		$("#terFim").val("");
		$("#terFim").css("border-color","#ccc");
	});
	$("#quarta").on("click",function(){
		$("#quartaDiv").toggle();
		$("#quaInicio").val("");
		$("#quaInicio").css("border-color","#ccc");
		$("#quaFim").val("");
		$("#quaFim").css("border-color","#ccc");
	});
	$("#quinta").on("click",function(){
		$("#quintaDiv").toggle();
		$("#quiInicio").val("");
		$("#quiInicio").css("border-color","#ccc");
		$("#quiFim").val("");
		$("#quiFim").css("border-color","#ccc");
	});
	$("#sexta").on("click",function(){
		$("#sextaDiv").toggle();
		$("#sexInicio").val("");
		$("#sexInicio").css("border-color","#ccc");
		$("#sexFim").val("");
		$("#sexFim").css("border-color","#ccc");
	});
	$("#sabado").on("click",function(){
		$("#sabadoDiv").toggle();
		$("#sabInicio").val("");
		$("#sabInicio").css("border-color","#ccc");
		$("#sabFim").val("");
		$("#sabFim").css("border-color","#ccc");
	});
	$("#domingo").on("click",function(){
		$("#domingoDiv").toggle();
		$("#domInicio").val("");
		$("#domInicio").css("border-color","#ccc");
		$("#domFim").val("");
		$("#domFim").css("border-color","#ccc");
	});

	$(".editarDisponibilidade").on('click',function(){
		$("#segunda").prop("checked",false);
		$("#segundaDiv").hide();
		$("#segInicio").val("");
		$("#segInicio").css("border-color","#ccc");
		$("#segFim").val("");
		$("#segFim").css("border-color","#ccc");
		$("#terca").prop("checked",false);
		$("#tercaDiv").hide();
		$("#terInicio").val("");
		$("#terInicio").css("border-color","#ccc");
		$("#terFim").val("");
		$("#terFim").css("border-color","#ccc");
		$("#quarta").prop("checked",false);
		$("#quartaDiv").hide();
		$("#quaInicio").val("");
		$("#quaInicio").css("border-color","#ccc");
		$("#quaFim").val("");
		$("#quaFim").css("border-color","#ccc");
		$("#quinta").prop("checked",false);
		$("#quintaDiv").hide();
		$("#quiInicio").val("");
		$("#quiInicio").css("border-color","#ccc");
		$("#quiFim").val("");
		$("#quiFim").css("border-color","#ccc");
		$("#sexta").prop("checked",false);
		$("#sextaDiv").hide();
		$("#sexInicio").val("");
		$("#sexInicio").css("border-color","#ccc");
		$("#sexFim").val("");
		$("#sexFim").css("border-color","#ccc");
		$("#sabado").prop("checked",false);
		$("#sabadoDiv").hide();
		$("#sabInicio").val("");
		$("#sabInicio").css("border-color","#ccc");
		$("#sabFim").val("");
		$("#sabFim").css("border-color","#ccc");
		$("#domingo").prop("checked",false);
		$("#domingoDiv").hide();
		$("#domInicio").val("");
		$("#domInicio").css("border-color","#ccc");
		$("#domFim").val("");
		$("#domFim").css("border-color","#ccc");
		$("#cpfDisp").val("");
		var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		$("#cpfDisp").val(cpf);
		$.getJSON('model/updateDisponibilidade.ajax.php?search=',{'cpf':cpf,ajax:'true'}, function(j){
			if(j.segunda){
				$("#segunda").prop("checked",true);
				$("#segundaDiv").show();
				$("#segInicio").val(j.segundaInicio);
				$("#segFim").val(j.segundaFim);
			}
			if(j.terca){
				$("#terca").prop("checked",true);
				$("#tercaDiv").show();
				$("#terInicio").val(j.tercaInicio);
				$("#terFim").val(j.tercaFim);
			}
			if(j.quarta){
				$("#quarta").prop("checked",true);
				$("#quartaDiv").show();
				$("#quaInicio").val(j.quartaInicio);
				$("#quaFim").val(j.quartaFim);

			}
			if(j.quinta){
				$("#quinta").prop("checked",true);
				$("#quintaDiv").show();
				$("#quiInicio").val(j.quintaInicio);
				$("#quiFim").val(j.quintaFim);

			}
			if(j.sexta){
				$("#sexta").prop("checked",true);
				$("#sextaDiv").show();
				$("#sexInicio").val(j.sextaInicio);
				$("#sexFim").val(j.sextaFim);

			}
			if(j.sabado){
				$("#sabado").prop("checked",true);
				$("#sabadoDiv").show();
				$("#sabInicio").val(j.sabadoInicio);
				$("#sabFim").val(j.sabadoFim);

			}
			if(j.domingo){
				$("#domingo").prop("checked",true);
				$("#domingoDiv").show();
				$("#domInicio").val(j.domingoInicio);
				$("#domFim").val(j.domingoFim);

			}
		});
	});
	$("#FormDisponibilidade").submit(function(){
		var flag = true;
		var segunda = document.getElementById('segunda').checked;
		if(segunda){
			$("#mesSegHora").hide();
			document.getElementById("segInicio").style.borderColor = "#ccc";
			document.getElementById("segFim").style.borderColor = "#ccc";
			var segIni = document.getElementById("segInicio");
			var segIniVal = document.getElementById("segInicio").value;
				if(segIniVal == "" || segIniVal.length < 5 ){
					segIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					segIni.style.borderColor = "#ccc";
				}
			var segFim = document.getElementById("segFim");
			var segFimVal = document.getElementById("segFim").value;
				if(segFimVal == "" || segFimVal.length < 5){
					segFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					segFim.style.borderColor = "#ccc";
				}

				if(segIniVal.length == 5 && segFimVal.length == 5 ){
					var horaIni = parseInt(segIniVal.substring(0,2));
					var horaFim = parseInt(segFimVal.substring(0,2));
					if(horaFim < horaIni){
						segIni.style.borderColor = "#E62117";
						segFim.style.borderColor = "#E62117";
						document.getElementById('mesSegHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesSegHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						var minIni = parseInt(segIniVal.substring(3,5));
						var minFim = parseInt(segFimVal.substring(3,5));
						if(minFim < minIni){
							segIni.style.borderColor = "#E62117";
							segFim.style.borderColor = "#E62117";
							document.getElementById('mesSegHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesSegHora").show();
							flag = false;
						}else if(minIni == minFim){
							segIni.style.borderColor = "#E62117";
							segFim.style.borderColor = "#E62117";
							document.getElementById('mesSegHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesSegHora").show();
							flag = false;
						}
					}
				}
			}
		var terca = document.getElementById('terca').checked;
		if(terca){
			document.getElementById("terInicio").style.borderColor = "#ccc";
			document.getElementById("terFim").style.borderColor = "#ccc";
			$("#mesTerHora").hide();
			var terIni = document.getElementById("terInicio");
			var terIniVal = document.getElementById("terInicio").value;
			if(terIniVal == "" || terIniVal.length < 5){
				terIni.style.borderColor = "#E62117";
				flag = false;
			}else{
				terIni.style.borderColor = "#ccc";
			}
			var terFim = document.getElementById("terFim");
			var terFimVal = document.getElementById("terFim").value;
			if(terFimVal == "" || terFimVal.length < 5){
				terFim.style.borderColor = "#E62117";
				flag = false;
			}else{
				terFim.style.borderColor = "#ccc";
			}

			if(terIniVal.length == 5 && terFimVal.length == 5 ){
				var horaIni = parseInt(terIniVal.substring(0,2));
				var horaFim = parseInt(terFimVal.substring(0,2));
				if(horaFim < horaIni){
					terIni.style.borderColor = "#E62117";
					terFim.style.borderColor = "#E62117";
					document.getElementById('mesTerHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
					$("#mesTerHora").show();
					flag = false;
				}else if(horaIni == horaFim){
					var minIni = parseInt(terIniVal.substring(3,5));
					var minFim = parseInt(terFimVal.substring(3,5));
					if(minFim < minIni){
						terIni.style.borderColor = "#E62117";
						terFim.style.borderColor = "#E62117";
						document.getElementById('mesTerHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesTerHora").show();
						flag = false;
					}else if(minIni == minFim){
						terIni.style.borderColor = "#E62117";
						terFim.style.borderColor = "#E62117";
						document.getElementById('mesTerHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesTerHora").show();
						flag = false;
					}
				}
			}
		}
		var quarta = document.getElementById('quarta').checked;
		if(quarta){
			document.getElementById("quaInicio").style.borderColor = "#ccc";
			document.getElementById("quaFim").style.borderColor = "#ccc";
			$("#mesQuaHora").hide();
			var quaIni = document.getElementById("quaInicio");
			var quaIniVal = document.getElementById("quaInicio").value;
			if(quaIniVal == "" || quaIniVal.length < 5){
				quaIni.style.borderColor = "#E62117";
				flag = false;
			}else{
				quaIni.style.borderColor = "#ccc";
			}
			var quaFim = document.getElementById("quaFim");
			var quaFimVal = document.getElementById("quaFim").value;
			if(quaFimVal == "" || quaFimVal.length < 5){
				quaFim.style.borderColor = "#E62117";
				flag = false;
			}else{
				quaFim.style.borderColor = "#ccc";
			}


			if(quaIniVal.length == 5 && quaFimVal.length == 5 ){
				var horaIni = parseInt(quaIniVal.substring(0,2));
				var horaFim = parseInt(quaFimVal.substring(0,2));
				if(horaFim < horaIni){
					quaIni.style.borderColor = "#E62117";
					quaFim.style.borderColor = "#E62117";
					document.getElementById('mesQuaHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
					$("#mesQuaHora").show();
					flag = false;
				}else if(horaIni == horaFim){
					var minIni = parseInt(quaIniVal.substring(3,5));
					var minFim = parseInt(quaFimVal.substring(3,5));
					if(minFim < minIni){
						quaIni.style.borderColor = "#E62117";
						quaFim.style.borderColor = "#E62117";
						document.getElementById('mesQuaHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesQuaHora").show();
						flag = false;
					}else if(minIni == minFim){
						quaIni.style.borderColor = "#E62117";
						quaFim.style.borderColor = "#E62117";
						document.getElementById('mesQuaHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesQuaHora").show();
						flag = false;
					}
				}
			}
		}
		var quinta = document.getElementById('quinta').checked;
		if(quinta){
			document.getElementById("quiInicio").style.borderColor = "#ccc";
			document.getElementById("quiFim").style.borderColor = "#ccc";
			$("#mesQuiHora").hide();
			var quiIni = document.getElementById("quiInicio");
			var quiIniVal = document.getElementById("quiInicio").value;
			if(quiIniVal == "" || quiIniVal.length < 5){
				quiIni.style.borderColor = "#E62117";
				flag = false;
			}else{
				quiIni.style.borderColor = "#ccc";
			}
			var quiFim = document.getElementById("quiFim");
			var quiFimVal = document.getElementById("quiFim").value;
			if(quiFimVal == "" || quiFimVal.length < 5){
				quiFim.style.borderColor = "#E62117";
				flag = false;
			}else{
				quiFim.style.borderColor = "#ccc";
			}

			if(quiIniVal.length == 5 && quiFimVal.length == 5 ){
				var horaIni = parseInt(quiIniVal.substring(0,2));
				var horaFim = parseInt(quiFimVal.substring(0,2));
				if(horaFim < horaIni){
					quiIni.style.borderColor = "#E62117";
					quiFim.style.borderColor = "#E62117";
					document.getElementById('mesQuiHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
					$("#mesQuiHora").show();
					flag = false;
				}else if(horaIni == horaFim){
					var minIni = parseInt(quiIniVal.substring(3,5));
					var minFim = parseInt(quiFimVal.substring(3,5));
					if(minFim < minIni){
						quiIni.style.borderColor = "#E62117";
						quiFim.style.borderColor = "#E62117";
						document.getElementById('mesQuiHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesQuiHora").show();
						flag = false;
					}else if(minIni == minFim){
						quiIni.style.borderColor = "#E62117";
						quiFim.style.borderColor = "#E62117";
						document.getElementById('mesQuiHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesQuiHora").show();
						flag = false;
					}
				}
			}
		}
		var sexta = document.getElementById('sexta').checked;
		if(sexta){
			document.getElementById("sexInicio").style.borderColor = "#ccc";
			document.getElementById("sexFim").style.borderColor = "#ccc";
			$("#mesSexHora").hide();
			var sexIni = document.getElementById("sexInicio");
			var sexIniVal = document.getElementById("sexInicio").value;
			if(sexIniVal == "" || sexIniVal.length < 5){
				sexIni.style.borderColor = "#E62117";
				flag = false;
			}else{
				sexIni.style.borderColor = "#ccc";
			}
			var sexFim = document.getElementById("sexFim");
			var sexFimVal = document.getElementById("sexFim").value;
			if(sexFimVal == "" || sexFimVal.length < 5){
				sexFim.style.borderColor = "#E62117";
				flag = false;
			}else{
				sexFim.style.borderColor = "#ccc";
			}

			if(sexIniVal.length == 5 && sexFimVal.length == 5 ){
				var horaIni = parseInt(sexIniVal.substring(0,2));
				var horaFim = parseInt(sexFimVal.substring(0,2));
				if(horaFim < horaIni){
					sexIni.style.borderColor = "#E62117";
					sexFim.style.borderColor = "#E62117";
					document.getElementById('mesSexHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
					$("#mesSexHora").show();
					flag = false;
				}else if(horaIni == horaFim){
					var minIni = parseInt(sexIniVal.substring(3,5));
					var minFim = parseInt(sexFimVal.substring(3,5));
					if(minFim < minIni){
						sexIni.style.borderColor = "#E62117";
						sexFim.style.borderColor = "#E62117";
						document.getElementById('mesSexHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesSexHora").show();
						flag = false;
					}else if(minIni == minFim){
						sexIni.style.borderColor = "#E62117";
						sexFim.style.borderColor = "#E62117";
						document.getElementById('mesSexHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesSexHora").show();
						flag = false;
					}
				}
			}
		}
		var sabado = document.getElementById('sabado').checked;
		if(sabado){
			document.getElementById("sabInicio").style.borderColor = "#ccc";
			document.getElementById("sabFim").style.borderColor = "#ccc";
			$("#mesSabHora").hide();
			var sabIni = document.getElementById("sabInicio");
			var sabIniVal = document.getElementById("sabInicio").value;
			if(sabIniVal == "" || sabIniVal.length < 5){
				sabIni.style.borderColor = "#E62117";
				flag = false;
			}else{
				sabIni.style.borderColor = "#ccc";
			}
			var sabFim = document.getElementById("sabFim");
			var sabFimVal = document.getElementById("sabFim").value;
			if(sabFimVal == "" || sabFimVal.length < 5){
				sabFim.style.borderColor = "#E62117";
				flag = false;
			}else{
				sabFim.style.borderColor = "#ccc";
			}

			if(sabIniVal.length == 5 && sabFimVal.length == 5 ){
				var horaIni = parseInt(sabIniVal.substring(0,2));
				var horaFim = parseInt(sabFimVal.substring(0,2));
				if(horaFim < horaIni){
					sabIni.style.borderColor = "#E62117";
					sabFim.style.borderColor = "#E62117";
					document.getElementById('mesSabHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
					$("#mesSabHora").show();
					flag = false;
				}else if(horaIni == horaFim){
					var minIni = parseInt(sabIniVal.substring(3,5));
					var minFim = parseInt(sabFimVal.substring(3,5));
					if(minFim < minIni){
						sabIni.style.borderColor = "#E62117";
						sabFim.style.borderColor = "#E62117";
						document.getElementById('mesSabHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesSabHora").show();
						flag = false;
					}else if(minIni == minFim){
						sabIni.style.borderColor = "#E62117";
						sabFim.style.borderColor = "#E62117";
						document.getElementById('mesSabHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesSabHora").show();
						flag = false;
					}
				}
			}
		}
		var domingo = document.getElementById('domingo').checked;
		if(domingo){
			document.getElementById("domInicio").style.borderColor = "#ccc";
			document.getElementById("domFim").style.borderColor = "#ccc";
			$("#mesDomHora").hide();
			var domIni = document.getElementById("domInicio");
			var domIniVal = document.getElementById("domInicio").value;
			if(domIniVal == "" || domIniVal.length < 5){
				domIni.style.borderColor = "#E62117";
				flag = false;
			}else{
				domIni.style.borderColor = "#ccc";
			}
			var domFim = document.getElementById("domFim");
			var domFimVal = document.getElementById("domFim").value;
			if(domFimVal == "" || domFimVal.length < 5){
				domFim.style.borderColor = "#E62117";
				flag = false;
			}else{
				domFim.style.borderColor = "#ccc";

			}

			if(domIniVal.length == 5 && domFimVal.length == 5 ){
				var horaIni = parseInt(domIniVal.substring(0,2));
				var horaFim = parseInt(domFimVal.substring(0,2));
				if(horaFim < horaIni){
					domIni.style.borderColor = "#E62117";
					domFim.style.borderColor = "#E62117";
					document.getElementById('mesDomHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
					$("#mesDomHora").show();
					flag = false;
				}else if(horaIni == horaFim){
					domIni.style.borderColor = "#E62117";
					domFim.style.borderColor = "#E62117";
					var minIni = parseInt(domIniVal.substring(3,5));
					var minFim = parseInt(domFimVal.substring(3,5));
					if(minFim < minIni){
						document.getElementById('mesDomHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesDomHora").show();
						flag = false;
					}else if(minIni == minFim){
						domIni.style.borderColor = "#E62117";
						domFim.style.borderColor = "#E62117";
						document.getElementById('mesDomHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
						$("#mesDomHora").show();
						flag = false;
					}
				}
			}
		}

		if(flag){
			$("#messDisponibilidade").hide();
			return true;
		}else{
			$("#messDisponibilidade").show();
			return false;
		}
	});
</script>

<!-- Status -->
<script type="text/javascript">
	$( function() {
		$(".editarStatus").click(function (e){
			var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
			$("#cpfStatus").val(cpf);
			$.getJSON('model/prestadorstatus.ajax.php?search=',{cod_estados:cpf,ajax:'true'}, function(est){
				var options;
				for (var i = 0; i < est.length; i++) {
					options += '<option value="' + est[i].idStatus + '">' + est[i].status + '</option>';
				}
				$('#prestadorStatus').html(options).show();
			});
		});
	});
</script>

<!-- Conta Script -->
<script type="text/javascript">
	cpf = "";
	//Listar Conta
	function listaConta(){
		var table = document.getElementById("TabelaConta").rows.length;
			var i = 0;
			for(i=table-1; i > 0; i--){
			    document.getElementById("TabelaConta").deleteRow(i);
			}
			$.getJSON('model/contaList.ajax.php?search=',{V_cpf:cpf,ajax:'true'}, function(est){
				for (var i = 0; i < est.length; i++) {
					var table = document.getElementById("TabelaConta");
					// Captura a quantidade de linhas já existentes na tabela
					var numOfRows = table.rows.length;
					// Captura a quantidade de colunas da última linha da tabela
					var numOfCols = table.rows[numOfRows-1].cells.length;

					// Insere uma linha no fim da tabela.
					var newRow = table.insertRow(numOfRows);

					// Faz um loop para criar as colunas
					for (var j = 0; j < numOfCols; j++) {
						switch(j){
							case 0:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = est[i].id;
								break;
							case 1:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = est[i].tpconta;
								break;
							case 2:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = est[i].codBanco;
								break;
							case 3:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = est[i].agencia;
								break;
							case 4:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = est[i].verificador;
								break;
							case 5:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = est[i].conta;
								break;
							case 6:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								var op = "Padrao";
								if(est[i].padrao == "f")
									op = "-";
								newCell.innerHTML = op;
								break;
							case 7:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								// Insere um conteúdo na coluna
								newCell.innerHTML = "<a class='btn btn-default btn-md editarContaLista' "
								+"data-toggle='modal' data-target='#modalUpdateContas'  data-original-title>"
								+"Editar</a>";
								break;
							case 8:
								// Insere uma coluna na nova linha
								newCell = newRow.insertCell(j);
								var op = "disabled";
								if(est[i].padrao == "f")
									op = "-";
								// Insere um conteúdo na coluna
								newCell.innerHTML = "<a class='btn btn-default btn-md "
								+"excluirContaLista'"+op+">Excluir</a>";
								break;
							default:
							alert("deu error -> table");
								break;
						}
					}
				}
			});
	}
	$(".editarContas").click(function (e){
		cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		listaConta();
	});
	//Povoar Inserir Conta
	$(document).on("click",  ".inserirContaLista" ,function() {
		$("#cpfContaIns").val(cpf);
		document.getElementById("tpcontaIns").selectedIndex = 0;
		document.getElementById("tpcontaIns").style.borderColor = "#ccc";
		document.getElementById("bancoIns").selectedIndex = 0;
		document.getElementById("bancoIns").style.borderColor = "#ccc";
		document.getElementById("agenciaIns").value = "";;
		document.getElementById("agenciaIns").style.borderColor = "#ccc";
		document.getElementById("contaIns").value = "";
		document.getElementById("contaIns").style.borderColor = "#ccc";
		document.getElementById("verificadorIns").value = "";
		document.getElementById("verificadorIns").style.borderColor = "#ccc"
		$("#padraoIns").prop("checked", false);
		$("#confirmCamposConta").hide();
	});
	//Inserir conta
	$(document).on("click",  "#inserirConta" ,function() {
		var bad = "#E62117";
		var flag = true;

		var tpconta = document.getElementById("tpcontaIns");
		var codtpconta = tpconta.options[tpconta.selectedIndex].value;
		if(codtpconta == ""){
			tpconta.style.borderColor = bad;
			flag = false;
		}else{
			tpconta.style.borderColor = "#ccc";
		}

		var banco = document.getElementById("bancoIns");
		var codBanco = banco.options[banco.selectedIndex].value;
		if(codBanco == ""){
			banco.style.borderColor = bad;
			flag = false;
		}else{
			banco.style.borderColor = "#ccc";
		}

		var agencia = document.getElementById("agenciaIns");
		var valAgencia = document.getElementById("agenciaIns").value;
		if(valAgencia == ""){
			agencia.style.borderColor = bad;
			flag = false;
		}else{
			agencia.style.borderColor = "#ccc";
		}

		var verificador = document.getElementById("verificadorIns");
		var valVerificador = document.getElementById("verificadorIns").value;
		if(valVerificador == ""){
			verificador.style.borderColor = bad;
			flag = false;
		}else{
			verificador.style.borderColor = "#ccc";
		}

		var conta = document.getElementById("contaIns");
		var valConta = document.getElementById("contaIns").value;
		if(valConta == ""){
			conta.style.borderColor = bad;
			flag = false;
		}else{
			conta.style.borderColor = "#ccc";
		}
		var padrao = document.getElementById("padraoIns").checked;
		var cpfConta = document.getElementById('cpfContaIns').value;

		if(flag){
			$('#confirmCamposConta').hide();
			$.getJSON('model/InserirContaFunction.ajax.php?search=',{'cpf': cpfConta,'tpconta': codtpconta,'banco': codBanco,
			   'agencia': valAgencia ,'verificador': valVerificador ,'conta': valConta,'padrao': padrao ,ajax: 'true'}, function(dado){

				if(dado.success){
					$('#modalInsirirContas').modal('hide');
					listaConta();
					$("#confirmCamposConta").hide();
					$("#MessExcluirContaTrue").show();
					document.getElementById('msgTrue').innerHTML = dado.msg;
					setTimeout(function(){$("#MessExcluirContaTrue").hide();}, 5000);

				}else{
					$('#modalInsirirContas').modal('hide');
					$("#confirmCamposConta").hide();
					$("#MessExcluirContaFalse").show();
					document.getElementById('msgFalse').innerHTML = dado.msg;
					setTimeout(function(){$("#MessExcluirContaFalse").hide();}, 5000);
				}
			});
		}else{
			$('#confirmCamposConta').show();
		}
	});
	//Povoar editar Conta
	$(document).on("click",  ".editarContaLista" ,function() {
		document.getElementById("tpconta").style.borderColor = "#ccc";
		document.getElementById("banco").style.borderColor = "#ccc";
		document.getElementById("agencia").value = "";;
		document.getElementById("agencia").style.borderColor = "#ccc";
		document.getElementById("conta").value = "";
		document.getElementById("conta").style.borderColor = "#ccc";
		document.getElementById("verificador").value = "";
		document.getElementById("verificador").style.borderColor = "#ccc"
		$("#padrao").prop("checked", false);
		$("#confirmCamposContaEditar").hide();
		var id = $(this).closest('tr').find('td').eq(0).text();
		var  tpconta= $(this).closest('tr').find('td').eq(1).text();
		var banco = $(this).closest('tr').find('td').eq(2).text();
		var agencia = $(this).closest('tr').find('td').eq(3).text();
		var verificador = $(this).closest('tr').find('td').eq(4).text();
		var numero = $(this).closest('tr').find('td').eq(5).text();
		var padrao = $(this).closest('tr').find('td').eq(6).text();
		$("#idConta").val(id);
		$("#cpfConta").val(cpf);
		$("#agencia").val(agencia);
		$("#conta").val(numero);
		$("#verificador").val(verificador);
		if(padrao == "Padrao"){
			$("#padrao").prop("checked", true);
			$("#padrao").prop("disabled", true);
		}else{
			$("#padrao").prop("checked", false);
			$("#padrao").prop("disabled", false);
		}

		$.getJSON('model/tpcontaupdate.ajax.php?search=',{V_tpconta:tpconta, ajax:'true'}, function(est){
			var options = "";
			for (var i = 0; i < est.length; i++) {
				options += '<option value="' + est[i].idTpConta + '">' + est[i].nome + '</option>';
			}
			$('#tpconta').html(options).show();
		});

		$.getJSON('model/bancoupdate.ajax.php?search=',{V_banco:banco,ajax:'true'}, function(est){
			var options = "";
			for (var i = 0; i < est.length; i++) {
				options += '<option value="' + est[i].idBanco + '">' +est[i].idBanco+" | "+est[i].nome + '</option>';
			}
			$('#banco').html(options).show();
		});
	});
	//Editar Conta
	$(document).on("click",  "#editarConta" ,function() {
		var bad = "#E62117";
		var flag = true;

		var tpconta = document.getElementById("tpconta");
		var codtpconta = tpconta.options[tpconta.selectedIndex].value;
		if(codtpconta == ""){
			tpconta.style.borderColor = bad;
			flag = false;
		}else{
			tpconta.style.borderColor = "#ccc";
		}

		var banco = document.getElementById("banco");
		var codBanco = banco.options[banco.selectedIndex].value;
		if(codBanco == ""){
			banco.style.borderColor = bad;
			flag = false;
		}else{
			banco.style.borderColor = "#ccc";
		}

		var agencia = document.getElementById("agencia");
		var valAgencia = document.getElementById("agencia").value;
		if(valAgencia == ""){
			agencia.style.borderColor = bad;
			flag = false;
		}else{
			agencia.style.borderColor = "#ccc";
		}

		var verificador = document.getElementById("verificador");
		var valVerificador = document.getElementById("verificador").value;
		if(valVerificador == ""){
			verificador.style.borderColor = bad;
			flag = false;
		}else{
			verificador.style.borderColor = "#ccc";
		}

		var conta = document.getElementById("conta");
		var valConta = document.getElementById("conta").value;
		if(valConta == ""){
			conta.style.borderColor = bad;
			flag = false;
		}else{
			conta.style.borderColor = "#ccc";
		}

		var padrao = document.getElementById("padrao").checked;
		if(padrao == true){
			padrao = "true";
		}else{
			padrao = "false";
		}
		alert(padrao);
		var id = document.getElementById('idConta').value;
		var cpf = document.getElementById('cpfConta').value;
		if(flag){
			$('#confirmCamposContaEditar').hide();
			$.getJSON('model/editarContaFunction.ajax.php?search=',{'id': id,'cpf':cpf,'tpconta': codtpconta,'banco': codBanco,
			   'agencia': valAgencia ,'verificador': valVerificador ,'conta': valConta, 'padrao': padrao ,ajax: 'true'}, function(dado){
				alert(dado.exec);
				if(dado.success){
					$('#modalUpdateContas').modal('hide');
					listaConta();
					$("#confirmCamposContaEditar").hide();
					$("#MessExcluirContaTrue").show();
					document.getElementById('msgTrue').innerHTML = dado.msg;
					setTimeout(function(){$("#MessExcluirContaTrue").hide();}, 5000);

				}else{
					$('#modalUpdateContas').modal('hide');
					$("#confirmCamposContaEditar").hide();
					$("#MessExcluirContaFalse").show();
					document.getElementById('msgFalse').innerHTML = dado.msg;
					setTimeout(function(){$("#MessExcluirContaFalse").hide();}, 5000);
				}
			});
		}else{
			$('#confirmCamposContaEditar').show();
		}
	});
	//Excluir Conta
	$(document).on("click",  ".excluirContaLista" ,function() {
		var  id = $(this).closest('tr').find('td').eq(0).text();
		$.getJSON('model/excluirContaLista.ajax.php?search=',{V_id:id ,ajax:'true'}, function(est){
			if(est[0].sucess){
				document.getElementById('msgTrue').innerHTML = est[0].msg;
				$("#MessExcluirContaTrue").show();
				setTimeout(function(){$("#MessExcluirContaTrue").hide();}, 5000);
				listaConta();
			}else{
				document.getElementById('msgTrue').innerHTML = est[0].msg;
				$("#MessExcluirContaFalse").show();
				setTimeout(function(){$("#MessExcluirContaFalse").hide();}, 5000);
			}
		});
	});
</script>

<!-- Criar Tabela Documento -->
<script type="text/javascript">
	cpf = "";
	function listaDocumento(){
		var table = document.getElementById("TabelaDocumento").rows.length;
		var i = 0;
		for(i=table-1; i > 0; i--){
		    document.getElementById("TabelaDocumento").deleteRow(i);
		}

		$.getJSON('model/docList.ajax.php?search=',{V_cpf:cpf,ajax:'true'}, function(est){
			for (var i = 0; i < est.length; i++) {
				// Captura a referência da tabela com id “minhaTabela”
				var table = document.getElementById("TabelaDocumento");
				// Captura a quantidade de linhas já existentes na tabela
				var numOfRows = table.rows.length;
				// Captura a quantidade de colunas da última linha da tabela
				var numOfCols = table.rows[numOfRows-1].cells.length;

				// Insere uma linha no fim da tabela.
				var newRowDoc = table.insertRow(numOfRows);

				// Faz um loop para criar as colunas
				for (var k = 0; k < numOfCols; k++) {
					switch(k){
						case 0:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].tpdoc;
							break;
						case 1:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].numero;
							break;
						case 2:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].orgao;
							break;
						case 3:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].estado;
							break;
						case 4:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
								newCellDoc.innerHTML = "<a class='color-link' data-toggle='modal' "
								+"data-target='#modalFrenteImage' data-original-title id='clickFrente'>"
								+est[i].frenteName+"</a>";
							break;
						case 5:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = "<a class='color-link' data-toggle='modal' "
								+"data-target='#modalTrasImage' data-original-title id='clickTras'>"
								+est[i].trasName+"</a>";
							break;
						case 6:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = "<a class='btn btn-default btn-md editarDocumentoLista' "
							+"data-toggle='modal' data-target='#modalUpdateDocumentos' data-original-title>"
							+"Editar</a>";
							break;
						case 7:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = "<a class='btn btn-default btn-md excluirDocumentoLista'>"
							+"Excluir</a>";
							break;
						default:
							alert("deu error -> table");
							break;
					}
				}
			}
		});
	}
	function mostrarComponente(){
		document.getElementById("tpdoc").style.borderColor = "#ccc";
		document.getElementById("orgao").value = "";
		document.getElementById("orgao").style.borderColor = "#ccc";
		document.getElementById("numeroDoc").value = "";
		document.getElementById("numeroDoc").style.borderColor = "#ccc";
		document.getElementById("estadoDoc").selectedIndex = 0;
		document.getElementById("estadoDoc").style.borderColor = "#ccc";
		$("#confirmTpdoc").hide();
		$("#confirmNdoc").hide();
		$("#confirmOrgao").hide();
		$("#confirmEstadoDoc").hide();
		$("#confirmTras").hide();
		$("#confirmFrente").hide();
		var tpdoc = document.getElementById("tpdoc");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;

		if(codtpdoc == "1"){
			$('#tipoDocs').removeClass('carregando');
		}else{
			$('#tipoDocs').addClass('carregando');
		}

		$.getJSON('model/mostrartipodoc.ajax.php?search=',{V_tpdoc: codtpdoc, ajax:'true'}, function(est){
			if(est[0].obrigatorio == "t"){
				$('#numDoc').removeClass('carregando');
			}else{
				$('#numDoc').addClass('carregando');
			}
		});
	}
	function mostrarComponenteIns(){
		document.getElementById("tpdocIns").style.borderColor = "#ccc";
		document.getElementById("orgaoIns").value = "";
		document.getElementById("orgaoIns").style.borderColor = "#ccc";
		document.getElementById("numeroDocIns").value = "";
		document.getElementById("numeroDocIns").style.borderColor = "#ccc";
		document.getElementById("estadoDocIns").selectedIndex = 0;
		document.getElementById("estadoDocIns").style.borderColor = "#ccc";
		$("#confirmTpdocIns").hide();
		$("#confirmNdocIns").hide();
		$("#confirmOrgaoIns").hide();
		$("#confirmEstadoDocIns").hide();
		$("#confirmTrasIns").hide();
		$("#confirmFrenteIns").hide();

		var tpdoc = document.getElementById("tpdocIns");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;

		if(codtpdoc == "1"){
			$('#tipoDocsIns').removeClass('carregando');
		}else{
			$('#tipoDocsIns').addClass('carregando');
		}

		$.getJSON('model/mostrartipodoc.ajax.php?search=',{V_tpdoc: codtpdoc, ajax:'true'}, function(est){
			if(est[0].obrigatorio == "t"){
				$('#numDocIns').removeClass('carregando');
			}else{
				$('#numDocIns').addClass('carregando');
			}
		});
	}
	flagOb = true;
	function retornarObrigatorio(){
		var tpdoc = document.getElementById("tpdocIns");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;

		$.getJSON('model/mostrartipodoc.ajax.php?search=',{V_tpdoc: codtpdoc, ajax:'true'}, function(est){
			if(est[0].obrigatorio == "t"){
				flagOb =  true;
			}else{
				flagOb = false;
			}
		});

		return flagOb;
	}
	$(document).on("click",  ".editarDocs" ,function() {
		cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		listaDocumento();
	});
	//Download Imagem Frente
	$(document).on("click", "#clickFrente" ,function() {
		var img = $(this).closest('tr').find('td').eq(4).text();
		$("#frenteImgClick").attr("src","../img/doc/"+img);
		$("#downloadFrente").attr("href","../img/doc/"+img);
	});
	//Download Imagem Tras
	$(document).on("click", "#clickTras" ,function() {
		var img = $(this).closest('tr').find('td').eq(5).text();
		$("#trasImgClick").attr("src","../img/doc/"+img);
		$("#downloadTras").attr("href","../img/doc/"+img);
	});
	//Povoar Inserir Documento
	$(document).on("click",  ".inserirDocumentoLista" ,function() {
		document.getElementById('docsErrorIns').innerHTML = '';
		document.getElementById("tpdocIns").selectedIndex = 0;
		document.getElementById("tpdocIns").style.borderColor = "#ccc";
		document.getElementById("orgaoIns").value = "";
		document.getElementById("orgaoIns").style.borderColor = "#ccc";
		document.getElementById("numeroDocIns").value = "";
		document.getElementById("numeroDocIns").style.borderColor = "#ccc";
		document.getElementById("estadoDocIns").selectedIndex = 0;
		document.getElementById("estadoDocIns").style.borderColor = "#ccc";
		document.getElementById('frenteIns').value = "";
		document.getElementById('trasIns').value = "";
		$("#confirmTpdocIns").hide();
		$("#confirmNdocIns").hide();
		$("#confirmOrgaoIns").hide();
		$("#confirmEstadoDocIns").hide();
		$("#confirmTrasIns").hide();
		$("#confirmFrenteIns").hide();

		$('#tipoDocsIns').addClass('carregando');
		$('#numDocIns').addClass('carregando');
	    	$("#cpfDocIns").val(cpf);
	});
	//inserir Documento
	$(document).on("click",  "#inserirDocumento" ,function() {
		var bad = "#E62117";
		var flag = true;

		var tpdoc = document.getElementById("tpdocIns");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;
		var valtpdoc = tpdoc.options[tpdoc.selectedIndex].text;
		var orgao = document.getElementById("orgaoIns");
		var valOrgao = document.getElementById("orgaoIns").value;
		var numeroDoc = document.getElementById("numeroDocIns");
		var valNumeroDoc = document.getElementById("numeroDocIns").value;
		var estadoDoc = document.getElementById("estadoDocIns");
		var codEstadoDoc = estadoDoc.options[estadoDoc.selectedIndex].value;
		var valEstadoDoc = estadoDoc.options[estadoDoc.selectedIndex].text;

		if(codtpdoc == ""){
			$("#confirmTpdocIns").show();
			tpdoc.style.borderColor = bad;
			tpdoc.focus();
			flag = false;
		}else{
			$("#confirmTpdocIns").hide();
			tpdoc.style.borderColor = "#ccc";
		}

		if(codtpdoc == "1"){
			if(codEstadoDoc == ""){
				$("#confirmEstadoDocIns").show();
				estadoDoc.style.borderColor = bad;
				estadoDoc.focus();
				flag = false;
			}else{
				$("#confirmEstadoDocIns").hide();
				estadoDoc.style.borderColor = "#ccc";
			}

			if(valOrgao == ""){
				$("#confirmOrgaoIns").show();
				orgao.style.borderColor = bad;
				orgao.focus();
				flag = false;
			}else{
				$("#confirmOrgaoIns").hide();
				orgao.style.borderColor = "#ccc";
			}

			if(valNumeroDoc == ""){
				$("#confirmNdocIns").show();
				numeroDoc.style.borderColor = bad;
				numeroDoc.focus();
				flag = false;
			}else{
				$("#confirmNdocIns").hide();
				numeroDoc.style.borderColor = "#ccc";
			}
		}else {
			codEstadoDoc = "null";
			valEstadoDoc = "";
			valOrgao = "";
			var ob = retornarObrigatorio();
			if(ob){
				if(valNumeroDoc == ""){
					$("#confirmNdocIns").show();
					numeroDoc.style.borderColor = bad;
					numeroDoc.focus();
					flag = false;
				}else{
					$("#confirmNdocIns").hide();
					numeroDoc.style.borderColor = "#ccc";
				}
			}
		}
		var file_frente;
		var frente = document.getElementById("frenteIns");
		var valFrente = document.getElementById("frenteIns").value;
		if(valFrente == ""){
			$("#confirmFrenteIns").show();
			flag = false;
		}else{
			$("#confirmFrenteIns").hide();
			file_frente = frente.files[0];
		}
		var file_tras;
		var tras = document.getElementById("trasIns");
		var valTras = document.getElementById("trasIns").value;
		if(valTras == ""){
			$("#confirmTrasIns").show();
			flag = false;
		}else{
			$("#confirmTrasIns").hide();
			file_tras = tras.files[0];
		}

		var cpf = document.getElementById('cpfDocIns').value

		var formData = new FormData();
		formData.append('cpf',cpf);
		formData.append('dataTras', file_tras);
		formData.append('dataFrente', file_frente);
		formData.append('codtpdoc',codtpdoc);
		formData.append('estadoDoc',codEstadoDoc);
		formData.append('orgao',valOrgao);
		formData.append('numeroDoc',valNumeroDoc);

		if(flag){

			$( "#inserirDocumento" ).prop( "disabled", true );
			document.getElementById('docsErrorIns').innerHTML = "Processando Dados...";

			$.ajax({
			        url: 'model/inserirDocumento.ajax.php',
			        type: 'POST',
			        data: formData,
			        cache: false,
			        dataType: 'json',
			        processData: false, // Don't process the files
			        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			        success: function(dado){
					if(dado.success){
						listaDocumento();
						$( "#inserirDocumento" ).prop( "disabled", false);
						$('#modalInsirirDocumentos').modal('hide');
						document.getElementById('docsErrorIns').innerHTML = '';
						document.getElementById('msgDocTrue').innerHTML = dado.msg;
						$("#MessExcluirDocTrue").show();
						setTimeout(function(){$("#MessExcluirDocTrue").hide();}, 5000);

					}else{
						$( "#inserirDocumento" ).prop("disabled",false);
						$('#modalInsirirDocumentos').modal('hide');
						document.getElementById('msgDocFalse').innerHTML = dado.msg;
						$("#MessExcluirDocFalse").show();
						setTimeout(function(){$("#MessExcluirDocFalse").hide();}, 5000);
					}
				},
				error: function(req, textStatus, errorThrown) {
					        //this is going to happen when you send something different from a 200 OK HTTP
					        console.log('Ooops, something happened: ' + textStatus + ' ' +errorThrown);
					}

			});
		}
	});
	//Povoar Editar Documento
	$(document).on("click",  ".editarDocumentoLista" ,function() {
		document.getElementById("tpdoc").style.borderColor = "#ccc";
		document.getElementById("orgao").value = "";
		document.getElementById("orgao").style.borderColor = "#ccc";
		document.getElementById("numeroDoc").value = "";
		document.getElementById("numeroDoc").style.borderColor = "#ccc";
		document.getElementById("estadoDoc").selectedIndex = 0;
		document.getElementById("estadoDoc").style.borderColor = "#ccc";
		document.getElementById('frente').value = "";
		document.getElementById('tras').value = "";
		$("#confirmTpdoc").hide();
		$("#confirmNdoc").hide();
		$("#confirmOrgao").hide();
		$("#confirmEstadoDoc").hide();
		$("#confirmTras").hide();
		$("#confirmFrente").hide();
		$('#tipoDocs').addClass('carregando');
		$('#numDoc').addClass('carregando');

		var  tpdoc= $(this).closest('tr').find('td').eq(0).text();
		var numeroDoc = $(this).closest('tr').find('td').eq(1).text();
		var orgao = $(this).closest('tr').find('td').eq(2).text();
		var estado = $(this).closest('tr').find('td').eq(3).text();
		$.getJSON('model/tpdocupdate.ajax.php?search=',{V_tpdoc:tpdoc, ajax:'true'}, function(est){
			var options = '';
			for (var i = 0; i < est.length; i++) {
				options += '<option value="' + est[i].idTpDoc + '">' + est[i].nome + '</option>';
			}
			$('#tpdoc').html(options).show();
		});

		$.getJSON('model/estadodocupdate.ajax.php?search=',{V_estado:estado,ajax:'true'}, function(est){
			var options = "";
			for (var i = 0; i < est.length; i++) {
				options += '<option value="' + est[i].idEstado + '">' + est[i].nome + '</option>';
			}
			$('#estadoDoc').html(options).show();

			$(function(){
				mostrarComponente();
				$("#numeroDoc").val(numeroDoc);
				$("#orgao").val(orgao);
				$("#cpfDoc").val(cpf);
			});
		});
	});
	//Editar Documento
	$(document).on("click",  "#editarDocumento" ,function() {
		var bad = "#E62117";
		var flag = true;

		var tpdoc = document.getElementById("tpdoc");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;
		var valtpdoc = tpdoc.options[tpdoc.selectedIndex].text;
		var orgao = document.getElementById("orgao");
		var valOrgao = document.getElementById("orgao").value;
		var numeroDoc = document.getElementById("numeroDoc");
		var valNumeroDoc = document.getElementById("numeroDoc").value;
		var estadoDoc = document.getElementById("estadoDoc");
		var codEstadoDoc = estadoDoc.options[estadoDoc.selectedIndex].value;
		var valEstadoDoc = estadoDoc.options[estadoDoc.selectedIndex].text;

		if(codtpdoc == ""){
			$("#confirmTpdoc").show();
			tpdoc.style.borderColor = bad;
			tpdoc.focus();
			flag = false;
		}else{
			$("#confirmTpdoc").hide();
			tpdoc.style.borderColor = "#ccc";
		}

		if(codtpdoc == "1"){
			if(codEstadoDoc == ""){
				$("#confirmEstadoDoc").show();
				estadoDoc.style.borderColor = bad;
				estadoDoc.focus();
				flag = false;
			}else{
				$("#confirmEstadoDoc").hide();
				estadoDoc.style.borderColor = "#ccc";
			}

			if(valOrgao == ""){
				$("#confirmOrgao").show();
				orgao.style.borderColor = bad;
				orgao.focus();
				flag = false;
			}else{
				$("#confirmOrgao").hide();
				orgao.style.borderColor = "#ccc";
			}

			if(valNumeroDoc == ""){
				$("#confirmNdoc").show();
				numeroDoc.style.borderColor = bad;
				numeroDoc.focus();
				flag = false;
			}else{
				$("#confirmNdoc").hide();
				numeroDoc.style.borderColor = "#ccc";
			}
		}else {
			codEstadoDoc = "null";
			valEstadoDoc = "";
			valOrgao = "";
			var ob = retornarObrigatorio();
			if(ob){
				if(valNumeroDoc == ""){
					$("#confirmNdoc").show();
					numeroDoc.style.borderColor = bad;
					numeroDoc.focus();
					flag = false;
				}else{
					$("#confirmNdoc").hide();
					numeroDoc.style.borderColor = "#ccc";
				}
			}
		}
		var file_frente;
		var frente = document.getElementById("frente");
		var valFrente = document.getElementById("frente").value;
		if(valFrente == ""){
			file_frente = null;
		}else{
			file_frente = frente.files[0];
		}
		var file_tras;
		var tras = document.getElementById("tras");
		var valTras = document.getElementById("tras").value;
		if(valTras == ""){
			file_tras = null;
		}else{
			file_tras = tras.files[0];
		}
		var cpf = document.getElementById('cpfDoc').value

		var formData = new FormData();
		formData.append('cpf',cpf);
		formData.append('dataTras', file_tras);
		formData.append('dataFrente', file_frente);
		formData.append('codtpdoc',codtpdoc);
		formData.append('estadoDoc',codEstadoDoc);
		formData.append('orgao',valOrgao);
		formData.append('numeroDoc',valNumeroDoc);

		if(flag){
			$( "#editarDocumento" ).prop( "disabled", true );
			document.getElementById('docsError').innerHTML = "Processando Dados...";

			$.ajax({
			        url: 'model/editarDocumento.ajax.php',
			        type: 'POST',
			        data: formData,
			        cache: false,
			        dataType: 'json',
			        processData: false, // Don't process the files
			        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			        success: function(dado){
					if(dado.success){
						listaDocumento();
						$( "#editarDocumento" ).prop( "disabled", false);
						$('#modalUpdateDocumentos').modal('hide');
						document.getElementById('docsError').innerHTML = '';
						document.getElementById('msgDocTrue').innerHTML = dado.msg;
						$("#MessExcluirDocTrue").show();
						setTimeout(function(){$("#MessExcluirDocTrue").hide();}, 5000);

					}else{
						$( "#editarDocumento" ).prop("disabled",false);
						$('#modalUpdateDocumentos').modal('hide');
						document.getElementById('msgDocFalse').innerHTML = dado.msg;
						$("#MessExcluirDocFalse").show();
						setTimeout(function(){$("#MessExcluirDocFalse").hide();}, 5000);
					}

					if(dado.flag == 1){
						document.getElementById('msgDocFotoTrue').innerHTML = dado.msgFoto;
						$("#MessFotoDocTrue").show();
						setTimeout(function(){$("#MessFotoDocTrue").hide();}, 5000);
					}else if(dado.flag == 0){
						document.getElementById('msgDocFotoFalse').innerHTML = dado.msgFoto;
						$("#MessFotoDocFalse").show();
						setTimeout(function(){$("#MessFotoDocFalse").hide();}, 5000);
					}
				},
				error: function(req, textStatus, errorThrown) {
						//this is going to happen when you send something different from a 200 OK HTTP
						console.log('Ooops, something happened: ' + textStatus + ' ' +errorThrown);
						$( "#editarDocumento" ).prop( "disabled", false);
						$('#modalUpdateDocumentos').modal('hide');
						document.getElementById('docsErrorIns').innerHTML = '';
					}

			});
		}
	});
	//Excluir Documento
	$(document).on("click",  ".excluirDocumentoLista" ,function() {
	     var  tpdoc= $(this).closest('tr').find('td').eq(0).text();
	     var imgFrente = $(this).closest('tr').find('td').eq(4).text();
	     var imgTras = $(this).closest('tr').find('td').eq(5).text();
	     $.getJSON('model/excluirDocumentoLista.ajax.php?search=',{V_cpf:cpf ,V_tpdoc:tpdoc,V_imgFrente:imgFrente,V_imgTras:imgTras,
	     	ajax:'true'}, function(est){
	     	if(est[0].sucess){
	     		listaDocumento();
	     		document.getElementById('msgDocTrue').innerHTML = est[0].msg
	     		$("#MessExcluirDocTrue").show();
			setTimeout(function(){$("#MessExcluirDocTrue").hide();}, 5000);
	     	}else{
	     		document.getElementById('msgDocFalse').innerHTML = est[0].msg
	     		$("#MessExcluirDocFalse").show();
			setTimeout(function(){$("#MessExcluirDocFalse").hide();}, 5000);
	     	}
	     });
	});
</script>

</html>