<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<style type="text/css">
	.carregando{
		display:none;
	}
</style>

<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
				Clientes <small>Listagem de Clientes</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-users"></i> Lista
					</li>
				</ol>
				<a href="cliente.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
				<?php
					//EDITAR
					$FormCliente = @$_POST["FormClienteEditar"];
					if(!empty($FormCliente)){
						require_once("controller/controllerCliente.php");
						$class = new controllerCliente;
						echo"<br/><br/>";
						$class->Editar();
					}
					//EXCLUIR
					$FormCliente = @$_POST["FormClienteExcluir"];
					if(!empty($FormCliente)){
						require_once("controller/controllerCliente.php");
						$cod = @$_POST["cpfexcluir"];
						$idStatus = @$_POST["status"];
							$class = new controllerCliente;
							echo"<br/><br/>";
							$class->Excluir($cod,$idStatus);
					}
				?>
				<div id="lista">
				<?php
					require_once("controller/controllerCliente.php");
					//LISTAR
					$class = new controllerCliente;
					$class->Listar();
				?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ModalUpdate Status -->
<div class='modal fade' id='modalStatus' tabindex='-1'>
	<div class='modal-dialog'>
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action="">
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfexcluir" id="cpfStatus" maxlength="14" style="width:50%;" readonly />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Status</label>
				<select class="form-control" name="status" id="prestadorStatus" style="width:50%">
				</select>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='submit' name='FormClienteExcluir' class='btn btn-success' value='Mudar Status'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalUpdate -->
<div class='modal fade' id='contact' tabindex='-1'>
	<div class='modal-dialog'>
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form action='' method='POST' id='Form'>
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class='form-inline'>
				<input class='form-control' id="cpf" placeholder=" Digite o CPF aqui..."  name='cpf' maxlength='14' style='width:50%;' readonly />
				<span id='validcpf'></span>
				</div>
				<br/>
				<label>Nome *</label>
				<input class="form-control" placeholder=" Digite o nome aqui..."  name="nome" id='nome' value="<?php echo @$_POST["nome"]; ?>" />
				<span id="confirmNomeNull" style="display: none; color:#E62117;">  Nome é obrigatório</span>
				<span id="confirmNome" style="display: none; color:#E62117;">  Nome deve conter no mínimo 12 caracteres</span>
				<br/>
				<label>Data de Nascimento *</label>
				<input type="text" class="form-control data" id="dtnasc" name="dtnasc" value="<?php echo @$_POST["dtnasc"]; ?>" />
				<span id="confirmDtnasc" style="display: none; color:#E62117;">  Data inválida</span>
				<span id="confirmDtnascMaior" style="display: none; color:#E62117;">  Obrigatório ser maior de 18 anos</span>
				<br/>
				<label>Email *</label>
				<input type="email" class="form-control" placeholder=" Digite o email aqui..." id='email' name="email" value="<?php echo @$_POST["email"]; ?>" />
				<span id="confirmEmailNull" style="display: none; color:#E62117;">  Email é obrigatório</span>
				<span id="confirmEmail" style="display: none; color:#E62117;">  Formato de email incorreto</span>
				<br/>
				<label>Celular *</label>
				<div class="form-inline">
				<input class="form-control" placeholder=" Digite o celular aqui..." id="cell" name="cell" style="width:50%" onkeyup="lengthCell();" value="<?php echo @$_POST["cell"]; ?>" />
				<span id="confirmCell"></span>
				</div>
				<br/>
				<label>Telefone</label>
				<input class='form-control' placeholder=' Digite o telefone aqui...' id='fone' name='fone' type='text' />
				<br/>
				<label class='control-label'>Sexo *</label>
				<select name='sexo' id="sexo" class='form-control' style='width:50%;'>
				</select>
				<span id="confirmSexo" class="confirmSexo" style="display: none; color:#E62117;">  Sexo é obrigatório</span>
				<br/>
				<label>Cep</label>
				<div class='form-inline'>
				<input class='form-control cep' id='cep' placeholder=' Digite o CEP aqui...'  name='cep' maxlength='9' style='width:50%;'  />
				<span id='validcep'></span>
				</div>
				<br/>
				<label>Rua</label>
				<input class='form-control' id='rua' name='rua' placeholder=' Digite a rua aqui...'  type='text' />
				<br/>
				<label>Numero</label>
				<input class='form-control' id='numero' name='numero' placeholder=' Digite o numero aqui...'  type='text' />
				<br/>
				<label>Bairro</label>
				<input class='form-control' placeholder=' Digite o bairro aqui...' id='bairro' name='bairro'  type='text' />
				<br/>
				<label>Complemento</label>
				<textarea class='form-control' placeholder=' Digite o complento do endereço aqui...'  name='complemento' id="complemento" type='text'></textarea>
				<br/>
				<div class='form-inline'>
				<label>Estado:</label>
				<select class="form-control" id="estado" name="estado" style="width:50%;">
					<option value="">-- Escolha um estado --</option>
				</select>
				</div>
				<br/>
				<div class="form-inline">
				<label>Cidade:</label>
				<span class="carregando">Aguarde, carregando...</span>
				<select class="form-control" name="cidade" id="cod_cidades" style="width:50%;">
					<option value="">-- Escolha uma cidade --</option>
				</select>
				</div>
				<br/>
				<label>linkedin</label>
				<input class='form-control' id="linkedin" name='linkedin' placeholder=' Digite o linkedin aqui...'  type='email' />
				<br/>
				<label>googleplus</label>
				<input class='form-control' id="googleplus" name='googleplus' placeholder=' Digite o googleplus aqui...'  type='email' />
				<br/>
				<label>Facebook</label>
				<input class='form-control' id="facebook" name='facebook' placeholder=' Digite o Facebook aqui...'  type='email' />
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='submit' name='FormClienteEditar' class='btn btn-success' value='Editar'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalListaFilho -->
<div class='modal fade' id='modalListaFilhos' tabindex='-1'>
	<div class='modal-dialog' style="width:90%;">
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Listagem</h4>
			</div>
			<div class='modal-body' style='padding: 5px;'>
			&emsp;
			<a class="btn btn-default btn-lg" id="insertFilho"  style="margin-bottom:5px;" data-toggle='modal' data-target='#modalInsirirFilhos' data-original-title><i class="fa fa-plus-circle" aria-hidden="true"> Inserir Filho</i></a>
			&emsp;&emsp;
			<div id="MessExcluirFilhoTrue" style="display: none;">
				<div id='MessConta' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-ok'></span>
						Filho excluido com sucesso
				</div>
			</div>
			<div id="MessExcluirFilhoFalse" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Desculpe ocorreu um erro, tente novamente mais tarde !
				</div>
			</div>
			<div id="MessInserirFilhoTrue" style="display: none;">
				<div id='MessConta' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-ok'></span>
						Filho Registrado Com sucesso
				</div>
			</div>
			<div id="MessInserirFilhoFalse" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Verifique todos os campos obrigatórios e tente novamente !
				</div>
			</div>
			<div id="MessInserirFilhoError" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Filho já cadastrado !
				</div>
			</div>
				<table class='table table-hover' id='TabelaFilho'>
					<tr>
						<th>ID</th>
						<th>Nome</th>
						<th>Data de nascimento</th>
						<th>Sexo</th>
						<th>Cuidado Especial</th>
						<th>Descriçao</th>
						<th>Excluir</th>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal Insirir Filho -->
<div class='modal fade' id='modalInsirirFilhos' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfFilho" id="cpfFilho" maxlength="14" style="width:50%;" required="true" readonly="true" />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Nome Completo *</label>
				<input class="form-control" id="nomeFilho" placeholder=" Digite o nome aqui..."  name="nomeFilho"/>
				<span id="confirmNomeFilho" class="confirmNomeFilho" style="display: none; color:#E62117;">  Nome deve conter no mínimo 12 caracteres</span>
				<br/>
				<label>Data de Nascimento *</label>
				<input type="text" class="form-control data" id="dtnascFilho" name="dtnascFilho" />
				<span id="confirmDtnascFilho" style="display: none; color:#E62117;">  Data inválida</span>
				<br/>
				<label class="control-label">Sexo *</label>
				<select name="sexoFilho" id="sexoFilho" class="form-control" style="width:50%;">
					<option value="">-- Escolha o sexo -- </option>
					<option value="Masculino">Masculino</option>
					<option value="Feminino">Feminino</option>
				</select>
				<span id="confirmSexoFilho" class="confirmSexoFilho" style="display: none; color:#E62117;">  Sexo é obrigatório</span>
				<br/>
				<input type="checkbox" id="cuidadoEspecialFilho" value="true" name="cuidadoEspecialFilho" /> <strong>Cuidado Especial</strong>
				<br/>
				<div id="EspecialFilho" style="display: none;">
					<label>Descrição *</label>
					<textarea class="form-control" id="descricaoEspecialFilho" placeholder=" Digite a descrição aqui..."  name="descricaoEspecialFilho"></textarea>
					<span id="confirmDescricaoFilho" class="confirmDescricaoFilho" style="display: none; color:#E62117;">  Descrição é obrigatório</span>
				</div>
				<br/>
			<div class='panel-footer' style='margin-bottom:0px;'>
				<button name='FormInserirConta' class='btn btn-success' id='FormFilho' >Inserir</button>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
		</div>
	</div>
</div>

<!-- ModalListaIdoso -->
<div class='modal fade' id='modalListaIdosos' tabindex='-1'>
	<div class='modal-dialog' style="width:90%;">
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Listagem</h4>
			</div>
			<div class='modal-body' style='padding: 5px;'>
			&emsp;
			<a class="btn btn-default btn-lg" id="insertIdoso"  style="margin-bottom:5px;" data-toggle='modal' data-target='#modalInsirirIdosos' data-original-title><i class="fa fa-plus-circle" aria-hidden="true"> Inserir Idoso</i></a>
			&emsp;&emsp;
			<div id="MessExcluirIdosoTrue" style="display: none;">
				<div id='MessConta' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-ok'></span>
						Idoso excluido com sucesso
				</div>
			</div>
			<div id="MessExcluirIdosoFalse" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Desculpe ocorreu um erro, tente novamente mais tarde !
				</div>
			</div>
			<div id="MessInserirIdosoTrue" style="display: none;">
				<div id='MessConta' class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-ok'></span>
						Idoso Registrado Com sucesso
				</div>
			</div>
			<div id="MessInserirIdosoFalse" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Verifique todos os campos obrigatórios e tente novamente !
				</div>
			</div>
			<div id="MessInserirIdosoError" style="display: none;">
				<div id='MessConta' class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Idoso já cadastrado !
				</div>
			</div>
				<table class='table table-hover' id='TabelaIdoso'>
					<tr>
						<th>ID</th>
						<th>Nome</th>
						<th>Data de nascimento</th>
						<th>Sexo</th>
						<th>Cuidado Especial</th>
						<th>Descriçao</th>
						<th>Excluir</th>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal Insirir Idoso -->
<div class='modal fade' id='modalInsirirIdosos' tabindex='-1'>
	<div class='modal-dialog' >
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfIdoso" id="cpfIdoso" maxlength="14" style="width:50%;" required="true"'' readonly="true" />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Nome Completo *</label>
				<input class="form-control" id="nomeIdoso" placeholder=" Digite o nome aqui..."  name="nomeIdoso"/>
				<span id="confirmNomeIdoso" class="confirmNomeIdoso" style="display: none; color:#E62117;">  Nome deve conter no mínimo 12 caracteres</span>
				<br/>
				<label>Data de Nascimento *</label>
				<input type="text" class="form-control data" id="dtnascIdoso" name="dtnascIdoso" />
				<span id="confirmDtnascIdoso" class="confirmDtnascIdoso" style="display: none; color:#E62117;">  Data inválida</span>
				<br/>
				<label class="control-label">Sexo *</label>
				<select name="sexoIdoso" id="sexoIdoso" class="form-control" style="width:50%;">
					<option value="">-- Escolha o sexo -- </option>
					<option value="Masculino">Masculino</option>
					<option value="Feminino">Feminino</option>
				</select>
				<span id="confirmSexoIdoso" class="confirmSexoIdoso" style="display: none; color:#E62117;">  Sexo é obrigatório</span>
				<br/>
				<input type="checkbox" id="cuidadoEspecialIdoso" value="true" name="cuidadoEspecialIdoso" /> <strong>Cuidado Especial</strong>
				<br/>
				<div id="EspecialIdoso" style="display: none;">
					<label>Descrição *</label>
					<textarea class="form-control" id="descricaoEspecialIdoso" placeholder=" Digite a descrição aqui..."  name="descricaoEspecialIdoso"></textarea>
					<span id="confirmDescricaoIdoso" class="confirmDescricaoIdoso" style="display: none; color:#E62117;">  Descrição é obrigatório</span>
				</div>
				<br/>
			<div class='panel-footer' style='margin-bottom:0px;'>
				<button name='FormInserirConta' class='btn btn-success' id='FormIdoso' >Inserir</button>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
		</div>
	</div>
</div>

</body>

<!-- masks -->
<script type="text/javascript">
	$( function() {

		$(".data").mask("00/00/0000",{placeholder: "dd/mm/aaaa"});

		$("#cpf").mask("999.999.999-99", {placeholder: "999.999.999-99"});

		$(".cep").mask('00000-000',{placeholder: '00000-000'});

		$('#fone').mask('(00)0000-0000',{placeholder: '(00)0000-0000'});

		$('#cell').mask("(00)00000-0000",{placeholder: "(00)00000-0000"});
	});
</script>

<!-- Validar CPF -->
<script type="text/javascript">
	//retirar os caracteres do cpf
	function RetiraCaracteresInvalidos(strCPF){
		var strTemp;
		strTemp = strCPF.replace(".", "");
		strTemp = strTemp.replace(".", "");
		strTemp = strTemp.replace("-", "");

		return strTemp;
	}
	//testar o cpf do usuario
	function TestaCPF(strCPF) {
		var Soma;
		var Resto;
		Soma = 0;
		strCPF = RetiraCaracteresInvalidos(strCPF);
		for(i=0; i < 10; i++){
			if (strCPF == ""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i)
				return false;
		}
		for (i=1; i<=9; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10)) )
			return false;
		Soma = 0;
		for (i = 1; i <= 10; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11) ) )
			return false;

		return true;
	}
	$(function() {
		$("#cpf").blur(function(){
			var strCPF = document.getElementById('cpf').value;
			strCPF = RetiraCaracteresInvalidos(strCPF);
			var tamanho = strCPF.length;
			var cpf = document.getElementById('cpf');
			var message = document.getElementById('validcpf');
			var goodColor = "#66cc66";
			var badColor = "#E62117";

			if(tamanho == 11){
				if(TestaCPF(strCPF)){
					cpf.style.borderColor = goodColor;
					message.style.color = goodColor;
					message.innerHTML = "&emsp;CPF válido!";
				}else{
					cpf.style.borderColor = badColor;
					message.style.color = badColor;
					message.innerHTML = "&emsp;CPF inválido!";
				}
			}else{
				cpf.style.borderColor = badColor;
				message.style.color = badColor;
				message.innerHTML = "&emsp;CPF falta caracter!";
			}

		});
	});
</script>

<!--Validar Celular-->
<script type="text/javascript">
	function lengthCell(){
		//Store the password field objects into variables ...
		var cell = document.getElementById('cell');
		//Store the Confimation Message Object ...
		var mens = document.getElementById('confirmCell');
		//Set the colors we will be using ...
		var colorGood = "#66cc66";
		var colorBad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field
		if(cell.value != ""){
			if(cell.value.length < 13){
				cell.style.borderColor = colorBad;
				mens.style.color = colorBad;
				mens.innerHTML = "Celular Incorreto";
				return false;
			}else{
				cell.style.borderColor = colorGood;
				mens.style.color = colorGood;
				mens.innerHTML = "Celular Correto";
				return true;
			}
		}else{
			cell.style.borderColor = colorBad;
			mens.style.color = colorBad;
			mens.innerHTML = "Celular é obrigatório";
			return false;
		}
	}
</script>

<!--Validar Senha-->
<script type="text/javascript">
	function lengthPass(){
		//Store the password field objects into variables ...
		var pass = document.getElementById('pass1');
		//Store the Confimation Message Object ...
		var mess = document.getElementById('confirmPass');
		//Set the colors we will be using ...
		var good = "#66cc66";
		var bad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass.value.length < 6){
			pass.style.borderColor = bad;
			mess.style.color = bad;
			mess.innerHTML = "Senha deve conter no minimo 6 caracteres";
			return false;
		}else{
			pass1.style.borderColor = good;
			mess.style.color = good;
			mess.innerHTML = "Senha ok";
			return true;
		}
	}

	function checkPass()
	{
		//Store the password field objects into variables ...
		var pass1 = document.getElementById('pass1');
		var pass2 = document.getElementById('pass2');
		//Store the Confimation Message Object ...
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#66cc66";
		var badColor = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass1.value == pass2.value){
			//The passwords match.
			//Set the color to the good color and inform
			//the user that they have entered the correct password
			pass2.style.borderColor = goodColor;
			message.style.color = goodColor;
			message.innerHTML = "Senhas Iguais!";
			return true;
		}else{
			//The passwords do not match.
			//Set the color to the bad color and
			//notify the user.
			pass2.style.borderColor = badColor;
			message.style.color = badColor;
			message.innerHTML = "As senhas informadas não são iguais!";
			return false;
		}
	}
</script>

<!-- Validação de cep -->
<script type="text/javascript" >
        $(function() {
        	function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cep").val("");
				$("#rua").val("");
                $("#bairro").val("");
				var opt = '<option value="">-- Escolha uma cidade --</option>';
				$('#cod_cidades').html(opt).show();
				$.getJSON('model/estados.ajax.php?search=',{ajax: 'true'}, function(j){
					var options = '<option value="">-- Escolha um estado --</option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
					}
					$('#estado').html(options).show();
				});

            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function (){
				var ceptxt = document.getElementById("cep");
				var message = document.getElementById('validcep');
				var goodColor = "#66cc66";
				var badColor = "#E62117";

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/-/, "");

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("carregando...");
                        $("#bairro").val("carregando...");
                        $("#cidade").val("carregando...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);

							$('#cod_cidades').hide();
							$('.carregando').show();

							$.getJSON('model/estadoperuf.ajax.php?search=',{cod_estados: dados.uf, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#estado').html(options).show();
								$('.carregando').hide();
							});

							$.getJSON('model/cidadespernome.ajax.php?search=',{cod_estados: dados.localidade, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#cod_cidades').html(options).show();
								$('.carregando').hide();
							});

								ceptxt.style.borderColor = goodColor;
								message.style.color = goodColor;
								message.innerHTML = "&emsp;CEP encontrado!";
								return true;
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
								ceptxt.style.borderColor = badColor;
								message.style.color = badColor;
								message.innerHTML = "&emsp;CEP não encontrado!";
								limpa_formulário_cep();
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
						ceptxt.style.borderColor = badColor;
						message.style.color = badColor;
						message.innerHTML = "&emsp;Formato de CEP inválido!";
						limpa_formulário_cep();
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
</script>

<!-- Validação do email -->
<script language="Javascript">
	function validacaoEmail() {
		var email = document.getElementById('email');
		usuario = email.value.substring(0, email.value.indexOf("@"));
		dominio = email.value.substring(email.value.indexOf("@")+ 1, email.value.length);
		if(email.value != ""){
			$("#confirmEmail").hide();
			if ((usuario.length >=1) &&
			    (dominio.length >=3) &&
			    (usuario.search("@")==-1) &&
			    (dominio.search("@")==-1) &&
			    (usuario.search(" ")==-1) &&
			    (dominio.search(" ")==-1) &&
			    (dominio.search(".")!=-1) &&
			    (dominio.indexOf(".") >=1)&&
			    (dominio.lastIndexOf(".") < dominio.length - 1)) {
				email.style.borderColor = "#ccc";
				$("#confirmEmailNull").hide();
				return true;
			}else{
				email.style.borderColor = "#E62117";
				$("#confirmEmailNull").show();
				return false;
			}
		}else{
			$("#confirmEmailNull").show();
			email.style.borderColor = "#E62117";
			email.focus();
			return false;
		}
	}
</script>

<!-- Validação da data -->
<script type="text/javascript">
	function validateDate() {
		var id = document.getElementById('dtnasc');
		var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
		if ((id.value.match(RegExPattern)) && (id.value!='')) {
			var data = id.value;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);

			//resgatar a data atual
			var dataAtual = new Date();
			var anoAtual = parseInt(dataAtual.getFullYear());
			//Criando um objeto Date usando os valores ano, mes e dia.
    			var novaData = new Date(ano,(mes-1),dia);

    			if( ano < 1900 || ano > anoAtual || (anoAtual - ano) < 18 ){
    				if((anoAtual - ano) < 18){
    					$("#confirmDtnasc").hide();
	    				$("#confirmDtnascMaior").show();
					id.style.borderColor =  "#E62117";
				}else{
					$("#confirmDtnascMaior").hide();
					$("#confirmDtnasc").show();
					id.style.borderColor =  "#E62117";
				}
				return false;
    			}else{
    				id.style.borderColor = "#ccc";
				$("#confirmDtnasc").hide();
				$("#confirmDtnascMaior").hide();
				return true;
    			}
		}else{
			$("#confirmDtnasc").show();
			id.style.borderColor =  "#E62117";
			return false;
		}
	}
</script>


<!-- Editar Cliente e Editar Status -->
<script type="text/javascript">
	//Editar Cliente
	function clearUpdateModalCliente(){
		document.getElementById('nome').style.borderColor = "#ccc";
		$("#confirmNome").hide();
		$("#confirmNomeNull").hide();
		document.getElementById('dtnasc').style.borderColor = "#ccc";
		$("#confirmDtnasc").hide();
		document.getElementById('email').style.borderColor = "#ccc";
		$("#confirmEmail").hide();
		$("#confirmEmailNull").hide();
		document.getElementById('cell').style.borderColor = "#ccc";
		document.getElementById('confirmCell').innerHTML = '';
		document.getElementById('sexo').style.borderColor = "#ccc";
		$("#confirmSexo").hide();
	}
	$(".editarUsuario").click(function (e){
		clearUpdateModalCliente();
		var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		$.getJSON('model/updateCliente.ajax.php?search=',{cod_estados: cpf, ajax: 'true'}, function(j){
			var data = j[0].dtnasc;
			var ano = data.substring(0,4);
			var mes = data.substring(5,7);
			var dia = data.substring(8,10);
			j[0].dtnasc = dia+"/"+mes+"/"+ano;
			$("#cpf").val(j[0].cpf);
			$("#nome").val(j[0].nome);
			$("#dtnasc").val(j[0].dtnasc);
			$("#email").val(j[0].email);
			$("#fone").val(j[0].fone);
			$("#cell").val(j[0].cell);
			$("#cep").val(j[0].cep);
			$("#rua").val(j[0].rua);
			$("#numero").val(j[0].numero);
			$("#bairro").val(j[0].bairro);
			$("#complemento").val(j[0].complemento);
			$("#linkedin").val(j[0].linkedin);
			$("#googleplus").val(j[0].googleplus);
			$("#facebook").val(j[0].facebook);
			//sexo
			var optSexo;
			if(j[0].sexo == "Masculino"){
				optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
				optSexo += '<option value="Feminino">Feminino</option>';
			}else{
				optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
				optSexo += '<option value="Masculino">Masculino</option>';
			}
			$('#sexo').html(optSexo).show();
			//estado
			$.getJSON('model/estadopercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(est){
				var options;
				if(j[0].idCidade == null)
					options = '<option value="">-- Escolha um estado -- </option>;'
				for (var i = 0; i < est.length; i++) {
					options += '<option value="' + est[i].cod_cidades + '">' + est[i].nome + '</option>';
				}
				$('#estado').html(options).show();
			});
			//cidade
			$.getJSON('model/cidadespercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(cid){
				var options;
				if(j[0].idCidade == null)
					options = '<option value="">-- Escolha uma cidade -- </option>;'
				for (var i = 0; i < cid.length; i++) {
					options += '<option value="' + cid[i].cod_cidades + '">' + cid[i].nome + '</option>';
				}
				$('#cod_cidades').html(options).show();
			});
		});
	});
	//Editar Status do Cliente
	$(".editarStatus").click(function (e){
		var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		$("#cpfStatus").val(""+cpf);
		$.getJSON('model/clientestatus.ajax.php?search=',{cod_estados:cpf,ajax:'true'}, function(est){
			var options;
			for (var i = 0; i < est.length; i++) {
				options += '<option value="' + est[i].idStatus + '">' + est[i].status + '</option>';
			}
			$('#prestadorStatus').html(options).show();
		});
	});
	//Submit do Form Cliente (validações de dados)
	$("#Form").submit(function(){
			var nome = document.getElementById('nome');
				if(nome.value != ""){
					$('#confirmNomeNull').hide();
					if(nome.value.length < 11){
						$('#confirmNome').show();
						nome.style.borderColor = "#E62117";
						nome.focus();
						return false;
					}else{
						nome.style.borderColor = "#ccc";
						$('#confirmNome').hide();
					}
				}else{
					nome.style.borderColor = "#E62117";
					nome.focus();
					$('#confirmNomeNull').show();
					return false;
				}
			var cpfFocus = document.getElementById('cpf');
			var valorCPF = document.getElementById('cpf').value;
			var bollCPF = TestaCPF(valorCPF);
				if(!bollCPF){
					cpfFocus.focus();
					return false;
				}
			var dtnasc = document.getElementById('dtnasc');
			var dtnascValid = validateDate();
				if(!dtnascValid){
					dtnasc.focus();
					return false;
				}
			var email = document.getElementById('email');
			var emailValid = validacaoEmail();
				if(!emailValid){
					email.focus();
					return false;
				}
			var cellFocus = document.getElementById('cell');
			var cellLength = lengthCell();
				if(!cellLength){
					cellFocus.style.borderColor = "#E62117";
					cellFocus.focus();
					return false;
				}else{
					cellFocus.style.borderColor = "#ccc";
				}
			var sexoFocus = document.getElementById('sexo');
				if(sexoFocus.options[sexoFocus.selectedIndex].value == ""){
					$('#confirmSexo').show();
					sexoFocus.style.borderColor = "#E62117";
					$('#confirmSexo').color = "#E62117";
					sexoFocus.focus();
					return false;
				}else{
					sexoFocus.style.borderColor = "#ccc	";
					$('#confirmSexo').hide();
				}

		return true;
	});
</script>

<!-- Filhos -->
<script type="text/javascript">
	var cpf = "";
	//Chama a funcao para listar Filhos
	$('.listarFilhos').on('click', function() {
		cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		listarFilhos(cpf);
	});
	//Criar Table Filhos
	function listarFilhos(cpf){
		var table = document.getElementById("TabelaFilho").rows.length;
		var i = 0;
		for(i=table-1; i > 0; i--){
		    document.getElementById("TabelaFilho").deleteRow(i);
		}
		$.getJSON('model/filhoList.ajax.php?search=',{'cpf':cpf,ajax:'true'}, function(est){
			for (var i = 0; i < est.length; i++) {
				// Captura a referência da tabela com id “minhaTabela”
				var table = document.getElementById("TabelaFilho");
				// Captura a quantidade de linhas já existentes na tabela
				var numOfRows = table.rows.length;
				// Captura a quantidade de colunas da última linha da tabela
				var numOfCols = table.rows[numOfRows-1].cells.length;

				// Insere uma linha no fim da tabela.
				var newRowDoc = table.insertRow(numOfRows);

				// Faz um loop para criar as colunas
				for (var k = 0; k < numOfCols; k++) {
					switch(k){
						case 0:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].id;
							break;
						case 1:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].nome;
							break;
						case 2:
							var data = est[i].dtnasc;
							var ano = data.substring(0,4);
							var mes = data.substring(5,7);
							var dia = data.substring(8,10);
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = dia+"/"+mes+"/"+ano;
							break;
						case 3:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].sexo;
							break;
						case 4:
							var aux;
							if(est[i].cuidadoEspecial == "t" ){
								aux = "sim";
							}else{
								aux = "não";
							}
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = aux;
							break;
						case 5:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].descricao;
							break;
						case 6:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = "<a class='btn btn-default btn-md ExcluirFilho' >Excluir</a>";
							break;
						default:
							alert("deu error -> table");
							break;
					}
				}
			}
		});
	}
	//Validação de data
	function validateDateFilho() {
		var id = document.getElementById('dtnascFilho');
		var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
		if ((id.value.match(RegExPattern)) && (id.value!='')) {
			var data = id.value;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);

			//resgatar a data atual
			var dataAtual = new Date();
			var anoAtual = parseInt(dataAtual.getFullYear());
			//Criando um objeto Date usando os valores ano, mes e dia.
    			var novaData = new Date(ano,(mes-1),dia);

    			if( ano < 1900 || ano > anoAtual ){
    				$("#confirmDtnascFilho").show();
				id.style.borderColor =  "#E62117";
				return false;
    			}else{
    				id.style.borderColor = "#ccc";
				$("#confirmDtnascFilho").hide();
				return true;
    			}
		}else{
			$("#confirmDtnascFilho").show();
			id.style.borderColor =  "#E62117";
			return false;
		}
	}
	//Limpa Modal de inserir
	$('#insertFilho').on('click', function() {
		document.getElementById("nomeFilho").value = "";
		document.getElementById("nomeFilho").style.borderColor = "#ccc";
		$("#confirmNomeFilho").hide();
        		document.getElementById("dtnascFilho").value = "";
        		document.getElementById("dtnascFilho").style.borderColor = "#ccc";
        		$("#confirmDtnascFilho").hide();
        		document.getElementById("sexoFilho").selectedIndex = 0;
        		document.getElementById("sexoFilho").style.borderColor = "#ccc";
        		$("#confirmSexoFilho").hide();
        		document.getElementById("descricaoEspecialFilho").value = "";
        		document.getElementById("descricaoEspecialFilho").style.borderColor = "#ccc";
        		$("#confirmDescricaoFilho").hide();
        		$("#cuidadoEspecialFilho").prop("checked", false);
        		$("#EspecialFilho").hide();
		$("#cpfFilho").val(cpf);
	});
	//Div Cuidado Especial
	$('#cuidadoEspecialFilho').on('click', function() {
		$('#EspecialFilho').toggle();
		document.getElementById("descricaoEspecialFilho").value = "";
		document.getElementById("descricaoEspecialFilho").style.borderColor = "#ccc";
		$("#confirmDescricaoFilho").hide();
	});
	//Inserir Filhos
	$("#FormFilho").on('click',function(){
		var bad = "#E62117";
		var normal = "#ccc";
		var flag = true;

		var nome = document.getElementById("nomeFilho");
		var nomeVal = document.getElementById("nomeFilho").value;
		if(nome.value != ""){
			$('#confirmNomeFilho').hide();
			if(nome.value.length < 11){
				$('#confirmNomeFilho').show();
				nome.style.borderColor = "#E62117";
				nome.focus();
				flag = false;
			}else{
				nome.style.borderColor = "#ccc";
				$('#confirmNomeFilho').hide();
			}
		}else{
			nome.style.borderColor = "#E62117";
			nome.focus();
			$('#confirmNomeFilho').show();
			flag = false;
		}

		var dtnasc = document.getElementById("dtnascFilho");
		var dtnascVal = document.getElementById("dtnascFilho").value;
		if(validateDateFilho()){
			dtnasc.style.borderColor = normal;
			$("#confirmDtnascFilho").hide();
			var data = dtnascVal;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);
			dtnascVal = ano+"-"+mes+"-"+dia;
		}else{
			dtnasc.style.borderColor = bad;
			$("#confirmDtnascFilho").show();
			flag = false;
		}

		var sexo = document.getElementById("sexoFilho");
		var sexoVal = sexo.options[sexo.selectedIndex].value;
		if(sexoVal == ""){
			sexo.style.borderColor = bad;
			$("#confirmSexoFilho").show();
			flag = false;
		}else{
			sexo.style.borderColor = normal;
			$("#confirmSexoFilho").hide();
		}

		var cuidadoEspecialVal = document.getElementById("cuidadoEspecialFilho").checked;
		var descricao = document.getElementById("descricaoEspecialFilho");
		var descricaoVal = document.getElementById("descricaoEspecialFilho").value;
		if(cuidadoEspecialVal == true){
			cuidadoEspecialVal = "true";
			if(descricaoVal == ""){
				descricao.style.borderColor = bad;
				$("#confirmDescricaoFilho").show();
				flag = false;
			}else{
				descricao.style.borderColor = normal;
				$("#confirmDescricaoFilho").hide();
			}
		}else{
			cuidadoEspecialVal = "false";
		}
		var cpfFilho = document.getElementById("cpfFilho").value;
		var formData = new FormData();
		formData.append('cpf',cpfFilho);
		formData.append('nome',nomeVal);
		formData.append('dtnasc',dtnascVal);
		formData.append('sexo', sexoVal);
		formData.append('cuidadoEspecial',cuidadoEspecialVal);
		formData.append('descricao', descricaoVal);

		if(flag){
			$.ajax({
			        url: 'model/functionInsertFilho.ajax.php',
			        type: 'POST',
			        data: formData,
			        cache: false,
			        contentType: false,
			        processData: false,
			        dataType: 'json',
			        success: function(dado){
			        	if(dado.aux){
				        	if(dado.success){
				        		$('#modalInsirirFilhos').modal('hide');
						$("#MessInserirFilhoTrue").show();
						setTimeout(function(){$("#MessInserirFilhoTrue").hide();}, 5000);
				        		listarFilhos(cpf);
				        		nome.value = "";
				        		dtnasc.value = "";
				        		sexo.selectedIndex = 0;
				        		descricao.value = "";
				        		$("#cuidadoEspecialFilho").prop("checked", false);
				        		$("#EspecialFilho").hide();
				        	}else{
				        		$('#modalInsirirFilhos').modal('hide');
				        		$("#MessInserirFilhoFalse").show();
						setTimeout(function(){$("#MessInserirFilhoFalse").hide();}, 5000);
						nome.value = "";
				        		dtnasc.value = "";
				        		sexo.selectedIndex = 0;
				        		descricao.value = "";
				        		$("#cuidadoEspecialFilho").prop("checked", false);
				        		$("#EspecialFilho").hide();
				        	}
				}else{
					$('#modalInsirirFilhos').modal('hide');
					$("#MessInserirFilhoError").show();
					setTimeout(function(){$("#MessInserirFilhoError").hide();}, 5000);
				}
			        },
			        error: function(dado){

			        }
			});

		}
	});
	//Excluir Filhos
	$(document).on("click",  ".ExcluirFilho" ,function() {
	     var  id = $(this).closest('tr').find('td').eq(0).text();
	     $.getJSON('model/excluirFilhoLista.ajax.php?search=',{'id':id ,ajax:'true'}, function(est){
	     	if(est[0].sucess){
	     		$("#MessExcluirFilhoTrue").show();
	            		setTimeout(function(){$("#MessExcluirFilhoTrue").hide();}, 5000);
	            		listarFilhos(cpf);
	     	}else{
	     		$("#MessExcluirFilhoFalse").show();
	     		setTimeout(function(){$("#MessExcluirFilhoFalse").hide();}, 5000);
	     	}
	     });
	});
</script>

<!-- Idosos -->
<script type="text/javascript">
	var cpf = "";
	//Chama a funcao para listar Idosos
	$('.listarIdosos').on('click', function() {
		cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
		listarIdosos(cpf);
	});
	//Criar Table Idosos
	function listarIdosos(cpf){
		var table = document.getElementById("TabelaIdoso").rows.length;
		var i = 0;
		for(i=table-1; i > 0; i--){
		    document.getElementById("TabelaIdoso").deleteRow(i);
		}
		$.getJSON('model/IdosoList.ajax.php?search=',{'cpf':cpf,ajax:'true'}, function(est){
			for (var i = 0; i < est.length; i++) {
				// Captura a referência da tabela com id “minhaTabela”
				var table = document.getElementById("TabelaIdoso");
				// Captura a quantidade de linhas já existentes na tabela
				var numOfRows = table.rows.length;
				// Captura a quantidade de colunas da última linha da tabela
				var numOfCols = table.rows[numOfRows-1].cells.length;

				// Insere uma linha no fim da tabela.
				var newRowDoc = table.insertRow(numOfRows);

				// Faz um loop para criar as colunas
				for (var k = 0; k < numOfCols; k++) {
					switch(k){
						case 0:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].id;
							break;
						case 1:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].nome;
							break;
						case 2:
							var data = est[i].dtnasc;
							var ano = data.substring(0,4);
							var mes = data.substring(5,7);
							var dia = data.substring(8,10);
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = dia+"/"+mes+"/"+ano;
							break;
						case 3:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].sexo;
							break;
						case 4:
							var aux;
							if(est[i].cuidadoEspecial == "t" ){
								aux = "sim";
							}else{
								aux = "não";
							}
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = aux;
							break;
						case 5:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].descricao;
							break;
						case 6:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = "<a class='btn btn-default btn-md ExcluirIdoso' >Excluir</a>";
							break;
						default:
							alert("deu error -> table");
							break;
					}
				}
			}
		});
	}
	//Validação de data
	function validateDateIdoso() {
		var id = document.getElementById('dtnascIdoso');
		var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
		if ((id.value.match(RegExPattern)) && (id.value!='')) {
			var data = id.value;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);

			//resgatar a data atual
			var dataAtual = new Date();
			var anoAtual = parseInt(dataAtual.getFullYear());
			//Criando um objeto Date usando os valores ano, mes e dia.
    			var novaData = new Date(ano,(mes-1),dia);

    			if( ano < 1900 || ano > anoAtual || (anoAtual - ano) < 18 ){
    				$("#confirmDtnascIdoso").show();
				id.style.borderColor =  "#E62117";
				return false;
    			}else{
    				id.style.borderColor = "#ccc";
				$("#confirmDtnascIdoso").hide();
				return true;
    			}
		}else{
			$("#confirmDtnascIdoso").show();
			id.style.borderColor =  "#E62117";
			return false;
		}
	}
	//Limpa Modal de inserir
	$('#insertIdoso').on('click', function() {
		document.getElementById("nomeIdoso").value = "";
		document.getElementById("nomeIdoso").style.borderColor = "#ccc";
		$("#confirmNomeIdoso").hide();
        		document.getElementById("dtnascIdoso").value = "";
        		document.getElementById("dtnascIdoso").style.borderColor = "#ccc";
        		$("#confirmDtnascIdoso").hide();
        		document.getElementById("sexoIdoso").selectedIndex = 0;
        		document.getElementById("sexoIdoso").style.borderColor = "#ccc";
        		$("#confirmSexoIdoso").hide();
        		document.getElementById("descricaoEspecialIdoso").value = "";
        		document.getElementById("descricaoEspecialIdoso").style.borderColor = "#ccc";
        		$("#confirmDescricaoIdoso").hide();
        		$("#cuidadoEspecialIdoso").prop("checked", false);
        		$("#EspecialIdoso").hide();
		$("#cpfIdoso").val(cpf);
	});
	//Div Cuidado Especial
	$('#cuidadoEspecialIdoso').on('click', function() {
		$('#EspecialIdoso').toggle();
		document.getElementById("descricaoEspecialIdoso").value = "";
		document.getElementById("descricaoEspecialIdoso").style.borderColor = "#ccc";
		$("#confirmDescricaoIdoso").hide();
	});
	//Inserir Idosos
	$("#FormIdoso").on('click',function(){
		var bad = "#E62117";
		var normal = "#ccc";
		var flag = true;

		var nome = document.getElementById("nomeIdoso");
		var nomeVal = document.getElementById("nomeIdoso").value;
		if(nome.value != ""){
			$('#confirmNomeIdoso').hide();
			if(nome.value.length < 11){
				$('#confirmNomeIdoso').show();
				nome.style.borderColor = "#E62117";
				nome.focus();
				flag = false
			}else{
				nome.style.borderColor = "#ccc";
				$('#confirmNomeIdoso').hide();
			}
		}else{
			nome.style.borderColor = "#E62117";
			nome.focus();
			$('#confirmNomeIdoso').show();
			flag = false
		}

		var dtnasc = document.getElementById("dtnascIdoso");
		var dtnascVal = document.getElementById("dtnascIdoso").value;
		if(validateDateIdoso()){
			dtnasc.style.borderColor = normal;
			$("#confirmDtnascFilho").hide();
			var data = dtnascVal;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);
			dtnascVal = ano+"-"+mes+"-"+dia;
		}else{
			dtnasc.style.borderColor = bad;
			$("#confirmDtnascFilho").show();
			flag = false;
		}

		var sexo = document.getElementById("sexoIdoso");
		var sexoVal = sexo.options[sexo.selectedIndex].value;
		if(sexoVal == ""){
			sexo.style.borderColor = bad;
			$("#confirmSexoIdoso").show();
			flag = false;
		}else{
			sexo.style.borderColor = normal;
			$("#confirmSexoIdoso").hide();
		}

		var cuidadoEspecialVal = document.getElementById("cuidadoEspecialIdoso").checked;
		var descricao = document.getElementById("descricaoEspecialIdoso");
		var descricaoVal = document.getElementById("descricaoEspecialIdoso").value;
		if(cuidadoEspecialVal == true){
			cuidadoEspecialVal = "true";
			if(descricaoVal == ""){
				descricao.style.borderColor = bad;
				$("#confirmDescricaoIdoso").show();
				flag = false;
			}else{
				descricao.style.borderColor = normal;
				$("#confirmDescricaoIdoso").hide();
			}
		}else{
			cuidadoEspecialVal = "false";
		}
		var cpfIdoso = document.getElementById("cpfIdoso").value;
		var formData = new FormData();
		formData.append('cpf',cpfIdoso);
		formData.append('nome',nomeVal);
		formData.append('dtnasc',dtnascVal);
		formData.append('sexo', sexoVal);
		formData.append('cuidadoEspecial',cuidadoEspecialVal);
		formData.append('descricao', descricaoVal);

		if(flag){
			$.ajax({
			        url: 'model/functionInsertIdoso.ajax.php',
			        type: 'POST',
			        data: formData,
			        cache: false,
			        contentType: false,
			        processData: false,
			        dataType: 'json',
			        success: function(dado){
			        	if(dado.aux){
				        	if(dado.success){
				        		$('#modalInsirirIdosos').modal('hide');
						$("#MessInserirIdosoTrue").show();
						setTimeout(function(){$("#MessInserirIdosoTrue").hide();}, 5000);
				        		listarIdosos(cpf);
				        		nome.value = "";
				        		dtnasc.value = "";
				        		sexo.selectedIndex = 0;
				        		descricao.value = "";
				        		$("#cuidadoEspecialIdoso").prop("checked", false);
				        		$("#EspecialIdoso").hide();
				        	}else{
				        		$('#modalInsirirIdosos').modal('hide');
				        		$("#MessInserirIdosoFalse").show();
						setTimeout(function(){$("#MessInserirIdosoFalse").hide();}, 5000);
						nome.value = "";
				        		dtnasc.value = "";
				        		sexo.selectedIndex = 0;
				        		descricao.value = "";
				        		$("#cuidadoEspecialIdoso").prop("checked", false);
				        		$("#EspecialIdoso").hide();
				        	}
				}else{
					$('#modalInsirirIdosos').modal('hide');
					$("#MessInserirIdosoError").show();
					setTimeout(function(){$("#MessInserirIdosoError").hide();}, 5000);
				}
			        },
			        error: function(dado){

			        }
			});

		}
	});
	//Excluir Idosos
	$(document).on("click",  ".ExcluirIdoso" ,function() {
	     var  id = $(this).closest('tr').find('td').eq(0).text();
	     $.getJSON('model/excluirIdosoLista.ajax.php?search=',{'id':id ,ajax:'true'}, function(est){
	     	if(est[0].sucess){
	     		$("#MessExcluirIdosoTrue").show();
	            		setTimeout(function(){$("#MessExcluirIdosoTrue").hide();}, 5000);
	            		  listarIdosos(cpf);
	     	}else{
	     		$("#MessExcluirIdosoFalse").show();
	     		setTimeout(function(){$("#MessExcluirIdosoFalse").hide();}, 5000);
	     	}
	     });
	});
</script>

</html>