<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div id="tela" class="tela">
	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
				Trocar Senha <small>Mudança de Senha</small>
				</h1>
				<form role="form" action="" method="POST">
					<div class="form-group">
						<label>Senha *</label>
						<input class="form-control"  name="pass1" id="pass1" type="password"  required>

						<label>Confirmar Senha *</label>
						<input class="form-control" name="pass2" id="pass2" onkeyup="checkPass(); return false;" type="password" required>
						<span id="confirmMessage" class="confirmMessage"></span>
						<br>
<?php
require_once("controller/controllerUsuario.php");
//SALVAR
$FormUsuarioTrocarSenha = @$_POST["FormUsuarioTrocarSenha"];
if(!empty($FormUsuarioTrocarSenha)){
		$class = new controllerUsuario;
		$class->TrocarSenha();
}
?>
						<br>
						<button type="submit" class="btn btn-default btn-lg" value="salvar" name="FormUsuarioTrocarSenha">Mudar Senha</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>