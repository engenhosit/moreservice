<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div id="tela" class="tela">
	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
				Tipo de Conta <small>Cadastro Tipo de Conta</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-users"></i> Cadastro
					</li>
				</ol>

<?php
require_once("controller/controllerTipoConta.php");
//SALVAR
$FormTipoConta = @$_POST["FormTipoContaSalvar"];
if(!empty($FormTipoConta)){
		$class = new controllerTipoConta;
		$class->Salvar();
}
?>
				<form role="form" action="" method="POST">
					<div class="form-group">
						<label>descrição tipo conta *</label>
						<br>
						<input class="form-control" placeholder=" Digite a descrição aqui" name="descricao" required />
						<br>

						<button type="submit" class="btn btn-default btn-lg" value="salvar" name="FormTipoContaSalvar">Salvar</button>
						<a href="lista-tipoConta.php"  class="btn btn-default btn-lg">Listar</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>