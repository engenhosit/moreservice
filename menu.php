<?php
session_start();
include "model/mostra_erros.php";
?>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>+Service Admin</title>

	<!-- Inserir icone na URL -->
	<link rel="icon" href="images/logo/logo_web.png" type="image/x-icon" />

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Front-End CSS -->
	<link href="css/sb-admin.css" rel="stylesheet">

	<!-- @media CSS -->
	<link href="css/media.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- jQuery -->
	<script src="js/jquery-3.1.1.min.js"></script>
</head>

<body>
	<div id="wrapper" >
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php" ><font size="45">+Services</font></a>
			</div>
			<!-- Top Menu Items -->
			<ul class="nav navbar-right top-nav" >
				<!--Usuario-->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php if(isset($_SESSION["login"])) if($_SESSION['login'][1]) echo $_SESSION["login"][4]['email']; ?><b class="caret"></b></a>
						<?PHP
							if(isset($_SESSION["login"])){
								if($_SESSION['login'][1]){
						?>
						<ul class='dropdown-menu' style='z-index: 10000;'>
							<li class='divider'></li>
							<li>
								<a href='troca_senha.php'><i class='fa fa-fw fa-key'></i> Mudar Senha</a>
							</li>
							<li class='divider'></li>
							<li>
								<a href='' style='padding-right:20px;height: 30px'>
									<form action='' method='POST' >
										<input type='hidden' value='sair' name='formSair'>
										<button type='submit' class='btn-link' style='color:black;'><i class='fa fa-fw fa-power-off'></i> Sair </button>
									</form>
								</a>
							</li>
						</ul>
						<?PHP
								}
							}

							if(isset($_POST["formSair"])){
								require_once("controller/controllerLogin.php");
								$class = new controllerLogin;
								$class->sair();
							}

							if(isset($_POST["formUpdatePassword"])){
								require_once("controller/controllerLogin.php");
								$class = new controllerLogin;
								$class->updatePassword();
							}
						?>
				</li>
			</ul>
			<?php
				if(isset($_SESSION["login"])){
					if($_SESSION['login'][1]){
			?>
			<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
			<div id='main' class='collapse navbar-collapse navbar-ex1-collapse'>
				<ul class='nav navbar-nav side-nav'>
					<li style='margin-top:5%;'>
						<a href='javascript:;' data-toggle='collapse' data-target='#usuario'><i class='fa fa-fw fa-users'></i> Usuarios - Administrativo <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='usuario' class='collapse'>
							<li>
								<a href='usuario.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Usuarios</a>
							</li>
							<li>
								<a href='lista-usuario.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Usuarios</a>
							</li>
						</ul>
					</li>
					<li style='margin-top:5%;'>
						<a href='javascript:;' data-toggle='collapse' data-target='#cliente'><i class='fa fa-fw fa-users'></i> Cliente <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='cliente' class='collapse'>
							<li>
								<a href='cliente.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Cliente</a>
							</li>
							<li>
								<a href='lista-cliente.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Clientes</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#prestador'><i class='fa fa-fw fa-male'></i> Prestador <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='prestador' class='collapse'>
							<li>
								<a href='prestador.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Prestador</a>
							</li>
							<li>
								<a href='lista-prestador.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Prestador</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#bancos'><i class='fa fa-fw fa-bank'></i> Bancos <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='bancos' class='collapse'>
								<li>
									<a href='banco.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Bancos</a>
								</li>
								<li>
									<a href='lista-banco.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Bancos</a>
								</li>
							</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#tipo_servico'><i class='fa fa-fw fa-table'></i> Tipo de Serviços <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='tipo_servico' class='collapse'>
							<li>
								<a href='tipo_servico.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipos de Serviços</a>
							</li>
							<li>
								<a href='lista-tipo_servico.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipos de Serviços</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#serv'><i class='fa fa-fw fa-desktop'></i> Tipo Conta <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='serv' class='collapse'>
							<li>
								<a href='tipoConta.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipo de Conta</a>
							</li>
							<li>
								<a href='lista-tipoConta.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipos de Contas</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#docs'><i class='fa fa-fw fa-desktop'></i> Tipo Documentos <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='docs' class='collapse'>
							<li>
								<a href='tipoDocumento.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Documento</a>
							</li>
							<li>
								<a href='lista-tipoDocumento.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Documentos</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#escolaridade'><i class='fa fa-fw fa-desktop'></i> Escolaridade <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='escolaridade' class='collapse'>
							<li>
								<a href='escolaridade.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Escolaridade</a>
							</li>
							<li>
								<a href='lista-escolaridade.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Escolaridades</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#especialidade'><i class='fa fa-fw fa-desktop'></i> Especialidades <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='especialidade' class='collapse'>
								<li>
									<a href='especialidade.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Escolaridade</a>
								</li>
								<li>
									<a href='lista-especialidade.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Escolaridades</a>
								</li>
							</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#turno'><i class='fa fa-fw fa-desktop'></i> Turno <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='turno' class='collapse'>
							<li>
								<a href='turno.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir turno</a>
							</li>
							<li>
								<a href='lista-turno.php'><i class='fa fa-fw fa-chevron-right'></i> Listar turnos</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#ocupacao'><i class='fa fa-fw fa-desktop'></i> Ocupação <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='ocupacao' class='collapse'>
							<li>
								<a href='ocupacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Ocupação</a>
							</li>
							<li>
								<a href='lista-ocupacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Ocupação</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#tipostatusMovimentacao'><i class='fa fa-fw fa-desktop'></i> Tipo Status Movimentações <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='tipostatusMovimentacao' class='collapse'>
							<li>
								<a href='tipoStatusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipo Status Movimentações</a>
							</li>
							<li>
								<a href='lista-tipoStatusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipo Status Movimentações</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#valorAvaliacao'><i class='fa fa-fw fa-desktop'></i> Valor Avaliação <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='valorAvaliacao' class='collapse'>
							<li>
								<a href='valorAvaliacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Valor Avaliação</a>
							</li>
							<li>
								<a href='lista-valorAvaliacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Valores Avaliações</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#tipoUsuario'><i class='fa fa-fw fa-desktop'></i> Tipo Usuário <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='tipoUsuario' class='collapse'>
							<li>
								<a href='tipoUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipo de Usuário</a>
							</li>
							<li>
								<a href='lista-tipoUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipos de Usuários</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#statusUsuario'><i class='fa fa-fw fa-desktop'></i> Status Usuário <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='statusUsuario' class='collapse'>
							<li>
								<a href='statusUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Status Usuário</a>
							</li>
							<li>
								<a href='lista-statusUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Status Usuários</a>
							</li>
						</ul>
					</li>
					<li>
						<a href='javascript:;' data-toggle='collapse' data-target='#statusMovimentacao'><i class='fa fa-fw fa-desktop'></i> Status Movimentações <i class='fa fa-fw fa-caret-down'></i></a>
						<ul id='statusMovimentacao' class='collapse'>
							<li>
								<a href='statusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Status Movimentações</a>
							</li>
							<li>
								<a href='lista-statusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Status Movimentações</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- Sidebar Mobile -->
			<div id='mobile-bar' class='nav-bar-mobile-bar nav-aberta'>
				<div class='wrap'>
					<ul>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#usu'><i class='fa fa-fw fa-users'></i> Usuarios - Administrativo <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='usu' class='collapse'>
								<li>
									<a href='usuario.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Usuarios</a>
								</li>
								<li>
									<a href='lista-usuario.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Usuarios</a>
								</li>
							</ul>
						</li>
						<li style='margin-top:5%;'>
							<a href='javascript:;' data-toggle='collapse' data-target='#cliente'><i class='fa fa-fw fa-users'></i> Usuarios - Administrativo <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='cliente' class='collapse'>
								<li>
									<a href='cliente.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Usuarios</a>
								</li>
								<li>
									<a href='lista-cliente.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Usuarios</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#pres'><i class='fa fa-fw fa-edit'></i> Prestador <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='pres' class='collapse'>
								<li>
									<a href='prestador.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Prestador</a>
								</li>
								<li>
									<a href='lista-prestador.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Prestador</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#banc'><i class='fa fa-fw fa-bank'></i> Bancos <i class='fa fa-fw fa-caret-down'></i></a>

							<ul id='banc' class='collapse'>
								<li>
									<a href='banco.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Bancos</a>
								</li>
								<li>
									<a href='lista-banco.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Bancos</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#t_s'><i class='fa fa-fw fa-table'></i> Tipo de Serviços <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='t_s' class='collapse'>
								<li>
									<a href='tipo_servico.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipos de Serviços</a>
								</li>
								<li>
									<a href='lista-tipo_servico.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipos de Serviços</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#serv'><i class='fa fa-fw fa-desktop'></i> Tipo Conta <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='serv' class='collapse'>
								<li>
									<a href='tipoConta.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipo de Conta</a>
								</li>
								<li>
									<a href='lista-tipoConta.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipos de Contas</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#docs'><i class='fa fa-fw fa-desktop'></i> Tipo Documentos <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='docs' class='collapse'>
								<li>
									<a href='tipoDocumento.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Documento</a>
								</li>
								<li>
									<a href='lista-tipoDocumento.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Documentos</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#escolaridade'><i class='fa fa-fw fa-desktop'></i> Escolaridade <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='escolaridade' class='collapse'>
								<li>
									<a href='escolaridade.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Escolaridade</a>
								</li>
								<li>
									<a href='lista-escolaridade.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Escolaridades</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#especialidade'><i class='fa fa-fw fa-desktop'></i> Especialidades <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='especialidade' class='collapse'>
								<li>
									<a href='especialidade.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Escolaridade</a>
								</li>
								<li>
									<a href='lista-especialidade.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Escolaridades</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#turno'><i class='fa fa-fw fa-desktop'></i> Turno <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='turno' class='collapse'>
								<li>
									<a href='turno.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir turno</a>
								</li>
								<li>
									<a href='lista-turno.php'><i class='fa fa-fw fa-chevron-right'></i> Listar turnos</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#ocupacao'><i class='fa fa-fw fa-desktop'></i> Ocupação <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='ocupacao' class='collapse'>
								<li>
									<a href='especialidade.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Ocupação</a>
								</li>
								<li>
									<a href='lista-especialidade.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Ocupação</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#tipostatusMovimentacao'><i class='fa fa-fw fa-desktop'></i> Tipo Status Movimentações <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='tipostatusMovimentacao' class='collapse'>
								<li>
									<a href='tipoStatusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipo Status Movimentações</a>
								</li>
								<li>
									<a href='lista-tipoStatusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipo Status Movimentações</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#valorAvaliacao'><i class='fa fa-fw fa-desktop'></i> Valor Avaliação <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='valorAvaliacao' class='collapse'>
								<li>
									<a href='valorAvaliacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Valor Avaliação</a>
								</li>
								<li>
									<a href='lista-valorAvaliacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Valores Avaliações</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#tipoUsuario'><i class='fa fa-fw fa-desktop'></i> Tipo Usuário <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='tipoUsuario' class='collapse'>
								<li>
									<a href='tipoUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Tipo de Usuário</a>
								</li>
								<li>
									<a href='lista-tipoUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Tipos de Usuários</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#statusUsuario'><i class='fa fa-fw fa-desktop'></i> Status Usuário <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='statusUsuario' class='collapse'>
								<li>
									<a href='statusUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Status Usuário</a>
								</li>
								<li>
									<a href='lista-statusUsuario.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Status Usuários</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='javascript:;' data-toggle='collapse' data-target='#statusMovimentacao'><i class='fa fa-fw fa-desktop'></i> Status Movimentações <i class='fa fa-fw fa-caret-down'></i></a>
							<ul id='statusMovimentacao' class='collapse'>
								<li>
									<a href='statusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Inserir Status Movimentações</a>
								</li>
								<li>
									<a href='lista-statusMovimentacao.php'><i class='fa fa-fw fa-chevron-right'></i> Listar Status Movimentações</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<?PHP
					}
				}
			?>

		</nav>
	</div>

<!-- jQuery Form-->
<script src="js/jquery.mask.min.js"></script>

<!-- Menu mobile-->
<script src="js/menu-bar.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Filter -->
<script src="js/Filter.js" async></script>

<!-- AJAX change estado -->
<script type="text/javascript">
	$(function(){
		$('#estado').change(function(){
			if( $(this).val() ) {
				$('#cod_cidades').hide();
				$('.carregando').show();
				$.getJSON('model/cidades.ajax.php?search=',{cod_estados: $(this).val(), ajax: 'true'}, function(j){
					var options;
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
					}
					$('#cod_cidades').html(options).show();
					$('.carregando').hide();
				});
			} else {
				$('#cod_cidades').html('<option value="">– Escolha um estado –</option>');
			}
		});
	});
</script>

<!-- Mudar Valor do required-->
<script type="text/javascript">
	$(document).ready(function() {
	    var elements = document.getElementsByTagName("INPUT");
	    for (var i = 0; i < elements.length; i++) {
	        elements[i].oninvalid = function(e) {
	            e.target.setCustomValidity("");
	            if (!e.target.validity.valid) {
	                e.target.setCustomValidity("Campo Obrigatório.");
	                e.target.style.borderColor = "#E62117";
	            }
	        };
	        elements[i].oninput = function(e) {
	            e.target.setCustomValidity("");
	            e.target.style.borderColor = "#ccc";
	        };
	    }
	})
</script>