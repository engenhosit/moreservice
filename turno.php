<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
	<div id="tela" class="tela">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Turno <small>Cadastro de Turno</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Cadastro
                            </li>
                        </ol>

					<div class="form-group">
						<form method="POST" action="" id="Formturno">
							<label>Turno *</label>
							<input type="text" id="descricao" name="turno" class="form-control" placeholder="Digite a descrição do turno aqui..." required>
							<br/>
							<label>hora inicio *</label>
							<input type="text" name="inicio" class="form-control time" placeholder="Digite o valor da hora de início aqui..." required>
							<br/>
							<label>hora fim *</label>
							<input type="text" name="fim" class="form-control time" placeholder="Digite o valor da hora fim aqui..." required>
							<br/>
							<?php
//SALVAR
$Formturno = @$_POST["FormTurnoSalvar"];
if(!empty($Formturno)){
	require_once("controller/controllerTurno.php");
	$class = new controllerTurno;
	$class->Salvar();
}
?>
							<input type="submit" value="Salvar" name="FormTurnoSalvar" class="btn btn-default btn-lg">
							<a href="lista-turno.php"  class="btn btn-default btn-lg">Listar</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$( function() {
		 $('.time').mask('00:00:00');
	});
	</script>
</body>
</html>