<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
				Status Usuário <small>Listagem de Status Usuário</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-users"></i> Lista
					</li>
				</ol>
				<a href="statusUsuario.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
				<br/><br/>
			<?php
				require_once("controller/controllerStatusUsuario.php");
				//EDITAR
				$Form = @$_POST["FormStatusUsuarioEditar"];
				if(!empty($Form)){
						$class = new controllerStatusUsuario;
						echo"<br/><br/>";
						$class->Editar();
				}
				//EXCLUIR
				$Form = @$_POST["FormStatusUsuarioExcluir"];
				if(!empty($Form)){
					$cod = @$_POST["CodExcluir"];
						$class = new controllerStatusUsuario;
						echo"<br/><br/>";
						$class->Excluir($cod);
				}
			?>
					<div id="lista">
					<?php
						//LISTAR
						$class = new controllerStatusUsuario;
						$class->Listar();
					?>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>