<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<style type="text/css">
	.carregando{
		display:none;
	}
</style>
<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
				Usuários <small>Listagem de Usuários</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-users"></i> Lista
					</li>
				</ol>
				<a href="usuario.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
			<?php
				require_once("controller/controllerUsuario.php");
				//EDITAR
				$FormUsuario = @$_POST["FormUsuarioEditar"];
				if(!empty($FormUsuario)){
						$class = new controllerUsuario;
						echo"<br/><br/>";
						$class->Editar();
				}
				//EXCLUIR
				$FormUsuario = @$_POST["FormUsuarioExcluir"];
				if(!empty($FormUsuario)){
					$cod = @$_POST["cpfexcluir"];
					$idStatus = @$_POST["status"];
						$class = new controllerUsuario;
						echo"<br/><br/>";
						$class->Excluir($cod,$idStatus);
				}
			?>
					<div id="lista">
					<?php
						//LISTAR
						$class = new controllerUsuario;
						$class->Listar();
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- ModalUpdate Status -->
<div class='modal fade' id='modalStatus' tabindex='-1'>
	<div class='modal-dialog'>
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form method="POST" action="">
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class="form-inline">
				<input class="form-control"  name="cpfexcluir" id="cpfStatus" maxlength="14" style="width:50%;" readonly />
				<span id="validcpf"></span>
				</div>
				<br/>
				<label>Status</label>
				<select class="form-control" name="status" id="prestadorStatus" style="width:50%">
				</select>
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='submit' name='FormUsuarioExcluir' class='btn btn-success' value='Mudar Status'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- ModalUpdate -->
<div class='modal fade' id='contact' tabindex='-1'>
	<div class='modal-dialog'>
		<div class='panel panel-primary'>
			<div class='panel-heading'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
				<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
			</div>
			<form action='' method='POST' id='formUpdate'>
			<div class='modal-body' style='padding: 5px;'>
				<label>CPF *</label>
				<div class='form-inline'>
				<input class='form-control' id="cpf" placeholder=" Digite o CPF aqui..."  name='cpf' maxlength='14' style='width:50%;' readonly />
				<span id='validcpf'></span>
				</div>
				<br/>
				<label>Nome *</label>
				<input class="form-control" placeholder=" Digite o nome aqui..."  name="nome" id='nome' value="<?php echo @$_POST["nome"]; ?>" />
				<span id="confirmNomeNull" style="display: none; color:#E62117;">  Nome é obrigatório</span>
				<span id="confirmNome" style="display: none; color:#E62117;">  Nome deve conter no mínimo 12 caracteres</span>
				<br/>
				<label>Data de Nascimento *</label>
				<input type="text" class="form-control data" id="dtnasc" name="dtnasc" value="<?php echo @$_POST["dtnasc"]; ?>" />
				<span id="confirmDtnasc" style="display: none; color:#E62117;">  Data inválida</span>
				<span id="confirmDtnascMaior" style="display: none; color:#E62117;">  Obrigatório ser maior de 18 anos</span>
				<br/>
				<label>Email *</label>
				<input type="email" class="form-control" placeholder=" Digite o email aqui..." id='email' name="email" value="<?php echo @$_POST["email"]; ?>" />
				<span id="confirmEmailNull" style="display: none; color:#E62117;">  Email é obrigatório</span>
				<span id="confirmEmail" style="display: none; color:#E62117;">  Formato de email incorreto</span>
				<br/>
				<label>Celular *</label>
				<div class="form-inline">
				<input class="form-control" placeholder=" Digite o celular aqui..." id="cell" name="cell" style="width:50%" onkeyup="lengthCell();" value="<?php echo @$_POST["cell"]; ?>" />
				<span id="confirmCell"></span>
				</div>
				<br/>
				<label>Telefone</label>
				<input class="form-control" placeholder=" Digite o telefone aqui..." id="fone" name="fone" type="text" value="<?php echo @$_POST["fone"]; ?>" />
				<br/>
				<label class='control-label'>Sexo *</label>
				<select name='sexo' id="sexo" class='form-control' style='width:50%;'>
				</select>
				<br/>
				<label>Cep</label>
				<div class='form-inline'>
				<input class='form-control cep' id='cep' placeholder=' Digite o CEP aqui...'  name='cep' maxlength='9' style='width:50%;'  />
				<span id='validcep'></span>
				</div>
				<br/>
				<label>Rua</label>
				<input class='form-control' id='rua' name='rua' placeholder=' Digite a rua aqui...'  type='text' />
				<br/>
				<label>Numero</label>
				<input class='form-control' id='numero' name='numero' placeholder=' Digite o numero aqui...'  type='text' />
				<br/>
				<label>Bairro</label>
				<input class='form-control' placeholder=' Digite o bairro aqui...' id='bairro' name='bairro'  type='text' />
				<br/>
				<label>Complemento</label>
				<textarea class='form-control' placeholder=' Digite o complento do endereço aqui...'  name='complemento' id="complemento" type='text'></textarea>
				<br/>
				<div class='form-inline'>
				<label>Estado:</label>
				<select class="form-control" id="estado" name="estado" style="width:50%;">
					<option value="">-- Escolha um estado --</option>
				</select>
				</div>
				<br/>
				<div class="form-inline">
				<label>Cidade:</label>
				<span class="carregando">Aguarde, carregando...</span>
				<select class="form-control" name="cidade" id="cod_cidades" style="width:50%;">
					<option value="">-- Escolha uma cidade --</option>
				</select>
				</div>
				<br/>
				<label>linkedin</label>
				<input class='form-control' id="linkedin" name='linkedin' placeholder=' Digite o linkedin aqui...'  type='email' />
				<br/>
				<label>googleplus</label>
				<input class='form-control' id="googleplus" name='googleplus' placeholder=' Digite o googleplus aqui...'  type='email' />
				<br/>
				<label>Facebook</label>
				<input class='form-control' id="facebook" name='facebook' placeholder=' Digite o Facebook aqui...'  type='email' />
				<br/>
			<div class='panel-footer' style='margin-bottom:-14px;'>
				<input type='submit' name='FormUsuarioEditar' class='btn btn-success' value='Editar'/>
				<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
			</div>
			</div>
			</form>
		</div>
	</div>
</div>
</body>

<!-- masks -->
<script type="text/javascript">
	$( function() {

		$(".data").mask("00/00/0000",{placeholder: "dd/mm/aaaa"});

		$("#cpf").mask("999.999.999-99", {placeholder: "999.999.999-99"});

		$(".cep").mask('00000-000',{placeholder: '00000-000'});

		$('#fone').mask('(00)0000-0000',{placeholder: '(00)0000-0000'});

		$('#cell').mask("(00)00000-0000",{placeholder: "(00)00000-0000"});
	});
</script>

<!-- Validar CPF -->
<script type="text/javascript">
	//retirar os caracteres do cpf
	function RetiraCaracteresInvalidos(strCPF){
		var strTemp;
		strTemp = strCPF.replace(".", "");
		strTemp = strTemp.replace(".", "");
		strTemp = strTemp.replace("-", "");

		return strTemp;
	}
	//testar o cpf do usuario
	function TestaCPF(strCPF) {
		var Soma;
		var Resto;
		Soma = 0;
		strCPF = RetiraCaracteresInvalidos(strCPF);
		for(i=0; i < 10; i++){
			if (strCPF == ""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i)
				return false;
		}
		for (i=1; i<=9; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10)) )
			return false;
		Soma = 0;
		for (i = 1; i <= 10; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11) ) )
			return false;

		return true;
	}
	$(function() {
		$("#cpf").blur(function(){
			var strCPF = document.getElementById('cpf').value;
			strCPF = RetiraCaracteresInvalidos(strCPF);
			var tamanho = strCPF.length;
			var cpf = document.getElementById('cpf');
			var message = document.getElementById('validcpf');
			var goodColor = "#66cc66";
			var badColor = "#E62117";

			if(tamanho == 11){
				if(TestaCPF(strCPF)){
					cpf.style.borderColor = goodColor;
					message.style.color = goodColor;
					message.innerHTML = "&emsp;CPF válido!";
				}else{
					cpf.style.borderColor = badColor;
					message.style.color = badColor;
					message.innerHTML = "&emsp;CPF inválido!";
				}
			}else{
				cpf.style.borderColor = badColor;
				message.style.color = badColor;
				message.innerHTML = "&emsp;CPF falta caracter!";
			}

		});
	});
</script>

<!--Validar Celular-->
<script type="text/javascript">
	function lengthCell(){
		//Store the password field objects into variables ...
		var cell = document.getElementById('cell');
		//Store the Confimation Message Object ...
		var mens = document.getElementById('confirmCell');
		//Set the colors we will be using ...
		var colorGood = "#66cc66";
		var colorBad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field
		if(cell.value != ""){
			if(cell.value.length < 13){
				cell.style.borderColor = colorBad;
				mens.style.color = colorBad;
				mens.innerHTML = "Celular Incorreto";
				return false;
			}else{
				cell.style.borderColor = colorGood;
				mens.style.color = colorGood;
				mens.innerHTML = "Celular Correto";
				return true;
			}
		}else{
			cell.style.borderColor = colorBad;
			mens.style.color = colorBad;
			mens.innerHTML = "Celular é obrigatório";
			return false;
		}
	}
</script>

<!--Validar Senha-->
<script type="text/javascript">
	function lengthPass(){
		//Store the password field objects into variables ...
		var pass = document.getElementById('pass1');
		//Store the Confimation Message Object ...
		var mess = document.getElementById('confirmPass');
		//Set the colors we will be using ...
		var good = "#66cc66";
		var bad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass.value.length < 6){
			pass.style.borderColor = bad;
			mess.style.color = bad;
			mess.innerHTML = "Senha deve conter no minimo 6 caracteres";
			return false;
		}else{
			pass1.style.borderColor = good;
			mess.style.color = good;
			mess.innerHTML = "Senha ok";
			return true;
		}
	}

	function checkPass()
	{
		//Store the password field objects into variables ...
		var pass1 = document.getElementById('pass1');
		var pass2 = document.getElementById('pass2');
		//Store the Confimation Message Object ...
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#66cc66";
		var badColor = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass1.value == pass2.value){
			//The passwords match.
			//Set the color to the good color and inform
			//the user that they have entered the correct password
			pass2.style.borderColor = goodColor;
			message.style.color = goodColor;
			message.innerHTML = "Senhas Iguais!";
			return true;
		}else{
			//The passwords do not match.
			//Set the color to the bad color and
			//notify the user.
			pass2.style.borderColor = badColor;
			message.style.color = badColor;
			message.innerHTML = "As senhas informadas não são iguais!";
			return false;
		}
	}
</script>

<!-- Validação de cep -->
<script type="text/javascript" >
        $(function() {
        	function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cep").val("");
				$("#rua").val("");
                $("#bairro").val("");
				var opt = '<option value="">-- Escolha uma cidade --</option>';
				$('#cod_cidades').html(opt).show();
				$.getJSON('model/estados.ajax.php?search=',{ajax: 'true'}, function(j){
					var options = '<option value="">-- Escolha um estado --</option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
					}
					$('#estado').html(options).show();
				});

            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function (){
				var ceptxt = document.getElementById("cep");
				var message = document.getElementById('validcep');
				var goodColor = "#66cc66";
				var badColor = "#E62117";

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/-/, "");

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("carregando...");
                        $("#bairro").val("carregando...");
                        $("#cidade").val("carregando...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);

							$('#cod_cidades').hide();
							$('.carregando').show();

							$.getJSON('model/estadoperuf.ajax.php?search=',{cod_estados: dados.uf, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#estado').html(options).show();
								$('.carregando').hide();
							});

							$.getJSON('model/cidadespernome.ajax.php?search=',{cod_estados: dados.localidade, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#cod_cidades').html(options).show();
								$('.carregando').hide();
							});

								ceptxt.style.borderColor = goodColor;
								message.style.color = goodColor;
								message.innerHTML = "&emsp;CEP encontrado!";
								return true;
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
								ceptxt.style.borderColor = badColor;
								message.style.color = badColor;
								message.innerHTML = "&emsp;CEP não encontrado!";
								limpa_formulário_cep();
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
						ceptxt.style.borderColor = badColor;
						message.style.color = badColor;
						message.innerHTML = "&emsp;Formato de CEP inválido!";
						limpa_formulário_cep();
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
</script>

<!-- Validação do email -->
<script language="Javascript">
	function validacaoEmail() {
		var email = document.getElementById('email');
		usuario = email.value.substring(0, email.value.indexOf("@"));
		dominio = email.value.substring(email.value.indexOf("@")+ 1, email.value.length);
		if(email.value != ""){
			$("#confirmEmail").hide();
			if ((usuario.length >=1) &&
			    (dominio.length >=3) &&
			    (usuario.search("@")==-1) &&
			    (dominio.search("@")==-1) &&
			    (usuario.search(" ")==-1) &&
			    (dominio.search(" ")==-1) &&
			    (dominio.search(".")!=-1) &&
			    (dominio.indexOf(".") >=1)&&
			    (dominio.lastIndexOf(".") < dominio.length - 1)) {
				email.style.borderColor = "#ccc";
				$("#confirmEmailNull").hide();
				return true;
			}else{
				email.style.borderColor = "#E62117";
				$("#confirmEmailNull").show();
				return false;
			}
		}else{
			$("#confirmEmailNull").show();
			email.style.borderColor = "#E62117";
			email.focus();
			return false;
		}
	}
</script>

<!-- Validação da data -->
<script type="text/javascript">
	function validateDate() {
		var id = document.getElementById('dtnasc');
		var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
		if ((id.value.match(RegExPattern)) && (id.value!='')) {
			var data = id.value;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);

			//resgatar a data atual
			var dataAtual = new Date();
			var anoAtual = parseInt(dataAtual.getFullYear());
			//Criando um objeto Date usando os valores ano, mes e dia.
    			var novaData = new Date(ano,(mes-1),dia);

    			if( ano < 1900 || ano > anoAtual || (anoAtual - ano) < 18 ){
    				if((anoAtual - ano) < 18){
    					$("#confirmDtnasc").hide();
	    				$("#confirmDtnascMaior").show();
					id.style.borderColor =  "#E62117";
				}else{
					$("#confirmDtnascMaior").hide();
					$("#confirmDtnasc").show();
					id.style.borderColor =  "#E62117";
				}
				return false;
    			}else{
    				id.style.borderColor = "#ccc";
				$("#confirmDtnasc").hide();
				$("#confirmDtnascMaior").hide();
				return true;
    			}
		}else{
			$("#confirmDtnasc").show();
			id.style.borderColor =  "#E62117";
			return false;
		}
	}
</script>

<!-- Update Status -->
<script type="text/javascript">
	$( function() {
		$(".editarStatus").click(function (e){
			var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
			$("#cpfStatus").val(""+cpf);
			$.getJSON('model/usuariostatus.ajax.php?search=',{cod_estados:cpf,ajax:'true'}, function(est){
				var options;
				for (var i = 0; i < est.length; i++) {
					options += '<option value="' + est[i].idStatus + '">' + est[i].status + '</option>';
				}
				$('#prestadorStatus').html(options).show();
			});
		});
	});
</script>

<!-- AJAX - resgatar Dados -->
<script type="text/javascript">
	function clearUpdateModal(){
		document.getElementById('nome').style.borderColor = "#ccc";
		$("#confirmNome").hide();
		$("#confirmNomeNull").hide();
		document.getElementById('dtnasc').style.borderColor = "#ccc";
		$("#confirmDtnasc").hide();
		document.getElementById('email').style.borderColor = "#ccc";
		$("#confirmEmail").hide();
		$("#confirmEmailNull").hide();
		document.getElementById('cell').style.borderColor = "#ccc";
		document.getElementById('confirmCell').innerHTML = '';
		document.getElementById('sexo').style.borderColor = "#ccc";
		$("#confirmSexo").hide();
	}
	$( function() {
		$(".editarUsuario").click(function (e){
			clearUpdateModal();
			var cpf = $(this).closest('tr').find('td[data-cpf]').data('cpf');
			$.getJSON('model/updateUsuario.ajax.php?search=',{cod_estados: cpf, ajax: 'true'}, function(j){
				var data = j[0].dtnasc;
				var ano = data.substring(0,4);
				var mes = data.substring(5,7);
				var dia = data.substring(8,10);
				j[0].dtnasc = dia+"/"+mes+"/"+ano;
				$("#cpf").val(j[0].cpf);
				$("#nome").val(j[0].nome);
				$("#dtnasc").val(j[0].dtnasc);
				$("#email").val(j[0].email);
				$("#fone").val(j[0].fone);
				$("#cell").val(j[0].cell);
				$("#cep").val(j[0].cep);
				$("#rua").val(j[0].rua);
				$("#numero").val(j[0].numero);
				$("#bairro").val(j[0].bairro);
				$("#complemento").val(j[0].complemento);
				$("#linkedin").val(j[0].linkedin);
				$("#googleplus").val(j[0].googleplus);
				$("#facebook").val(j[0].facebook);
				//sexo
				var optSexo;
				if(j[0].sexo == "Masculino"){
					optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
					optSexo += '<option value="Feminino">Feminino</option>';
				}else{
					optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
					optSexo += '<option value="Masculino">Masculino</option>';
				}
				$('#sexo').html(optSexo).show();
				if(j[0].idCidade != null){
					//estado
					$.getJSON('model/estadopercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(est){
						var options;
						if(j[0].idCidade == null)
							options = '<option value="">-- Escolha um estado -- </option>;'
						for (var i = 0; i < est.length; i++) {
							options += '<option value="' + est[i].cod_cidades + '">' + est[i].nome + '</option>';
						}
						$('#estado').html(options).show();
					});
				}
				if(j[0].idCidade != null){
					//cidade
					$.getJSON('model/cidadespercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(cid){
						var options;
						if(j[0].idCidade == null)
							options = '<option value="">-- Escolha uma cidade -- </option>;'
						for (var i = 0; i < cid.length; i++) {
							options += '<option value="' + cid[i].cod_cidades + '">' + cid[i].nome + '</option>';
						}
						$('#cod_cidades').html(options).show();
					});
				}
			});
		});
	});
</script>

<!-- Submit Form -->
<script>
	$("#Form").submit(function(){
		var nome = document.getElementById('nome');
			if(nome.value != ""){
				$('#confirmNomeNull').hide();
				if(nome.value.length < 11){
					$('#confirmNome').show();
					nome.style.borderColor = "#E62117";
					nome.focus();
					return false;
				}else{
					nome.style.borderColor = "#ccc";
					$('#confirmNome').hide();
				}
			}else{
				nome.style.borderColor = "#E62117";
				nome.focus();
				$('#confirmNomeNull').show();
				return false;
			}
		var cpfFocus = document.getElementById('cpf');
		var valorCPF = document.getElementById('cpf').value;
		var bollCPF = TestaCPF(valorCPF);
			if(!bollCPF){
				cpfFocus.focus();
				return false;
			}
		var dtnasc = document.getElementById('dtnasc');
		var dtnascValid = validateDate();
			if(!dtnascValid){
				dtnasc.focus();
				return false;
			}
		var email = document.getElementById('email');
		var emailValid = validacaoEmail();
			if(!emailValid){
				email.focus();
				return false;
			}
		var cellFocus = document.getElementById('cell');
		var cellLength = lengthCell();
			if(!cellLength){
				cellFocus.style.borderColor = "#E62117";
				cellFocus.focus();
				return false;
			}else{
				cellFocus.style.borderColor = "#ccc";
			}
		var sexoFocus = document.getElementById('sexo');
			if(sexoFocus.options[sexoFocus.selectedIndex].value == ""){
				$('#confirmSexo').show();
				sexoFocus.style.borderColor = "#E62117";
				$('#confirmSexo').color = "#E62117";
				sexoFocus.focus();
				return false;
			}else{
				sexoFocus.style.borderColor = "#ccc	";
				$('#confirmSexo').hide();
			}

		return true;
	});
</script>

</html>