<?php
include 'menu.php';
if(isset($_SESSION["login"])){
	if($_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	window.location.replace('Tela-inicial.php');
</script>";
	}
}
?>
<!--modal redefinir senha-->
<div id="TrocarPasswordIndex" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h1 class="text-center">Redefinir a senha</h1>
			</div>
			<div class="modal-body">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="text-center">
							<p>Insira o email que deseja criar uma senha.</p>
							<div class="panel-body">
								<fieldset>
									<form action="" method="POST">
										<input class="form-control input-lg"  placeholder="E-mail" name="emailUpdate" type="email" required/>
										<br/>
										<input class="btn btn-lg btn-primary btn-block" value="Redefinir Senha" name="formUpdatePassword" type="submit">
									</form>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- fim moldal -->
	<h1 style="text-align: center;top: 2%;position: relative;">Login</h1>
	<hr style="width: 95%;"/>
		<div id="login-tela" class='login-tela'>
			<form method='post' action='' role='login'>
				<img src="images/logo/logo_web.png" width="35%">
				<input type='email' name='email' placeholder="Login / Email" class='form-control input-lg' required/>

				<input type='password' class='form-control input-lg' name='password' placeholder='Senha' required/>

				<?php
				require_once('controller/controllerLogin.php');
				$class = new controllerLogin;
				if(isset($_POST['go'])){
					$email = @$_POST['email'];
					$password = @$_POST['password'];
					$class->VerificarLogin($email,$password);
				}
				?>
				<input type='submit' name='go' class='btn btn-lg btn-primary btn-block' value='Entrar'>
				<div>
					<a href="" data-toggle='modal' data-target='#TrocarPasswordIndex' data-original-title>Esqueceu sua senha ?</a>
				</div>
			</form>
		</div>
</body>
</html>
