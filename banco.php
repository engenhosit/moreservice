<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
	<div id="tela" class="tela">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Banco <small>Cadastro de banco</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-bank"></i> Cadastro
                            </li>
                        </ol>

					<div class="form-group">
						<form method="POST" action="" id="FormBanco">
							<label>Código do Banco *</label>
							<input type="text" id="cod" name="cod" class="form-control" placeholder="Digite o código do banco..." required />
							<br/>
							<label>Nome do Banco *</label>
							<input type="text" id="nome" name="nome" class="form-control" placeholder="Digite o nome do banco aqui..." required />
							<br/>
							<label>Site</label>
							<input type="text" id="site" name="site" class="form-control" placeholder="Digite o site do banco aqui..." />
							<br/>
							<?php
							//SALVAR
							$FormBanco = @$_POST["FormBancoSalvar"];
							if(!empty($FormBanco)){
								require_once("controller/controllerBanco.php");
								$class = new controllerBanco;
								$class->Salvar();
							}
							?>
							<input type="submit" value="Salvar" name="FormBancoSalvar" class="btn btn-default btn-lg">
							<a href="lista-banco.php"  class="btn btn-default btn-lg">Listar</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>