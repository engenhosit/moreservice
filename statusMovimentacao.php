<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
	<div id="tela" class="tela">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Status Movimentações <small>Cadastro de Status de Movimentações</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Cadastro
                            </li>
                        </ol>

					<div class="form-group">
						<form method="POST" action="" id="FormOcupacao">
							<label>Descricão Resumida *</label>
							<input type="text" id="descricao" name="resumida" class="form-control" placeholder="Descricão Resumida" required>
							<br/>
							<label>Descricão Detalhada *</label>
							<input type="text" id="descricao" name="detalhada" class="form-control" placeholder="Descricão Detalhada" required>
							<br/>
							<label>Tipo Status Movimentações:</label>
							<select name="tipoStatus" class="form-control" style="width:50%;">
							<?php
							require_once("controller/controllerStatusMovimentacao.php");
							$class = new controllerStatusMovimentacao;
							$class->ListarTipoStatus();
							?>
							</select>
							<br/>
<?php
//SALVAR
$FormOcupacao = @$_POST["FormStatusMovimentacaoSalvar"];
if(!empty($FormOcupacao)){
		require_once("controller/controllerStatusMovimentacao.php");
		$class = new controllerStatusMovimentacao;
		$class->Salvar();
}
?>
							<input type="submit" value="Salvar" name="FormStatusMovimentacaoSalvar" class="btn btn-default btn-lg">
							<a href="lista-statusMovimentacao.php"  class="btn btn-default btn-lg">Listar</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>