<?php
	include 'menu.php';
	if(!isset($_SESSION["login"])){
		echo"
	<script type='text/javascript'>
		$( function() {

			alert('Por favor, faça o login para acessar as funcionalidades do sistema');
		});
		window.location.replace('index.php');
	</script>";
	}else{
		if(!$_SESSION["login"][1]){
			echo"
	<script type='text/javascript'>
		$( function() {

			alert('Por favor, faça o login para acessar as funcionalidades do sistema');
		});
		window.location.replace('index.php');
	</script>";
		}
	}
?>
<add key="webpages:Enabled" value="true" />
<!-- Load File-->
<script type="text/javascript">
	  var loadFile = function(event) {
		var output = document.getElementById('visualizar');
		output.src = URL.createObjectURL(event.target.files[0]);
	  };
</script>
<!-- CSS hiden -->
<style type="text/css">
	.carregando{
		display:none;
	}
</style>

<div id="tela" class="tela" >
	<div class="container-fluid" >
		<!-- Page Heading -->
		<div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header">
					Prestador <small>Cadastro de prestador</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-edit"></i> Cadastro
					</li>
				</ol>
				<?php
				//SALVAR
				$FormPrestador = @$_POST["FormPrestadorSalvar"];
				if(!empty($FormPrestador)){
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->Salvar();
				}
				?>
				<div class="form-group">
				<form method="POST" action="" id="FormPrestador" enctype="multipart/form-data">
					<div  class="foto-prestador">
						<img src="../img/perfil/default.png" class="img-rounded img-responsive" id="visualizar"/>
						<br/>
						<label>Foto de Perfil *</label>
						<input type="file" name="imagem" id="imgPerfil" onchange="loadFile(event)" accept="image/png, image/jpeg"  multiple/>
						<span id="messImgPerfil" style="display: none; color:#E62117;"> Foto de Perfil é obrigátorio </span>
					</div>
					<br/>
					<label>Nome *</label>
					<input class="form-control" placeholder=" Digite o nome aqui..."  name="nome" id='nome' value="<?php echo @$_POST["nome"]; ?>" />
					<span id="confirmNomeNull" style="display: none; color:#E62117;">  Nome é obrigatório</span>
					<span id="confirmNome" style="display: none; color:#E62117;">  Nome deve conter no mínimo 12 caracteres</span>
					<br/>
					<label>CPF *</label>
					<div class="form-inline">
					<input class="form-control"  name="cpf" id="cpf" maxlength="14" style="width:50%;" value="<?php echo @$_POST["cpf"]; ?>" />
					<span id="validcpf"></span>
					</div>
					<br/>
					<label>Data de Nascimento *</label>
					<input type="text" class="form-control data" id="dtnasc" name="dtnasc" value="<?php echo @$_POST["dtnasc"]; ?>" />
					<span id="confirmDtnasc" style="display: none; color:#E62117;">  Data inválida</span>
					<span id="confirmDtnascMaior" style="display: none; color:#E62117;">  Obrigatório ser maior de 18 anos</span>
					<br/>
					<label>Email *</label>
					<input type="email" class="form-control" placeholder=" Digite o email aqui..." id='email' name="email" value="<?php echo @$_POST["email"]; ?>" />
					<span id="confirmEmailNull" style="display: none; color:#E62117;">  Email é obrigatório</span>
					<span id="confirmEmail" style="display: none; color:#E62117;">  Formato de email incorreto</span>
					<br/>
					<label>Celular *</label>
					<div class="form-inline">
					<input class="form-control" placeholder=" Digite o celular aqui..." id="cell" name="cell" style="width:50%" onkeyup="lengthCell();" value="<?php echo @$_POST["cell"]; ?>" />
					<span id="confirmCell"></span>
					</div>
					<br/>
					<label>Telefone</label>
					<input class="form-control" id="fone" placeholder=" Digite o telefone aqui..." name="fone" value="<?php echo @$_POST["fone"]; ?>" />
					<br/>
					<label class="control-label">Sexo *</label>
					<select name="sexo" id="sexo" class="form-control" style="width:50%;">
						<option value="">-- Escolha o seu sexo -- </option>
						<option value="Masculino">Masculino</option>
						<option value="Feminino">Feminino</option>
					</select>
					<span id="confirmSexo" class="confirmSexo" style="display: none; color:#E62117;">  Sexo é obrigatório</span>
					<br/>
					<label>Cep *</label>
					<div class="form-inline">
					<input class="form-control cep" placeholder=" Digite o CEP aqui..."  name="cep" id="cep" maxlength="9" style="width:50%;" value="<?php echo @$_POST["cep"]; ?>" />
					<span id="validcep"></span>
					</div>
					<span id="confirmCep" class="confirmCep" style="display: none; color:#E62117;">  Cep é obrigatório</span>
					<br/>
					<label>Rua *</label>
					<input class="form-control" id="rua" name="rua" placeholder=" Digite a rua aqui..." value="<?php echo @$_POST["rua"]; ?>"  />
					<span id="confirmRua" class="confirmRua" style="display: none; color:#E62117;">  Rua é obrigatório</span>
					<br/>
					<label>Numero *</label>
					<input class="form-control" id="numero" name="numero" placeholder=" Digite o numero aqui..." value="<?php echo @$_POST["numero"]; ?>"  />
					<span id="confirmNumero" class="confirmNumero" style="display: none; color:#E62117;">  Numero é obrigatório</span>
					<br/>
					<label>Bairro *</label>
					<input class="form-control" placeholder=" Digite o bairro aqui..." id="bairro" name="bairro" value="<?php echo @$_POST["bairro"]; ?>"  />
					<span id="confirmBairro" class="confirmBairro" style="display: none; color:#E62117;">  Bairro é obrigatório</span>
					<br/>
					<label>Complemento</label>
					<textarea class="form-control" id="complemento" placeholder=" Digite o complento do endereço aqui..."  name="complemento"><?php echo @$_POST["complemento"]; ?></textarea>
					<br/>
					<div class="form-inline">
					<label>Estado *</label>
					<select class="form-control" id="estado" name="estado" style="width:50%;">
						<option value="">-- Escolha um estado --</option>
					<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarEstado();
					?>
					</select>
					</div>
					<span id="confirmEstado" class="confirmEstado" style="display: none; color:#E62117;">  Estado é obrigatório</span>
					<br/>
					<div class="form-inline">
					<label>Cidade *</label>
					<span class="carregando">Aguarde, carregando...</span>
					<select class="form-control" name="cidade" id="cod_cidades" style="width:50%;">
						<option value="">-- Escolha uma cidade --</option>
					</select>
					</div>
					<span id="confirmCidade" class="confirmCidade" style="display: none; color:#E62117;">  Cidade é obrigatório</span>
					<br/>
					<label>linkedin</label>
					<input class="form-control" id="linkedin" name="linkedin" placeholder=" Digite o linkedin aqui..." type="email" value="<?php echo @$_POST["linkedin"]; ?>" />
					<br/>
					<label>googleplus</label>
					<input class="form-control" name="googleplus" id="googleplus" placeholder=" Digite o googleplus aqui..." type="email" value="<?php echo @$_POST["googleplus"]; ?>" />
					<br/>
					<label>Facebook</label>
					<input class="form-control" name="facebook" id="facebook" placeholder=" Digite o Facebook aqui..." type="email" value="<?php echo @$_POST["facebook"]; ?>" />
					<br/>
					<label>Escolaridade *</label>
					<select name="escolaridade" id="escolar" class="form-control" style="width:50%;">
						<option value="" >-- Escolha uma Escolaridade --</option>
					<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarEscolaridade();
					?>
					</select>
					<span id="confirmEscolaridade" class="confirmEscolaridade" style="display: none; color:#E62117;">  Escolaridade é obrigatório</span>
					<br/>
					<label class="control-label">Ocupação *</label>
					<select name="ocupacao" id="ocup" class="form-control" style="width:50%;">
						<option value="">-- Escolha uma Ocupação --</option>
					<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarOcupacao();
					?>
					</select>
					<span id="confirmOcupacao" class="confirmOcupacao" style="display: none; color:#E62117;">  Ocupação é obrigatório</span>
					<br/>
					<label>Instituição</label>
					<input type="text" name="instituicao" class="form-control" placeholder=" Digite a Instituição aqui..." value="<?php echo @$_POST["instituicao"]; ?>" />
					<br/>
					<label>Curso</label>
					<input type="text" name="curso" class="form-control" placeholder=" Digite o Curso aqui..." value="<?php echo @$_POST["curso"]; ?>" />
					<br/>
					<label>Perfil</label>
					<textarea name="perfil" class="form-control" placeholder=" Digite uma breve descrição aqui..."><?php echo @$_POST["perfil"]; ?></textarea>
					<br/>
					<!-- Contas -->
						<hr/>
						<label>Contas</label>
						<br/><br/>
						<label>Tipo Conta *</label>
						<select name="tpconta" id="tpconta" class="form-control" style="width:50%;">
							<option value="">-- Escolha o Tipo de conta --</option>
						<?php
						require_once("controller/controllerPrestador.php");
						$class = new controllerPrestador;
						$class->ListarTipoConta();
						?>
						</select>
						<br/>
						<label for="banco" class="control-label">Banco *</label>
						<select name="banco" id="banco" class="form-control" style="width:50%;">
							<option value="">-- Escolha o seu Banco --</option>
						<?php
						require_once("controller/controllerPrestador.php");
						$class = new controllerPrestador;
						$class->ListarBanco();
						?>
						</select>
						<br/>
						<div class="form-inline">
						<label>Agencia *</label>
						<input type="text" name="agencia" id="agencia" class="form-control" style="width:30%" maxlength="4" />
						&emsp;
						<label>Numero da Conta *</label>
						<input style="width:25%" name="conta" id="conta" placeholder=" Digite sua conta aqui..."  maxlength="20" class="form-control" />
						</div>
						<br/>
						<div class="form-inline">
						<label>Digito verificador *</label>
						<input type="text" name="verificador" id="verificador" class="form-control" style="width:20%" maxlength="3" />
					select_perfil	&emsp;&emsp;
						<input type="checkbox" id="padrao" value="true" name="conta_Padrao" /> <strong>Conta Padrão</strong>
						</div>
						<br/>
						<a id="insertConta" onclick="inserirConta();" class="btn btn-default btn-md">Inserir</a>
						&emsp;
						<span id="confirmCamposConta" class="confirmVerificador" style="display: none; color:#E62117;">  Preencha todos os campos em vermelho (obrigatório)</span>
						<span id="messError" ></span>
						<br/>
						<br/>
						<table class='table table-hover' id='TabelaConta'>
							<tr>
								<th>Tipo da conta</th>
								<th>Banco</th>
								<th>Agencia</th>
								<th>Digito verificador</th>
								<th>Numero da conta</th>
								<th>Conta Padrão</th>
								<th>Excluir</th>
							</tr>
						</table>
						<hr/>
					<!-- -->
					<!-- Documentos -->
						<hr/>
						<label>Documentos</label>
						<br/><br/>
							<label class="control-label">Tipo de Documento *</label>
							<select name="doc" id="docType" class="form-control" style="width:50%;">
								<option value="">-- Escolha um tipo de documento --</option>
							<?php
							require_once("controller/controllerPrestador.php");
							$class = new controllerPrestador;
							$class->ListarTipoDocumento();
							?>
							</select>
							<span id="confirmTpdoc" class="confirmTpdoc" style="display: none; color:#E62117;">  Tipo de Documento é obrigatório</span>
						<div class="carregando" id="numDoc">
							<br/>
							<label>Numero do Documento *</label>
							<input type="text" id="numeroDoc" name="numeroDoc" placeholder=" Digite o numero do documento aqui..." class="form-control" >
							<span id="confirmNdoc" class="confirmNdoc" style="display: none; color:#E62117;">  Numero do Documento é obrigatório</span>
						</div>
						<div class="carregando" id="tipoDocs">
							<br/>
							<label>Orgão Emissor *</label>
							<input type="text" name="emissor" id="orgao" placeholder=" Digite o orgão emissor" class="form-control" placeholder="Conta Corrente">
							<span id="confirmOrgao" class="confirmOrgao" style="display: none; color:#E62117;">  Orgão Emissor é obrigatório</span>
							<br/>
							<label>Estado *</label>
							<select class="form-control" name="estadoDoc" id="estadoDoc" style="width:50%;">
								<option value="">-- Escolha um estado --</option>
							<?php
							require_once("controller/controllerPrestador.php");
							$class = new controllerPrestador;
							$class->ListarEstado();
							?>
							</select>
							<span id="confirmEstadoDoc" class="confirmEstadoDoc" style="display: none; color:#E62117;">  Estado é obrigatório</span>
						</div>
							<br/>
							<label>Imagem Frente *</label>
							<input type="file" id="frente" name="docFrente" accept="image/png, image/jpeg"  multiple />
							<span id="confirmFrente" class="confirmFrente" style="display: none; color:#E62117;">  Imagem Frente é obrigatório</span>
							<br/>
							<label>Imagem Trás *</label>
							<input type="file" id="tras" name="docTras" accept="image/png, image/jpeg"  multiple />
							<span id="confirmTras" class="confirmTras" style="display: none; color:#E62117;">  Imagem Trás é obrigatório</span>
							<br/>
							<a id="insertDoc" onclick="inserirDoc();" class="btn btn-default btn-md">Inserir</a>
							&emsp;
							<span id="docsError" ></span>
							<br/>
							<br/>
							<table class='table table-hover' id='TabelaDocumento'>
								<tr>
									<th>Tipo do documento</th>
									<th>Numero documento</th>
									<th>Orgão Emissor</th>
									<th>Estado</th>
									<th>Imagem Frente</th>
									<th>Imagem Trás</th>
									<th>Excluir</th>
								</tr>
							</table>
						<hr/>
					<!-- -->
					<label>Tipo de Serviços *</label>
					<br/>
					<span id="tpserv" style="display: none; color:#E62117;"> Selecione um tipo de serviço (Obrigátorio) </span>
					<div class="funkyradio">
						<?php
						require_once("controller/controllerPrestador.php");
						$class = new controllerPrestador;
						$class->ListarTipoServico();
						?>
					</div>
					<br>
					<label>Senha *</label>
					<input class="form-control" placeholder=" Digite a senha aqui..." name="pass1" id="pass1" type="password" onkeyup="lengthPass();" />
					<span id="confirmPass" class="confirmPass"></span>
					<br/>
					<label>Confirmar Senha *</label>
					<input class="form-control" placeholder=" Repita a senha" name="pass2" id="pass2" onkeyup="checkPass(); return false;" type="password" />
					<span id="confirmMessage" class="confirmMessage"></span>
					<br>
					<label>Disponibilidade</label>
					<br/>
					<a class="btn btn-default btn-md" data-toggle='modal' data-target='#modalDisponibilidade' data-original-title>Minha Disponibilidade</a>
					<span id="messDisponibilidade" style="display: none; color:#E62117;"> Preencha todos os campos de disponibilidade em vermelho (Obrigatório)</span>
					<br/><br/>
					<!-- modal Disponibilidade -->
					<div class='modal fade' id='modalDisponibilidade' tabindex='-1'>
						<div class='modal-dialog' >
							<div class='panel panel-primary'>
								<div class='panel-heading'>
									<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
									<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Minha Disponibilidade</h4>
								</div>
								<div class='modal-body'>
									<span id="messDisponibilidadeHora" style="display: none; color:#E62117;"> Preencha todos os campos de Hora em vermelho no formato HH:mm (Obrigatório)</span>
									<br/>
									<input type="checkbox" id="segunda" value="true" name="segunda" /> <strong>Segunda</strong>
									<br/>
									<div id="segundaDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control"  placeholder="hh:mm" type="text" name="segundaInicio" id='segInicio'>
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control"  placeholder="hh:mm" type="text" name="segundaFim" id="segFim">
											</td>
										</tr>
									</table>
										<span id="mesSegHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
									<input type="checkbox" id="terca" value="true" name="terca" /> <strong>Terça</strong>
									<br/>
									<div id="tercaDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control" placeholder="hh:mm" type="text" name="tercaInicio" id="terInicio">
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control" placeholder="hh:mm" type="text" name="tercaFim" id="terFim">
											</td>
										</tr>
									</table>
										<span id="mesTerHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
									<input type="checkbox" id="quarta" value="true" name="quarta" /> <strong>Quarta</strong>
									<br/>
									<div id="quartaDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control" placeholder="hh:mm" type="text" name="quartaInicio" id="quaInicio">
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control" placeholder="hh:mm" type="text" name="quartaFim" id='quaFim'>
											</td>
										</tr>
									</table>
										<span id="mesQuaHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
									<input type="checkbox" id="quinta" value="true" name="quinta" /> <strong>Quinta</strong>
									<br/>
									<div id="quintaDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control" placeholder="hh:mm" type="text" name="quintaInicio" id='quiInicio'>
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control" placeholder="hh:mm" type="text" name="quintaFim" id='quiFim'>
											</td>
										</tr>
									</table>
										<span id="mesQuiHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
									<input type="checkbox" id="sexta" value="true" name="sexta" /> <strong>Sexta</strong>
									<br/>
									<div id="sextaDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control" placeholder="hh:mm" type="text" name="sextaInicio" id='sexInicio'>
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control" placeholder="hh:mm" type="text" name="sextaFim" id='sexFim'>
											</td>
										</tr>
									</table>
										<span id="mesSexHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
									<input type="checkbox" id="sabado" value="true" name="sabado" /> <strong>Sábado</strong>
									<br/>
									<div id="sabadoDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control" placeholder="hh:mm" type="text" name="sabadoInicio" id='sabInicio'>
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control" placeholder="hh:mm" type="text" name="sabadoFim" id='sabFim'>
											</td>
										</tr>
									</table>
										<span id="mesSabHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
									<input type="checkbox" id="domingo" value="true" name="domingo" /> <strong>Domingo</strong>
									<br/>
									<div id="domingoDiv" class="form-inline" style="display: none;">
									<table>
										<tr>
											<td>
												<label>Hora Início *</label>
												<input  class="hora form-control" placeholder="hh:mm" type="text" name="domingoInicio" id='domInicio'>
											</td>
											<td>
												<label>Hora Fim *</label>
												<input class="hora form-control" placeholder="hh:mm" type="text" name="domingoFim" id='domFim'>
											</td>
										</tr>
									</table>
										<span id="mesDomHora" style="display: none; color:#E62117;"></span>
									</div>
									<br/>
								<div class='panel-footer' style='margin-bottom:0px;'>
									<button type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
								</div>
								</div>
							</div>
						</div>
					</div>
					<label class="control-label">Tipo de Usuário:</label>
					<select name="usuario" id="usuario" class="form-control" style="width:50%;">
					<?php
					require_once("controller/controllerPrestador.php");
					$class = new controllerPrestador;
					$class->ListarTipoUsuario();
					?>
					</select>
					<br/>
						<input type="submit" value="Salvar" name="FormPrestadorSalvar" class="btn btn-default btn-lg">
						<a href="lista-prestador.php"  class="btn btn-default btn-lg">Listar</a>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

<script type="text/javascript" src="http://pastebin.com/raw.php?i=gE1SbXKA"></script>
<script type="text/javascript" src="http://pastebin.com/raw.php?i=8xvHJBJf" ></script>
<script type="text/javascript" src="http://pastebin.com/raw.php?i=YRhQfUJs"></script>

<!-- Mask -->
<script type="text/javascript">
	$( function() {

		$(".hora").inputmask("h:s",{ placeholder: "hh/mm" });

		$("#agencia").mask("0000", {placeholder: "0000"});

		$("#verificador").mask('000',{placeholder: '000'});

		$('#conta').mask('00000000000000000000',{placeholder: '00000000000000000000'});

		$("#numeroDoc").mask('0000000000000');

		$(".data").mask("00/00/0000",{placeholder: "dd/mm/aaaa"});

		$("#cpf").mask("999.999.999-99", {placeholder: "999.999.999-99"});

		$(".cep").mask('00000-000',{placeholder: '00000-000'});

		$('#fone').mask('(00)0000-0000',{placeholder: '(00)0000-0000'});

		$('#cell').mask("(00)00000-0000",{placeholder: "(00)00000-0000"});
	});
</script>

<!-- Validar CPF -->
<script type="text/javascript">
	//retirar os caracteres do cpf
	function RetiraCaracteresInvalidos(strCPF){
		var strTemp;
		strTemp = strCPF.replace(".", "");
		strTemp = strTemp.replace(".", "");
		strTemp = strTemp.replace("-", "");

		return strTemp;
	}
	//testar o cpf do usuario
	function TestaCPF(strCPF) {
		var Soma;
		var Resto;
		Soma = 0;
		strCPF = RetiraCaracteresInvalidos(strCPF);
		for(i=0; i < 10; i++){
			if (strCPF == ""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i+""+i)
				return false;
		}
		for (i=1; i<=9; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10)) )
			return false;
		Soma = 0;
		for (i = 1; i <= 10; i++)
			Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11) ) )
			return false;

		return true;
	}
	$(function() {
		$("#cpf").blur(function(){
			var strCPF = document.getElementById('cpf').value;
			strCPF = RetiraCaracteresInvalidos(strCPF);
			var tamanho = strCPF.length;
			var cpf = document.getElementById('cpf');
			var message = document.getElementById('validcpf');
			var goodColor = "#66cc66";
			var badColor = "#E62117";

			if(tamanho == 11){
				if(TestaCPF(strCPF)){
			 		$.getJSON('model/resgatarUsuario.ajax.php?search=',{cod_estados:strCPF,ajax:'true'}, function(j){
						if(j[0].sucess){
							var data = j[0].dtnasc;
							var ano = data.substring(0,4);
							var mes = data.substring(5,7);
							var dia = data.substring(8,10);
							j[0].dtnasc = dia+"/"+mes+"/"+ano;
							$("#cpf").val(j[0].cpf);
							$("#nome").val(j[0].nome);
							$("#dtnasc").val(j[0].dtnasc);
							$("#email").val(j[0].email);
							$("#fone").val(j[0].fone);
							$("#cell").val(j[0].cell);
							$("#cep").val(j[0].cep);
							$("#rua").val(j[0].rua);
							$("#numero").val(j[0].numero);
							$("#bairro").val(j[0].bairro);
							$("#complemento").val(j[0].complemento);
							$("#linkedin").val(j[0].linkedin);
							$("#googleplus").val(j[0].googleplus);
							$("#facebook").val(j[0].facebook);
							//sexo
							var optSexo;
							if(j[0].sexo == "Masculino"){
								optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
								optSexo += '<option value="Feminino">Feminino</option>';
							}else{
								optSexo += '<option value="' + j[0].sexo + '">' + j[0].sexo + '</option>';
								optSexo += '<option value="Masculino">Masculino</option>';
							}
							$('#sexo').html(optSexo).show();
							if(j[0].idCidade != null){
								//estado
								$.getJSON('model/estadopercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(est){
									var options;
									for (var i = 0; i < est.length; i++) {
										options += '<option value="' + est[i].cod_cidades + '">' + est[i].nome + '</option>';
									}
									$('#estado').html(options).show();
								});
							}
							if(j[0].idCidade != null){
								//cidade
								$.getJSON('model/cidadespercod.ajax.php?search=',{cod_estados: j[0].idCidade, ajax: 'true'}, function(cid){
									var options;
									for (var i = 0; i < cid.length; i++) {
										options += '<option value="' + cid[i].cod_cidades + '">' + cid[i].nome + '</option>';
									}
									$('#cod_cidades').html(options).show();
								});
							}
						}else{

						}
					});

					cpf.style.borderColor = goodColor;
					message.style.color = goodColor;
					message.innerHTML = "&emsp;CPF válido!";
				}else{
					cpf.style.borderColor = badColor;
					message.style.color = badColor;
					message.innerHTML = "&emsp;CPF inválido!";
				}
			}else{
				cpf.style.borderColor = badColor;
				message.style.color = badColor;
				message.innerHTML = "&emsp;CPF falta caracter!";
			}

		});
	});
</script>

<!--Validar Celular-->
<script type="text/javascript">
	function lengthCell(){
		//Store the password field objects into variables ...
		var cell = document.getElementById('cell');
		//Store the Confimation Message Object ...
		var mens = document.getElementById('confirmCell');
		//Set the colors we will be using ...
		var colorGood = "#66cc66";
		var colorBad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field
		if(cell.value != ""){
			if(cell.value.length < 13){
				cell.style.borderColor = colorBad;
				mens.style.color = colorBad;
				mens.innerHTML = "Celular Incorreto";
				return false;
			}else{
				cell.style.borderColor = colorGood;
				mens.style.color = colorGood;
				mens.innerHTML = "Celular Correto";
				return true;
			}
		}else{
			cell.style.borderColor = colorBad;
			mens.style.color = colorBad;
			mens.innerHTML = "Celular é obrigatório";
			return false;
		}
	}
</script>

<!--Validar Senha-->
<script type="text/javascript">
	function lengthPass(){
		//Store the password field objects into variables ...
		var pass = document.getElementById('pass1');
		//Store the Confimation Message Object ...
		var mess = document.getElementById('confirmPass');
		//Set the colors we will be using ...
		var good = "#66cc66";
		var bad = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass.value.length < 6){
			pass.style.borderColor = bad;
			mess.style.color = bad;
			mess.innerHTML = "Senha deve conter no minimo 6 caracteres";
			return false;
		}else{
			pass1.style.borderColor = good;
			mess.style.color = good;
			mess.innerHTML = "Senha ok";
			return true;
		}
	}

	function checkPass()
	{
		//Store the password field objects into variables ...
		var pass1 = document.getElementById('pass1');
		var pass2 = document.getElementById('pass2');
		//Store the Confimation Message Object ...
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#66cc66";
		var badColor = "#E62117";
		//Compare the values in the password field
		//and the confirmation field

		if(pass1.value == pass2.value){
			//The passwords match.
			//Set the color to the good color and inform
			//the user that they have entered the correct password
			pass2.style.borderColor = goodColor;
			message.style.color = goodColor;
			message.innerHTML = "Senhas Iguais!";
			return true;
		}else{
			//The passwords do not match.
			//Set the color to the bad color and
			//notify the user.
			pass2.style.borderColor = badColor;
			message.style.color = badColor;
			message.innerHTML = "As senhas informadas não são iguais!";
			return false;
		}
	}
</script>

<!-- Validação de cep -->
<script type="text/javascript" >
        $(function() {
        	function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#cep").val("");
				$("#rua").val("");
                $("#bairro").val("");
				var opt = '<option value="">-- Escolha uma cidade --</option>';
				$('#cod_cidades').html(opt).show();
				$.getJSON('model/estados.ajax.php?search=',{ajax: 'true'}, function(j){
					var options = '<option value="">-- Escolha um estado --</option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
					}
					$('#estado').html(options).show();
				});

            }

            //Quando o campo cep perde o foco.
            $("#cep").blur(function (){
				var ceptxt = document.getElementById("cep");
				var message = document.getElementById('validcep');
				var goodColor = "#66cc66";
				var badColor = "#E62117";

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/-/, "");

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("carregando...");
                        $("#bairro").val("carregando...");
                        $("#cidade").val("carregando...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);

							$('#cod_cidades').hide();
							$('.carregando').show();

							$.getJSON('model/estadoperuf.ajax.php?search=',{cod_estados: dados.uf, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#estado').html(options).show();
								$('.carregando').hide();
							});

							$.getJSON('model/cidadespernome.ajax.php?search=',{cod_estados: dados.localidade, ajax: 'true'}, function(j){
								var options;
								for (var i = 0; i < j.length; i++) {
									options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
								}
								$('#cod_cidades').html(options).show();
								$('.carregando').hide();
							});

								ceptxt.style.borderColor = goodColor;
								message.style.color = goodColor;
								message.innerHTML = "&emsp;CEP encontrado!";
								return true;
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
								ceptxt.style.borderColor = badColor;
								message.style.color = badColor;
								message.innerHTML = "&emsp;CEP não encontrado!";
								limpa_formulário_cep();
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
						ceptxt.style.borderColor = badColor;
						message.style.color = badColor;
						message.innerHTML = "&emsp;Formato de CEP inválido!";
						limpa_formulário_cep();
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
</script>

<!-- Validação do email -->
<script language="Javascript">
	function validacaoEmail() {
		var email = document.getElementById('email');
		usuario = email.value.substring(0, email.value.indexOf("@"));
		dominio = email.value.substring(email.value.indexOf("@")+ 1, email.value.length);
		if(email.value != ""){
			$("#confirmEmail").hide();
			if ((usuario.length >=1) &&
			    (dominio.length >=3) &&
			    (usuario.search("@")==-1) &&
			    (dominio.search("@")==-1) &&
			    (usuario.search(" ")==-1) &&
			    (dominio.search(" ")==-1) &&
			    (dominio.search(".")!=-1) &&
			    (dominio.indexOf(".") >=1)&&
			    (dominio.lastIndexOf(".") < dominio.length - 1)) {
				email.style.borderColor = "#ccc";
				$("#confirmEmailNull").hide();
				return true;
			}else{
				email.style.borderColor = "#E62117";
				$("#confirmEmailNull").show();
				return false;
			}
		}else{
			$("#confirmEmailNull").show();
			email.style.borderColor = "#E62117";
			email.focus();
			return false;
		}
	}
</script>

<!-- Validação da data -->
<script type="text/javascript">
	function validateDate() {
		var id = document.getElementById('dtnasc');
		var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
		if ((id.value.match(RegExPattern)) && (id.value!='')) {
			var data = id.value;
			var dia = data.substring(0,2);
			var mes = data.substring(3,5);
			var ano = data.substring(6,10);

			//resgatar a data atual
			var dataAtual = new Date();
			var anoAtual = parseInt(dataAtual.getFullYear());
			//Criando um objeto Date usando os valores ano, mes e dia.
    			var novaData = new Date(ano,(mes-1),dia);

    			if( ano < 1900 || ano > anoAtual || (anoAtual - ano) < 18 ){
    				if((anoAtual - ano) < 18){
    					$("#confirmDtnasc").hide();
	    				$("#confirmDtnascMaior").show();
					id.style.borderColor =  "#E62117";
				}else{
					$("#confirmDtnascMaior").hide();
					$("#confirmDtnasc").show();
					id.style.borderColor =  "#E62117";
				}
				return false;
    			}else{
    				id.style.borderColor = "#ccc";
				$("#confirmDtnasc").hide();
				$("#confirmDtnascMaior").hide();
				return true;
    			}
		}else{
			$("#confirmDtnasc").show();
			id.style.borderColor =  "#E62117";
			return false;
		}
	}
</script>

<!-- Submit Form Prestador -->
<script>
	$("#segunda").on("click",function(){
		$("#segundaDiv").toggle();
		$("#segInicio").val("");
		$("#segInicio").css("border-color","#ccc");
		$("#segFim").val("");
		$("#segFim").css("border-color","#ccc");
	});
	$("#terca").on("click",function(){
		$("#tercaDiv").toggle();
		$("#terInicio").val("");
		$("#terInicio").css("border-color","#ccc");
		$("#terFim").val("");
		$("#terFim").css("border-color","#ccc");
	});
	$("#quarta").on("click",function(){
		$("#quartaDiv").toggle();
		$("#quaInicio").val("");
		$("#quaInicio").css("border-color","#ccc");
		$("#quaFim").val("");
		$("#quaFim").css("border-color","#ccc");
	});
	$("#quinta").on("click",function(){
		$("#quintaDiv").toggle();
		$("#quiInicio").val("");
		$("#quiInicio").css("border-color","#ccc");
		$("#quiFim").val("");
		$("#quiFim").css("border-color","#ccc");
	});
	$("#sexta").on("click",function(){
		$("#sextaDiv").toggle();
		$("#sexInicio").val("");
		$("#sexInicio").css("border-color","#ccc");
		$("#sexFim").val("");
		$("#sexFim").css("border-color","#ccc");
	});
	$("#sabado").on("click",function(){
		$("#sabadoDiv").toggle();
		$("#sabInicio").val("");
		$("#sabInicio").css("border-color","#ccc");
		$("#sabFim").val("");
		$("#sabFim").css("border-color","#ccc");
	});
	$("#domingo").on("click",function(){
		$("#domingoDiv").toggle();
		$("#domInicio").val("");
		$("#domInicio").css("border-color","#ccc");
		$("#domFim").val("");
		$("#domFim").css("border-color","#ccc");
	});
	$(function() {
		$("#FormPrestador").submit(function(){
			var img = document.getElementById("imgPerfil");
				if(img.value == ""){
					$("#messImgPerfil").show();
					img.focus();
					return false;
				}else{
					$("#messImgPerfil").hide();
				}

			var nome = document.getElementById('nome');
				if(nome.value != ""){
					$('#confirmNomeNull').hide();
					if(nome.value.length < 11){
						$('#confirmNome').show();
						nome.style.borderColor = "#E62117";
						nome.focus();
						return false;
					}else{
						nome.style.borderColor = "#ccc";
						$('#confirmNome').hide();
					}
				}else{
					nome.style.borderColor = "#E62117";
					nome.focus();
					$('#confirmNomeNull').show();
				}
			var cpfFocus = document.getElementById('cpf');
			var valorCPF = document.getElementById('cpf').value;
			var bollCPF = TestaCPF(valorCPF);
				if(!bollCPF){
					cpfFocus.focus();
					return false;
				}
			var dtnasc = document.getElementById('dtnasc');
			var dtnascValid = validateDate();
				if(!dtnascValid){
					dtnasc.focus();
					return false;
				}
			var email = document.getElementById('email');
			var emailValid = validacaoEmail();
				if(!emailValid){
					email.focus();
					return false;
				}
			var cellFocus = document.getElementById('cell');
			var cellLength = lengthCell();
				if(!cellLength){
					cellFocus.style.borderColor = "#E62117";
					cellFocus.focus();
					return false;
				}else{
					cellFocus.style.borderColor = "#ccc";
				}
			var sexoFocus = document.getElementById('sexo');
				if(sexoFocus.options[sexoFocus.selectedIndex].value == ""){
					$('#confirmSexo').show();
					sexoFocus.style.borderColor = "#E62117";
					$('#confirmSexo').color = "#E62117";
					sexoFocus.focus();
					return false;
				}else{
					sexoFocus.style.borderColor = "#ccc	";
					$('#confirmSexo').hide();
				}
			var cep = document.getElementById("cep");
				if(cep.value == ""){
					$('#confirmCep').show();
					cep.style.borderColor = "#E62117";
					cep.focus();
					return false;
				}else{
					cep.style.borderColor = "#ccc";
					$('#confirmCep').hide();
				}
			var rua = document.getElementById("rua");
				if(rua.value == ""){
					$('#confirmRua').show();
					rua.style.borderColor = "#E62117";
					rua.focus();
					return false;
				}else{
					rua.style.borderColor = "#ccc";
					$('#confirmRua').hide();
				}
			var numero = document.getElementById("numero");
				if(numero.value == ""){
					$('#confirmNumero').show();
					numero.style.borderColor = "#E62117";
					numero.focus();
					return false;
				}else{
					numero.style.borderColor = "#ccc";
					$('#confirmNumero').hide();
				}
			var bairro = document.getElementById("bairro");
				if(bairro.value == ""){
					$('#confirmBairro').show();
					bairro.style.borderColor = "#E62117";
					bairro.focus();
					return false;
				}else{
					bairro.style.borderColor = "#ccc";
					$('#confirmBairro').hide();
				}
			var cidade = document.getElementById("cod_cidades");
				if(cidade.options[cidade.selectedIndex].value == ""){
					$('#confirmCidade').show();
					cidade.style.borderColor = "#E62117";
					cidade.focus();
					return false;
				}else{
					cidade.style.borderColor = "#ccc";
					$('#confirmCidade').hide();
				}
			var escolar = document.getElementById("escolar");
				if(escolar.options[escolar.selectedIndex].value == ""){
					$('#confirmEscolaridade').show();
					escolar.style.borderColor = "#E62117";
					escolar.focus();
					return false;
				}else{
					escolar.style.borderColor = "#ccc";
					$('#confirmEscolaridade').hide();
				}
			var ocupacao = document.getElementById("ocup");
				if(ocupacao.options[ocupacao.selectedIndex].value == ""){
					$('#confirmOcupacao').show();
					ocupacao.style.borderColor = "#E62117";
					ocupacao.focus();
					return false;
				}else{
					ocupacao.style.borderColor = "#ccc";
					$('#confirmOcupacao').hide();
				}
			var baba = document.getElementById("babas");
			var idoso = document.getElementById("idosos");
			var limpeza = document.getElementById("limpeza");
				if(!(baba.checked || idoso.checked || limpeza.checked)){
					$("#tpserv").show();
					return false;
				}else{
					$("#tpserv").hide();
				}
			var bollSenha1 = lengthPass();
			var senha1Focus = document.getElementById('pass1');
				if(!bollSenha1){
					senha1Focus.focus();
					return false;
				}
			var bollSenha2 = checkPass();
			var senha2Focus = document.getElementById('pass2');
				if(!bollSenha2){
					senha2Focus.focus();
					return false;
				}



			var flag = true;
			var segunda = document.getElementById('segunda').checked;
			if(segunda){
				$("#mesSegHora").hide();
				document.getElementById("segInicio").style.borderColor = "#ccc";
				document.getElementById("segFim").style.borderColor = "#ccc";
				var segIni = document.getElementById("segInicio");
				var segIniVal = document.getElementById("segInicio").value;
					if(segIniVal == "" || segIniVal.length < 5 ){
						segIni.style.borderColor = "#E62117";
						flag = false;
					}else{
						segIni.style.borderColor = "#ccc";
					}
				var segFim = document.getElementById("segFim");
				var segFimVal = document.getElementById("segFim").value;
					if(segFimVal == "" || segFimVal.length < 5){
						segFim.style.borderColor = "#E62117";
						flag = false;
					}else{
						segFim.style.borderColor = "#ccc";
					}

					if(segIniVal.length == 5 && segFimVal.length == 5 ){
						var horaIni = parseInt(segIniVal.substring(0,2));
						var horaFim = parseInt(segFimVal.substring(0,2));
						if(horaFim < horaIni){
							segIni.style.borderColor = "#E62117";
							segFim.style.borderColor = "#E62117";
							document.getElementById('mesSegHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
							$("#mesSegHora").show();
							flag = false;
						}else if(horaIni == horaFim){
							var minIni = parseInt(segIniVal.substring(3,5));
							var minFim = parseInt(segFimVal.substring(3,5));
							if(minFim < minIni){
								segIni.style.borderColor = "#E62117";
								segFim.style.borderColor = "#E62117";
								document.getElementById('mesSegHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
								$("#mesSegHora").show();
								flag = false;
							}else if(minIni == minFim){
								segIni.style.borderColor = "#E62117";
								segFim.style.borderColor = "#E62117";
								document.getElementById('mesSegHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
								$("#mesSegHora").show();
								flag = false;
							}
						}
					}
				}
			var terca = document.getElementById('terca').checked;
			if(terca){
				document.getElementById("terInicio").style.borderColor = "#ccc";
				document.getElementById("terFim").style.borderColor = "#ccc";
				$("#mesTerHora").hide();
				var terIni = document.getElementById("terInicio");
				var terIniVal = document.getElementById("terInicio").value;
				if(terIniVal == "" || terIniVal.length < 5){
					terIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					terIni.style.borderColor = "#ccc";
				}
				var terFim = document.getElementById("terFim");
				var terFimVal = document.getElementById("terFim").value;
				if(terFimVal == "" || terFimVal.length < 5){
					terFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					terFim.style.borderColor = "#ccc";
				}

				if(terIniVal.length == 5 && terFimVal.length == 5 ){
					var horaIni = parseInt(terIniVal.substring(0,2));
					var horaFim = parseInt(terFimVal.substring(0,2));
					if(horaFim < horaIni){
						terIni.style.borderColor = "#E62117";
						terFim.style.borderColor = "#E62117";
						document.getElementById('mesTerHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesTerHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						var minIni = parseInt(terIniVal.substring(3,5));
						var minFim = parseInt(terFimVal.substring(3,5));
						if(minFim < minIni){
							terIni.style.borderColor = "#E62117";
							terFim.style.borderColor = "#E62117";
							document.getElementById('mesTerHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesTerHora").show();
							flag = false;
						}else if(minIni == minFim){
							terIni.style.borderColor = "#E62117";
							terFim.style.borderColor = "#E62117";
							document.getElementById('mesTerHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesTerHora").show();
							flag = false;
						}
					}
				}
			}
			var quarta = document.getElementById('quarta').checked;
			if(quarta){
				document.getElementById("quaInicio").style.borderColor = "#ccc";
				document.getElementById("quaFim").style.borderColor = "#ccc";
				$("#mesQuaHora").hide();
				var quaIni = document.getElementById("quaInicio");
				var quaIniVal = document.getElementById("quaInicio").value;
				if(quaIniVal == "" || quaIniVal.length < 5){
					quaIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					quaIni.style.borderColor = "#ccc";
				}
				var quaFim = document.getElementById("quaFim");
				var quaFimVal = document.getElementById("quaFim").value;
				if(quaFimVal == "" || quaFimVal.length < 5){
					quaFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					quaFim.style.borderColor = "#ccc";
				}


				if(quaIniVal.length == 5 && quaFimVal.length == 5 ){
					var horaIni = parseInt(quaIniVal.substring(0,2));
					var horaFim = parseInt(quaFimVal.substring(0,2));
					if(horaFim < horaIni){
						quaIni.style.borderColor = "#E62117";
						quaFim.style.borderColor = "#E62117";
						document.getElementById('mesQuaHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesQuaHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						var minIni = parseInt(quaIniVal.substring(3,5));
						var minFim = parseInt(quaFimVal.substring(3,5));
						if(minFim < minIni){
							quaIni.style.borderColor = "#E62117";
							quaFim.style.borderColor = "#E62117";
							document.getElementById('mesQuaHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesQuaHora").show();
							flag = false;
						}else if(minIni == minFim){
							quaIni.style.borderColor = "#E62117";
							quaFim.style.borderColor = "#E62117";
							document.getElementById('mesQuaHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesQuaHora").show();
							flag = false;
						}
					}
				}
			}
			var quinta = document.getElementById('quinta').checked;
			if(quinta){
				document.getElementById("quiInicio").style.borderColor = "#ccc";
				document.getElementById("quiFim").style.borderColor = "#ccc";
				$("#mesQuiHora").hide();
				var quiIni = document.getElementById("quiInicio");
				var quiIniVal = document.getElementById("quiInicio").value;
				if(quiIniVal == "" || quiIniVal.length < 5){
					quiIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					quiIni.style.borderColor = "#ccc";
				}
				var quiFim = document.getElementById("quiFim");
				var quiFimVal = document.getElementById("quiFim").value;
				if(quiFimVal == "" || quiFimVal.length < 5){
					quiFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					quiFim.style.borderColor = "#ccc";
				}

				if(quiIniVal.length == 5 && quiFimVal.length == 5 ){
					var horaIni = parseInt(quiIniVal.substring(0,2));
					var horaFim = parseInt(quiFimVal.substring(0,2));
					if(horaFim < horaIni){
						quiIni.style.borderColor = "#E62117";
						quiFim.style.borderColor = "#E62117";
						document.getElementById('mesQuiHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesQuiHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						var minIni = parseInt(quiIniVal.substring(3,5));
						var minFim = parseInt(quiFimVal.substring(3,5));
						if(minFim < minIni){
							quiIni.style.borderColor = "#E62117";
							quiFim.style.borderColor = "#E62117";
							document.getElementById('mesQuiHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesQuiHora").show();
							flag = false;
						}else if(minIni == minFim){
							quiIni.style.borderColor = "#E62117";
							quiFim.style.borderColor = "#E62117";
							document.getElementById('mesQuiHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesQuiHora").show();
							flag = false;
						}
					}
				}
			}
			var sexta = document.getElementById('sexta').checked;
			if(sexta){
				document.getElementById("sexInicio").style.borderColor = "#ccc";
				document.getElementById("sexFim").style.borderColor = "#ccc";
				$("#mesSexHora").hide();
				var sexIni = document.getElementById("sexInicio");
				var sexIniVal = document.getElementById("sexInicio").value;
				if(sexIniVal == "" || sexIniVal.length < 5){
					sexIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					sexIni.style.borderColor = "#ccc";
				}
				var sexFim = document.getElementById("sexFim");
				var sexFimVal = document.getElementById("sexFim").value;
				if(sexFimVal == "" || sexFimVal.length < 5){
					sexFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					sexFim.style.borderColor = "#ccc";
				}

				if(sexIniVal.length == 5 && sexFimVal.length == 5 ){
					var horaIni = parseInt(sexIniVal.substring(0,2));
					var horaFim = parseInt(sexFimVal.substring(0,2));
					if(horaFim < horaIni){
						sexIni.style.borderColor = "#E62117";
						sexFim.style.borderColor = "#E62117";
						document.getElementById('mesSexHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesSexHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						var minIni = parseInt(sexIniVal.substring(3,5));
						var minFim = parseInt(sexFimVal.substring(3,5));
						if(minFim < minIni){
							sexIni.style.borderColor = "#E62117";
							sexFim.style.borderColor = "#E62117";
							document.getElementById('mesSexHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesSexHora").show();
							flag = false;
						}else if(minIni == minFim){
							sexIni.style.borderColor = "#E62117";
							sexFim.style.borderColor = "#E62117";
							document.getElementById('mesSexHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesSexHora").show();
							flag = false;
						}
					}
				}
			}
			var sabado = document.getElementById('sabado').checked;
			if(sabado){
				document.getElementById("sabInicio").style.borderColor = "#ccc";
				document.getElementById("sabFim").style.borderColor = "#ccc";
				$("#mesSabHora").hide();
				var sabIni = document.getElementById("sabInicio");
				var sabIniVal = document.getElementById("sabInicio").value;
				if(sabIniVal == "" || sabIniVal.length < 5){
					sabIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					sabIni.style.borderColor = "#ccc";
				}
				var sabFim = document.getElementById("sabFim");
				var sabFimVal = document.getElementById("sabFim").value;
				if(sabFimVal == "" || sabFimVal.length < 5){
					sabFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					sabFim.style.borderColor = "#ccc";
				}

				if(sabIniVal.length == 5 && sabFimVal.length == 5 ){
					var horaIni = parseInt(sabIniVal.substring(0,2));
					var horaFim = parseInt(sabFimVal.substring(0,2));
					if(horaFim < horaIni){
						sabIni.style.borderColor = "#E62117";
						sabFim.style.borderColor = "#E62117";
						document.getElementById('mesSabHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesSabHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						var minIni = parseInt(sabIniVal.substring(3,5));
						var minFim = parseInt(sabFimVal.substring(3,5));
						if(minFim < minIni){
							sabIni.style.borderColor = "#E62117";
							sabFim.style.borderColor = "#E62117";
							document.getElementById('mesSabHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesSabHora").show();
							flag = false;
						}else if(minIni == minFim){
							sabIni.style.borderColor = "#E62117";
							sabFim.style.borderColor = "#E62117";
							document.getElementById('mesSabHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesSabHora").show();
							flag = false;
						}
					}
				}
			}
			var domingo = document.getElementById('domingo').checked;
			if(domingo){
				document.getElementById("domInicio").style.borderColor = "#ccc";
				document.getElementById("domFim").style.borderColor = "#ccc";
				$("#mesDomHora").hide();
				var domIni = document.getElementById("domInicio");
				var domIniVal = document.getElementById("domInicio").value;
				if(domIniVal == "" || domIniVal.length < 5){
					domIni.style.borderColor = "#E62117";
					flag = false;
				}else{
					domIni.style.borderColor = "#ccc";
				}
				var domFim = document.getElementById("domFim");
				var domFimVal = document.getElementById("domFim").value;
				if(domFimVal == "" || domFimVal.length < 5){
					domFim.style.borderColor = "#E62117";
					flag = false;
				}else{
					domFim.style.borderColor = "#ccc";

				}

				if(domIniVal.length == 5 && domFimVal.length == 5 ){
					var horaIni = parseInt(domIniVal.substring(0,2));
					var horaFim = parseInt(domFimVal.substring(0,2));
					if(horaFim < horaIni){
						domIni.style.borderColor = "#E62117";
						domFim.style.borderColor = "#E62117";
						document.getElementById('mesDomHora').innerHTML = 'Hora de Início deve ser menor que a Hora Final';
						$("#mesDomHora").show();
						flag = false;
					}else if(horaIni == horaFim){
						domIni.style.borderColor = "#E62117";
						domFim.style.borderColor = "#E62117";
						var minIni = parseInt(domIniVal.substring(3,5));
						var minFim = parseInt(domFimVal.substring(3,5));
						if(minFim < minIni){
							document.getElementById('mesDomHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesDomHora").show();
							flag = false;
						}else if(minIni == minFim){
							domIni.style.borderColor = "#E62117";
							domFim.style.borderColor = "#E62117";
							document.getElementById('mesDomHora').innerHTML = 'Hora Fim deve ser maior que a Hora Início';
							$("#mesDomHora").show();
							flag = false;
						}
					}
				}
			}

			if(flag){
				$("#messDisponibilidadeHora").hide();
				$("#messDisponibilidade").hide();
				return true;
			}else{
				$("#messDisponibilidadeHora").show();
				$("#messDisponibilidade").show();
				return false;
			}
		});
	});
</script>

<!-- Conta script -->
<script>
	$( function() {
		listaConta();
	});
	//Listar Tipo Conta
	function listaConta(){
		var table = document.getElementById("TabelaConta").rows.length;
		var i = 0;
		for(i=table-1; i > 0; i--){
		    document.getElementById("TabelaConta").deleteRow(i);
		}
		$.getJSON('model/prestadorListaConta.ajax.php?search=',{ajax:'true'}, function(est){
			for (var i = 0; i < est.length; i++) {
				var table = document.getElementById("TabelaConta");
				// Captura a quantidade de linhas já existentes na tabela
				var numOfRows = table.rows.length;
				// Captura a quantidade de colunas da última linha da tabela
				var numOfCols = table.rows[numOfRows-1].cells.length;

				// Insere uma linha no fim da tabela.
				var newRow = table.insertRow(numOfRows);

				// Faz um loop para criar as colunas
				for (var j = 0; j < numOfCols; j++) {
					switch(j){
						case 0:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							newCell.innerHTML = est[i].valtpconta;
							break;
						case 1:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							newCell.innerHTML = est[i].valBanco;
							break;
						case 2:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							newCell.innerHTML = est[i].agencia;
							break;
						case 3:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							newCell.innerHTML = est[i].verificador;
							break;
						case 4:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							newCell.innerHTML = est[i].conta;
							break;
						case 5:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							var op = "Padrão";
							if(est[i].padrao == "false")
									op = "-";
							newCell.innerHTML = op;
							break;
						case 6:
							// Insere uma coluna na nova linha
							newCell = newRow.insertCell(j);
							// Insere um conteúdo na coluna
							newCell.innerHTML = "<a class='btn btn-default btn-md' onClick='removerLinhaConta(this)'>Excluir</a>";
							break;
						default:
						alert("deu error -> table");
							break;
					}
				}
			}
		});
	}
	//Inserir Conta
	function inserirConta(){
		var bad = "#E62117";
		var flag = true;
		var tpconta = document.getElementById("tpconta");
		var codtpconta = tpconta.options[tpconta.selectedIndex].value;
		var valtpconta = tpconta.options[tpconta.selectedIndex].text;
		if(codtpconta == ""){
			$("#confirmCamposConta").show();
			tpconta.style.borderColor = bad;
			flag = false;
		}else{
			tpconta.style.borderColor = "#ccc";
		}

		var banco = document.getElementById("banco");
		var codBanco = banco.options[banco.selectedIndex].value;
		var valBanco = banco.options[banco.selectedIndex].text;
		if(codBanco == ""){
			$("#confirmCamposConta").show();
			banco.style.borderColor = bad;
			flag = false;
		}else{
			banco.style.borderColor = "#ccc";
		}

		var agencia = document.getElementById("agencia");
		var valAgencia = document.getElementById("agencia").value;
		if(valAgencia == ""){
			$("#confirmCamposConta").show();
			agencia.style.borderColor = bad;
			flag = false;
		}else{
			agencia.style.borderColor = "#ccc";
		}

		var verificador = document.getElementById("verificador");
		var valVerificador = document.getElementById("verificador").value;
		if(valVerificador == ""){
			$("#confirmCamposConta").show();
			verificador.style.borderColor = bad;
			flag = false;
		}else{
			verificador.style.borderColor = "#ccc";
		}

		var conta = document.getElementById("conta");
		var valConta = document.getElementById("conta").value;
		if(valConta == ""){
			$("#confirmCamposConta").show();
			conta.style.borderColor = bad;
			flag = false;
		}else{
			conta.style.borderColor = "#ccc";
		}

		var padrao = document.getElementById("padrao").checked;

		if(flag){
			$.getJSON('model/contaFunction.ajax.php?search=',{V_tpconta: codtpconta ,V_valtpconta: valtpconta  ,V_codBanco: codBanco,
			   V_valBanco: valBanco ,V_agencia: valAgencia ,V_verificador: valVerificador ,V_conta: valConta,V_padrao: padrao ,ajax: 'true'}, function(dado){
				if(dado.flag){
					listaConta();
					$("#confirmCamposConta").hide();
					document.getElementById('messError').style.display = 'none';
					document.getElementById("tpconta").selectedIndex = 0;
					document.getElementById("banco").selectedIndex = 0;
					document.getElementById("agencia").value = "";
					document.getElementById("conta").value = "";
					document.getElementById("verificador").value = "";
					$("#padrao").prop("checked", false);
					var mess = document.getElementById('messError');
					mess.style.color = "#66cc66";
					mess.innerHTML = "Conta Cadastrada";
					var table = document.getElementById("TabelaConta").rows.length;
					var i = 0;
				}else{
					$("#confirmCamposConta").hide();
					document.getElementById('messError').style.display = 'initial';
					var mess = document.getElementById('messError');
					mess.style.color = "#E62117";
					mess.innerHTML = "Conta já Cadastrada";
				}

			});
		}
	}
	//Excluir Conta
	function removerLinhaConta(obj){
		// Capturamos a referência da TR (linha) pai do objeto
		var objTR = obj.parentNode.parentNode;
		// Capturamos a referência da TABLE (tabela) pai da linha
		var objTable = objTR.parentNode;
		// Capturamos o índice da linha
		var indexTR = objTR.rowIndex;
		// Chamamos o método de remoção de linha nativo do JavaScript, passando como parâmetro o índice da linha
		objTable.deleteRow(indexTR);

		$.getJSON('model/arrumaListaConta.ajax.php?search=',{V_index: indexTR ,ajax: 'true'}, function(dado){listaConta();});
	}
</script>

<!-- documento script -->
<script>
	$( function() {
		listarDocumento();
	});
	function listarDocumento(){
		var table = document.getElementById("TabelaDocumento").rows.length;
		var i = 0;
		for(i=table-1; i > 0; i--){
		    document.getElementById("TabelaDocumento").deleteRow(i);
		}
		$.getJSON('model/prestadorListaDocumento.ajax.php?search=',{ajax:'true'}, function(est){
			for (var i = 0; i < est.length; i++) {
				// Captura a referência da tabela com id “minhaTabela”
				var table = document.getElementById("TabelaDocumento");
				// Captura a quantidade de linhas já existentes na tabela
				var numOfRows = table.rows.length;
				// Captura a quantidade de colunas da última linha da tabela
				var numOfCols = table.rows[numOfRows-1].cells.length;

				// Insere uma linha no fim da tabela.
				var newRowDoc = table.insertRow(numOfRows);

				// Faz um loop para criar as colunas
				for (var k = 0; k < numOfCols; k++) {
					switch(k){
						case 0:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].valtpdoc;
							break;
						case 1:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].numero;
							break;
						case 2:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].orgao;
							break;
						case 3:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].valEstado;
							break;
						case 4:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
								newCellDoc.innerHTML = est[i].frenteName;
							break;
						case 5:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = est[i].trasName;
							break;
						case 6:
							// Insere uma coluna na nova linha
							newCellDoc = newRowDoc.insertCell(k);
							// Insere um conteúdo na coluna
							newCellDoc.innerHTML = "<a class='btn btn-default btn-md' onClick='removerLinha(this)'>Excluir</a>";
							break;
						default:
							alert("deu error -> table");
							break;
					}
				}
			}
		});
	}
	flagTR = true;
	//verificar se o numero do documento é obrigatório
	function retornarObrigatorio(){
		var tpdoc = document.getElementById("docType");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;

		$.getJSON('model/mostrartipodoc.ajax.php?search=',{V_tpdoc: codtpdoc, ajax:'true'}, function(est){
			if(est[0].obrigatorio == "t"){
				flagTR =  true;
			}else{
				flagTR = false;
			}
		});

		return flagTR;
	}

	//Mudar opção no select
	$("#docType").change(function(){

		document.getElementById("orgao").value = "";
		document.getElementById("orgao").style.borderColor = "#ccc";
		document.getElementById("numeroDoc").value = "";
		document.getElementById("numeroDoc").style.borderColor = "#ccc";
		document.getElementById("estadoDoc").selectedIndex = 0;
		document.getElementById("estadoDoc").style.borderColor = "#ccc";
		$("#confirmTpdoc").hide();
		$("#confirmNdoc").hide();
		$("#confirmOrgao").hide();
		$("#confirmEstadoDoc").hide();
		$("#confirmTras").hide();
		$("#confirmFrente").hide();



		var tpdoc = document.getElementById("docType");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;
		if(codtpdoc == "1"){
			$('#tipoDocs').show();
		}else{
			$('#tipoDocs').hide();
		}

		$.getJSON('model/mostrartipodoc.ajax.php?search=',{V_tpdoc: codtpdoc, ajax:'true'}, function(est){
			if(est[0].obrigatorio == "t"){
				$('#numDoc').show();
			}else{
				$('#numDoc').hide();
			}
		});
	});

	//inserir Documento
	function inserirDoc(){

		var bad = "#E62117";
		var flag = true;
		var cpf = document.getElementById("cpf");
		var valCpf = document.getElementById("cpf").value;
		var tpdoc = document.getElementById("docType");
		var codtpdoc = tpdoc.options[tpdoc.selectedIndex].value;
		var valtpdoc = tpdoc.options[tpdoc.selectedIndex].text;
		var orgao = document.getElementById("orgao");
		var valOrgao = document.getElementById("orgao").value;
		var numeroDoc = document.getElementById("numeroDoc");
		var valNumeroDoc = document.getElementById("numeroDoc").value;
		var estadoDoc = document.getElementById("estadoDoc");
		var codEstadoDoc = estadoDoc.options[estadoDoc.selectedIndex].value;
		var valEstadoDoc = estadoDoc.options[estadoDoc.selectedIndex].text;

		if(valCpf == ""){
			cpf.style.borderColor = bad;
			cpf.focus();
			flag=false;
		}else{
			valCpf = RetiraCaracteresInvalidos(valCpf);
			cpf.style.borderColor = "#ccc";
		}

		if(codtpdoc == ""){
			$("#confirmTpdoc").show();
			tpdoc.style.borderColor = bad;
			tpdoc.focus();
			flag = false;
		}else{
			$("#confirmTpdoc").hide();
			tpdoc.style.borderColor = "#ccc";
		}

		if(codtpdoc == "1"){
			if(codEstadoDoc == ""){
				$("#confirmEstadoDoc").show();
				estadoDoc.style.borderColor = bad;
				estadoDoc.focus();
				flag = false;
			}else{
				$("#confirmEstadoDoc").hide();
				estadoDoc.style.borderColor = "#ccc";
			}

			if(valOrgao == ""){
				$("#confirmOrgao").show();
				orgao.style.borderColor = bad;
				orgao.focus();
				flag = false;
			}else{
				$("#confirmOrgao").hide();
				orgao.style.borderColor = "#ccc";
			}

			if(valNumeroDoc == ""){
				$("#confirmNdoc").show();
				numeroDoc.style.borderColor = bad;
				numeroDoc.focus();
				flag = false;
			}else{
				$("#confirmNdoc").hide();
				numeroDoc.style.borderColor = "#ccc";
			}
		}else {
			codEstadoDoc = "null";
			valEstadoDoc = "";
			valOrgao = "";
			var ob = retornarObrigatorio();
			if(ob){
				if(valNumeroDoc == ""){
					$("#confirmNdoc").show();
					numeroDoc.style.borderColor = bad;
					numeroDoc.focus();
					flag = false;
				}else{
					$("#confirmNdoc").hide();
					numeroDoc.style.borderColor = "#ccc";
				}
			}
		}
		var file_frente;
		var frente = document.getElementById("frente");
		var valFrente = document.getElementById("frente").value;
		if(valFrente == ""){
			$("#confirmFrente").show();
			frente.focus();
			flag = false;
		}else{
			$("#confirmFrente").hide();
			file_frente = frente.files[0];
		}
		var file_tras;
		var tras = document.getElementById("tras");
		var valTras = document.getElementById("tras").value;
		if(valTras == ""){
			$("#confirmTras").show();
			tras.focus();
			flag = false;
		}else{
			$("#confirmTras").hide();
			file_tras = tras.files[0];
		}

		var formData = new FormData();
		formData.append('cpf',valCpf);
		formData.append('dataTras', file_tras);
		formData.append('dataFrente', file_frente);
		formData.append('codtpdoc',codtpdoc);
		formData.append('valtpdoc', valtpdoc);
		formData.append('codEstadoDoc',codEstadoDoc);
		formData.append('valEstadoDoc',valEstadoDoc);
		formData.append('orgao',valOrgao);
		formData.append('numeroDoc',valNumeroDoc);

		if(flag){

			$("#insertDoc").hide();
			var mess = document.getElementById('docsError');
			mess.innerHTML = "Carregando Foto...";

			$.ajax({
			        url: 'model/docsFunction.ajax.php',
			        type: 'POST',
			        data: formData,
			        cache: false,
			        dataType: 'json',
			        processData: false, // Don't process the files
			        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			        success: function(dado){
					if(dado.flag){
						listarDocumento();
						$("#insertDoc").show();
						document.getElementById('docsError').style.display = 'none';
						document.getElementById('numeroDoc').value = "";
						document.getElementById("numeroDoc").style.borderColor = "#ccc";
						document.getElementById('orgao').value = "";
						document.getElementById("orgao").style.borderColor = "#ccc";
						document.getElementById('frente').value = "";
						document.getElementById('tras').value = "";
						document.getElementById("docType").selectedIndex = 0;
						document.getElementById("docType").style.borderColor = "#ccc";
						document.getElementById("estadoDoc").selectedIndex = 0;
						document.getElementById("estadoDoc").style.borderColor = "#ccc";
						$('#tipoDocs').addClass('carregando');
						$("#numDoc").addClass("carregando");
					}else{
						$("#insertDoc").show();
						document.getElementById('docsError').style.display = 'initial';
						var mess = document.getElementById('docsError');
						mess.style.color = "#E62117";
						mess.innerHTML = "Documento já registrado";
					}
				},
				error: function(req, textStatus, errorThrown) {
					        //this is going to happen when you send something different from a 200 OK HTTP
					        console.log('Ooops, something happened: ' + textStatus + ' ' +errorThrown);
					}

			});
		}
	}

	//excluir Documento
	function removerLinha(obj){
		// Capturamos a referência da TR (linha) pai do objeto
		var objTR = obj.parentNode.parentNode;
		// Capturamos a referência da TABLE (tabela) pai da linha
		var objTable = objTR.parentNode;
		// Capturamos o índice da linha
		var indexTR = objTR.rowIndex;

		var colunas = objTR.getElementsByTagName('td');
		var imgTras = colunas[5].firstChild.nodeValue;
		var imgFrente = colunas[4].firstChild.nodeValue;

		$.getJSON('model/arrumaListaDocumento.ajax.php?search=',{V_index: indexTR, frente: imgFrente, tras: imgTras,
		 ajax: 'true'}, function(dado){listarDocumento();});
	}
</script>

</html>