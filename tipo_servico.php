<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
	<div id="tela" class="tela">
		<div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Tipo de Serviço <small>Cadastro de Tipos de Serviços</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Cadastro
                            </li>
                        </ol>

					<div class="form-group">
						<form method="POST" action="" id="FormServico">
							<label>Tipo de Serviço *</label>
							<input type="text" id="descricao" name="Servico" class="form-control" placeholder="Digite a Descrição aqui..." required>
							<br/>
							<label>valor hora padrao *</label>
							<input type="text" id="valorHora" name="valor" class="form-control" placeholder="Digite o valor da hora padrão aqui..." required>
							<br/>
							<?php
//SALVAR
$FormServico = @$_POST["FormServicoSalvar"];
if(!empty($FormServico)){
	$descricao = @$_POST["Servico"];
		require_once("controller/controllerTipo_Servico.php");
		$class = new controllerTipo_Servico;
		$class->Salvar();
}
?>
							<input type="submit" value="Salvar" name="FormServicoSalvar" class="btn btn-default btn-lg">
							<a href="lista-tipo_servico.php"  class="btn btn-default btn-lg">Listar</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$( function() {
		$('#valorHora').mask("#.##0,00", {reverse: true});
	});
	</script>
</body>
</html>