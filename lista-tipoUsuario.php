<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
				Tipo de Usuário <small>Listagem Tipo de Usuário</small>
				</h1>
				<ol class="breadcrumb">
					<li class="active">
						<i class="fa fa-fw fa-users"></i> Lista
					</li>
				</ol>
				<a href="tipoUsuario.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
				<br/><br/>
			<?php
				require_once("controller/controllerTipoUsuario.php");
				//EDITAR
				$FormTipoConta = @$_POST["FormTipoUsuarioEditar"];
				if(!empty($FormTipoConta)){
						$class = new controllerTipoUsuario;
						echo"<br/><br/>";
						$class->Editar();
				}
				//EXCLUIR
				$FormTipoConta = @$_POST["FormTipoUsuarioExcluir"];
				if(!empty($FormTipoConta)){
					$cod = @$_POST["CodExcluir"];
						$class = new controllerTipoUsuario;
						echo"<br/><br/>";
						$class->Excluir($cod);
				}
			?>
					<div id="lista">
					<?php
						//LISTAR
						$class = new controllerTipoUsuario;
						$class->Listar();
					?>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>