<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
						Status Movimentações <small>Listagem dos Status de Movimentações</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Lista
                            </li>
                        </ol>
				<a href="statusMovimentacao.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
			<?php
				require_once("controller/controllerStatusMovimentacao.php");
				//EDITAR
				$FormOcupacao = @$_POST["FormStatusMovimentacaoEditar"];
				if(!empty($FormOcupacao)){
						$class = new controllerStatusMovimentacao;
						echo"<br/><br/>";
						$class->Editar();
				}
				//EXCLUIR
				$FormOcupacao = @$_POST["FormOcupacaoExcluir"];
				if(!empty($FormOcupacao)){
					$cod = @$_POST["CodExcluir"];
						$class = new controllerStatusMovimentacao;
						echo"<br/><br/>";
						$class->Excluir($cod);
				}
			?>
					<div id="lista">
						<?php
						//LISTAR
						$class = new controllerStatusMovimentacao;
						$class->Listar();
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>