<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
						Turnos <small>Listagem de Turnos</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Lista
                            </li>
                        </ol>
				<a href="turno.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
			<?php
				require_once("controller/controllerTurno.php");
				//EDITAR
				$FormTurno = @$_POST["FormTurnoEditar"];
				if(!empty($FormTurno)){
						$class = new controllerTurno;
						echo"<br/><br/>";
						$class->Editar();
				}
				//EXCLUIR
				$FormTurno = @$_POST["FormTurnoExcluir"];
				if(!empty($FormTurno)){
					$cod = @$_POST["CodExcluir"];
						$class = new controllerTurno;
						echo"<br/><br/>";
						$class->Excluir($cod);
				}
			?>
					<div id="lista">
						<?php
						//LISTAR
						$class = new controllerTurno;
						$class->Listar();
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$( function() {
		 $('.time').mask('00:00:00');
	});
	</script>
</body>
</html>