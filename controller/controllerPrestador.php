<?php
require_once("model/bean/BeanPrestador.php");
require_once("model/dao/DaoPrestador.php");
	class controllerPrestador extends BeanPrestador{

		public function inverteData($data){
			if(count(explode("/",$data)) > 1){
				return implode("-",array_reverse(explode("/",$data)));
			}elseif(count(explode("-",$data)) > 1){
				return implode("/",array_reverse(explode("-",$data)));
			}
		}

		public function Salvar(){
			$pass1 = @$_POST["pass1"];
			$pass2 = @$_POST["pass2"];

			if($pass1 == $pass2){
			//array para retirar caracteres
			$pontos = array("-", ".");
			// recebendo
			$nome = $_POST["nome"];
			$nome = strtolower($nome);
			$nome = ucwords($nome);
			$cpf = $_POST['cpf'];
			$cpf = str_replace($pontos,"",$cpf);
			$dtnasc = $_POST["dtnasc"];
			$dtnasc = $this->inverteData($dtnasc);
			$email = $_POST["email"];

			//imagem do prestador
			$extensao = pathinfo($_FILES['imagem']['name']);
			$extensao = ".".@$extensao['extension'];
			$foto = $extensao;//receber o nome da foto
			$temp = $_FILES["imagem"]["tmp_name"];//receber o local onde a foto está armazenada

			$cell = @$_POST["cell"];
			$fone = @$_POST["fone"];
			$sexo = $_POST["sexo"];
			$cep = @$_POST["cep"];
			$cep = str_replace($pontos, "", $cep);
			$rua = @$_POST["rua"];
			$numero = @$_POST["numero"];
			$bairro = @$_POST["bairro"];
			$complemento = @$_POST["complemento"];
			$cidade = @$_POST["cidade"];
			$linkedin = @$_POST["linkedin"];
			$googleplus = @$_POST["googleplus"];
			$facebook = @$_POST["facebook"];

			$escolaridade = @$_POST["escolaridade"];
			$ocupacao = @$_POST["ocupacao"];
			if(empty($ocupacao))
				$ocupacao = "null";
			$instituicao = @$_POST["instituicao"];
			$curso = @$_POST["curso"];
			$perfil = @$_POST["perfil"];

			$baba = @$_POST["babas"];
			$limpeza = @$_POST["limpeza"];
			$idoso = @$_POST["idosos"];
			$idTipoUsuario = @$_POST["usuario"];

			if(isset($_POST['segunda'])){
				$segunda = $_POST['segunda'];
				$segundaInicio = "'".$_POST['segundaInicio']."'";
				$segundaFim = "'".$_POST['segundaFim']."'";
			}else{
				$segunda = "false";
				$segundaInicio = "null";
				$segundaFim = "null";
			}
			if(isset($_POST['terca'])){
				$terca = $_POST['terca'];
				$tercaInicio = "'".$_POST['tercaInicio']."'";
				$tercaFim = "'".$_POST['tercaFim']."'";
			}else{
				$terca = "false";
				$tercaInicio = "null";
				$tercaFim = "null";
			}
			if(isset($_POST['quarta'])){
				$quarta = $_POST['quarta'];
				$quartaInicio = "'".$_POST['quartaInicio']."'";
				$quartaFim = "'".$_POST['quartaFim']."'";
			}else{
				$quarta = "false";
				$quartaInicio = "null";
				$quartaFim = "null";
			}
			if(isset($_POST['quinta'])){
				$quinta = $_POST['quinta'];
				$quintaInicio = "'".$_POST['quintaInicio']."'";
				$quintaFim = "'".$_POST['quintaFim']."'";
			}else{
				$quinta = "false";
				$quintaInicio = "null";
				$quintaFim = "null";
			}
			if(isset($_POST['sexta'])){
				$sexta = $_POST['sexta'];
				$sextaInicio = "'".$_POST['sextaInicio']."'";
				$sextaFim = "'".$_POST['sextaFim']."'";
			}else{
				$sexta = "false";
				$sextaInicio = "null";
				$sextaFim = "null";
			}
			if(isset($_POST['sabado'])){
				$sabado = $_POST['sabado'];
				$sabadoInicio = "'".$_POST['sabadoInicio']."'";
				$sabadoFim = "'".$_POST['sabadoFim']."'";
			}else{
				$sabado = "false";
				$sabadoInicio = "null";
				$sabadoFim = "null";
			}
			if(isset($_POST['domingo'])){
				$domingo = $_POST['domingo'];
				$domingoInicio = "'".$_POST['domingoInicio']."'";
				$domingoFim = "'".$_POST['domingoFim']."'";
			}else{
				$domingo = "false";
				$domingoInicio = "null";
				$domingoFim = "null";
			}

			//passando dados para bean
			$this->_set('nome',$nome);
			$this->_set('cpf',$cpf);
			$this->_set('dtnasc',$dtnasc);
			$this->_set('email',$email);
			$this->_set('imgName',$foto);
			$this->_set('imgTemp',$temp);
			$this->_set('fone',$fone);
			$this->_set('cell',$cell);
			$this->_set('sexo',$sexo);
			$this->_set('cep',$cep);
			$this->_set('rua',$rua);
			$this->_set('numero',$numero);
			$this->_set('bairro',$bairro);
			$this->_set('complemento',$complemento);
			$this->_set('idCidade',$cidade);
			$this->_set('linkedin',$linkedin);
			$this->_set('googleplus',$googleplus);
			$this->_set('facebook',$facebook);
			$this->_set('senha',sha1($pass2));

			$this->_set('escolaridade',$escolaridade);
			$this->_set('ocupacao',$ocupacao);
			$this->_set('instituicao',$instituicao);
			$this->_set('curso',$curso);
			$this->_set('perfil',$perfil);
			$this->_set('baba',$baba);
			$this->_set('limpeza',$limpeza);
			$this->_set('idoso',$idoso);

			$this->_set('segunda',$segunda);
			$this->_set('segundaInicio',$segundaInicio);
			$this->_set('segundaFim',$segundaFim);
			$this->_set('terca',$terca);
			$this->_set('tercaInicio',$tercaInicio);
			$this->_set('tercaFim',$tercaFim);
			$this->_set('quarta',$quarta);
			$this->_set('quartaInicio',$quartaInicio);
			$this->_set('quartaFim',$quartaFim);
			$this->_set('quinta',$quinta);
			$this->_set('quintaInicio',$quintaInicio);
			$this->_set('quintaFim',$quintaFim);
			$this->_set('sexta',$sexta);
			$this->_set('sextaInicio',$sextaInicio);
			$this->_set('sextaFim',$sextaFim);
			$this->_set('sabado',$sabado);
			$this->_set('sabadoInicio',$sabadoInicio);
			$this->_set('sabadoFim',$sabadoFim);
			$this->_set('domingo',$domingo);
			$this->_set('domingoInicio',$domingoInicio);
			$this->_set('domingoFim',$domingoFim);

			$this->_set('idTipoUsuario',$idTipoUsuario);
			//instanciar a dao
			$dao=new DaoPrestador;
			$dao->salvar($this);

			}else{
				echo"<div class='alert alert-danger' style = 'height:35px;padding-top:8px;width:100%;Float:left;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:0px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

					Desculpe, senha diferentes !
            </div>
			";
			}
		}//salvar

		function Listar(){
			$dao=new DaoPrestador;
			$dao->Listar();
		}

		function Editar(){
			//array para retirar caracteres
			$pontos = array("-", ".");
			// recebendo
			$nome = $_POST["nome"];
			$nome = strtolower($nome);
			$nome = ucwords($nome);
			$cpf = $_POST['cpf'];
			$cpf = str_replace($pontos,"",$cpf);
			$dtnasc = $_POST["dtnasc"];
			$email = $_POST["email"];

			//imagem do prestador
			$extensao = pathinfo($_FILES['imagem']['name']);
			$extensao = ".".@$extensao['extension'];
			$foto = $extensao;//receber o nome da foto
			$temp = $_FILES["imagem"]["tmp_name"];//receber o local onde a foto está armazenada

			$cell = @$_POST["cell"];
			$fone = @$_POST["fone"];
			$sexo = $_POST["sexo"];
			$cep = @$_POST["cep"];
			$cep = str_replace($pontos, "", $cep);
			$rua = @$_POST["rua"];
			$numero = @$_POST["numero"];
			$bairro = @$_POST["bairro"];
			$complemento = @$_POST["complemento"];
			$cidade = @$_POST["cidade"];
			$linkedin = @$_POST["linkedin"];
			$googleplus = @$_POST["googleplus"];
			$facebook = @$_POST["facebook"];

			$escolaridade = @$_POST["escolaridade"];
			$ocupacao = @$_POST["ocupacao"];
			$instituicao = @$_POST["instituicao"];
			$curso = @$_POST["curso"];
			$perfil = @$_POST["perfil"];
			$banco = @$_POST["banco"];
			$verificador = @$_POST["verificador"];
			$agencia = @$_POST["agencia"];
			$conta = @$_POST["conta"];
			$tpConta = @$_POST["tpconta"];

			$baba = @$_POST["babas"];
			$limpeza = @$_POST["limpeza"];
			$idoso = @$_POST["idosos"];
			$idTipoUsuario = @$_POST["usuario"];

			//passando dados para bean
			$this->_set('nome',$nome);
			$this->_set('cpf',$cpf);
			$this->_set('dtnasc',$dtnasc);
			$this->_set('email',$email);
			$this->_set('imgName',$foto);
			$this->_set('imgTemp',$temp);
			$this->_set('fone',$fone);
			$this->_set('cell',$cell);
			$this->_set('sexo',$sexo);
			$this->_set('cep',$cep);
			$this->_set('rua',$rua);
			$this->_set('numero',$numero);
			$this->_set('bairro',$bairro);
			$this->_set('complemento',$complemento);
			$this->_set('idCidade',$cidade);
			$this->_set('linkedin',$linkedin);
			$this->_set('googleplus',$googleplus);
			$this->_set('facebook',$facebook);

			$this->_set('escolaridade',$escolaridade);
			$this->_set('ocupacao',$ocupacao);
			$this->_set('instituicao',$instituicao);
			$this->_set('curso',$curso);
			$this->_set('perfil',$perfil);

			$this->_set('baba',$baba);
			$this->_set('limpeza',$limpeza);
			$this->_set('idoso',$idoso);

			$this->_set('idTipoUsuario',$idTipoUsuario);
			//instanciar a dao
			$dao=new DaoPrestador;
			$dao->Editar($this);
		}//Editar

		function Excluir($cpf,$status){
			//instanciar a dao
			$dao=new DaoPrestador;
			$dao->Excluir($cpf,$status);
		}

		function editarDisponibilidade(){
			$pontos = array("-", ".");
			$cpf = $_POST['cpf'];
			$cpf = str_replace($pontos,"",$cpf);

			if(isset($_POST['segunda'])){
				$segunda = $_POST['segunda'];
				$segundaInicio = "'".$_POST['segundaInicio']."'";
				$segundaFim = "'".$_POST['segundaFim']."'";
			}else{
				$segunda = "false";
				$segundaInicio = "null";
				$segundaFim = "null";
			}
			if(isset($_POST['terca'])){
				$terca = $_POST['terca'];
				$tercaInicio = "'".$_POST['tercaInicio']."'";
				$tercaFim = "'".$_POST['tercaFim']."'";
			}else{
				$terca = "false";
				$tercaInicio = "null";
				$tercaFim = "null";
			}
			if(isset($_POST['quarta'])){
				$quarta = $_POST['quarta'];
				$quartaInicio = "'".$_POST['quartaInicio']."'";
				$quartaFim = "'".$_POST['quartaFim']."'";
			}else{
				$quarta = "false";
				$quartaInicio = "null";
				$quartaFim = "null";
			}
			if(isset($_POST['quinta'])){
				$quinta = $_POST['quinta'];
				$quintaInicio = "'".$_POST['quintaInicio']."'";
				$quintaFim = "'".$_POST['quintaFim']."'";
			}else{
				$quinta = "false";
				$quintaInicio = "null";
				$quintaFim = "null";
			}
			if(isset($_POST['sexta'])){
				$sexta = $_POST['sexta'];
				$sextaInicio = "'".$_POST['sextaInicio']."'";
				$sextaFim = "'".$_POST['sextaFim']."'";
			}else{
				$sexta = "false";
				$sextaInicio = "null";
				$sextaFim = "null";
			}
			if(isset($_POST['sabado'])){
				$sabado = $_POST['sabado'];
				$sabadoInicio = "'".$_POST['sabadoInicio']."'";
				$sabadoFim = "'".$_POST['sabadoFim']."'";
			}else{
				$sabado = "false";
				$sabadoInicio = "null";
				$sabadoFim = "null";
			}
			if(isset($_POST['domingo'])){
				$domingo = $_POST['domingo'];
				$domingoInicio = "'".$_POST['domingoInicio']."'";
				$domingoFim = "'".$_POST['domingoFim']."'";
			}else{
				$domingo = "false";
				$domingoInicio = "null";
				$domingoFim = "null";
			}

			$this->_set('cpf',$cpf);
			$this->_set('segunda',$segunda);
			$this->_set('segundaInicio',$segundaInicio);
			$this->_set('segundaFim',$segundaFim);
			$this->_set('terca',$terca);
			$this->_set('tercaInicio',$tercaInicio);
			$this->_set('tercaFim',$tercaFim);
			$this->_set('quarta',$quarta);
			$this->_set('quartaInicio',$quartaInicio);
			$this->_set('quartaFim',$quartaFim);
			$this->_set('quinta',$quinta);
			$this->_set('quintaInicio',$quintaInicio);
			$this->_set('quintaFim',$quintaFim);
			$this->_set('sexta',$sexta);
			$this->_set('sextaInicio',$sextaInicio);
			$this->_set('sextaFim',$sextaFim);
			$this->_set('sabado',$sabado);
			$this->_set('sabadoInicio',$sabadoInicio);
			$this->_set('sabadoFim',$sabadoFim);
			$this->_set('domingo',$domingo);
			$this->_set('domingoInicio',$domingoInicio);
			$this->_set('domingoFim',$domingoFim);

			//instanciar a dao
			$dao=new DaoPrestador;
			$dao->editarDisponibilidade($this);
		}

		function ListarTipoServico(){
			$dao = new DaoPrestador;
			$dao->ListarTipoServico();
		}

		function ListarBanco(){
			$dao=new DaoPrestador;
			$dao->ListarBanco();
		}

		function ListarTipoUsuario(){
			$dao = new DaoPrestador;
			$dao->ListarTipoUsuario();
		}

		function ListarEscolaridade(){
			$dao = new DaoPrestador;
			$dao->ListarEscolaridade();
		}

		function ListarOcupacao(){
			$dao = new DaoPrestador;
			$dao->ListarOcupacao();
		}

		function ListarTipoDocumento(){
			$dao = new DaoPrestador;
			$dao->ListarTipoDocumento();
		}

		function ListarEstado(){
			$dao = new DaoPrestador;
			$dao->ListarEstado();
		}

		function ListarTipoConta(){
			$dao = new DaoPrestador;
			$dao->ListarTipoConta();
		}

	}//class
?>