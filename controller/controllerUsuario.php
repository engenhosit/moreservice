<?php
require_once("model/bean/BeanUsuario.php");
require_once("model/dao/DaoUsuario.php");
	class controllerUsuario extends BeanUsuario{

		public function inverteData($data){
			if(count(explode("/",$data)) > 1){
				return implode("-",array_reverse(explode("/",$data)));
			}elseif(count(explode("-",$data)) > 1){
				return implode("/",array_reverse(explode("-",$data)));
			}
		}

		public function Salvar(){
			//array para retirar caracteres
			$pontos = array("-", ".");
			// recebendo
			$nome = $_POST["nome"];
			$nome = strtolower($nome);
			$nome = ucwords($nome);
			$cpf = $_POST['cpf'];
			$cpf = str_replace($pontos,"",$cpf);
			$dtnasc = $_POST["dtnasc"];
			$dtnasc = $this->inverteData($dtnasc);
			$email = $_POST["email"];
			$fone = @$_POST["fone"];
			$cell = @$_POST["cell"];
			$sexo = $_POST["sexo"];
			$cep = @$_POST["cep"];
			$cep = str_replace($pontos, "", $cep);
			$rua = @$_POST["rua"];
			$numero = @$_POST["numero"];
			$bairro = @$_POST["bairro"];
			$complemento = @$_POST["complemento"];
			$cidade = @$_POST["cidade"];
			$linkedin = @$_POST["linkedin"];
			$googleplus = @$_POST["googleplus"];
			$facebook = @$_POST["facebook"];
			$pass1 = @$_POST["pass1"];
			$pass2 = @$_POST["pass2"];
			$idTipoUsuario = @$_POST["tipo_usu"];//recebe o tipo de usuario
			if($pass1 == $pass2){

				if(empty($cidade))
					$cidade = "null";
				if(empty($cep))
					$cep = "null";

				//passando dados para bean
				$this->_set('nome',$nome);
				$this->_set('cpf',$cpf);
				$this->_set('dtnasc',$dtnasc);
				$this->_set('email',$email);
				$this->_set('fone',$fone);
				$this->_set('cell',$cell);
				$this->_set('sexo',$sexo);
				$this->_set('cep',$cep);
				$this->_set('rua',$rua);
				$this->_set('numero',$numero);
				$this->_set('bairro',$bairro);
				$this->_set('complemento',$complemento);
				$this->_set('idCidade',$cidade);
				$this->_set('linkedin',$linkedin);
				$this->_set('googleplus',$googleplus);
				$this->_set('facebook',$facebook);
				$this->_set('senha',sha1($pass2));
				$this->_set('idTipoUsuario',$idTipoUsuario);
				//instanciar a dao
				$dao=new DaoUsuario;
				$dao->salvar($this);
			}else{
				echo"<div class='alert alert-danger' style = 'height:35px;padding-top:8px;width:100%;Float:left;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:0px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

					Desculpe, senha diferentes !
            </div>
			";
			}
		}//salvar

		function Listar(){
			$dao=new DaoUsuario;
			$dao->Listar();
		}

		function Editar(){
			//array para retirar caracteres
			$pontos = array("-", ".");
			// recebendo
			$nome = $_POST["nome"];
			$nome = strtolower($nome);
			$nome = ucwords($nome);
			$cpf = $_POST['cpf'];
			$cpf = str_replace($pontos,"",$cpf);
			$dtnasc = $_POST["dtnasc"];
			$dtnasc = $this->inverteData($dtnasc);
			$email = $_POST["email"];
			$fone = @$_POST["fone"];
			$cell = @$_POST["cell"];
			$sexo = $_POST["sexo"];
			$cep = @$_POST["cep"];
			$cep = str_replace($pontos, "", $cep);
			$rua = @$_POST["rua"];
			$numero = @$_POST["numero"];
			$bairro = @$_POST["bairro"];
			$complemento = @$_POST["complemento"];
			$idCidade = @$_POST["cidade"];
			$linkedin = @$_POST["linkedin"];
			$googleplus = @$_POST["googleplus"];
			$facebook = @$_POST["facebook"];
			$idTipoUsuario = @$_POST["tipo_usu"];//recebe o tipo de usuario

				if(empty($cidade))
					$cidade = "null";
				if(empty($cep))
					$cep = "null";

				$this->_set('nome',$nome);
				$this->_set('cpf',$cpf);
				$this->_set('dtnasc',$dtnasc);
				$this->_set('email',$email);
				$this->_set('fone',$fone);
				$this->_set('cell',$cell);
				$this->_set('sexo',$sexo);
				$this->_set('cep',$cep);
				$this->_set('rua',$rua);
				$this->_set('numero',$numero);
				$this->_set('bairro',$bairro);
				$this->_set('complemento',$complemento);
				$this->_set('idCidade',$idCidade);
				$this->_set('linkedin',$linkedin);
				$this->_set('googleplus',$googleplus);
				$this->_set('facebook',$facebook);
				$this->_set('idTipoUsuario',$idTipoUsuario);

				//instanciar a dao
				$dao=new DaoUsuario;
				$dao->Editar($this);
		}//Editar

		function Excluir($cpf,$idStatus){
			//instanciar a dao
			$dao=new DaoUsuario;
			$dao->Excluir($cpf,$idStatus);
		}

		function ListarTipoUsuario(){
			$dao=new DaoUsuario;
			$dao->ListarTipoUsuario();
		}
		function ListarEstado(){
			$dao=new DaoUsuario;
			$dao->ListarEstado();
		}
		function ListarCidade(){
			$dao=new DaoUsuario;
			$dao->ListarCidade();
		}
		function ListarCidadeCod($nomeCidade){
			$dao=new DaoUsuario;
			$dao->ListarCidadeCod($nomeCidade);
		}
		function TrocarSenha(){
			$pass1 = @$_POST["pass1"];
			$pass2 = @$_POST["pass2"];
			if($pass1 == $pass2){
				//instanciar a dao
				$dao=new DaoUsuario;
				$dao->TrocarSenha($pass2);
			}else{
				echo"<div class='alert alert-danger' style = 'height:35px;padding-top:8px;width:100%;Float:left;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:0px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

					Desculpe, senha diferentes !
            </div>
			";
			}
		}

	}//class
?>