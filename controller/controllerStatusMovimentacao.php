<?php
require_once("model/bean/BeanStatusMovimentacao.php");
require_once("model/dao/DaoStatusMovimentacao.php");
	class controllerStatusMovimentacao extends BeanStatusMovimentacao{
		
		public function Salvar(){
			// recebendo
			$resumida = @$_POST["resumida"];
			$resumida = strtolower($resumida);
			$resumida = ucfirst($resumida);
			$detalhada = @$_POST["detalhada"];
			$detalhada = strtolower($detalhada);
			$detalhada = ucfirst($detalhada);
			$tipoStatus = @$_POST["tipoStatus"];
			//passando dados para bean
			$this->_set('resumida',$resumida);
			$this->_set('detalhada',$detalhada);
			$this->_set('idTipoStatus',$tipoStatus);
			//instanciar a dao
			$dao=new DaoStatusMovimentacao;
			$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoStatusMovimentacao;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$cod = @$_POST["CodUpdate"];
			$resumida = @$_POST["resumida"];
			$resumida = strtolower($resumida);
			$resumida = ucfirst($resumida);
			$detalhada = @$_POST["detalhada"];
			$detalhada = strtolower($detalhada);
			$detalhada = ucfirst($detalhada);
			$tipoStatus = @$_POST["tipoStatus"];
			//passando dados para bean
			$this->_set('cod',$cod);
			$this->_set('resumida',$resumida);
			$this->_set('detalhada',$detalhada);
			$this->_set('idTipoStatus',$tipoStatus);
			//instanciar a dao
			$dao=new DaoStatusMovimentacao;
			$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoStatusMovimentacao;
			$dao->Excluir($cod);
		}
		
		function ListarTipoStatus(){
			$dao = new DaoStatusMovimentacao;
			$dao->ListarTipoStatus();
		}
	}//class
?>