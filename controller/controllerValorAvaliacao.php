<?php
require_once("model/bean/BeanValorAvaliacao.php");
require_once("model/dao/DaoValorAvaliacao.php");
	class controllerValorAvaliacao extends BeanValorAvaliacao{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucwords($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoValorAvaliacao;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoValorAvaliacao;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucwords($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoValorAvaliacao;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoValorAvaliacao;
			$dao->Excluir($cod);
		}
		
	}//class
?>