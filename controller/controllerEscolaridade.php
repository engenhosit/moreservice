<?php
require_once("model/bean/BeanEscolaridade.php");
require_once("model/dao/DaoEscolaridade.php");
	class controllerEscolaridade extends BeanEscolaridade{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoEscolaridade;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoEscolaridade;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoEscolaridade;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoEscolaridade;
			$dao->Excluir($cod);
		}
		
	}//class
?>