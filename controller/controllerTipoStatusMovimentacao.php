<?php
require_once("model/bean/BeanTipoStatusMovimentacao.php");
require_once("model/dao/DaoTipoStatusMovimentacao.php");
	class controllerTipoStatusMovimentacao extends BeanTipoStatusMovimentacao{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoTipoStatusMovimentacao;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoTipoStatusMovimentacao;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoTipoStatusMovimentacao;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoTipoStatusMovimentacao;
			$dao->Excluir($cod);
		}
		
	}//class
?>