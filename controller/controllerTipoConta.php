<?php
require_once("model/bean/BeanTipoConta.php");
require_once("model/dao/DaoTipoConta.php");
	class controllerTipoConta extends BeanTipoConta{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoTipoConta;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoTipoConta;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoTipoConta;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoTipoConta;
			$dao->Excluir($cod);
		}
		
	}//class
?>