<?php
require_once("model/bean/BeanEspecialidade.php");
require_once("model/dao/DaoEspecialidade.php");
	class controllerEspecialidade extends BeanEspecialidade{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoEspecialidade;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoEspecialidade;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoEspecialidade;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoEspecialidade;
			$dao->Excluir($cod);
		}
		
	}//class
?>