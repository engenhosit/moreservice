<?php
require_once("model/bean/BeanTipoUsuario.php");
require_once("model/dao/DaoTipoUsuario.php");
	class controllerTipoUsuario extends BeanTipoUsuario{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoTipoUsuario;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoTipoUsuario;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoTipoUsuario;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoTipoUsuario;
			$dao->Excluir($cod);
		}
		
	}//class
?>