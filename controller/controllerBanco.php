<?php
require_once("model/bean/BeanBanco.php");
require_once("model/dao/DaoBanco.php");
	class controllerBanco extends BeanBanco{
		
		public function Salvar(){
			// recebendo
			$cod = $_POST["cod"];
			$nome = $_POST["nome"];
			$nome = strtolower($nome);
			$nome = ucwords($nome);
			$site = $_POST["site"];
			//passando dados para bean
			$this->_set('cod',$cod);
			$this->_set('nome',$nome);
			$this->_set('site',$site);
			
			//instanciar a dao
			$dao=new DaoBanco;
			$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoBanco;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$cod = $_POST["cod"];
			$nome = $_POST["nome"];
			$nome = strtolower($nome);
			$nome = ucwords($nome);
			$site = $_POST["site"];
			
			//passando dados para bean
			$this->_set('cod',$cod);
			$this->_set('nome',$nome);
			$this->_set('site',$site);
			
			//instanciar a dao
			$dao=new DaoBanco;
			$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoBanco;
			$dao->Excluir($cod);
		}
	}//class
?>