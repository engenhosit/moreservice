<?php
require_once("model/bean/BeanOcupacao.php");
require_once("model/dao/DaoOcupacao.php");
	class controllerOcupacao extends BeanOcupacao{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["ocupacao"];
			$descricao = strtolower($descricao);
			$descricao = ucfirst($descricao);
			$cbo = @$_POST["cbo"];
			//passando dados para bean
			$this->_set('descricao',$descricao);
			$this->_set('cbo',$cbo);
			//instanciar a dao
			$dao=new DaoOcupacao;
			$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoOcupacao;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$cod = @$_POST["CodUpdate"];
			$descricao = @$_POST["ocupacao"];
			$descricao = strtolower($descricao);
			$descricao = ucfirst($descricao);
			$cbo = @$_POST["cbo"];
			//passando dados para bean
			$this->_set('descricao',$descricao);
			$this->_set('cod',$cod);
			$this->_set('cbo',$cbo);
			//instanciar a dao
			$dao=new DaoOcupacao;
			$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoOcupacao;
			$dao->Excluir($cod);
		}
	}//class
?>