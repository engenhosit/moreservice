<?php
require_once("model/bean/BeanTipoDocumento.php");
require_once("model/dao/DaoTipoDocumento.php");
	class controllerTipoDocumento extends BeanTipoDocumento{

		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
			$obrigatorio = @$_POST['obrigatorio'];
			if(empty($_POST['obrigatorio']))
				$obrigatorio = "false";

				//passando dados para bean
				$this->_set('descricao',$descricao);
				$this->_set('obrigatorio',$obrigatorio);
				//instanciar a dao
				$dao=new DaoTipoDocumento;
				$dao->salvar($this);
		}//salvar

		function Listar(){
			$dao=new DaoTipoDocumento;
			$dao->Listar();
		}

		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
			$obrigatorio = @$_POST['obrigatorio'];
			if(empty($_POST['obrigatorio']))
				$obrigatorio = "false";

				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				$this->_set('obrigatorio',$obrigatorio);
				//instanciar a dao
				$dao=new DaoTipoDocumento;
				$dao->Editar($this);
		}//Editar

		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoTipoDocumento;
			$dao->Excluir($cod);
		}

	}//class
?>