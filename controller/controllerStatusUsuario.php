<?php
require_once("model/bean/BeanStatusUsuario.php");
require_once("model/dao/DaoStatusUsuario.php");
	class controllerStatusUsuario extends BeanStatusUsuario{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoStatusUsuario;
				$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoStatusUsuario;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$codigo = @$_POST["CodUpdate"];
			$descricao = @$_POST["descricao"];
			$descricao = strtolower($descricao);
			$descricao=ucfirst($descricao);
				//passando dados para bean
				$this->_set('id',$codigo);
				$this->_set('descricao',$descricao);
				//instanciar a dao
				$dao=new DaoStatusUsuario;
				$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoStatusUsuario;
			$dao->Excluir($cod);
		}
		
	}//class
?>