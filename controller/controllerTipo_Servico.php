<?php
require_once("model/bean/BeanTipo_Servico.php");
require_once("model/dao/DaoTipo_Servico.php");
	class controllerTipo_Servico extends BeanTipo_Servico{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["Servico"];
			$descricao = strtolower($descricao);
			$descricao = ucfirst($descricao);
			$valor = @$_POST["valor"];
			$valor = str_replace(".","",$valor);
			$valor = str_replace(",",".",$valor);
			//passando dados para bean
			$this->_set('descricao',$descricao);
			$this->_set('valor',$valor);
			//instanciar a dao
			$dao=new DaoTipo_Servico;
			$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoTipo_Servico;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$cod = @$_POST["CodUpdate"];
			$descricao = @$_POST["Servico"];
			$descricao = strtolower($descricao);
			$descricao = ucfirst($descricao);
			$valor = @$_POST["valor"];
			$valor = str_replace(".","",$valor);
			$valor = str_replace(",",".",$valor);
			//passando dados para bean
			$this->_set('descricao',$descricao);
			$this->_set('cod',$cod);
			$this->_set('valor',$valor);
			//instanciar a dao
			$dao=new DaoTipo_Servico;
			$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoTipo_Servico;
			$dao->Excluir($cod);
		}
	}//class
?>