<?php
require_once("model/bean/BeanTurno.php");
require_once("model/dao/DaoTurno.php");
	class controllerTurno extends BeanTurno{
		
		public function Salvar(){
			// recebendo
			$descricao = @$_POST["turno"];
			$descricao = strtolower($descricao);
			$descricao = ucfirst($descricao);
			$inicio = @$_POST["inicio"];
			$fim = @$_POST["fim"];
			//passando dados para bean
			$this->_set('descricao',$descricao);
			$this->_set('horaInicio',$inicio);
			$this->_set('horaFim',$fim);
			//instanciar a dao
			$dao=new DaoTurno;
			$dao->salvar($this);
		}//salvar
		
		function Listar(){
			$dao=new DaoTurno;
			$dao->Listar();
		}
		
		function Editar(){
			// recebendo
			$cod = @$_POST["CodUpdate"];
			$descricao = @$_POST["turno"];
			$descricao = strtolower($descricao);
			$descricao = ucfirst($descricao);
			$inicio = @$_POST["inicio"];
			$fim = @$_POST["fim"];
			//passando dados para bean
			$this->_set('cod',$cod);
			$this->_set('descricao',$descricao);
			$this->_set('horaInicio',$inicio);
			$this->_set('horaFim',$fim);
			//instanciar a dao
			$dao=new DaoTurno;
			$dao->Editar($this);
		}//Editar
		
		function Excluir($cod){
			//instanciar a dao
			$dao=new DaoTurno;
			$dao->Excluir($cod);
		}
	}//class
?>