<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cod_estados = $_REQUEST['cod_estados'] ;
	if(empty($cod_estados)){
		$cidades = array();

		$sql = "SELECT codigo_banco,nome_banco
				FROM tbbanco order by nome_banco";
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'idBanco'	=> $row['codigo_banco'],
				'nome'			=> $row['nome_banco'],
			);
		}

		echo( json_encode( $cidades ) );
	}else{
		$cidades = array();

		$sql = "SELECT codigo_banco,nome_banco FROM tbbanco
				where codigo_banco <> '".$cod_estados."' order by nome_banco";
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'idBanco'	=> $row['codigo_banco'],
				'nome'			=> $row['nome_banco'],
			);
		}

		echo( json_encode( $cidades ) );
	}

	pg_close();
?>