<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	$idosos = array();



	if(!isset($_SESSION["idosos"]))
		$_SESSION["idosos"] = array();

	foreach ($_SESSION["idosos"] as  $obj) {
		$idosos[] = array(
			'nome'			=> $obj['nome'],
			'dtnasc'			=> $obj['dtnasc'],
			'sexo'			=> $obj['sexo'],
			'cuidadoEspecial'	=>  $obj['cuidadoEspecial'],
			'descricao'		=> $obj['descricao'],
		);
	}


		echo( json_encode( $idosos ) );
?>