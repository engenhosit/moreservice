<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cod_estados = @$_REQUEST['cod_estados'] ;
	$cidades = array();

	$sql = "select * from tbusuario usu
					inner join tbusuario_has_tbtipousuario tpusu on usu.cpf_usuario = tpusu.cpf_usuario
					where id_tipo_usuario = 1 and usu.cpf_usuario = '".$cod_estados."'";
	$res = pg_query($db,$sql);
	while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
		$cidades[] = array(
			'nome'			=> $row['nome_usuario'],
			'cpf'			=> $row['cpf_usuario'],
			'dtnasc'		=> $row['nasc_usuario'],
			'email'			=> $row['email_usuario'],
			'fone'			=> $row['telefone_usuario'],
			'cell'			=> $row['celular_usuario'],
			'sexo'			=> $row['sexo_usuario'],
			'cep'			=> $row['cep_usuario'],
			'rua'			=> $row['rua_usuario'],
			'numero'		=> $row['numero_usuario'],
			'bairro'		=> $row['bairro_usuario'],
			'complemento'	=> $row['complemento_usuario'],
			'idCidade'		=> $row['id_cidade'],
			'linkedin'		=> $row['linkedin_usuario'],
			'googleplus'	=> $row['googleplus_usuario'],
			'facebook'		=> $row['facebook_usuario'],
		);
	}

	echo( json_encode( $cidades ) );

	pg_close();
?>