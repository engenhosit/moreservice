<?php
	class BeanPrestador{
		//campos da tabela Usuario
		private $cpf,$nome,$dtnasc,$fone,$cell,$email,$sexo,$senha;
		private $cep,$rua,$numero,$bairro,$complemento;
		private $linkedin,$googleplus,$facebook,$imagem;
		private $idTipoUsuario,$imgTemp,$imgName;
		//variaveis da tabela cidade e estado
		private $idCidade,$cidade,$idEstado,$estado;

		//variaveis da tabela prestador
		private $perfil,$instituicao,$curso;
		//variaveis para regastar as escolaridades
		private $codEscolaridade,$escolaridade;
		//variaveis para regastar as ocupações
		private $codOcupacao,$ocupacao;

		//inserção na tabela conta prestador
		private $agencia,$conta,$verificador,$padrao;
		//variaveis da tabela Banco
		private $codBanco,$banco;
		//variaveis da tebela tipo conta
		private $codTpConta,$tpConta;

		//inserção na tabela documentos
		private $numeroDoc,$emissor,$estadoDoc,$imgFrente,$imgTras,$tempFrente,$tempTras;
		//tipo documentos
		private $codDoc,$doc;

		private $idServico,$servico,$status;

		//tabela Disponibilidade
		private $segunda, $segundaInicio, $segundaFim, $terca, $tercaInicio, $tercaFim, $quarta, $quartaInicio, $quartaFim;
		private $quinta,$quintaInicio, $quintaFim, $sexta, $sextaInicio, $sextaFim, $sabado, $sabadoInicio, $sabadoFim;
		private $domingo, $domingoInicio, $domingoFim;


		public function _set($prop,$value){
			$this->$prop=$value;
		}//set

		public function _get($prop){
			 return $this->$prop;
		}//set
	}
?>