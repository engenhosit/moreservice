<?php
	class BeanCliente{
		private $idUsuario;
		//campos obrigatorios
		private $idTipoUsuario,$nome,$cpf,$dtnasc,$email,$senha,$status;
		//campos relacionados a endereço
		private $cep,$rua,$numero,$bairro,$complemento;
		//campos relacionados a rede social e outros
		private $linkedin,$googleplus,$facebook,$fone,$cell,$sexo;
		//variaveis para resgatar as cidades
		private $idCidade,$cidade,$idEstado,$estado;
		public function _set($prop,$value){
			$this->$prop=$value;
		}//set
		
		public function _get($prop){
			 return $this->$prop;
		}//set
	}
?>