<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	if(!isset($_SESSION["filhos"]))
		$_SESSION["filhos"] = array();

	$nome = @$_REQUEST['nome'];
	$dtnasc = @$_REQUEST['dtnasc'];
	$sexo = @$_REQUEST['sexo'];
	$cuidadoEspecial = @$_REQUEST['cuidadoEspecial'];
	$descricao = @$_REQUEST['descricao'];

	$flag = true;

	foreach($_SESSION["filhos"] as $obj){
		if($obj['nome'] == $nome && $obj['dtnasc'] == $dtnasc){
			$flag = false;
		}
	}

	if($flag){
		$filhos = array();
		$filhos = array(
				'nome'			=> $nome,
				'dtnasc'			=> $dtnasc,
				'sexo'			=> $sexo,
				'cuidadoEspecial'	=> $cuidadoEspecial,
				'descricao'		=> $descricao,
				'flag'			=>$flag,
		);
		$i = count($_SESSION["filhos"]);
		$_SESSION["filhos"][$i] = $filhos;

		echo( json_encode( $filhos ) );
	}else{
		$filhos = array();
		$filhos = array(
				'flag'			=> $flag,
			);
		echo( json_encode( $filhos ) );
	}
?>