<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	$contas = array();



	if(!isset($_SESSION["contas"]))
		$_SESSION["contas"] = array();

	foreach ($_SESSION["contas"] as  $obj) {
		$contas[] = array(
			'tpconta'		=> $obj['tpconta'],
			'valtpconta'	=> $obj['valtpconta'],
			'codBanco'		=> $obj['codBanco'],
			'valBanco'		=>  $obj['valBanco'],
			'agencia'		=> $obj['agencia'],
			'verificador'	=> $obj['verificador'],
			'conta'			=> $obj['conta'],
			'padrao'		=> $obj['padrao'],
		);
	}


		echo( json_encode( $contas ) );
?>