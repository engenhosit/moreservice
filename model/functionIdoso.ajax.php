<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	if(!isset($_SESSION["idosos"]))
		$_SESSION["idosos"] = array();

	$nome = @$_REQUEST['nome'];
	$dtnasc = @$_REQUEST['dtnasc'];
	$sexo = @$_REQUEST['sexo'];
	$cuidadoEspecial = @$_REQUEST['cuidadoEspecial'];
	$descricao = @$_REQUEST['descricao'];

	$flag = true;

	foreach($_SESSION["idosos"] as $obj){
		if($obj['nome'] == $nome && $obj['dtnasc'] == $dtnasc){
			$flag = false;
		}
	}

	if($flag){
		$idosos = array();
		$idosos = array(
				'nome'			=> $nome,
				'dtnasc'			=> $dtnasc,
				'sexo'			=> $sexo,
				'cuidadoEspecial'	=> $cuidadoEspecial,
				'descricao'		=> $descricao,
				'flag'			=>$flag,
		);
		$i = count($_SESSION["idosos"]);
		$_SESSION["idosos"][$i] = $idosos;

		echo( json_encode( $idosos ) );
	}else{
		$idosos = array();
		$idosos = array(
				'flag'			=> $flag,
			);
		echo( json_encode( $idosos ) );
	}
?>