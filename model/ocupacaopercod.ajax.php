<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cod_estados = $_REQUEST['cod_estados'] ;
	if(empty($cod_estados)){
		$cidades = array();

		$sql = "SELECT id, nome_ocupacao FROM tbocupacao";
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'idOcupacao'	=> $row['id'],
				'nome'			=> $row['nome_ocupacao'],
			);
		}

		echo( json_encode( $cidades ) );
	}else{
		$cidades = array();

		$sql = "SELECT id, nome_ocupacao FROM tbocupacao
				where id <> ".$cod_estados;
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'idOcupacao'	=> $row['id'],
				'nome'			=> $row['nome_ocupacao'],
			);
		}

		echo( json_encode( $cidades ) );
	}

	pg_close();
?>