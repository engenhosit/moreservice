<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cpf = @$_REQUEST['V_cpf'];

	$sql = "SELECT * FROM tbdocumentos d
			inner join tbtipodocumentos p on p.id = id_tipo_documento
			WHERE cpf_prestador = '".$cpf."'";
	$res = pg_query($db,$sql);

	$contas = array();
	$id_estado = "";

	while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {

		if(!empty($row['id_estado'])){
			$exec = "select * from tbestado where id = ".$row['id_estado'];
			$result = pg_query($db,$exec);
			while( $obj = pg_fetch_array($result, null, PGSQL_ASSOC) ){
				$row['id_estado'] = $obj['nome'];
			}
		}

		$contas[] = array(
			'numero'		=> $row['numero_documento'],
			'orgao'  		=> $row['orgao_emissor'],
			'frenteName' 	=> $row['link_imagem_frente'],
			'trasName' 		=> $row['link_imagem_verso'],
			'tpdoc'		=> $row['descricao'],
			'estado'		=> $row['id_estado'],
		);

		$id_estado = "";

	}
		echo( json_encode( $contas ) );
?>