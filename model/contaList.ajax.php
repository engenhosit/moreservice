<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cpf = @$_REQUEST['V_cpf'];

	$sql = "SELECT p.id, agencia,numero_conta,digito_verificador,is_conta_padrao,descricao_tipo_conta,nome_banco
	 FROM tbcontaprestador p
			inner join tbbanco b on b.codigo_banco = p.codigo_banco
			inner join tbtipoconta tp on tp.id = id_tipo_conta
			WHERE cpf_prestador = '".$cpf."' order by p.id";
	$res = pg_query($db,$sql);

	$contas = array();

	while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
		$contas[] = array(
				'id'		=> $row['id'],
				'agencia'		=> $row['agencia'],
				'conta'  		=> $row['numero_conta'],
				'verificador' 	=> $row['digito_verificador'],
				'padrao' 		=> $row['is_conta_padrao'],
				'tpconta'		=> $row['descricao_tipo_conta'],
				'codBanco'	 	=> $row['nome_banco'],
			);
	}
		echo( json_encode( $contas ) );
?>