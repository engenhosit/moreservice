<?php
require_once("model/dao/Connection.php");
class DaoLogin extends Connection{
	public function VerificarLogin($email,$password){

		$perfil = 1;

		$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
		inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
		where usu.email_usuario = '".$email."' and senha = '".sha1($password)."'
		and id_tipo_usuario = ".$perfil; //Query para verificar se o login é valido
		$data = $this->o_db->query($sql);
		$aux = $data->fetchObject();
		$id = $aux->cont;
		if($id >= 1){
			//Verifica o Status do usuario
			switch ($perfil) {
				case 1:
					$sql = "select status_usuario from tbadministradores tu
					inner join tbusuario us on us.cpf_usuario = tu.cpf_usuario
					where email_usuario = '".$email."'";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$status = $aux->status_usuario;
					break;
				case 2:
					$sql = "select status_cliente from tbcliente tu
					inner join tbusuario us on us.cpf_usuario = tu.cpf_cliente
					where email_usuario = '".$email."'";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$status = $aux->status_cliente;
					break;
				case 3:
					$sql = "select status_prestador from tbprestador tu
					inner join tbusuario us on us.cpf_usuario = tu.cpf_prestador
					where email_usuario = '".$email."'";
					 $data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$status = $aux->status_prestador;
					break;
				default:
					//Caso Impossivels
					$status = 0;
					break;
			}
			//1 - status Valido Para Login
			if($status == 1){
				$dadosUsuario = null;
				$dadosPrestador = null;
				//Resgata os campos do usuario
				$sql = "SELECT * from tbusuario where email_usuario = '".$email."'
				and senha = '".sha1($password)."'";
				$data = $this->o_db->query($sql);
				$obj = $data->fetchObject();

				$dadosUsuario = array(
					'nome' => $obj->nome_usuario,
					'cpf' => $obj->cpf_usuario,
					'dtnasc' => $obj->nasc_usuario,
					'email' => $obj->email_usuario,
					'fone' => $obj->telefone_usuario,
					'cell' => $obj->celular_usuario,
					'sexo' => $obj->sexo_usuario,
					'cep' => $obj->cep_usuario,
					'rua' => $obj->rua_usuario,
					'numero' => $obj->numero_usuario,
					'bairro' => $obj->bairro_usuario,
					'complemento' => $obj->complemento_usuario,
					'idCidade' => $obj->id_cidade,
					'linkedin' => $obj->linkedin_usuario,
					'googleplus' => $obj->googleplus_usuario,
					'facebook' => $obj->facebook_usuario,
					'foto' => $obj->imagem_usuario,
				);

				if($perfil == 1){
					$adm = true;
					$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
					inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
					where email_usuario = '".$email."' and id_tipo_usuario = 2";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$id = $aux->cont;
					if($id == 0){
						$cliente = false;
					}else{
						$cliente = true;
					}
					$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
					inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
					where email_usuario = '".$email."' and id_tipo_usuario = 3";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$id = $aux->cont;
					if($id == 0){
						$prestador = false;
					}else{
						$prestador = true;
					}
				}else if($perfil == 2){
					$cliente = true;
					$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
					inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
					where email_usuario = '".$email."' and id_tipo_usuario = 1";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$id = $aux->cont;
					if($id == 0){
						$adm = false;
					}else{
						$adm = true;
					}
					$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
					inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
					where email_usuario = '".$email."' and id_tipo_usuario = 3";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$id = $aux->cont;
					if($id == 0){
						$prestador = false;
					}else{
						$prestador = true;
					}
				}else{
					$prestador = true;
					$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
					inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
					where email_usuario = '".$email."' and id_tipo_usuario = 1";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$id = $aux->cont;
					if($id == 0){
						$adm = false;
					}else{
						$adm = true;
					}
					$sql = "SELECT count(*) as cont from tbusuario_has_tbtipousuario usu_tipo
					inner join tbusuario usu on usu.cpf_usuario = usu_tipo.cpf_usuario
					where email_usuario = '".$email."' and id_tipo_usuario = 2";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$id = $aux->cont;
					if($id == 0){
						$cliente = false;
					}else{
						$cliente = true;
					}
				}

				if($prestador || $perfil == 3){
					$sql = "select * from tbprestador tu
					inner join tbusuario us on us.cpf_usuario = tu.cpf_prestador
					where email_usuario = '".$email."'";
					$data = $this->o_db->query($sql);
					$aux = $data->fetchObject();
					$dadosPrestador = array(
						'perfil' => $aux->perfil_prestador,
						'instituicao' => $aux->instituicao_prestador,
						'curso' => $aux->curso_prestador,
						'escolaridade' => $aux->id_escolaridade,
						'ocupacao' => $aux->id_ocupacao,
					);

					$baba = $idoso = $limpeza = false;

					$sql = "SELECT id from tbprestador tu
					inner join tbusuario us on us.cpf_usuario = tu.cpf_prestador
					inner join tbprestador_has_tbtiposervico prestp on prestp.cpf_prestador = tu.cpf_prestador
					inner join tbtiposervico on id = id_tipo_servico
					where email_usuario = '".$email."'";
					$data = $this->o_db->query($sql);
					while($ts = $data->fetchObject()){
						if($ts->id == 2){
							$baba = true;
						}
						if($ts->id == 1){
							$idoso = true;
						}
						if($ts->id == 3){
							$limpeza = true;
						}
					}

					$dadosPrestador[] = array(
						'baba' => $baba,
						'idoso' => $idoso,
						'limpeza' => $limpeza,
					);
				}

				//setando valores na Session
				$_SESSION["login"] = array();
				$_SESSION["login"][0] = $perfil;//mostrar a que perfil está conectado => (Int)
				$_SESSION["login"][1] = $adm; //Verificar se é administrador => (Boolean)
				$_SESSION["login"][2] = $cliente; //Verifica se é Cliente => (Boolean)
			 	$_SESSION["login"][3] = $prestador; //Verifica se é Prestador  => (Boolean)
				$_SESSION["login"][4] = $dadosUsuario; //Dados do usuario => (Array)
				$_SESSION['login'][5] = $dadosPrestador; //Dados do usuario => (Array)

				echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-ok'></span>
						Login Realizado com Sucesso.
				</div>";

			//redirecionar para a pagina de acesso ao sistema
			echo"<script>
				setTimeout(\"window.location.replace('Tela-inicial.php');\",2000);
			</script>";

			}else
			{
				echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Login não autorizado
			            </div>";
			}

		}else{
			$sql = "SELECT count(*) as cont  from tbusuario  where email_usuario = '".$email."'";
			$data = $this->o_db->query($sql);
			$obj = $data->fetchObject();
			$id = $obj->cont;

			if($id == 0){
				echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						E-mail não cadastrado na plataforma
			            </div>";
			}else{
				$sql = "SELECT count(*) as cont  from tbusuario
				where email_usuario = '".$email."' and senha = '".sha1($password)."'";
				$data = $this->o_db->query($sql);
				$obj = $data->fetchObject();
				$id = $obj->cont;
				if($id == 0){
					echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
					<span class='glyphicon glyphicon-exclamation-sign'></span>
						Senha inválida
			            		</div>";
				}else{
					if($perfil == 1){
						echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
							<span class='glyphicon glyphicon-exclamation-sign'></span>
								Perfil de administrador não cadastrado
				            		</div>";
					}else if($perfil == 2){
						echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
							<span class='glyphicon glyphicon-exclamation-sign'></span>
								Perfil de cliente não cadastrado
					            </div>";
					}else{
						echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
							<span class='glyphicon glyphicon-exclamation-sign'></span>
								Perfil de prestador não cadastrado
					            </div>";
					}
				}
			}
		}
	}//verificaLogin

	function updatePassword($email){
		//gera query para verificar se email existe no BD
		$query = "select count(*) as cont from tbusuario where email_usuario='".$email."'";
		//executa query
		$data = $this->o_db->query($query);
		$obj = $data->fetchObject();
		$flag = $obj->cont;
		//se email existir
		if($flag == 1){
			//gera query para verificar se email existe no BD
			$query = "select nome_usuario from tbusuario where email_usuario='".$email."'";
			//executa query
			$data = $this->o_db->query($query);
			$obj = $data->fetchObject();
			$nome = $obj->nome_usuario;

			//gera nova senha
			$novasenha = substr(md5(time()),0,6);
			//gera query para alteração de senha criptografada no BD
			$query = "update tbusuario set senha='".sha1($novasenha)."' where email_usuario = '".$email."'";
			//executa query
			if($this->o_db->query($query)>0){//se query executada com sucesso
				//gera mensagem para email do usuário
				$msg = '<!DOCTYPE html>
					<html>
					<head>
					<title>E-mail +Services</title>
					<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:200|Roboto" rel="stylesheet">
					</head>
					<body style="width: 600px; text-align: center;">
					<table  style="font-family: \'Open Sans Condensed\', sans-serif; background-color: #00bad2; height: 200px; width: 100%" >
					<tr style="height: 35px"></tr>
					<tr>
					<td width="10%"></td>
					<td valign="top" ><img src="http://mservices.com.br/img/logo/logo-header.png" alt="+Services" width="120"></td>
					<td style="color: #fff; font-size: 20px;">UMA CONEXÃO DE QUALIDADE E RAPIDEZ ENTRE PROFISSIONAIS E CLIENTES</td>
					</tr>
					</table>

					<table width="100%" style="font-family: \'Roboto\', sans-serif;">
					<tr>
					<td width="0px"></td>
					<td valign="top"><h1>Olá, '.$nome.',</h1></td>
					</tr>
					<tr>
					<td width="10%"></td>
					<td valign="top" style="text-align: left;">Sua nova senha é: '.$novasenha.'</td>
					</tr>
					<tr>
					<td width="10%"></td>
					<td valign="top" style="text-align: left;">Atenciosamente,</td>
					</tr>
					<tr>
					<td width="10%"></td>
					<td valign="top" style="text-align: left;">Equipe +Services</td>
					</tr>
					<tr>
					<td width="10%"></td>
					<td valign="top" style="text-align: left;">Por favor, não responda esse email.</td>
					</tr>
					</table>

					<table width="100%" style="font-family: \'Open Sans Condensed\', sans-serif;">
					<tr style="height: 25px"></tr>
					<tr>
					<td valign="top" style="text-align: center; background-color:#f7ffd7; height: 100px; border:solid 1px #00bad2; color:#00bad2;"><a href="http://www.mservices.com.br" target="_blanc" ><button style="margin-top: 35px; background-color:#f7ffd7; border: none; font-size: 30px; color: #00bad2" >www.mservices.com.br</button></a></td>
					</tr>
					</table>
					</body>
					</html>';
				$header = 'From: suporte@mservices.com.br'."\r\n".'Content-type: text/html; charset=UTF-8';
				//envia email
				$ok = mail($email, "+Services - Solicitação de senha", $msg,$header);
				//se email enviado com sucesso --> true
				if($ok){
					$this->mensagem("Caro usuário, sua senha foi alterada com sucesso!");
				}else{
					$this->mensagem("Desculpe-nos, ouve um erro ao enviar e-mail. Tente Novamente !");
				}
			}else{
				$this->mensagem("Desculpe-nos, ouve um erro ao enviar e-mail. Tente Novamente !");
			}
		}else{
			$this->mensagem("Este e-mail não existe. Coloque um email válido !");
		}
	}//function

	function mensagem($m){
		echo"
		<script type='text/javascript'>
			$( function() {

				alert('".$m."');
			});
		</script>";
	}
}//class
?>