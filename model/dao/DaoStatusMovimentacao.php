<?php
include "Connection.php";
class DaoStatusMovimentacao extends Connection{
	function salvar(BeanStatusMovimentacao $obj){

		$exec = "select count(*) as cont from tbstatusmovimentacoes where descricao_resumida  ='".$obj->_get('resumida')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$exec="insert into tbstatusmovimentacoes
			(descricao_resumida ,descricao_detalhada,id_tipo_status)
			values
			('".$obj->_get('resumida')."','".$obj->_get('detalhada')."',".$obj->_get('idTipoStatus').")";
			if($this->o_db->exec($exec)>0){
				$message = "Status de movimentação Registrada com Sucesso.";
				$this->sucesso($message);
			}else{
				$message = "Verifique todos os campos obrigatórios ! Tente Novamente. ";
				$this->error($message);
			}
		}else{
			$message = "Este Status de movimentação já foi cadastrado.";
			$this->error($message);
		}

	}//function salvar

	function Listar(){
			$exec="select st.id,descricao_resumida,descricao_detalhada,id_tipo_status,descricao_tipo_status from tbstatusmovimentacoes st
					inner join tbtipostatusmovimentacoes tipo on tipo.id = id_tipo_status";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerStatusMovimentacao;
					$oc->_set('cod',$r->id);
					$oc->_set('resumida',$r->descricao_resumida);
					$oc->_set('detalhada',$r->descricao_detalhada);
					$oc->_set('idTipoStatus',$r->id_tipo_status);
					$oc->_set('tipoStatus',$r->descricao_tipo_status);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>Codigo</th>
								<th>Descrição Resumida </th>
								<th>Descrição Detalhada </th>
								<th>Tipo Status</th>
								<th>Alterar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>";
				$c = 0;//variavel controladora do modal --> vai identificar qual modal vai ser aberto ao clicar no botão editar
				foreach($lista as $obj){

				echo "<tr>
						<form action='' method='POST'>
							<td>".$obj->_get('cod')."</td>
							<td>".$obj->_get('resumida')."</td>
							<td>".$obj->_get('detalhada')."</td>
							<td>".$obj->_get('tipoStatus')."</td>
							<td><a class='btn btn-default btn-lg' data-toggle='modal' data-target='#contact".$c."' data-original-title>Editar</a></td>
							<td><input type='submit' value='Excluir' name='FormOcupacaoExcluir' class='btn btn-default btn-lg'></td>
							<input type='hidden' value='".$obj->_get('cod')."' name='CodExcluir'>
						</form>
					</tr>";
				$c++;//contadora
				}
			echo"</tbody>
				</table>";

				$c = 0; //variavel para modificar o id do modal
			foreach($lista as $obj){
			echo"<!-- ModalUpdate -->
						<div class='modal fade'	 id='contact".$c."' tabindex='-1' >
							<div class='modal-dialog'>
								<div class='panel panel-primary'>
									<div class='panel-heading'>
										<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
										<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
									</div>
									<form action='' method='POST'>
									<div class='modal-body' style='padding: 5px;'>
										<label>Codigo</label>
										<input class='form-control' name='CodUpdate' placeholder='Codigo' type='text' value='".$obj->_get('cod')."' readonly />
										<br/>
										<label>Descricão Resumida</label>
										<input value='".$obj->_get('resumida')."' id='descricao' name='resumida' class='form-control' placeholder='Descricão Resumida' required>
										<br/>
										<label>Descricão Detalhada </label>
										<input value='".$obj->_get('detalhada')."' id='descricao' name='detalhada' class='form-control' placeholder='Descricão Detalhada' required>
										<br/>
										<label>Tipo Status Movimentações:</label>
										<select name='tipoStatus' class='form-control' style='width:50%;'>";
										$this->ListarTipoStatusUpdate($obj->_get('idTipoStatus'));
									echo"</select>
									<br><br>
									<div class='panel-footer' style='margin-bottom:-14px;'>
										<input type='submit' name='FormStatusMovimentacaoEditar' class='btn btn-success' value='Editar'/>
										<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
									</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					<!-- Fim ModalUpdate -->";
					$c++;//contadora
			}
	}

	function Editar(BeanStatusMovimentacao $obj){

		$exec = "select count(*) as cont from tbstatusmovimentacoes where descricao_resumida ='".$obj->_get('resumida')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		$exec = "select * from tbstatusmovimentacoes where id = ".$obj->_get('cod');
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$desc = $aux->descricao_resumida;
		$detalhada = $aux->descricao_detalhada;
		$idStatus = $aux->id_tipo_status;

		if($desc != $obj->_get('resumida') || $detalhada != $obj->_get('detalhada') || $idStatus != $obj->_get('idTipoStatus')){
			if ($id == 0  || $desc == $obj->_get('resumida')){
				$exec="update tbstatusmovimentacoes   set
					descricao_resumida = '".$obj->_get('resumida')."',
					descricao_detalhada  = '".$obj->_get('detalhada')."',
					id_tipo_status = ".$obj->_get('idTipoStatus')."
					where id = ".$obj->_get('cod');
				if($this->o_db->exec($exec)>0){
					$message = "Status de movimentação Modificado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Verifique todos os campos obrigatórios ! Tente Novamente. ";
					$this->error($message);
				}
			}else{
				$message="Desculpe, Esta descrição é inválida.";
				$this->error($message);
			}
		}else{
			$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
			$this->error($message);
		}
	}//function

	function Excluir($cod){
		$exec="delete from tbstatusmovimentacoes
			where id=".$cod;
			$this->o_db->exec($exec);

		$exec = "select count(*) as cont from tbstatusmovimentacoes   where id = ".$cod;
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$message = "Status de movimentação Excluido com Sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Desculpe, ocorreu um erro na exclusão do Status de movimentação.";
			$this->error($message);
		}
	}//function

	function ListarTipoStatus(){
		$lista= array();
		$exec="select * from tbtipostatusmovimentacoes";
		$executar=$this->o_db->query($exec);
			while($r=$executar->fetchobject()){
				$oc= new controllerStatusMovimentacao;
				$oc->_set('idTipoStatus',$r->id);
				$oc->_set('tipoStatus',$r->descricao_tipo_status);
				array_push($lista, $oc);
			}//while
			foreach($lista as $obj){
				echo"<option value=".$obj->_get('idTipoStatus').">".$obj->_get('tipoStatus')."</option>";
			}
	}

	function ListarTipoStatusUpdate($id){
		$lista= array();
		$exec="select * from tbtipostatusmovimentacoes";
		$executar=$this->o_db->query($exec);
			while($r=$executar->fetchobject()){
				$oc= new controllerStatusMovimentacao;
				$oc->_set('idTipoStatus',$r->id);
				$oc->_set('tipoStatus',$r->descricao_tipo_status);
				array_push($lista, $oc);
			}//while
			foreach($lista as $obj){
				if($id == $obj->_get('idTipoStatus'))
					echo"<option value=".$obj->_get('idTipoStatus').">".$obj->_get('tipoStatus')."</option>";
			}

			foreach($lista as $obj){
				if($id != $obj->_get('idTipoStatus'))
					echo"<option value=".$obj->_get('idTipoStatus').">".$obj->_get('tipoStatus')."</option>";
			}
	}

	function sucesso($message){
				echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-ok'></span>

                    ".$message."
            </div>";
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

                    ".$message."
            </div>
			";
	}

}//class
?>