<?php
include "Connection.php";
class DaoPrestador extends Connection{
	function addLocalizacao($cep,$idCidade){

		$exec = "select * from tbcidade where id = ".$idCidade;
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$cidade = $aux->nome;

		$address = $cep."+".str_replace(" ", "+", $cidade);
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyDSaR7untqnjICwng2FCb6DNhZEsVF0IAQ';
		$dados = json_decode(file_get_contents($url));
		$lat = $dados->results[0]->geometry->location->lat;
		$lng = $dados->results[0]->geometry->location->lng;

		$loc = array('lat' => $lat, 'lng' => $lng);

		return $loc;
	}

	function salvar(BeanPrestador $obj){
		$obj->_set('imgName',$obj->_get('cpf').$obj->_get('imgName')); //renomea o nome da foto com o cpf e a extensao da imagem

		$exec = "select count(*) as cont from tbusuario usu
		inner join tbusuario_has_tbtipousuario tu on usu.cpf_usuario = tu.cpf_usuario
		where tu.cpf_usuario ='".$obj->_get('cpf')."' and id_tipo_usuario = 3";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){ //verifica se ja é prestador

			$exec = "select count(*) as cont from tbusuario where cpf_usuario ='".$obj->_get('cpf')."'";
			$o_data = $this->o_db->query($exec);
			$aux = $o_data->fetchObject();
			$id = $aux->cont;

			if($id == 0){ //verifica se o cpf ja existe

				$exec = "select count(*) as cont from tbusuario where email_usuario ='".$obj->_get('email')."'";
				$o_data = $this->o_db->query($exec);
				$aux = $o_data->fetchObject();
				$id = $aux->cont;
				if($id == 0){ //verifica se email ja existe

					$loc = $this->addLocalizacao($obj->_get('cep'),$obj->_get('idCidade'));

					$exec="insert into tbusuario values
					('".$obj->_get('cpf')."','".$obj->_get('nome')."','".$obj->_get('dtnasc')."','".$obj->_get('fone')."',
					'".$obj->_get('cell')."','".$obj->_get('email')."','".$obj->_get('sexo')."',".$obj->_get('idCidade').",
					'".$obj->_get('senha')."','".$obj->_get('rua')."','".$obj->_get('complemento')."','".$obj->_get('numero')."',
					'".$obj->_get('linkedin')."','".$obj->_get('googleplus')."','".$obj->_get('facebook')."','".$obj->_get('imgName')."',
					".$obj->_get('cep').",'".$obj->_get('bairro')."',now(),".$loc['lng'].",".$loc['lat'].")";

					if($this->o_db->exec($exec)>0){ // verifica se a inserção na tabela usuario ocorreu com sucesso
						$caminho = "../img/perfil/";//mostra o destino da pasta que a imagem vai ser salva
						if(move_uploaded_file($obj->_get('imgTemp'),$caminho.$obj->_get('imgName'))){//faz upload da imagem para o servidor --> (objeto temp,caminho para pasta e nome do arquivo)
							$exec="insert into tbusuario_has_tbtipousuario values ('".$obj->_get('cpf')."',".$obj->_get('idTipoUsuario').")";
							if($this->o_db->exec($exec)>0){ //verifica se a inserção na tabela usuario_has_tipoUsuario ocorreu com sucesso
								$exec = "insert into tbprestador values
								('".$obj->_get('cpf')."','".$obj->_get('perfil')."','".$obj->_get('instituicao')."','".$obj->_get('curso')."',
								".$obj->_get('escolaridade').",".$obj->_get('ocupacao').",2)";
								if($this->o_db->exec($exec)>0){ // verifica se a inserção na tabela prestador ocorreu com sucesso
									$exec = "insert into tbdisponibilidade
									values
									(".$obj->_get('segunda').",".$obj->_get('terca').",".$obj->_get('quarta').",".$obj->_get('quinta').",".$obj->_get('sexta').",".$obj->_get('sabado').",
									".$obj->_get('domingo').",".$obj->_get('segundaInicio').",".$obj->_get('tercaInicio').",".$obj->_get('quartaInicio').",".$obj->_get('quintaInicio')."
									,".$obj->_get('sextaInicio').",".$obj->_get('sabadoInicio').",".$obj->_get('domingoInicio').",".$obj->_get('segundaFim').",".$obj->_get('tercaFim')."
									,".$obj->_get('quartaFim').",".$obj->_get('quintaFim').",".$obj->_get('sextaFim').",".$obj->_get('sabadoFim').",".$obj->_get('domingoFim')."
									,'".$obj->_get('cpf')."')";
									if($this->o_db->exec($exec)>0){
										$flag = 1; // verificar se alguns do tipo de serviços ira acontecer algum erro de inserção
										if(!empty($obj->_get('baba'))){
										$exec = "insert into tbprestador_has_tbtiposervico values
										('".$obj->_get('cpf')."',".$obj->_get('baba').")";
											if($this->o_db->exec($exec)>0){
											}else{
												$flag = 0;
											}
										}
										if(!empty($obj->_get('idoso'))){
											$exec = "insert into tbprestador_has_tbtiposervico values
											('".$obj->_get('cpf')."',".$obj->_get('idoso').")";
											if($this->o_db->exec($exec)>0){
											}else{
												$flag = 0;
											}
										}
										if(!empty($obj->_get('limpeza'))){
											$exec = "insert into tbprestador_has_tbtiposervico values
											('".$obj->_get('cpf')."',".$obj->_get('limpeza').")";
											if($this->o_db->exec($exec)>0){
											}else{
												$flag = 0;
											}
										}

										if(!empty($_SESSION["contas"])){
											foreach($_SESSION["contas"] as $ob){
												$exec = "insert into tbcontaprestador
												(agencia,numero_conta,digito_verificador,codigo_banco,cpf_prestador,id_tipo_conta,is_conta_padrao)
												values
												('".$ob['agencia']."','".$ob['conta']."','".$ob['verificador']."','".$ob['codBanco']."',
												'".$obj->_get('cpf')."',".$ob['tpconta'].",".$ob['padrao'].")";
												if($this->o_db->exec($exec)>0){ // verifica inserção na tabela conta prestador ocorreu com sucesso
												}else{
													$message = "Erro ao cadastrar conta.".$ob["conta"];
													$this->error($message);
												}
											}
										}

										$cpf = $obj->_get('cpf');
										if(!empty($_SESSION["documentos"])){
											foreach($_SESSION["documentos"] as $ob){
												$exec="insert into tbdocumentos values
												('".$ob['numero']."','".$ob['orgao']."',".$ob['codEstado'].",'".$ob['frenteName']."',
												'".$ob['trasName']."',".$ob['tpdoc'].",'".$cpf."')";
												if($this->o_db->exec($exec)>0){  // verifica inserção na tabela documentos ocorreu com sucesso
												}else{
													unlink("../img/doc/".$ob['frenteName']);
													unlink("../img/doc/".$ob['trasName']);
													$message = "Erro ao cadastrar Documento.".$ob['numero'];
													$this->error($message);
												}
											}
										}


										if($flag == 1){
											//mensagem que usuario irá receber
											$msg = '<!DOCTYPE html>
												<html>
												<head>
												<title>E-mail +Services</title>
												<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:200|Roboto" rel="stylesheet">
												</head>
												<body style="width: 600px; text-align: center;">
												<table  style="font-family: \'Open Sans Condensed\', sans-serif; background-color: #00bad2; height: 200px; width: 100%" >
												<tr style="height: 35px"></tr>
												<tr>
												<td width="10%"></td>
												<td valign="top" ><img src="http://mservices.com.br/img/logo/logo-header.png" alt="+Services" width="120"></td>
												<td style="color: #fff; font-size: 20px;">UMA CONEXÃO DE QUALIDADE E RAPIDEZ ENTRE PROFISSIONAIS E CLIENTES</td>
												</tr>
												</table>

												<table width="100%" style="font-family: \'Roboto\', sans-serif;">
												<tr>
												<td width="0px"></td>
												<td valign="top"><h1>Olá, '.$obj->_get('nome').',</h1></td>
												</tr>
												<tr>
												<td width="10%"></td>
												<td valign="top" style="text-align: left;">obrigado por se cadastrar em nossa plataforma.</td>
												</tr>
												</table>

												<table width="100%" style="font-family: \'Roboto\', sans-serif;">
												<tr height="15px"></tr>
												<tr>
												<td valign="top" style="text-align: left;">Seu cadastro está em análise por motivos de segurança, nossa equipe entrará em contato antes da liberação do uso do aplicativo. Este processo é simples e rápido, mas muito importante. <br> Caso seu cadastro seja aprovado, você receberá um email de confirmação.</td>
												</tr>
												<tr height="15px"></tr>
												</table>

												<table width="100%" style="font-family: \'Roboto\', sans-serif;">
												<tr>
												<td width="10%"></td>
												<td valign="top" style="text-align: left;">Atenciosamente,</td>
												</tr>
												<tr>
												<td width="10%"></td>
												<td valign="top" style="text-align: left;">Equipe +Services</td>
												</tr>
												<tr>
												<td width="10%"></td>
												<td valign="top" style="text-align: left;">Por favor, não responda esse email.</td>
												</tr>
												</table>

												<table width="100%" style="font-family: \'Open Sans Condensed\', sans-serif;">
												<tr style="height: 25px"></tr>
												<tr>
												<td valign="top" style="text-align: center; background-color:#f7ffd7; height: 100px; border:solid 1px #00bad2; color:#00bad2;"><a href="http://www.mservices.com.br" target="_blanc" ><button style="margin-top: 35px; background-color:#f7ffd7; border: none; font-size: 30px; color: #00bad2" >www.mservices.com.br</button></a></td>
												</tr>
												</table>
												</body>
												</html>';
											//envia email
											$header = 'From: suporte@mservices.com.br'."\r\n".'Content-type: text/html; charset=UTF-8';
											$ok = mail($obj->_get('email'), "+Services - Cadastro", $msg,$header);
											//se email enviado com sucesso --> true
											if($ok){
												$_POST["nome"] = null;
												$_POST['cpf'] = null;
												$_POST["dtnasc"] = null;
												$_POST["email"] = null;
												$_POST["fone"] = null;
												$_POST["cell"] = null;
												$_POST["sexo"] = null;
												$_POST["cep"] = null;
												$_POST["rua"] = null;
												$_POST["numero"] = null;
												$_POST["bairro"]= null;
												$_POST["complemento"] = null;
												$_POST["cidade"]= null;
												$_POST["linkedin"] = null;
												$_POST["googleplus"] = null;
												$_POST["facebook"] = null;
												$_POST["instituicao"] = null;
												$_POST["curso"] = null;
												$_POST["perfil"] = null;
												$_SESSION["contas"] = array();
												$_SESSION["documentos"] = array();

												$message = "Prestador Registrado com Sucesso !";
												$this->sucesso($message);
											}else{
												$rollback = "delete from  tbdisponibilidade where cpf_prestador = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												$rollback = "delete from  tbprestador_has_tbtiposervico where cpf_prestador = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												$rollback = "delete from  tbdocumentos where cpf_prestador = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												$rollback = "delete from  tbcontaprestador where cpf_prestador = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												$rollback = "delete from  tbprestador where cpf_prestador = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												$rollback = "delete from  tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
												$this->o_db->exec($rollback);
												unlink("../img/perfil/".$obj->_get('imgName'));
												$message = "Não foi possivel enviar E-mail ! Tente Novamente mais tarde.";
												$this->error($message);
											}
										}else{
											$rollback = "delete from  tbdisponibilidade where cpf_prestador = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											$rollback = "delete from  tbprestador_has_tbtiposervico where cpf_prestador = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											$rollback = "delete from  tbdocumentos where cpf_prestador = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											$rollback = "delete from  tbcontaprestador where cpf_prestador = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											$rollback = "delete from  tbprestador where cpf_prestador = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											$rollback = "delete from  tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
											$this->o_db->exec($rollback);
											unlink("../img/perfil/".$obj->_get('imgName'));
											$message = "Verifique os campos obrigatórios ! Tente Novamente.";
											$this->error($message);

										}
									}else{
										$rollback = "delete from  tbprestador where cpf_prestador = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										unlink("../img/perfil/".$obj->_get('imgName'));
										$message = "0 Verifique os campos obrigatórios ! Tente Novamente.";
										$this->error($message);
									}
								}else{
									$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from  tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									unlink("../img/perfil/".$obj->_get('imgName'));
									$message = "Verifique os campos obrigatórios ! Tente Novamente.";
									$this->error($message);
								}
							}else{
								$rollback = "delete from  tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								unlink("../img/perfil/".$obj->_get('imgName'));
								$message = "Verifique os campos obrigatórios ! Tente Novamente.";
								$this->error($message);
							}
						}else{
							$rollback = "delete from  tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
							$this->o_db->exec($rollback);
							$message = "Desculpe, erro ao carregar foto de perfil ! Tente Novamente.";
							$this->error($message);
						}
					}else{
						$message = "Verifique os campos obrigatórios ! Tente Novamente.";
						$this->error($message);
					}
				}else{
					$message = "Este email ja foi cadastrado.";
					$this->error($message);
				}
			// caso ele ja seja um usuario e queria mudar de contexto
			}else{
				$loc = $this->addLocalizacao($obj->_get('cep'),$obj->_get('idCidade'));

				$exec = "update tbusuario set
				imagem_usuario ='".$obj->_get('imgName')."',
				id_cidade = ".$obj->_get('idCidade').",
				rua_usuario = '".$obj->_get('rua')."',
				complemento_usuario = '".$obj->_get('complemento')."',
				numero_usuario = '".$obj->_get('numero')."',
				linkedin_usuario = '".$obj->_get('linkedin')."',
				googleplus_usuario = '".$obj->_get('googleplus')."',
				facebook_usuario = '".$obj->_get('facebook')."',
				cep_usuario = ".$obj->_get('cep').",
				lat = ".$loc['lat'].",
				lng = ".$loc['lng'].",
				bairro_usuario = '".$obj->_get('bairro')."'
					where cpf_usuario = '".$obj->_get('cpf')."'";
				$this->o_db->exec($exec);
				$caminho = "../img/perfil/";//mostra o destino da pasta que a imagem vai ser salva
				if(move_uploaded_file($obj->_get('imgTemp'),$caminho.$obj->_get('imgName'))){//faz upload da imagem para o servidor --> (objeto temp,caminho para pasta e nome do arquivo)
					$exec="insert into tbusuario_has_tbtipousuario values ('".$obj->_get('cpf')."',".$obj->_get('idTipoUsuario').")";
					if($this->o_db->exec($exec)>0){ //verifica se a inserção na tabela usuario_has_tipoUsuario ocorreu com sucesso
						$exec = "insert into tbprestador values
						('".$obj->_get('cpf')."','".$obj->_get('perfil')."','".$obj->_get('instituicao')."','".$obj->_get('curso')."',
						".$obj->_get('escolaridade').",".$obj->_get('ocupacao').",2)";
						if($this->o_db->exec($exec)>0){ // verifica se a inserção na tabela prestador ocorreu com sucesso
							$exec = "insert into tbdisponibilidade
							values
							(".$obj->_get('segunda').",".$obj->_get('terca').",".$obj->_get('quarta').",".$obj->_get('quinta').",".$obj->_get('sexta').",".$obj->_get('sabado').",
							".$obj->_get('domingo').",".$obj->_get('segundaInicio').",".$obj->_get('tercaInicio').",".$obj->_get('quartaInicio').",".$obj->_get('quintaInicio')."
							,".$obj->_get('sextaInicio').",".$obj->_get('sabadoInicio').",".$obj->_get('domingoInicio').",".$obj->_get('segundaFim').",".$obj->_get('tercaFim')."
							,".$obj->_get('quartaFim').",".$obj->_get('quintaFim').",".$obj->_get('sextaFim').",".$obj->_get('sabadoFim').",".$obj->_get('domingoFim')."
							,'".$obj->_get('cpf')."')";
							if($this->o_db->exec($exec)>0){
								$flag = 1; // verificar se alguns do tipo de serviços ira acontecer algum erro de inserção
								if(!empty($obj->_get('baba'))){
									$exec = "insert into tbprestador_has_tbtiposervico values
									('".$obj->_get('cpf')."',".$obj->_get('baba').")";
									if($this->o_db->exec($exec)>0){
									}else{
										$flag = 0;
									}
								}
								if(!empty($obj->_get('idoso'))){
									$exec = "insert into tbprestador_has_tbtiposervico values
									('".$obj->_get('cpf')."',".$obj->_get('idoso').")";
									if($this->o_db->exec($exec)>0){
									}
									else{
										$flag = 0;
									}
								}
								if(!empty($obj->_get('limpeza'))){
									$exec = "insert into tbprestador_has_tbtiposervico values
									('".$obj->_get('cpf')."',".$obj->_get('limpeza').")";
									if($this->o_db->exec($exec)>0){
									}else{
										$flag = 0;
									}
								}

								if(!empty($_SESSION["contas"])){
									foreach($_SESSION["contas"] as $ob){
										$exec = "insert into tbcontaprestador
										(agencia,numero_conta,digito_verificador,codigo_banco,cpf_prestador,id_tipo_conta,is_conta_padrao)
										values
										('".$ob['agencia']."','".$ob['conta']."','".$ob['verificador']."','".$ob['codBanco']."',
										'".$obj->_get('cpf')."',".$ob['tpconta'].",".$ob['padrao'].")";
										if($this->o_db->exec($exec)>0){ // verifica inserção na tabela conta prestador ocorreu com sucesso
										}else{
											$message = "Erro ao cadastrar conta.".$ob["conta"];
											$this->error($message);
										}
									}
								}

								$cpf = $obj->_get('cpf');
								if(!empty($_SESSION["documentos"])){
									foreach($_SESSION["documentos"] as $ob){
										$exec="insert into tbdocumentos values
										('".$ob['numero']."','".$ob['orgao']."',".$ob['codEstado'].",'".$ob['frenteName']."',
										'".$ob['trasName']."',".$ob['tpdoc'].",'".$cpf."')";
										echo $exec;
										if($this->o_db->exec($exec)>0){  // verifica inserção na tabela documentos ocorreu com sucesso
										}else{
											unlink("../img/doc/".$ob['frenteName']);
											unlink("../img/doc/".$ob['trasName']);
											$message = "Erro ao cadastrar Documento.".$ob['numero'];
											$this->error($message);
										}
									}
								}



								if($flag == 1){
									//mensagem que usuario irá receber
									$msg = '<!DOCTYPE html>
										<html>
										<head>
										<title>E-mail +Services</title>
										<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:200|Roboto" rel="stylesheet">
										</head>
										<body style="width: 600px; text-align: center;">
										<table  style="font-family: \'Open Sans Condensed\', sans-serif; background-color: #00bad2; height: 200px; width: 100%" >
										<tr style="height: 35px"></tr>
										<tr>
										<td width="10%"></td>
										<td valign="top" ><img src="http://mservices.com.br/img/logo/logo-header.png" alt="+Services" width="120"></td>
										<td style="color: #fff; font-size: 20px;">UMA CONEXÃO DE QUALIDADE E RAPIDEZ ENTRE PROFISSIONAIS E CLIENTES</td>
										</tr>
										</table>

										<table width="100%" style="font-family: \'Roboto\', sans-serif;">
										<tr>
										<td width="0px"></td>
										<td valign="top"><h1>Olá, '.$obj->_get('nome').',</h1></td>
										</tr>
										<tr>
										<td width="10%"></td>
										<td valign="top" style="text-align: left;">obrigado por se cadastrar em nossa plataforma.</td>
										</tr>
										</table>

										<table width="100%" style="font-family: \'Roboto\', sans-serif;">
										<tr height="15px"></tr>
										<tr>
										<td valign="top" style="text-align: left;">Seu cadastro está em análise por motivos de segurança, nossa equipe entrará em contato antes da liberação do uso do aplicativo. Este processo é simples e rápido, mas muito importante. <br> Caso seu cadastro seja aprovado, você receberá um email de confirmação.</td>
										</tr>
										<tr height="15px"></tr>
										</table>

										<table width="100%" style="font-family: \'Roboto\', sans-serif;">
										<tr>
										<td width="10%"></td>
										<td valign="top" style="text-align: left;">Atenciosamente,</td>
										</tr>
										<tr>
										<td width="10%"></td>
										<td valign="top" style="text-align: left;">Equipe +Services</td>
										</tr>
										<tr>
										<td width="10%"></td>
										<td valign="top" style="text-align: left;">Por favor, não responda esse email.</td>
										</tr>
										</table>

										<table width="100%" style="font-family: \'Open Sans Condensed\', sans-serif;">
										<tr style="height: 25px"></tr>
										<tr>
										<td valign="top" style="text-align: center; background-color:#f7ffd7; height: 100px; border:solid 1px #00bad2; color:#00bad2;"><a href="http://www.mservices.com.br" target="_blanc" ><button style="margin-top: 35px; background-color:#f7ffd7; border: none; font-size: 30px; color: #00bad2" >www.mservices.com.br</button></a></td>
										</tr>
										</table>
										</body>
										</html>';
									//envia email
									$header = 'From: suporte@mservices.com.br'."\r\n".'Content-type: text/html; charset=UTF-8';
									$ok = mail($obj->_get('email'), "+Services - Cadastro", $msg,$header);
									//se email enviado com sucesso --> true
									if($ok){
										$_POST["nome"] = null;
										$_POST['cpf'] = null;
										$_POST["dtnasc"] = null;
										$_POST["email"] = null;
										$_POST["fone"] = null;
										$_POST["cell"] = null;
										$_POST["sexo"] = null;
										$_POST["cep"] = null;
										$_POST["rua"] = null;
										$_POST["numero"] = null;
										$_POST["bairro"]= null;
										$_POST["complemento"] = null;
										$_POST["cidade"]= null;
										$_POST["linkedin"] = null;
										$_POST["googleplus"] = null;
										$_POST["facebook"] = null;
										$_POST["instituicao"] = null;
										$_POST["curso"] = null;
										$_POST["perfil"] = null;
										$_SESSION["contas"] = array();
										$_SESSION["documentos"] = array();

										$message = "Prestador Registrado com Sucesso ! Verifique seu email para ativação de conta";
										$this->sucesso($message);
									}else{
										$rollback = "delete from  tbdisponibilidade where cpf_prestador = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbprestador_has_tbtiposervico where cpf_prestador = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbdocumentos where cpf_prestador = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbcontaprestador where cpf_prestador = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbprestador where cpf_prestador = '".$obj->_get('cpf')."'";
										$this->o_db->exec($rollback);
										$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."' and id_tipo_usuario = 3";
										$this->o_db->exec($rollback);
										unlink("../img/perfil/".$obj->_get('imgName'));
										$message = "Não foi possivel enviar E-mail ! Tente Novamente mais tarde.";
										$this->error($message);
									}
								}else{
									$rollback = "delete from  tbdisponibilidade where cpf_prestador = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from  tbprestador_has_tbtiposervico where cpf_prestador = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from  tbdocumentos where cpf_prestador = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from  tbcontaprestador where cpf_prestador = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from  tbprestador where cpf_prestador = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."' and id_tipo_usuario = 3";
									$this->o_db->exec($rollback);
									unlink("../img/perfil/".$obj->_get('imgName'));
									$message = "Verifique os campos obrigatórios ! Tente Novamente.";
									$this->error($message);
								}
							}else{
								$rollback = "delete from  tbprestador where cpf_prestador = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."' and id_tipo_usuario = 3";
								$this->o_db->exec($rollback);
								unlink("../img/perfil/".$obj->_get('imgName'));
								$message = "Verifique os campos obrigatórios ! Tente Novamente.";
								$this->error($message);

							}
						}else{
							$rollback = "delete from  tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."' and id_tipo_usuario = 3";
							$this->o_db->exec($rollback);
							unlink("../img/perfil/".$obj->_get('imgName'));
							$message = "Verifique os campos obrigatórios ! Tente Novamente.";
							$this->error($message);
						}
					}else{
						unlink("../img/perfil/".$obj->_get('imgName'));
						$message = "Desculpe, erro inesperado ! Tente Novamente.";
						$this->error($message);
					}
				}else{
					$message = "Erro no upload da foto, tente novamente mais tarde !";
					$this->error($message);
				}
			}
		}else{
			$message = "Este Usuario já foi cadastrado.";
			$this->error($message);
		}
	}//fim da função

	function Listar(){
			$exec="select * from tbusuario
					inner join tbprestador on cpf_usuario = cpf_prestador
					inner join tbstatususuario st on st.id = status_prestador
					order by 1";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerPrestador;
					$oc->_set('cpf',$r->cpf_prestador);
					$oc->_set('nome',$r->nome_usuario);
					$oc->_set('email',$r->email_usuario);
					$oc->_set('sexo',$r->sexo_usuario);
					$oc->_set('dtnasc',$r->nasc_usuario);
					$oc->_set('cell',$r->celular_usuario);
					$oc->_set('status',$r->descricao_status_usuario);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>CPF</th>
								<th>Nome</th>
								<th>Email</th>
								<th>Sexo</th>
								<th>Data de Nascimento</th>
								<th>Celular</th>
								<th>Status</th>
								<th>Alterar</th>
								<th>Disponibilidade</th>
								<th>Contas</th>
								<th>Documentos</th>
								<th>Mudar Status</th>
							</tr>
						</thead>
						<tbody>";
				foreach($lista as $obj){

				echo "<tr>
						<td  data-cpf='".$obj->_get('cpf')."'>".$obj->_get('cpf')."</td>
						<td>".$obj->_get('nome')."</td>
						<td>".$obj->_get('email')."</td>
						<td>".$obj->_get('sexo')."</td>
						<td>".$obj->_get('dtnasc')."</td>
						<td>".$obj->_get('cell')."</td>
						<td>".$obj->_get('status')."</td>
						<td><a class='btn btn-default btn-md editarPrestador' data-toggle='modal' data-target='#contact' data-original-title>Editar</a></td>
						<td><a class='btn btn-default btn-md editarDisponibilidade' data-toggle='modal' data-target='#modalDisponibilidade' data-original-title>Disponibilidade</a></td>
						<td><a class='btn btn-default btn-md editarContas' data-toggle='modal' data-target='#modalListaConta' data-original-title>Contas</a></td>
						<td><a class='btn btn-default btn-md editarDocs' data-toggle='modal' data-target='#modalListaDocumentos' data-original-title>Documentos</a></td>
						<td><a class='btn btn-default btn-md editarStatus' data-toggle='modal' data-target='#modalStatus' data-original-title>Mudar Status</a></td>
					</tr>";
				}
			echo"</tbody>
				</table>";
	}

	function Editar(BeanPrestador $obj){
		$flag = 0;
		$message = "Dados Alterados com sucesso !";
		$obj->_set('imgName',$obj->_get('cpf').$obj->_get('imgName')); //renomea o nome da foto com o cpf e a extensao da imagem
		$exec = "select * from tbusuario
		inner join tbprestador pr on cpf_usuario = pr.cpf_prestador
		inner join tbcontaprestador cp on pr.cpf_prestador = cp.cpf_prestador
		where is_conta_padrao = true and cpf_usuario = '".$obj->_get('cpf')."'";
			$o_data = $this->o_db->query($exec);
			$aux = $o_data->fetchObject();
			//estanciando os valores para comparar se houve alguma variavel modificada
			$usu = new controllerPrestador;
			$usu->_set('nome',$aux->nome_usuario);
			$usu->_set('cpf',$aux->cpf_usuario);
			$usu->_set('dtnasc',$aux->nasc_usuario);
			$usu->_set('email',$aux->email_usuario);
			$usu->_set('fone',$aux->telefone_usuario);
			$usu->_set('cell',$aux->celular_usuario);
			$usu->_set('sexo',$aux->sexo_usuario);
			$usu->_set('cep',$aux->cep_usuario);
			$usu->_set('rua',$aux->rua_usuario);
			$usu->_set('numero',$aux->numero_usuario);
			$usu->_set('bairro',$aux->bairro_usuario);
			$usu->_set('complemento',$aux->complemento_usuario);
			$usu->_set('idCidade',$aux->id_cidade);
			$usu->_set('linkedin',$aux->linkedin_usuario);
			$usu->_set('googleplus',$aux->googleplus_usuario);
			$usu->_set('facebook',$aux->facebook_usuario);
			$usu->_set('imgName',$aux->imagem_usuario);

			$usu->_set('perfil',$aux->perfil_prestador);
			$usu->_set('instituicao',$aux->instituicao_prestador);
			$usu->_set('curso',$aux->curso_prestador);
			$usu->_set('codEscolaridade',$aux->id_escolaridade);
			$usu->_set('codOcupacao',$aux->id_ocupacao);

			$usu->_set('agencia',$aux->agencia);
			$usu->_set('conta',$aux->numero_conta);
			$usu->_set('verificador',$aux->digito_verificador);
			$usu->_set('codBanco',$aux->codigo_banco);
			$usu->_set('codTpConta',$aux->id_tipo_conta);
			$usu->_set('baba',NULL);
			$usu->_set('idoso',NULL);
			$usu->_set('limpeza',NULL);

			$tipoServicos = array();
			$sql = "SELECT cpf_prestador,descricao_tipo_servico from tbprestador_has_tbtiposervico
					inner join tbtiposervico on id = id_tipo_servico
					where cpf_prestador = '".$obj->_get('cpf')."'";
			$o_data = $this->o_db->query($sql);
			while($ts = $o_data->fetchObject()){
				if($ts->descricao_tipo_servico == "Babás"){
					$usu->_set('baba',$ts->descricao_tipo_servico);
				}
				if($ts->descricao_tipo_servico == "Cuidador de idosos"){
					$usu->_set('idoso',$ts->descricao_tipo_servico);
				}
				if($ts->descricao_tipo_servico == "Limpeza doméstica"){
					$usu->_set('limpeza',$ts->descricao_tipo_servico);
				}
			}


			//verificando se as variaveis foi modificada
			if($usu->_get('nome') != $obj->_get('nome') || $usu->_get('cpf') != $obj->_get('cpf') || $usu->_get('dtnasc') != $obj->_get('dtnasc')
				|| $usu->_get('email') != $obj->_get('email') || $usu->_get('fone') != $obj->_get('fone') || $usu->_get('cell') != $obj->_get('cell')
				|| $usu->_get('sexo') != $obj->_get('sexo')|| $usu->_get('cep') != $obj->_get('cep') || $usu->_get('rua') != $obj->_get('rua')
				|| $usu->_get('numero') != $obj->_get('numero')|| $usu->_get('bairro') != $obj->_get('bairro') || $usu->_get('complemento') != $obj->_get('complemento')
				|| $usu->_get('idCidade') != $obj->_get('idCidade') || $usu->_get('linkedin') != $obj->_get('linkedin') || $usu->_get('googleplus') != $obj->_get('googleplus')
				|| $usu->_get('facebook') != $obj->_get('facebook') || $obj->_get('perfil') != $usu->_get('perfil') || $obj->_get('curso') != $usu->_get('curso')
				|| $obj->_get('instituicao') != $usu->_get('instituicao') || $obj->_get('escolaridade') != $usu->_get('codEscolaridade')
				|| $obj->_get('ocupacao') != $usu->_get('codOcupacao') || $obj->_get('agencia') != $usu->_get('agencia') || $obj->_get('conta') != $usu->_get('conta')
				|| $obj->_get('verificador') != $usu->_get('verificador') || $obj->_get('banco') != $usu->_get('codBanco')
				|| $obj->_get('tpConta') != $usu->_get('codTpConta') || $obj->_get('baba') != $usu->_get('baba')
				|| $obj->_get('idoso') != $usu->_get('idoso') || $obj->_get('limpeza') != $usu->_get('limpeza') || !empty($obj->_get('imgTemp')) ){
					if(!empty($obj->_get('imgTemp'))){
						$exec="update tbusuario set
						imagem_usuario = '".$obj->_get('imgName')."'
							where cpf_usuario = '".$obj->_get('cpf')."'
						";
						if($this->o_db->exec($exec)>0){
							$caminho = "images/perfil/";//mostra o destino da pasta que a imagem vai ser salva
							if(move_uploaded_file($obj->_get('imgTemp'),$caminho.$obj->_get('imgName'))){//faz upload da imagem para o servidor --> (objeto temp,caminho para pasta e nome do arquivo)
								$message = "Dados Alterados com sucesso !";
 							}else{
								$message = "Ocorreu um erro, tente novamente !";
								$this->error($message);
								return;
							}
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}

			if($usu->_get('nome') != $obj->_get('nome') || $usu->_get('cpf') != $obj->_get('cpf') || $usu->_get('dtnasc') != $obj->_get('dtnasc')
				|| $usu->_get('email') != $obj->_get('email') || $usu->_get('fone') != $obj->_get('fone') || $usu->_get('cell') != $obj->_get('cell')
				|| $usu->_get('sexo') != $obj->_get('sexo')|| $usu->_get('cep') != $obj->_get('cep') || $usu->_get('rua') != $obj->_get('rua')
				|| $usu->_get('numero') != $obj->_get('numero')|| $usu->_get('bairro') != $obj->_get('bairro') || $usu->_get('complemento') != $obj->_get('complemento')
				|| $usu->_get('idCidade') != $obj->_get('idCidade') || $usu->_get('linkedin') != $obj->_get('linkedin') || $usu->_get('googleplus') != $obj->_get('googleplus')
				|| $usu->_get('facebook') != $obj->_get('facebook') ){

					$loc = $this->addLocalizacao($obj->_get('cep'),$obj->_get('idCidade'));

					$exec="update tbusuario set
					nome_usuario ='".$obj->_get('nome')."',
					nasc_usuario = '".$obj->_get('dtnasc')."',
					telefone_usuario = '".$obj->_get('fone')."',
					celular_usuario = '".$obj->_get('cell')."',
					email_usuario = '".$obj->_get('email')."',
					sexo_usuario = '".$obj->_get('sexo')."',
					id_cidade = ".$obj->_get('idCidade').",
					rua_usuario = '".$obj->_get('rua')."',
					complemento_usuario = '".$obj->_get('complemento')."',
					numero_usuario = '".$obj->_get('numero')."',
					linkedin_usuario = '".$obj->_get('linkedin')."',
					googleplus_usuario = '".$obj->_get('googleplus')."',
					facebook_usuario = '".$obj->_get('facebook')."',
					cep_usuario = ".$obj->_get('cep').",
					lat = ".$loc['lat'].",
					lng = ".$loc['lng'].",
					bairro_usuario = '".$obj->_get('bairro')."'
						where cpf_usuario = '".$obj->_get('cpf')."'
					";
					if($this->o_db->exec($exec)>0){
						$message = "Dados Alterados com sucesso !";
					}else{
						$message = "Ocorreu um erro, tente novamente !";
						$this->error($message);
						return;
					}
				}
				if($obj->_get('perfil') != $usu->_get('perfil') || $obj->_get('curso') != $usu->_get('curso')
				|| $obj->_get('instituicao') != $usu->_get('instituicao') || $obj->_get('escolaridade') != $usu->_get('codEscolaridade')
				|| $obj->_get('ocupacao') != $usu->_get('codOcupacao')){
					$exec="update tbprestador set
					perfil_prestador = '".$obj->_get('perfil')."',
					instituicao_prestador = '".$obj->_get('instituicao')."',
					curso_prestador = '".$obj->_get('curso')."',
					id_escolaridade = ".$obj->_get('escolaridade').",
					id_ocupacao = ".$obj->_get('ocupacao')."
						where cpf_prestador = '".$obj->_get('cpf')."'
					";
					if($this->o_db->exec($exec)>0){
						$message = "Dados Alterados com sucesso !";
					}else{
						$message = "Ocorreu um erro, tente novamente !";
						$this->error($message);
						return;
					}
				}

				if($obj->_get('baba') != $usu->_get('baba') || $obj->_get('idoso') != $usu->_get('idoso')
				|| $obj->_get('limpeza') != $usu->_get('limpeza')){

					//BABA
					if($obj->_get('baba') == NULL && $usu->_get('baba') != NULL ){
						$exec = "delete from tbprestador_has_tbtiposervico
							where cpf_prestador = '".$obj->_get('cpf')."' and id_tipo_servico = 2
						";
						if($this->o_db->exec($exec)>0){
							$message = "Dados Alterados com sucesso !";
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}else if($obj->_get('baba') != NULL && $usu->_get('baba') == NULL){
						$exec = "insert into tbprestador_has_tbtiposervico values
							('".$obj->_get('cpf')."',2)
						";
						if($this->o_db->exec($exec)>0){
							$message = "Dados Alterados com sucesso !";
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}

					//IDOSO
					if($obj->_get('idoso') == NULL && $usu->_get('idoso') != NULL ){
						$exec = "delete from tbprestador_has_tbtiposervico
							where cpf_prestador = '".$obj->_get('cpf')."' and id_tipo_servico = 1
						";
						if($this->o_db->exec($exec)>0){
							$message = "Dados Alterados com sucesso !";
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}else if($obj->_get('idoso') != NULL && $usu->_get('idoso') == NULL){
						$exec = "insert into tbprestador_has_tbtiposervico values
							('".$obj->_get('cpf')."',1)
						";
						if($this->o_db->exec($exec)>0){
							$message = "Dados Alterados com sucesso !";
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}

					//LIMPEZA
					if($obj->_get('limpeza') == NULL && $usu->_get('limpeza') != NULL ){
						$exec = "delete from tbprestador_has_tbtiposervico
							where cpf_prestador = '".$obj->_get('cpf')."' and id_tipo_servico = 3
						";
						if($this->o_db->exec($exec)>0){
							$message = "Dados Alterados com sucesso !";
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}else if($obj->_get('limpeza') != NULL && $usu->_get('limpeza') == NULL ){
						$exec = "insert into tbprestador_has_tbtiposervico values
							('".$obj->_get('cpf')."',3)
						";
						if($this->o_db->exec($exec)>0){
							$message = "Dados Alterados com sucesso !";
						}else{
							$message = "Ocorreu um erro, tente novamente !";
							$this->error($message);
							return;
						}
					}

				}

					$this->sucesso($message);

				}else{
					$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
					$this->error($message);
				}
	}//fim da função

	function Excluir($cpf,$idStatus){
		$exec="update tbprestador set
		status_prestador = ".$idStatus."
		where cpf_prestador = '".$cpf."'";
		if($this->o_db->exec($exec)>0){
			$message = "Status do prestador alterado com Sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Erro na Mudança de status do Prestador.";
			$this->error($message);
		}
	}

	function editarDisponibilidade(BeanPrestador $obj){
		$exec = "update tbdisponibilidade set
		segunda =".$obj->_get('segunda').",
		terca = ".$obj->_get('terca').",
		quarta = ".$obj->_get('quarta').",
		quinta = ".$obj->_get('quinta').",
		sexta = ".$obj->_get('sexta').",
		sabado = ".$obj->_get('sabado').",
		domingo = ".$obj->_get('domingo').",
		hora_inicio_segunda = ".$obj->_get('segundaInicio').",
		hora_inicio_terca = ".$obj->_get('tercaInicio').",
		hora_inicio_quarta = ".$obj->_get('quartaInicio').",
		hora_inicio_quinta = ".$obj->_get('quintaInicio').",
		hora_inicio_sexta = ".$obj->_get('sextaInicio').",
		hora_inicio_sabado = ".$obj->_get('sabadoInicio').",
		hora_inicio_domingo = ".$obj->_get('domingoInicio').",
		hora_fim_segunda = ".$obj->_get('segundaFim').",
		hora_fim_terca = ".$obj->_get('tercaFim').",
		hora_fim_quarta = ".$obj->_get('quartaFim').",
		hora_fim_quinta = ".$obj->_get('quintaFim').",
		hora_fim_sexta = ".$obj->_get('sextaFim').",
		hora_fim_sabado = ".$obj->_get('sabadoFim').",
		hora_fim_domingo = ".$obj->_get('domingoFim')."
			where cpf_prestador = '".$obj->_get('cpf')."'";
		if($this->o_db->exec($exec)>0){
			$message = "Disponibilidade alterada com sucesso !";
			$this->sucesso($message);
		}else{
			$message = "Ocorreu um erro, tente novamente !";
			$this->error($message);
		}
	}

	function ListarTipoServico(){
		$exec = "select * from tbtiposervico order by descricao_tipo_servico";
		$dados = $this->o_db->query($exec);
		$lista= array();
		while($r=$dados->fetchobject()){
			$oc= new controllerPrestador;
			$oc->_set('idServico',$r->id);
			$oc->_set('servico',$r->descricao_tipo_servico);
			array_push($lista, $oc);
		}//while
		$c = 1;
		foreach($lista as $obj){
			if($obj->_get("servico") == "Babás"){
				$id = "babas";
			}
			if($obj->_get("servico") == "Cuidador de idosos"){
				$id = "idosos";
			}
			if($obj->_get("servico") == "Limpeza doméstica"){
				$id = "limpeza";
			}
			echo "
				<div class='funkyradio-info'>
					<input type='checkbox' id='".$id."' name='".$id."' id='checkbox$c' value='".$obj->_get('idServico')."'/>
					<label for='".$id."'>".$obj->_get('servico')."</label>
				</div>
			";
			$c++;
		}
	}

	function ListarTipoDocumento(){
		$exec="select * from tbtipodocumentos";
		$dados=$this->o_db->query($exec);
		$lista= array();
			while($r=$dados->fetchobject()){
				$oc= new controllerPrestador;
				$oc->_set('codDoc',$r->id);
				$oc->_set('doc',$r->descricao);
				array_push($lista, $oc);
			}//while
		foreach($lista as $obj){
			echo"<option value=".$obj->_get('codDoc').">".$obj->_get('doc')."</option>";
		}
	}

	function ListarBanco(){
		$exec="select * from tbbanco order by nome_banco";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerPrestador;
					$oc->_set('codBanco',$r->codigo_banco);
					$oc->_set('banco',$r->nome_banco);
					array_push($lista, $oc);
				}//while
		foreach($lista as $obj){
			echo"<option value=".$obj->_get('codBanco').">".$obj->_get('codBanco')." | ".$obj->_get('banco')."</option>";
		}
	}

	function ListarTipoUsuario(){
		$exec="select * from tbtipousuario where id= 3";
		$executar=$this->o_db->query($exec);
		$dados= $executar->fetchObject();
		$id = $dados->id;
		$desc = $dados->descricao_tipo_usuario;

		echo"<option value=".$id.">".$desc."</option>";
	}

	function ListarEscolaridade(){
		$exec="select * from tbescolaridade order by id";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerPrestador;
					$oc->_set('codEscolaridade',$r->id);
					$oc->_set('escolaridade',$r->descricao_escolaridade );
					array_push($lista, $oc);
				}//while
		foreach($lista as $obj){
			echo"<option value=".$obj->_get('codEscolaridade')." >".$obj->_get('escolaridade')."</option>";
		}
	}

	function ListarOcupacao(){
		$exec = "select * from tbocupacao order by nome_ocupacao";
		$dados  = $this->o_db->query($exec);
		$lista = array();

		while($r = $dados->fetchObject()){
			$oc = new controllerPrestador;
			$oc->_set('codOcupacao',$r->id);
			$oc->_set('ocupacao',$r->nome_ocupacao);
			array_push($lista,$oc);
		}
		if(empty($lista))
			echo"<option value=''>-- Indísponivel no momento --</option>";
		else{
			foreach($lista as $obj){
				echo"<option value=".$obj->_get('codOcupacao').">".$obj->_get('ocupacao')."</option>";
			}
		}
	}

	function sucesso($message){
		echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
                			<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                			<span class='glyphicon glyphicon-ok'></span>
                    				".$message."
            		</div>";
	}

	function ListarEstado(){
		$lista= array();
			$exec="select * from tbestado order by nome";
			$executar=$this->o_db->query($exec);
				while($r=$executar->fetchobject()){
					$oc= new controllerPrestador;
					$oc->_set('idEstado',$r->id);
					$oc->_set('estado',$r->nome);
					array_push($lista, $oc);
				}//while
				foreach($lista as $obj){
					echo"<option value=".$obj->_get('idEstado').">".$obj->_get('estado')."</option>";
				}
	}

	function ListarTipoConta(){
		$lista = array();
		$exec = "select * from tbtipoconta";
		$executar = $this->o_db->query($exec);
		while($obj = $executar->fetchObject()){
			$new = new controllerPrestador;
			$new->_set('codTpConta',$obj->id);
			$new->_set('tpConta',$obj->descricao_tipo_conta);
			array_push($lista,$new);
		}
		foreach($lista as $obj){
			echo"<option value=".$obj->_get('codTpConta').">".$obj->_get('tpConta')."</option>";
		}
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                		<button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                		<span class='glyphicon glyphicon-exclamation-sign'></span>
                    			".$message."
            	</div>";
	}
}
?>