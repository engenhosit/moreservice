<?php
include "Connection.php";
class DaoUsuario extends Connection{
	function salvar(BeanUsuario $obj){

		$exec = "select count(*) as cont from tbusuario usu
		inner join tbusuario_has_tbtipousuario tu on usu.cpf_usuario = tu.cpf_usuario
		where tu.cpf_usuario ='".$obj->_get('cpf')."' and id_tipo_usuario = 1";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){ //verifica se ja é adm

			$exec = "select count(*) as cont from tbusuario where cpf_usuario ='".$obj->_get('cpf')."'";
			$o_data = $this->o_db->query($exec);
			$aux = $o_data->fetchObject();
			$id = $aux->cont;

			if($id == 0){

				$exec = "select count(*) as cont from tbusuario where email_usuario ='".$obj->_get('email')."'";
				$o_data = $this->o_db->query($exec);
				$aux = $o_data->fetchObject();
				$id = $aux->cont;

				if($id == 0){ //verifica se email ja existe

					$exec="insert into tbusuario
					(cpf_usuario,nome_usuario,nasc_usuario,telefone_usuario,celular_usuario,email_usuario,sexo_usuario,id_cidade,senha,
					rua_usuario,complemento_usuario,numero_usuario,linkedin_usuario,googleplus_usuario,facebook_usuario,cep_usuario,bairro_usuario)
					values
					('".$obj->_get('cpf')."','".$obj->_get('nome')."','".$obj->_get('dtnasc')."','".$obj->_get('fone')."',
					'".$obj->_get('cell')."','".$obj->_get('email')."','".$obj->_get('sexo')."',".$obj->_get('idCidade').",
					'".$obj->_get('senha')."','".$obj->_get('rua')."','".$obj->_get('complemento')."','".$obj->_get('numero')."',
					'".$obj->_get('linkedin')."','".$obj->_get('googleplus')."','".$obj->_get('facebook')."',".$obj->_get('cep').",
					'".$obj->_get('bairro')."')";
						if($this->o_db->exec($exec)>0){
							$exec="insert into tbusuario_has_tbtipousuario values ('".$obj->_get('cpf')."',".$obj->_get('idTipoUsuario').")";
							if($this->o_db->exec($exec)>0){
								$exec="insert into tbadministradores values ('".$obj->_get('cpf')."',1)";
								if($this->o_db->exec($exec)>0){
									$_POST["nome"] = null;
									$_POST['cpf'] = null;
									$_POST["dtnasc"] = null;
									$_POST["email"] = null;
									$_POST["fone"] = null;
									$_POST["cell"] = null;
									$_POST["sexo"] = null;
									$_POST["cep"] = null;
									$_POST["rua"] = null;
									$_POST["numero"] = null;
									$_POST["bairro"]= null;
									$_POST["complemento"] = null;
									$_POST["cidade"]= null;
									$_POST["linkedin"] = null;
									$_POST["googleplus"] = null;
									$_POST["facebook"] = null;
									$message = "Usuário Registrado com Sucesso !";
									$this->sucesso($message);
								}else{
									$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$message = "Verifique os campos obrigatórios ! Tente Novamente.";
									$this->error($message);
								}
							}else{
								$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$rollback = "delete from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$message = "Verifique os campos obrigatórios ! Tente Novamente.";
								$this->error($message);
							}
						}else{
							$rollback = "delete from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
							$this->o_db->exec($rollback);
							$message = "Verifique os campos obrigatórios ! Tente Novamente.";
							$this->error($message);
						}
				}else{
					$message = "Este email ja foi cadastrado.";
					$this->error($message);
				}
			}else{
				$exec="insert into tbusuario_has_tbtipousuario values ('".$obj->_get('cpf')."',".$obj->_get('idTipoUsuario').")";
				if($this->o_db->exec($exec)>0){
					$exec="insert into tbadministradores values ('".$obj->_get('cpf')."',1)";
					if($this->o_db->exec($exec)>0){
						$_POST["nome"] = null;
						$_POST['cpf'] = null;
						$_POST["dtnasc"] = null;
						$_POST["email"] = null;
						$_POST["fone"] = null;
						$_POST["cell"] = null;
						$_POST["sexo"] = null;
						$_POST["cep"] = null;
						$_POST["rua"] = null;
						$_POST["numero"] = null;
						$_POST["bairro"]= null;
						$_POST["complemento"] = null;
						$_POST["cidade"]= null;
						$_POST["linkedin"] = null;
						$_POST["googleplus"] = null;
						$_POST["facebook"] = null;
						$message = "Usuário Registrado com Sucesso !";
						$this->sucesso($message);
					}else{
						$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
						$this->o_db->exec($rollback);
						$message = "Verifique os campos obrigatórios ! Tente Novamente.";
						$this->error($message);
					}
				}else{
					$message = "Verifique os campos obrigatórios ! Tente Novamente.";
					$this->error($message);
				}
			}
		}else{
			$message = "Este Usuário já foi cadastrado.";
			$this->error($message);
		}
	}//function

	function Listar(){
			$exec="select * from tbusuario usu
					inner join tbusuario_has_tbtipousuario tpusu on usu.cpf_usuario = tpusu.cpf_usuario
					inner join tbadministradores ad on usu.cpf_usuario = ad.cpf_usuario
					inner join tbstatususuario st on st.id = status_usuario
					where id_tipo_usuario = 1 and usu.cpf_usuario <> '00000000000' and usu.cpf_usuario <> '".$_SESSION["login"][4]['cpf']."'
					order by usu.nome_usuario";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerUsuario;
					$oc->_set('nome',$r->nome_usuario);
					$oc->_set('cpf',$r->cpf_usuario);
					$oc->_set('dtnasc',$r->nasc_usuario);
					$oc->_set('email',$r->email_usuario);
					$oc->_set('fone',$r->telefone_usuario);
					$oc->_set('cell',$r->celular_usuario);
					$oc->_set('sexo',$r->sexo_usuario);
					$oc->_set('cep',$r->cep_usuario);
					$oc->_set('rua',$r->rua_usuario);
					$oc->_set('numero',$r->numero_usuario);
					$oc->_set('bairro',$r->bairro_usuario);
					$oc->_set('complemento',$r->complemento_usuario);
					$oc->_set('idCidade',$r->id_cidade);
					$oc->_set('linkedin',$r->linkedin_usuario);
					$oc->_set('googleplus',$r->googleplus_usuario);
					$oc->_set('facebook',$r->facebook_usuario);
					$oc->_set('status',$r->descricao_status_usuario);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>CPF</th>
								<th>Nome</th>
								<th>email</th>
								<th>Celular</th>
								<th>Sexo</th>
								<th>Status</th>
								<th>Alterar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>";
				foreach($lista as $obj){

				echo "<tr>
						<form action='' method='POST'>
							<td  data-cpf='".$obj->_get('cpf')."'>".$obj->_get('cpf')."</td>
							<td>".$obj->_get('nome')."</td>
							<td>".$obj->_get('email')."</td>
							<td>".$obj->_get('cell')."</td>
							<td>".$obj->_get('sexo')."</td>
							<td>".$obj->_get('status')."</td>
							<td><a id='editarUsuario' class='btn btn-default btn-lg editarUsuario' data-toggle='modal' data-target='#contact' data-original-title>Editar</a></td>
							<td><a class='btn btn-default btn-lg editarStatus' data-toggle='modal' data-target='#modalStatus' data-original-title>Mudar Status</a></td>
						</form>
					</tr>";
				}
			echo"</tbody>
				</table>";

	}//function

	function Editar(BeanUsuario $obj){
		$exec = "select * from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		//estanciando os valores para comparar se houve alguma variavel modificada
		$usu = new controllerUsuario;
		$usu->_set('nome',$aux->nome_usuario);
		$usu->_set('cpf',$aux->cpf_usuario);
		$usu->_set('dtnasc',$aux->nasc_usuario);
		$usu->_set('email',$aux->email_usuario);
		$usu->_set('fone',$aux->telefone_usuario);
		$usu->_set('cell',$aux->celular_usuario);
		$usu->_set('sexo',$aux->sexo_usuario);
		$usu->_set('cep',$aux->cep_usuario);
		$usu->_set('rua',$aux->rua_usuario);
		$usu->_set('numero',$aux->numero_usuario);
		$usu->_set('bairro',$aux->bairro_usuario);
		$usu->_set('complemento',$aux->complemento_usuario);
		$usu->_set('idCidade',$aux->id_cidade);
		$usu->_set('linkedin',$aux->linkedin_usuario);
		$usu->_set('googleplus',$aux->googleplus_usuario);
		$usu->_set('facebook',$aux->facebook_usuario);

		//verificando se as variaveis foi modificada
		if($usu->_get('nome') != $obj->_get('nome') || $usu->_get('cpf') != $obj->_get('cpf') || $usu->_get('dtnasc') != $obj->_get('dtnasc')
			|| $usu->_get('email') != $obj->_get('email') || $usu->_get('fone') != $obj->_get('fone') || $usu->_get('cell') != $obj->_get('cell')
			|| $usu->_get('sexo') != $obj->_get('sexo')|| $usu->_get('cep') != $obj->_get('cep') || $usu->_get('rua') != $obj->_get('rua')
			|| $usu->_get('numero') != $obj->_get('numero')|| $usu->_get('bairro') != $obj->_get('bairro') || $usu->_get('complemento') != $obj->_get('complemento')
			|| $usu->_get('idCidade') != $obj->_get('idCidade') || $usu->_get('linkedin') != $obj->_get('linkedin') || $usu->_get('googleplus') != $obj->_get('googleplus')
			|| $usu->_get('facebook') != $obj->_get('facebook')){

				//verifica se o email ja existe
				$exec = "select count(*) as cont from tbusuario where email_usuario ='".$obj->_get('email')."'";
				$o_data = $this->o_db->query($exec);
				$aux = $o_data->fetchObject();
				$id = $aux->cont;
			//verifica se o email ja existe ou se é o proprio email do usuario
			if($id == 0 || $usu->_get('email') == $obj->_get('email')){
				$exec="update tbusuario set
						nome_usuario ='".$obj->_get('nome')."',
						nasc_usuario = '".$obj->_get('dtnasc')."',
						telefone_usuario = '".$obj->_get('fone')."',
						celular_usuario = '".$obj->_get('cell')."',
						email_usuario = '".$obj->_get('email')."',
						sexo_usuario = '".$obj->_get('sexo')."',
						id_cidade = ".$obj->_get('idCidade').",
						rua_usuario = '".$obj->_get('rua')."',
						complemento_usuario = '".$obj->_get('complemento')."',
						numero_usuario = '".$obj->_get('numero')."',
						linkedin_usuario = '".$obj->_get('linkedin')."',
						googleplus_usuario = '".$obj->_get('googleplus')."',
						facebook_usuario = '".$obj->_get('facebook')."',
						cep_usuario = ".$obj->_get('cep').",
						bairro_usuario = '".$obj->_get('bairro')."'
							where cpf_usuario = '".$obj->_get('cpf')."'
						";
				if($this->o_db->exec($exec)>0){
					//verifica e atualiza o email do usuario que estiver logado
					$_SESSION["login"][4]['email'] = $obj->_get('email');

					$message = "Usuario Modificado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Desculpe, ocorreu um erro ! Tente Novamente.";
					$this->error($message);
				}
			}else{
				$message = "Desculpe, Este Email é inválido.";
				$this->error($message);
			}
		}else{
			$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
			$this->error($message);

		}
	}//function

	function Excluir($cpf,$idStatus){
		$exec="update tbadministradores set
		status_usuario = ".$idStatus."
		where cpf_usuario = '".$cpf."'";
		if($this->o_db->exec($exec)>0){
			$message = "Status do usuário alterado com sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Erro na Mudança de status do usuário.";
			$this->error($message);
		}
	}

	function ListarTipoUsuario(){
			$exec="select * from tbtipousuario where id = 1";
			$executar=$this->o_db->query($exec);
			$dados= $executar->fetchObject();
			$id = $dados->id;
			$desc = $dados->descricao_tipo_usuario;

			echo"<option value=".$id.">".$desc."</option>";
			echo"</select>";
	}

	function ListarEstado(){
		$lista= array();
			$exec="select * from tbestado order by nome";
			$executar=$this->o_db->query($exec);
				while($r=$executar->fetchobject()){
					$oc= new controllerUsuario;
					$oc->_set('idEstado',$r->id);
					$oc->_set('estado',$r->nome);
					array_push($lista, $oc);
				}//while
				foreach($lista as $obj){
					echo"<option value=".$obj->_get('idEstado').">".$obj->_get('estado')."</option>";
				}
	}

	function ListarCidade(){
		$lista= array();
			$exec="select * from tbcidade order by nome";
			$executar=$this->o_db->query($exec);
				while($r=$executar->fetchobject()){
					$oc= new controllerUsuario;
					$oc->_set('idCidade',$r->id);
					$oc->_set('cidade',$r->nome);
					array_push($lista, $oc);
				}//while
				foreach($lista as $obj){
					echo"<option value=".$obj->_get('idCidade').">".$obj->_get('cidade')."</option>";
				}
	}

	function sucesso($message){
		echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
	                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
	                <span class='glyphicon glyphicon-ok'></span>

	                    ".$message."
	            </div>";
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

                    ".$message."
            </div>
			";
	}

	function TrocarSenha($password){
		if(strlen($password) > 5)
		{
			$exec="update tbusuario set
				senha = '".sha1($password)."'
				where cpf_usuario = '".$_SESSION["login"][4]['cpf']."'";

			if($this->o_db->query($exec)){
				$this->sucesso("Sua senha foi alterado com sucesso !");
			}
			else{
				$this->error("Desculpe-nos, ouve um erro. Tente novamente !");
			}
		}
		else{
			$this->error("Senha deve conter no minimo 6 caracteres");
		}
	}

}//class
?>