<?php
include "Connection.php";
class DaoTipoConta extends Connection{
	function salvar(BeanTipoConta $obj){

		$exec = "select count(*) as cont from tbtipoconta where descricao_tipo_conta = '".$obj->_get('descricao')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$exec="insert into tbtipoconta
			(descricao_tipo_conta)
			values
			('".$obj->_get('descricao')."')";
				if($this->o_db->exec($exec)>0){
					$message = "TipoConta Registrado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Desculpe, Este Email é inválido.";
					$this->error($message);
				}
		}else{
			$message = "Este serviço já foi cadastrado.";
			$this->error($message);
		}
	}//function

	function Listar(){
			$exec="select * from tbtipoconta";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerTipoConta;
					$oc->_set('id',$r->id);
					$oc->_set('descricao',$r->descricao_tipo_conta);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>Codigo</th>
								<th>Descrição</th>
								<th>Alterar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>";
				$c = 0;//variavel controladora do modal --> vai identificar qual modal vai ser aberto ao clicar no botão editar
				foreach($lista as $obj){

				echo "<tr>
						<form action='' method='POST'>
							<td>".$obj->_get('id')."</td>
							<td>".$obj->_get('descricao')."</td>
							<td><a class='btn btn-default btn-lg' data-toggle='modal' data-target='#contact".$c."' data-original-title>Editar</a></td>
							<td><input type='submit' value='Excluir' name='FormTipoContaExcluir' class='btn btn-default btn-lg'></td>
							<input type='hidden' value='".$obj->_get('id')."' name='CodExcluir'>
						</form>
					</tr>";
				$c++;//contadora
				}
			echo"</tbody>
				</table>";

				$c = 0; //variavel para modificar o id do modal
			foreach($lista as $obj){
			echo"<!-- ModalUpdate -->
						<div class='modal fade'	 id='contact".$c."' tabindex='-1' >
							<div class='modal-dialog'>
								<div class='panel panel-primary'>
									<div class='panel-heading'>
										<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
										<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
									</div>
									<form action='' method='POST'>
									<div class='modal-body' style='padding: 5px;'>
										<label>Codigo:</label>
										<input class='form-control' name='CodUpdate' placeholder='Codigo' type='text' value='".$obj->_get('id')."' required readonly />
										<br/>
										<label>Descrição</label>
										<br>
										<input class='form-control' name='descricao' value='".$obj->_get('descricao')."' />
										<br>
									<div class='panel-footer' style='margin-bottom:-14px;'>
										<input type='submit' name='FormTipoContaEditar' class='btn btn-success' value='Editar'/>
										<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
									</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					<!-- Fim ModalUpdate -->";
					$c++;//contadora
			}
	}//function

	function Editar(BeanTipoConta $obj){

		$exec = "select count(*) as cont from tbtipoconta where descricao_tipo_conta = '".$obj->_get('descricao')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		$exec = "select * from tbtipoconta where id = ".$obj->_get('id');
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$descricao = $aux->descricao_tipo_conta;

		if($descricao != $obj->_get('descricao')){
			if($id == 0){
				$exec="update tbtipoconta set
						descricao_tipo_conta = '".$obj->_get('descricao')."'
						where id = ".$obj->_get('id');
				if($this->o_db->exec($exec)>0){
					$message = "Tipo de Conta Modificado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Desculpe, ocorreu um erro ! Tente Novamente.";
					$this->error($message);
				}
			}else{
				$message = "Desculpe este Tipo de Conta já foi cadastrado.";
				$this->error($message);
			}
		}else{
			$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
			$this->error($message);

		}
	}//function

	function Excluir($cod){
		$exec="delete from tbtipoconta
			where id=".$cod;
			$this->o_db->exec($exec);

		$exec = "select count(*) as cont from tbtipoconta where id = ".$cod;
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$message = "Tipo de Conta Excluido com Sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Erro na exclusão do Tipo de Conta.";
			$this->error($message);
		}
	}

	function sucesso($message){
				echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-ok'></span>

                    ".$message."
            </div>";
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

                    ".$message."
            </div>
			";
	}
}//class
?>