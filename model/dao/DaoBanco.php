<?php
include "Connection.php";
class DaoBanco extends Connection{
	function salvar(BeanBanco $obj){

		$exec = "select count(*) as cont from tbbanco where nome_banco ='".$obj->_get('nome')."' or codigo_banco = '".$obj->_get('cod')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$exec="insert into tbbanco
			values (".$obj->_get('cod').",'".$obj->_get('nome')."','".$obj->_get('site')."')";
				if($this->o_db->exec($exec)>0){
					$message = "Banco Registrado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Desculpe, ocorreu um erro ! Tente Novamente.";
					$this->error($message);
				}
			}else{
				$message = "Este Banco já foi cadastrado.";
				$this->error($message);
			}
	}//function salvar

	function Listar(){
			$exec="select * from tbbanco";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerBanco;
					$oc->_set('cod',$r->codigo_banco);
					$oc->_set('nome',$r->nome_banco);
					$oc->_set('site',$r->site_banco);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>Codigo</th>
								<th>Banco</th>
								<th>Site</th>
								<th>Alterar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>";
				$c = 0;//variavel controladora do modal --> vai identificar qual modal vai ser aberto ao clicar no botão editar
				foreach($lista as $obj){

				echo "<tr>
						<form action='' method='POST'>
							<td>".$obj->_get('cod')."</td>
							<td>".$obj->_get('nome')."</td>
							<td>".$obj->_get('site')."</td>
							<td><a class='btn btn-default btn-lg' data-toggle='modal' data-target='#contact".$c."' data-original-title>Editar</a></td>
							<td><input type='submit' value='Excluir' name='FormBancoExcluir' class='btn btn-default btn-lg'></td>
							<input type='hidden' value='".$obj->_get('cod')."' name='CodExcluir'>
						</form>
					</tr>";
				$c++;//contadora
				}
			echo"</tbody>
				</table>";

				$c = 0; //variavel para modificar o id do modal
			foreach($lista as $obj){
			echo"<!-- ModalUpdate -->
						<div class='modal fade'	 id='contact".$c."' tabindex='-1' >
							<div class='modal-dialog'>
								<div class='panel panel-primary'>
									<div class='panel-heading'>
										<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
										<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
									</div>
									<form action='' method='POST'>
									<div class='modal-body' style='padding: 5px;'>
										<label>Código do Banco *</label>
										<input class='form-control' name='cod' placeholder='Digite o código do banco...' type='text' value='".$obj->_get('cod')."' readonly />
										<br/>
										<label>Nome do Banco *</label>
										<input class='form-control' name='nome' placeholder='Digite o nome do banco aqui...' type='text' value='".$obj->_get('nome')."' required autofocus />
										<br/>
										<label>Site</label>
										<input type='text' id='site' name='site' class='form-control' placeholder='Digite o site do banco aqui...' value='".$obj->_get('site')."' required/>
										<br/>
									<div class='panel-footer' style='margin-bottom:-14px;'>
										<input type='submit' name='FormBancoEditar' class='btn btn-success' value='Editar'/>
										<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
									</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					<!-- Fim ModalUpdate -->";
					$c++;//contadora
			}
	}

	function Editar(BeanBanco $obj){

		$exec = "select count(*) as cont from tbbanco where nome_banco = '".$obj->_get('nome')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		$exec = "select * from tbbanco where codigo_banco = '".$obj->_get('cod')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$nome = $aux->nome_banco;
		$site = $aux->site_banco;


		if($nome != $obj->_get('nome') || $site != $obj->_get('site')){
			if ($id == 0  || $nome == $obj->_get('nome')){
				$exec="update tbbanco set
						nome_banco = '".$obj->_get('nome')."',
						site_banco = '".$obj->_get('site')."'
							where codigo_banco = '".$obj->_get('cod')."'";
				if($this->o_db->exec($exec)>0){
					$message = "Banco Modificado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Desculpe, ocorreu um erro ! Tente Novamente.";
					$this->error($message);
				}
			}else{
				$message = "Desculpe, está descrição é inválida.";
				$this->error($message);
			}
		}else{
			$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
			$this->error($message);
		}
	}//function editar

	function Excluir($cod){
		$exec="delete from tbbanco
			where codigo_banco= '".$cod."'";
			$this->o_db->exec($exec);

		$exec = "select count(*) as cont from tbbanco where codigo_banco = '".$cod."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$message = "Banco Excluido com Sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Desculpe,ocorreu um erro na exclusão do Banco.";
			$this->error($message);
		}
	}

	function sucesso($message){
				echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-ok'></span>

                    ".$message."
            </div>";
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

                    ".$message."
            </div>
			";
	}

}//class
?>