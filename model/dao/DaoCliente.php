<?php
include "Connection.php";
class DaoCliente extends Connection{
	function salvar(BeanCliente $obj){

		$exec = "select count(*) as cont from tbusuario usu
		inner join tbusuario_has_tbtipousuario tu on usu.cpf_usuario = tu.cpf_usuario
		where tu.cpf_usuario ='".$obj->_get('cpf')."' and id_tipo_usuario = 2";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){ //verifica se ja é clinte

			$exec = "select count(*) as cont from tbusuario where cpf_usuario ='".$obj->_get('cpf')."'";
			$o_data = $this->o_db->query($exec);
			$aux = $o_data->fetchObject();
			$id = $aux->cont;

			if($id == 0){

				$exec = "select count(*) as cont from tbusuario where email_usuario ='".$obj->_get('email')."'";
				$o_data = $this->o_db->query($exec);
				$aux = $o_data->fetchObject();
				$id = $aux->cont;

				if($id == 0){ //verifica se email ja existe

				$exec="insert into tbusuario
				(cpf_usuario,nome_usuario,nasc_usuario,telefone_usuario,celular_usuario,email_usuario,sexo_usuario,id_cidade,senha,
				rua_usuario,complemento_usuario,numero_usuario,linkedin_usuario,googleplus_usuario,facebook_usuario,cep_usuario,bairro_usuario)
				values
				('".$obj->_get('cpf')."','".$obj->_get('nome')."','".$obj->_get('dtnasc')."','".$obj->_get('fone')."',
				'".$obj->_get('cell')."','".$obj->_get('email')."','".$obj->_get('sexo')."',".$obj->_get('idCidade').",
				'".$obj->_get('senha')."','".$obj->_get('rua')."','".$obj->_get('complemento')."','".$obj->_get('numero')."',
				'".$obj->_get('linkedin')."','".$obj->_get('googleplus')."','".$obj->_get('facebook')."',".$obj->_get('cep').",
				'".$obj->_get('bairro')."')";
					if($this->o_db->exec($exec)>0){
						$exec="insert into tbusuario_has_tbtipousuario values ('".$obj->_get('cpf')."',".$obj->_get('idTipoUsuario').")";
						if($this->o_db->exec($exec)>0){
							$exec="insert into tbcliente values ('".$obj->_get('cpf')."',2)";
							if($this->o_db->exec($exec)>0){


								if(!empty($_SESSION["filhos"])){
									foreach($_SESSION["filhos"] as $ob){
										$exec = "insert into tbfilhos
										(nome_filho,data_nasc,sexo,cuidado_especial,descricao_especial,cpf_cliente)
										values
										('".$ob['nome']."','".$ob['dtnasc']."','".$ob['sexo']."',".$ob['cuidadoEspecial'].",
										'".$ob['descricao']."','".$obj->_get('cpf')."')";
										if($this->o_db->exec($exec)>0){ // verifica inserção na tabela filho prestador ocorreu com sucesso
										}else{
											$message = "Erro ao cadastrar filho(a). ".$ob["nome"];
											$this->error($message);
										}
									}
								}

								if(!empty($_SESSION["idosos"])){
									foreach($_SESSION["idosos"] as $ob){
										$exec = "insert into tbidosos
										(nome_idoso,data_nasc,sexo,cuidado_especial,descricao_especial,cpf_cliente)
										values
										('".$ob['nome']."','".$ob['dtnasc']."','".$ob['sexo']."',".$ob['cuidadoEspecial'].",
										'".$ob['descricao']."','".$obj->_get('cpf')."')";
										if($this->o_db->exec($exec)>0){ // verifica inserção na tabela idoso prestador ocorreu com sucesso
										}else{
											$message = "Erro ao cadastrar idoso(a). ".$ob["nome"];
											$this->error($message);
										}
									}
								}

								//link de verificação de conta
								$link = "http://www.mservices.com.br/verificacaoCadastro.php?key=".base64_encode($obj->_get('cpf'))."&tp=2";
								//mensagem que usuario irá receber
								$msg = '<!DOCTYPE html>
									<html>
									<head>
									<title>E-mail +Services</title>
									<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:200|Roboto" rel="stylesheet">
									</head>
									<body style="width: 600px; text-align: center;">
									<table  style="font-family: \'Open Sans Condensed\', sans-serif; background-color: #00bad2; height: 200px; width: 100%" >
									<tr style="height: 35px"></tr>
									<tr>
									<td width="10%"></td>
									<td valign="top" ><img src="http://mservices.com.br/img/logo/logo-header.png" alt="+Services" width="120"></td>
									<td style="color: #fff; font-size: 20px;">UMA CONEXÃO DE QUALIDADE E RAPIDEZ ENTRE PROFISSIONAIS E CLIENTES</td>
									</tr>
									</table>

									<table width="100%" style="font-family: \'Roboto\', sans-serif;">
									<tr>
									<td width="0px"></td>
									<td valign="top"><h1>Olá, '.$obj->_get('nome').',</h1></td>
									</tr>
									<tr>
									<td width="10%"></td>
									<td valign="top" style="text-align: left;">obrigado por se cadastrar em nossa plataforma.</td>
									</tr>
									<tr>
									<td width="10%"></td>
									<td valign="top" style="text-align: left;">Para ter acesso é necessário que você valide seu email clicando abaixo:</td>
									</tr>
									</table>

									<table width="100%" style="font-family: \'Roboto\', sans-serif;">
									<tr height="15px"></tr>
									<tr>
									<td valign="top"><a href="'.$link.'" target="_blanc"><button style="cursor:pointer; border:none; padding:10px; color:#fff; background-color:#00bad2;">Clique aqui para validar seu email</button></a></td>
									</tr>
									<tr height="15px"></tr>
									</table>

									<table width="100%" style="font-family: \'Roboto\', sans-serif;">
									<tr>
									<td width="10%"></td>
									<td valign="top" style="text-align: left;">Caso você tenha problemas ao clicar no botão acima, copie e cole este link no endereço do seu navegador:</td>
									</tr>
									<tr>
									<td width="0px"></td>
									<td valign="top"><a href="'.$link.'" target="_blanc">'.$link.'</a></p></td>
									</tr>
									<tr>
									<td width="10%"></td>
									<td valign="top" style="text-align: left;">Atenciosamente,</td>
									</tr>
									<tr>
									<td width="10%"></td>
									<td valign="top" style="text-align: left;">Equipe +Services</td>
									</tr>
									<tr>
									<td width="10%"></td>
									<td valign="top" style="text-align: left;">Por favor, não responda esse email.</td>
									</tr>
									</table>

									<table width="100%" style="font-family: \'Open Sans Condensed\', sans-serif;">
									<tr style="height: 25px"></tr>
									<tr>
									<td valign="top" style="text-align: center; background-color:#f7ffd7; height: 100px; border:solid 1px #00bad2; color:#00bad2;"><a href="http://www.mservices.com.br" target="_blanc" ><button style="margin-top: 35px; background-color:#f7ffd7; border: none; font-size: 30px; color: #00bad2" >www.mservices.com.br</button></a></td>
									</tr>
									</table>
									</body>
									</html>';
								//envia email
								$header = 'From: suporte@mservices.com.br'."\r\n".'Content-type: text/html; charset=UTF-8';
								$ok = mail($obj->_get('email'), "+Services - Cadastro", $msg,$header);
								//se email enviado com sucesso --> true
								if($ok){
									$_POST["nome"] = null;
									$_POST['cpf'] = null;
									$_POST["dtnasc"] = null;
									$_POST["email"] = null;
									$_POST["fone"] = null;
									$_POST["cell"] = null;
									$_POST["sexo"] = null;
									$_POST["cep"] = null;
									$_POST["rua"] = null;
									$_POST["numero"] = null;
									$_POST["bairro"]= null;
									$_POST["complemento"] = null;
									$_POST["cidade"]= null;
									$_POST["linkedin"] = null;
									$_POST["googleplus"] = null;
									$_POST["facebook"] = null;
									$message = "Cliente Registrado com Sucesso ! Verifique seu email para ativação de conta";
									$this->sucesso($message);
								}else{
									$rollback = "delete from tbfilhos where cpf_cliente = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from tbidosos where cpf_cliente = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from tbcliente where cpf_cliente = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$rollback = "delete from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
									$this->o_db->exec($rollback);
									$message = "Não foi possivel enviar o email, tente novamente em alguns minutos !";
									$this->error($message);
								}
							}else{
								$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$rollback = "delete from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$message = "Verifique os campos obrigatórios ! Tente Novamente.";
								$this->error($message);
							}
						}else{
							$rollback = "delete from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
							$this->o_db->exec($rollback);
							$message = "Verifique os campos obrigatórios ! Tente Novamente.";
							$this->error($message);
						}
					}else{
						$message = "Verifique os campos obrigatórios ! Tente Novamente.";
						$this->error($message);
					}

					}else{
						$message = "Este email ja foi cadastrado.";
						$this->error($message);
					}
			}else{
				$exec="insert into tbusuario_has_tbtipousuario values ('".$obj->_get('cpf')."',".$obj->_get('idTipoUsuario').")";
				if($this->o_db->exec($exec)>0){
					$exec="insert into tbcliente values ('".$obj->_get('cpf')."',2)";
					if($this->o_db->exec($exec)>0){
							//link de verificação de conta
							$link = "http://www.mservices.com.br/verificacaoCadastro.php?key=".base64_encode($obj->_get('cpf'))."&tp=2";
							//mensagem que usuario irá receber
							$msg = '<!DOCTYPE html>
								<html>
								<head>
								<title>E-mail +Services</title>
								<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:200|Roboto" rel="stylesheet">
								</head>
								<body style="width: 600px; text-align: center;">
								<table  style="font-family: \'Open Sans Condensed\', sans-serif; background-color: #00bad2; height: 200px; width: 100%" >
								<tr style="height: 35px"></tr>
								<tr>
								<td width="10%"></td>
								<td valign="top" ><img src="http://mservices.com.br/img/logo/logo-header.png" alt="+Services" width="120"></td>
								<td style="color: #fff; font-size: 20px;">UMA CONEXÃO DE QUALIDADE E RAPIDEZ ENTRE PROFISSIONAIS E CLIENTES</td>
								</tr>
								</table>

								<table width="100%" style="font-family: \'Roboto\', sans-serif;">
								<tr>
								<td width="0px"></td>
								<td valign="top"><h1>Olá, '.$obj->_get('nome').',</h1></td>
								</tr>
								<tr>
								<td width="10%"></td>
								<td valign="top" style="text-align: left;">obrigado por se cadastrar em nossa plataforma.</td>
								</tr>
								<tr>
								<td width="10%"></td>
								<td valign="top" style="text-align: left;">Para ter acesso é necessário que você valide seu email clicando abaixo:</td>
								</tr>
								</table>

								<table width="100%" style="font-family: \'Roboto\', sans-serif;">
								<tr height="15px"></tr>
								<tr>
								<td valign="top"><a href="'.$link.'" target="_blanc"><button style="cursor:pointer; border:none; padding:10px; color:#fff; background-color:#00bad2;">Clique aqui para validar seu email</button></a></td>
								</tr>
								<tr height="15px"></tr>
								</table>

								<table width="100%" style="font-family: \'Roboto\', sans-serif;">
								<tr>
								<td width="10%"></td>
								<td valign="top" style="text-align: left;">Caso você tenha problemas ao clicar no botão acima, copie e cole este link no endereço do seu navegador:</td>
								</tr>
								<tr>
								<td width="0px"></td>
								<td valign="top"><a href="'.$link.'" target="_blanc">'.$link.'</a></p></td>
								</tr>
								<tr>
								<td width="10%"></td>
								<td valign="top" style="text-align: left;">Atenciosamente,</td>
								</tr>
								<tr>
								<td width="10%"></td>
								<td valign="top" style="text-align: left;">Equipe +Services</td>
								</tr>
								<tr>
								<td width="10%"></td>
								<td valign="top" style="text-align: left;">Por favor, não responda esse email.</td>
								</tr>
								</table>

								<table width="100%" style="font-family: \'Open Sans Condensed\', sans-serif;">
								<tr style="height: 25px"></tr>
								<tr>
								<td valign="top" style="text-align: center; background-color:#f7ffd7; height: 100px; border:solid 1px #00bad2; color:#00bad2;"><a href="http://www.mservices.com.br" target="_blanc" ><button style="margin-top: 35px; background-color:#f7ffd7; border: none; font-size: 30px; color: #00bad2" >www.mservices.com.br</button></a></td>
								</tr>
								</table>
								</body>
								</html>';
							$header = 'From: suporte@mservices.com.br'."\r\n".'Content-type: text/html; charset=UTF-8';
							//envia email
							$ok = mail($obj->_get('email'), "+Services - Cadastro", $msg);
							//se email enviado com sucesso --> true

							if($ok){
								$_POST["nome"] = null;
								$_POST['cpf'] = null;
								$_POST["dtnasc"] = null;
								$_POST["email"] = null;
								$_POST["fone"] = null;
								$_POST["cell"] = null;
								$_POST["sexo"] = null;
								$_POST["cep"] = null;
								$_POST["rua"] = null;
								$_POST["numero"] = null;
								$_POST["bairro"]= null;
								$_POST["complemento"] = null;
								$_POST["cidade"]= null;
								$_POST["linkedin"] = null;
								$_POST["googleplus"] = null;
								$_POST["facebook"] = null;
								$message = "Cliente Registrado com Sucesso ! Verifique seu email para ativação de conta";
								$this->sucesso($message);
							}else{
								$rollback = "delete from tbcliente where cpf_cliente = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
								$this->o_db->exec($rollback);
								$message = "Falha no enviou de email, tente novamente !";
								$this->error($message);
							}
					}else{
						$rollback = "delete from tbusuario_has_tbtipousuario where cpf_usuario = '".$obj->_get('cpf')."'";
						$this->o_db->exec($rollback);
						$message = "Verifique os campos obrigatórios ! Tente Novamente.";
						$this->error($message);
					}
				}else{
					$message = "Verifique os campos obrigatórios ! Tente Novamente.";
					$this->error($message);
				}
			}
		}else{
			$message = "Este Cliente já foi cadastrado.";
			$this->error($message);
		}
	}//function

	function Listar(){
			$exec="select * from tbusuario usu
					inner join tbusuario_has_tbtipousuario tpusu on usu.cpf_usuario = tpusu.cpf_usuario
					inner join tbcliente on usu.cpf_usuario = cpf_cliente
					inner join tbstatususuario st on st.id = status_cliente
					where id_tipo_usuario = 2 and usu.cpf_usuario <> '00000000000'
					order by usu.nome_usuario";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerCliente;
					$oc->_set('nome',$r->nome_usuario);
					$oc->_set('cpf',$r->cpf_usuario);
					$oc->_set('dtnasc',$r->nasc_usuario);
					$oc->_set('email',$r->email_usuario);
					$oc->_set('fone',$r->telefone_usuario);
					$oc->_set('cell',$r->celular_usuario);
					$oc->_set('sexo',$r->sexo_usuario);
					$oc->_set('cep',$r->cep_usuario);
					$oc->_set('rua',$r->rua_usuario);
					$oc->_set('numero',$r->numero_usuario);
					$oc->_set('bairro',$r->bairro_usuario);
					$oc->_set('complemento',$r->complemento_usuario);
					$oc->_set('idCidade',$r->id_cidade);
					$oc->_set('linkedin',$r->linkedin_usuario);
					$oc->_set('googleplus',$r->googleplus_usuario);
					$oc->_set('facebook',$r->facebook_usuario);
					$oc->_set('status',$r->descricao_status_usuario);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>CPF</th>
								<th>Nome</th>
								<th>email</th>
								<th>Celular</th>
								<th>Sexo</th>
								<th>Status</th>
								<th>Alterar</th>
								<th>Filhos</th>
								<th>Idosos</th>
								<th>Mudar Status</th>
							</tr>
						</thead>
						<tbody>";
				foreach($lista as $obj){

					echo "<tr>
						<td  data-cpf='".$obj->_get('cpf')."'>".$obj->_get('cpf')."</td>
						<td>".$obj->_get('nome')."</td>
						<td>".$obj->_get('email')."</td>
						<td>".$obj->_get('cell')."</td>
						<td>".$obj->_get('sexo')."</td>
						<td>".$obj->_get('status')."</td>
						<td><a class='btn btn-default btn-md editarUsuario' data-toggle='modal' data-target='#contact' data-original-title>Editar</a></td>
						<td><a class='btn btn-default btn-md listarFilhos' data-toggle='modal' data-target='#modalListaFilhos' data-original-title>Filhos</a></td>
						<td><a class='btn btn-default btn-md listarIdosos' data-toggle='modal' data-target='#modalListaIdosos' data-original-title>Idosos</a></td>
						<td><a class='btn btn-default btn-md editarStatus' data-toggle='modal' data-target='#modalStatus' data-original-title>Mudar Status</a></td>
					</tr>";
				}
			echo"</tbody>
				</table>";
	}//function

	function Editar(BeanCliente $obj){
		$exec = "select * from tbusuario where cpf_usuario = '".$obj->_get('cpf')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		//estanciando os valores para comparar se houve alguma variavel modificada
		$usu = new controllerCliente;
		$usu->_set('nome',$aux->nome_usuario);
		$usu->_set('cpf',$aux->cpf_usuario);
		$usu->_set('dtnasc',$aux->nasc_usuario);
		$usu->_set('email',$aux->email_usuario);
		$usu->_set('fone',$aux->telefone_usuario);
		$usu->_set('cell',$aux->celular_usuario);
		$usu->_set('sexo',$aux->sexo_usuario);
		$usu->_set('cep',$aux->cep_usuario);
		$usu->_set('rua',$aux->rua_usuario);
		$usu->_set('numero',$aux->numero_usuario);
		$usu->_set('bairro',$aux->bairro_usuario);
		$usu->_set('complemento',$aux->complemento_usuario);
		$usu->_set('idCidade',$aux->id_cidade);
		$usu->_set('linkedin',$aux->linkedin_usuario);
		$usu->_set('googleplus',$aux->googleplus_usuario);
		$usu->_set('facebook',$aux->facebook_usuario);

		//verificando se as variaveis foi modificada
		if($usu->_get('nome') != $obj->_get('nome') || $usu->_get('cpf') != $obj->_get('cpf') || $usu->_get('dtnasc') != $obj->_get('dtnasc')
			|| $usu->_get('email') != $obj->_get('email') || $usu->_get('fone') != $obj->_get('fone') || $usu->_get('cell') != $obj->_get('cell')
			|| $usu->_get('sexo') != $obj->_get('sexo')|| $usu->_get('cep') != $obj->_get('cep') || $usu->_get('rua') != $obj->_get('rua')
			|| $usu->_get('numero') != $obj->_get('numero')|| $usu->_get('bairro') != $obj->_get('bairro') || $usu->_get('complemento') != $obj->_get('complemento')
			|| $usu->_get('idCidade') != $obj->_get('idCidade') || $usu->_get('linkedin') != $obj->_get('linkedin') || $usu->_get('googleplus') != $obj->_get('googleplus')
			|| $usu->_get('facebook') != $obj->_get('facebook')){

				$exec="update tbusuario set
						nome_usuario ='".$obj->_get('nome')."',
						nasc_usuario = '".$obj->_get('dtnasc')."',
						telefone_usuario = '".$obj->_get('fone')."',
						celular_usuario = '".$obj->_get('cell')."',
						email_usuario = '".$obj->_get('email')."',
						sexo_usuario = '".$obj->_get('sexo')."',
						id_cidade = ".$obj->_get('idCidade').",
						rua_usuario = '".$obj->_get('rua')."',
						complemento_usuario = '".$obj->_get('complemento')."',
						numero_usuario = '".$obj->_get('numero')."',
						linkedin_usuario = '".$obj->_get('linkedin')."',
						googleplus_usuario = '".$obj->_get('googleplus')."',
						facebook_usuario = '".$obj->_get('facebook')."',
						cep_usuario = ".$obj->_get('cep').",
						bairro_usuario = '".$obj->_get('bairro')."'
							where cpf_usuario = '".$obj->_get('cpf')."'
						";
				if($this->o_db->exec($exec)>0){
					$message = "Cliente Modificado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Verifique os campos obrigatórios ! Tente Novamente.";
					$this->error($message);
				}
		}else{
			$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
			$this->error($message);
		}
	}//function

	function Excluir($cpf,$idStatus){
		$exec="update tbcliente set
		status_cliente = ".$idStatus."
		where cpf_cliente = '".$cpf."'";
		if($this->o_db->exec($exec)>0){
			$message = "Status do cliente alterado com Sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Erro na Mudança de status do Cliente.";
			$this->error($message);
		}
	}

	function ListarTipoUsuario(){
			$exec="select * from tbtipousuario where id = 2";
			$executar=$this->o_db->query($exec);
			$dados= $executar->fetchObject();
			$id = $dados->id;
			$desc = $dados->descricao_tipo_usuario;

			echo"<option value=".$id.">".$desc."</option>";
			echo"</select>";
	}

	function ListarEstado(){
		$lista= array();
			$exec="select * from tbestado order by nome";
			$executar=$this->o_db->query($exec);
				while($r=$executar->fetchobject()){
					$oc= new controllerCliente;
					$oc->_set('idEstado',$r->id);
					$oc->_set('estado',$r->nome);
					array_push($lista, $oc);
				}//while
				foreach($lista as $obj){
					echo"<option value=".$obj->_get('idEstado').">".$obj->_get('estado')."</option>";
				}
	}

	function ListarCidade(){
		$lista= array();
			$exec="select * from tbcidade order by nome";
			$executar=$this->o_db->query($exec);
				while($r=$executar->fetchobject()){
					$oc= new controllerCliente;
					$oc->_set('idCidade',$r->id);
					$oc->_set('cidade',$r->nome);
					array_push($lista, $oc);
				}//while
				foreach($lista as $obj){
					echo"<option value=".$obj->_get('idCidade').">".$obj->_get('cidade')."</option>";
				}
	}

	function sucesso($message){
				echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-ok'></span>

                    ".$message."
            </div>";
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

                    ".$message."
            </div>
			";
	}

}//class
?>