<?php
include "Connection.php";
class DaoTurno extends Connection{
	function salvar(BeanTurno $obj){

		$exec = "select count(*) as cont from tbturno where descricao_turno ='".$obj->_get('descricao')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;
		if($id == 0){
			$exec="insert into tbturno
			(descricao_turno,hora_inicio_turno,hora_fim_turno)
			values
			('".$obj->_get('descricao')."','".$obj->_get('horaInicio')."','".$obj->_get('horaFim')."')";
			if($this->o_db->exec($exec)>0){
				$message = "Turno Registrado com Sucesso.";
				$this->sucesso($message);
			}else{
				$message = "Desculpe, ocorreu um erro ! Tente Novamente.";
				$this->error($message);
			}
		}else{
			$message = "Este turno já foi cadastrado.";
			$this->error($message);
		}

	}//function salvar

	function Listar(){
			$exec="select * from tbturno order by id";
			$dados=$this->o_db->query($exec);
			$lista= array();
				while($r=$dados->fetchobject()){
					$oc= new controllerTurno;
					$oc->_set('cod',$r->id);
					$oc->_set('descricao',$r->descricao_turno);
					$oc->_set('horaInicio',$r->hora_inicio_turno);
					$oc->_set('horaFim',$r->hora_fim_turno);
					array_push($lista, $oc);
				}//while
				echo"<div class='panel-body'>
						<h3>Filtrar <small>( <i class='fa fa-search'></i> )</small></h3>
						<input type='text' class='form-control' id='pesquisar' data-action='filter' data-filters='#task-table' placeholder=' Filtro' />
					</div>

				<table class='table table-hover' id='dev-table'>
						<thead>
							<tr>
								<th>Codigo</th>
								<th>Descrição</th>
								<th>Hora Inicial</th>
								<th>Hora Final</th>
								<th>Alterar</th>
								<th>Excluir</th>
							</tr>
						</thead>
						<tbody>";
				$c = 0;//variavel controladora do modal --> vai identificar qual modal vai ser aberto ao clicar no botão editar
				foreach($lista as $obj){

				echo "<tr>
						<form action='' method='POST'>
							<td>".$obj->_get('cod')."</td>
							<td>".$obj->_get('descricao')."</td>
							<td>".$obj->_get('horaInicio')."</td>
							<td>".$obj->_get('horaFim')."</td>
							<td><a class='btn btn-default btn-lg' data-toggle='modal' data-target='#contact".$c."' data-original-title>Editar</a></td>
							<td><input type='submit' value='Excluir' name='FormTurnoExcluir' class='btn btn-default btn-lg'></td>
							<input type='hidden' value='".$obj->_get('cod')."' name='CodExcluir'>
						</form>
					</tr>";
				$c++;//contadora
				}
			echo"</tbody>
				</table>";

				$c = 0; //variavel para modificar o id do modal
			foreach($lista as $obj){
			echo"<!-- ModalUpdate -->
						<div class='modal fade'	 id='contact".$c."' tabindex='-1' >
							<div class='modal-dialog'>
								<div class='panel panel-primary'>
									<div class='panel-heading'>
										<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>
										<h4 class='panel-title' id='contactLabel'><span class='glyphicon glyphicon-info-sign'></span> Alterações</h4>
									</div>
									<form action='' method='POST'>
									<div class='modal-body' style='padding: 5px;'>
										<input class='form-control' name='CodUpdate' placeholder='Codigo' type='text' value='".$obj->_get('cod')."' readonly />
										<br/>
										<label>Turno</label>
										<input id='descricao' name='turno' class='form-control' placeholder='Digite a descrição do turno aqui...' value='".$obj->_get('descricao')."' required>
										<br/>
										<label>hora inicio</label>
										<input name='inicio' class='form-control time' placeholder='Digite o valor da hora de início aqui...' value='".$obj->_get('horaInicio')."' required>
										<br/>
										<label>hora fim</label>
										<input name='fim' class='form-control time' placeholder='Digite o valor da hora fim aqui...' value='".$obj->_get('horaFim')."' required>
										<br/>
									<div class='panel-footer' style='margin-bottom:-14px;'>
										<input type='submit' name='FormTurnoEditar' class='btn btn-success' value='Editar'/>
										<button style='float: right;' type='button' class='btn btn-default btn-close' data-dismiss='modal'>Close</button>
									</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					<!-- Fim ModalUpdate -->";
					$c++;//contadora
			}
	}

	function Editar(BeanTurno $obj){

		$exec = "select count(*) as cont from tbturno where descricao_turno ='".$obj->_get('descricao')."'";
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		$exec = "select * from tbturno where id = ".$obj->_get('cod');
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$desc = $aux->descricao_turno;
		$desc = strtolower($desc);
		$desc = ucfirst($desc);
		$inicio = $aux->hora_inicio_turno;
		$fim = $aux->hora_fim_turno;
		if($desc != $obj->_get('descricao') || $inicio != $obj->_get('horaInicio') || $fim != $obj->_get('horaFim')){
			if ($id == 0  || $desc == $obj->_get('descricao')){
				$exec="update tbturno set
					descricao_turno = '".$obj->_get('descricao')."',
					hora_inicio_turno = '".$obj->_get('horaInicio')."',
					hora_fim_turno = '".$obj->_get('horaFim')."'
					where id = ".$obj->_get('cod');
				if($this->o_db->exec($exec)>0){
					$message = "Turno Modificado com Sucesso.";
					$this->sucesso($message);
				}else{
					$message = "Desculpe, ocorreu um erro ! Tente Novamente.";
					$this->error($message);
				}
			}else{
				$message="Desculpe, Esta descrição é inválida.";
				$this->error($message);
			}
		}else{
			$message = "Nenhum campo foi alterado, por favor altere um dos campos !";
			$this->error($message);
		}
	}//function

	function Excluir($cod){
		$exec="delete from tbturno
			where id=".$cod;
			$this->o_db->exec($exec);

		$exec = "select count(*) as cont from tbturno where id = ".$cod;
		$o_data = $this->o_db->query($exec);
		$aux = $o_data->fetchObject();
		$id = $aux->cont;

		if($id == 0){
			$message = "Turno Excluido com Sucesso.";
			$this->sucesso($message);
		}else{
			$message = "Desculpe, ocorreu um erro na exclusão do Turno.";
			$this->error($message);
		}
	}//function

	function sucesso($message){
				echo"<div class='alert alert-success' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-ok'></span>

                    ".$message."
            </div>";
	}

	function error($message){
		echo"<div class='alert alert-danger' style = 'height:initial;padding-top:8px;width:100%;'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true' style='margin-top:8px;margin-bottom: 13px;'>×</button>
                <span class='glyphicon glyphicon-exclamation-sign'></span>

                    ".$message."
            </div>
			";
	}

}//class
?>