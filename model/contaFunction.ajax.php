<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	if(!isset($_SESSION["contas"]))
		$_SESSION["contas"] = array();

	$tpconta = @$_REQUEST['V_tpconta'];
	$valtpconta = @$_REQUEST['V_valtpconta'];
	$codBanco = @$_REQUEST['V_codBanco'];
	$valBanco = @$_REQUEST['V_valBanco'];
	$agencia = @$_REQUEST['V_agencia'];
	$verificador = @$_REQUEST['V_verificador'];
	$conta = @$_REQUEST['V_conta'];
	$padrao = @$_REQUEST['V_padrao'];

	if($padrao == "true"){
		foreach($_SESSION["contas"] as $obj => $padrao){
			$_SESSION['contas'][$obj]["padrao"] = "false";
		}
		$padrao = "true";
	}else{
		$flag = true;
		foreach($_SESSION["contas"] as $obj => $padrao){
			if($_SESSION['contas'][$obj]["padrao"] == "true"){
				$flag = false;
			}
		}

		if($flag){
			$padrao = "true";
		}else{
			$padrao = "false";
		}
	}

	$flag = true;

	foreach($_SESSION["contas"] as $obj){
		if($obj['tpconta'] == $tpconta && $obj['valtpconta'] == $valtpconta && $obj['codBanco'] == $codBanco && $obj['valBanco'] == $valBanco
		&& $obj['agencia'] == $agencia && $obj['verificador'] == $verificador && $obj['conta']	== $conta && $obj['padrao'] == $padrao ){
			$flag = false;
		}
	}

	if($flag){
		$contas = array();
		$contas = array(
				'tpconta'		=> $tpconta,
				'valtpconta'	=> $valtpconta,
				'codBanco'		=> $codBanco,
				'valBanco'		=> $valBanco,
				'agencia'		=> $agencia,
				'verificador'	=> $verificador,
				'conta'			=> $conta,
				'padrao'		=> $padrao,
				'flag'			=> $flag
			);
		$i = count($_SESSION["contas"]);
		$_SESSION["contas"][$i] = $contas;

		echo( json_encode( $contas ) );
	}else{
		$contas = array();
		$contas = array(
				'tpconta'		=> "",
				'valtpconta'	=> "",
				'codBanco'		=> "",
				'valBanco'		=> "",
				'agencia'		=> "",
				'verificador'	=> "",
				'conta'			=> "",
				'padrao'		=> "",
				'flag'			=> $flag
			);
		echo( json_encode( $contas ) );
	}
?>