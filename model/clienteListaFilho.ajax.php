<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	$filhos = array();



	if(!isset($_SESSION["filhos"]))
		$_SESSION["filhos"] = array();

	foreach ($_SESSION["filhos"] as  $obj) {
		$filhos[] = array(
			'nome'			=> $obj['nome'],
			'dtnasc'			=> $obj['dtnasc'],
			'sexo'			=> $obj['sexo'],
			'cuidadoEspecial'	=>  $obj['cuidadoEspecial'],
			'descricao'		=> $obj['descricao'],
		);
	}


		echo( json_encode( $filhos ) );
?>