<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	$success = false;
	$flag = 1;

	require_once("ConnectionPDO.ajax.php");

	$cpf = @$_POST['cpf'];
	$doc = @$_POST['codtpdoc'];
	$estadoDoc = @$_POST['estadoDoc'];
	$numeroDoc = @$_POST['numeroDoc'];
	$orgao = @$_POST['orgao'];
	$frenteName = @$_FILES['dataFrente']['name'];
	$trasName = @$_FILES["dataTras"]["name"];

	$frente = pathinfo($frenteName);
	$frente = ".".@$frente['extension'];
	$frenteName = $doc."-".$cpf."-Frente".$frente;

	$tras = pathinfo($trasName);
	$tras = ".".@$tras['extension'];
	$trasName = $doc."-".$cpf."-Tras".$tras;



	$exec = "select * from tbdocumentos where cpf_prestador = '".$cpf."' and id_tipo_documento = ".$doc;
	$query  = $db->query($exec);
	$aux = $query->fetchobject();
	$frente = $aux->link_imagem_frente;
	$tras = $aux->link_imagem_tras;

	if(!empty($_FILES['dataFrente']['name'])){
		unlink("../../img/doc/".$frente);
		$exec = "update  tbdocumentos set
		link_imagem_frente = '".$frenteName."'
		where cpf_prestador = '".$cpf."' and id_tipo_documento = ".$doc;
		if($db->exec($exec)>0){
			$caminho = "../../img/doc/";//mostra o destino da pasta que a imagem vai ser salva
			if(move_uploaded_file($_FILES["dataFrente"]["tmp_name"],$caminho.$frenteName ) ){
				$flag = 1;
				$msgFrente = null;
			}else{
				$msgFrente = "Erro ao carregar foto Frente.";
				$flag = 0;
			}
		}else{
			$msgFrente = "Erro ao atuliar dados da foto frente.";
			$flag = 0;
		}

	}

	if(!empty($_FILES['dataTras']['name'])){
		unlink("../../img/doc/".$tras);
		$exec = "update  tbdocumentos set
		link_imagem_verso = '".$trasName."'
		where cpf_prestador = '".$cpf."' and id_tipo_documento = ".$doc;
		if($db->exec($exec)>0){
			$caminho = "../../img/doc/";//mostra o destino da pasta que a imagem vai ser salva
			if(move_uploaded_file($_FILES["dataTras"]["tmp_name"],$caminho.$trasName ) ){
				$flag = 1;
				$msgTras = null;
			}else{
				$msgTras = "Erro ao carregar foto Tras.";
			}
		}else{
			$flag = 0;
			$msgTras = "Erro ao atuliar dados da foto verso.";
		}
	}

	$exec = "update  tbdocumentos set
		numero_documento = '".$numeroDoc."',
		orgao_emissor = '".$orgao."',
		id_estado = ".$estadoDoc."
		where cpf_prestador = '".$cpf."' and id_tipo_documento = ".$doc;
		if($db->exec($exec)>0){
			$msg = "Documento atualizado com sucesso !";
			$success = true;
		}else{
			$msg = "Falha em upload ! Tente Novamente.";
			$success = false;
		}

		if(empty($_FILES['dataTras']['name']) && empty($_FILES['dataFrente']['name']))
			$flag = 2;

		$msgFoto = null;
		if($flag == 0){
			if(!empty($msgTras))
				$msgFoto = $msgTras;
			if(!empty($msgFrente)){
				if(!empty($msgFoto)){
					$msgFoto += "<br/>"+$msgFrente;
				}else{
					$msgFoto = $msgFrente;
				}
			}
		}else if($flag == 1){
			$msgFoto = "Fotos do documento atualizada com sucesso";
		}

	$documentos = array();
	$documentos = array(
		'success'			=> $success,
		'msg'				=> $msg,
		'flag'				=> $flag,
		'msgFoto'			=> $msgFoto,
	);

	echo( json_encode( $documentos ) );
?>