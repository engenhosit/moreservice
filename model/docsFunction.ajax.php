<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );
	$cpf = @$_POST['cpf'];
	$frenteName = @$_FILES['dataFrente']['name'];
	$trasName = @$_FILES["dataTras"]["name"];
	$tpdoc = @$_POST['codtpdoc'];
	$valtpdoc = @$_POST['valtpdoc'];
	$codEstadoDoc = @$_POST['codEstadoDoc'];
	$estadoDoc = @$_POST['valEstadoDoc'];
	$numeroDoc = @$_POST['numeroDoc'];
	$orgao = @$_POST['orgao'];

	$frente = pathinfo($frenteName);
	$frente = ".".@$frente['extension'];
	$frenteName = $tpdoc."-".$cpf."-Frente".$frente;

	$tras = pathinfo($trasName);
	$tras = ".".@$tras['extension'];
	$trasName = $tpdoc."-".$cpf."-Tras".$tras;

	if(move_uploaded_file($_FILES["dataFrente"]["tmp_name"],"../../img/doc/".$frenteName) &&
		move_uploaded_file($_FILES["dataTras"]["tmp_name"],"../../img/doc/".$trasName)){

	session_start();

	if(!isset($_SESSION["documentos"]))
		$_SESSION["documentos"] = array();

	$flag = true;

	foreach($_SESSION["documentos"] as $obj){
		if($obj['tpdoc'] == $tpdoc){
			$flag = false;
		}
	}

	if($flag){
		$documentos = array();
		$documentos = array(
				'tpdoc'			=> $tpdoc,
				'valtpdoc'		=> $valtpdoc,
				'codEstado'		=> $codEstadoDoc,
				'valEstado'		=> $estadoDoc,
				'numero'		=> $numeroDoc,
				'orgao'			=> $orgao,
				'frenteName'	=> $frenteName,
				'trasName'		=> $trasName,
				'flag'			=> $flag,
			);

		$i = count($_SESSION["documentos"]);
		$_SESSION["documentos"][$i] = $documentos;

		echo( json_encode( $documentos ) );
	}else{
		$documentos = array();
		$documentos = array(
				'tpdoc'			=> "",
				'valtpdoc'		=> "",
				'codEstado'		=> "",
				'valEstado'		=> "",
				'numero'		=> "",
				'orgao'			=> "",
				'frenteName'	=> "",
				'trasName'		=> "",
				'flag'			=> $flag,
			);

		$i = count($_SESSION["documentos"]);
		$_SESSION["documentos"][$i] = $documentos;

		echo( json_encode( $documentos ) );
	}

	}else{

		$documentos = array();
		$documentos = array(
				'tpdoc'			=> "",
				'valtpdoc'		=> "",
				'codEstado'		=> "",
				'valEstado'		=> "",
				'numero'		=> "",
				'orgao'			=> "",
				'frenteName'	=> "",
				'frenteTemp'   	=> "",
				'trasName'		=> "",
				'trasTemp'		=> "",
				'flag'			=> false,
			);

		echo( json_encode( $documentos ) );
	}
?>