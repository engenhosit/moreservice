<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	session_start();
	$documentos = array();



	if(!isset($_SESSION["documentos"]))
		$_SESSION["documentos"] = array();

	foreach ($_SESSION["documentos"] as  $obj) {
		$documentos[] = array(
			'tpdoc'			=> $obj['tpdoc'],
			'valtpdoc'		=> $obj['valtpdoc'],
			'codEstado'		=> $obj['codEstado'],
			'valEstado'		=> $obj['valEstado'],
			'numero'		=> $obj['numero'],
			'orgao'			=> $obj['orgao'],
			'frenteName'	=> $obj['frenteName'],
			'frenteTemp'   	=>$obj['frenteTemp'],
			'trasName'		=> $obj['trasName'],
			'trasTemp'		=> $obj['trasTemp'],
		);
	}


		echo( json_encode( $documentos ) );
?>