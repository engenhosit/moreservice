<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("ConnectionPDO.ajax.php");

	$cpf = @$_REQUEST['cpf'];
	$disp = array();

	$exec = "select * from tbdisponibilidade where cpf_prestador = '".$cpf."'";
	$res = $db->query($exec);
	while ( $row =  $res->fetchobject()) {
		$disp = array(
			'segunda' => $row->segunda,
			'terca' => $row->terca,
			'quarta' => $row->quarta,
			'quinta' => $row->quinta,
			'sexta' => $row->sexta,
			'sabado' => $row->sabado,
			'domingo' => $row->domingo,
			'segundaInicio' => $row->hora_inicio_segunda,
			'tercaInicio' => $row->hora_inicio_terca,
			'quartaInicio' => $row->hora_inicio_quarta,
			'quintaInicio' => $row->hora_inicio_quinta,
			'sextaInicio' => $row->hora_inicio_sexta,
			'sabadoInicio' => $row->hora_inicio_sabado,
			'domingoInicio' => $row->hora_inicio_domingo,
			'segundaFim' => $row->hora_fim_segunda,
			'tercaFim' => $row->hora_fim_terca,
			'quartaFim' => $row->hora_fim_quarta,
			'quintaFim' => $row->hora_fim_quinta,
			'sextaFim' => $row->hora_fim_sexta,
			'sabadoFim' => $row->hora_fim_sabado,
			'domingoFim' => $row->hora_fim_domingo,
		);
	}

	echo( json_encode( $disp ) );

?>