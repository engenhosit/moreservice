<?php
//[ mostra_erros.php ]

/*Retirar do erros de serem mostrados na tela
error_reporting(0);
ini_set("display_errors", 0);*/

/*Mostrar o erros novamente*/
error_reporting(E_ALL);
ini_set("display_errors", 1 );

/*Tipos de errros possiveis que podem acontecer em php -- Codigos possiveis para serem usados que demonstram erros
------------------------------------------------------Tipos de erros---------------------------------------------------------------------------------------
E_ERROR: Estes indicam erros que não podem ser recuperados, como problemas de alocação de memória. A execução do script é interrompida.
E_WARNING: Avisos em tempo de execução (erros não fatais). A execução do script não é interrompida.
E_PARSE: Erro em tempo de compilação. Erros gerados pelo interpretador.
E_NOTICE: Indica que o script encontrou alguma coisa que pode indicar um erro, mas que também possa acontecer durante a execução normal do script.
E_STRICT: Permite ao PHP sugerir mudanças ao seu código as quais irão assegurar melhor interoperabilidade e compatibilidade futura do seu código.
E_ALL: Todos erros e avisos, como suportado, exceto de nível E_STRICT
-----------------------------------------------------------------------------------------------------------------------------------------------------
Caso você deseje exibir apenas os erros de tipo E_WARNING deve ser usado o seguinte código:
error_reporting(E_WARNING);
ini_set(“display_errors”, 1 );

Podemos ainda utilizar as seguinte combinações:
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set(“display_errors”, 1 );

Desta forma apenas os erros do tipo E_ERROR, E_WARNING e E_PARSE serão exibidos na tela. Uma outra forma seria:
error_reporting(E_ALL ^ E_WARNING);
ini_set(“display_errors”, 1 );
-----------------------------------------------------------------------------------------------------------------------------------------------------
*/
?>