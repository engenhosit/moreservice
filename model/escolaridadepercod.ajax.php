<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cod_estados = $_REQUEST['cod_estados'] ;
	if(empty($cod_estados)){
		$cidades = array();

		$sql = "SELECT id, descricao_escolaridade FROM tbescolaridade order by id";
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'idEscolaridade'	=> $row['id'],
				'nome'			=> $row['descricao_escolaridade'],
			);
		}

		echo( json_encode( $cidades ) );
	}else{
		$cidades = array();

		$sql = "SELECT id, descricao_escolaridade FROM tbescolaridade
				where id <> ".$cod_estados." order by id";
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'idEscolaridade'	=> $row['id'],
				'nome'			=> $row['descricao_escolaridade'],
			);
		}

		echo( json_encode( $cidades ) );
	}

	pg_close();
?>