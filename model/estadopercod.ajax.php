<?php
	header( 'Cache-Control: no-cache' );
	header( 'Content-type: application/xml; charset="utf-8"', true );

	require_once("Connection.ajax.php");

	$cod_estados = $_REQUEST['cod_estados'] ;
	if(empty($cod_estados)){
		$cidades = array();

		$sql = "SELECT id, nome
				FROM tbestado";
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'cod_cidades'	=> $row['id'],
				'nome'			=> $row['nome'],
			);
		}

		echo( json_encode( $cidades ) );
	}else{
		$cidades = array();

		$sql = "SELECT est.id, est.nome FROM tbestado est
				inner join tbcidade cid on  est.id =  cid.id_estado
				where cid.id = ".$cod_estados;
		$res = pg_query($db,$sql);
		while ( $row = pg_fetch_array($res, null, PGSQL_ASSOC) ) {
			$cidades[] = array(
				'cod_cidades'	=> $row['id'],
				'nome'			=> $row['nome'],
			);
		}

		echo( json_encode( $cidades ) );
	}

	pg_close();
?>