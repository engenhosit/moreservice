function menu() {
		$('.nav-toggle').click(function() {
			if($("#mobile-bar").hasClass("side-fechado")) {
				$('#mobile-bar').animate({
				    left: "0px",
				}, 100, function() {
				    $("#mobile-bar").removeClass("side-fechado");
				});
			}
			else {
				$('#mobile-bar').animate({
				    left: "-175px",
				}, 100, function() {
				    $("#mobile-bar").addClass("side-fechado");
				});
			}
		});
	}
	
	//Menu Sidebar
	$(window).resize(function() {
		var tamanhoJanela = $(window).width();
		$(".nav-toggle").remove();
		
		if (tamanhoJanela < 800) { 
			$('#mobile-bar').css('left', '-175px').addClass('side-fechado');
			$('#mobile-bar').append( "<div class='nav-toggle'>Menu</div>" );
		} else {
			$('#mobile-bar').css('left', '0px').addClass('side-fechado');
		}
		
		menu();
	});
	
	$(document).ready(function() {
		var tamanhoJanela = $(window).width();
		$(".nav-toggle").remove();
		
		if (tamanhoJanela < 800) { 
			$('#mobile-bar').css('left', '-175px').addClass('side-fechado');
			$('#mobile-bar').append( "<div class='nav-toggle'>Menu</div>" );
		} else {
			$('#mobile-bar').css('left', '0px').addClass('side-fechado');
		}
		
		menu();
	});

