<?php
include 'menu.php';
if(!isset($_SESSION["login"])){
	echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
}else{
	if(!$_SESSION["login"][1]){
		echo"
<script type='text/javascript'>
	$( function() {

		alert('Por favor, faça o login para acessar as funcionalidades do sistema');
	});
	window.location.replace('index.php');
</script>";
	}
}
?>
<div  class="tela-lista">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
			<h1 class="page-header">
						Tipo de Serviço <small>Listagem de Tipos de Serviços</small>
					</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-fw fa-table"></i> Lista
                            </li>
                        </ol>
				<a href="tipo_servico.php"  class="btn btn-default btn-lg"><i class="fa fa-plus-circle" aria-hidden="true"> Inserir</i></a>
			<?php
				require_once("controller/controllerTipo_Servico.php");
				//EDITAR
				$FormServico = @$_POST["FormServicoEditar"];
				if(!empty($FormServico)){
						$class = new controllerTipo_Servico;
						echo"<br/><br/>";
						$class->Editar();
				}
				//EXCLUIR
				$FormServico = @$_POST["FormServicoExcluir"];
				if(!empty($FormServico)){
					$cod = @$_POST["CodExcluir"];
						$class = new controllerTipo_Servico;
						echo"<br/><br/>";
						$class->Excluir($cod);
				}
			?>
					<div id="lista">
						<?php
						//LISTAR
						$class = new controllerTipo_Servico;
						$class->Listar();
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$( function() {
		$('.valorHora').mask("#.##0,00", {reverse: true});
	});
	</script>
</body>
</html>